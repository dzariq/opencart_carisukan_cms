<?php

class Cart {

    private $config;
    private $db;
    private $data = array();

    public function __construct($registry) {
        $this->config = $registry->get('config');
        $this->customer = $registry->get('customer');
        $this->session = $registry->get('session');
        $this->db = $registry->get('db');

        if (!isset($this->session->data['cart']) || !is_array($this->session->data['cart'])) {
            $this->session->data['cart'] = array();
        }
    }

    public function getProducts() {

        if (!$this->data) {

            foreach ($this->session->data['cart'] as $key => $quantity) {
                $product = explode(':', $key);
                $product_id = $product[0];
                $booking_id = $product[1];

                $product_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) WHERE p.product_id = '" . (int) $product_id . "' AND pd.language_id = '" . (int) $this->config->get('config_language_id') . "' AND p.date_available <= NOW() AND p.status = '1'");
                $bookingdetails_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "bookings WHERE id = '" . $booking_id . "' ");
                if ($product_query->num_rows && $bookingdetails_query->num_rows) {
                    $this->data[$key] = array(
                        'key' => $key,
                        'product_id' => $product_query->row['product_id'],
                        'name' => $product_query->row['name'],
                        'model' => $product_query->row['model'],
                        'image' => $bookingdetails_query->row['room_image'],
                        'bookingdetails' => $bookingdetails_query->row,
                        'option' => "",
                        'quantity' => $quantity,
                        'minimum' => $product_query->row['minimum'],
                        'subtract' => $product_query->row['subtract'],
                        'stock' => true,
                        'price' => $bookingdetails_query->row['price'],
                        'rawprice' => $bookingdetails_query->row['price'],
                        'total' => $bookingdetails_query->row['price'],
                    );
                } else {
                    $this->remove($key);
                }
            }
        }

        return $this->data;
    }

    public function add($product_id, $qty = 1, $bookingDetails) {
        global $log;
        $log->write("Library Cart/add");
        $key = (int) $product_id . ':' . $bookingDetails;

        if ((int) $qty && ((int) $qty > 0)) {
            if (!isset($this->session->data['cart'][$key])) {
                $this->session->data['cart'][$key] = (int) $qty;
            } else {
                $this->session->data['cart'][$key] += (int) $qty;
            }
        }

        $this->data = array();
    }

    public function update($key, $qty) {
        if ((int) $qty && ((int) $qty > 0)) {
            $this->session->data['cart'][$key] = (int) $qty;
        } else {
            $this->remove($key);
        }

        $this->data = array();
    }

    public function remove($key) {
        if (isset($this->session->data['cart'][$key])) {
            unset($this->session->data['cart'][$key]);
        }

        $this->data = array();
    }

    public function clear() {
        $this->session->data['cart'] = array();
        $this->data = array();
    }

    public function getSubTotal() {
        $total = 0;



        foreach ($this->getProducts() as $product) {
            $total += $product['total'];
        }
        return $total;
    }

    public function getTotal() {
        $total = 0;

        foreach ($this->getProducts() as $product) {
            $total += $this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax')) * $product['quantity'];
        }

        return $total;
    }

    public function countProducts() {
        $product_total = 0;

        $products = $this->getProducts();

        foreach ($products as $product) {
            $product_total += $product['quantity'];
        }

        return $product_total;
    }

    public function hasProducts() {
        return count($this->session->data['cart']);
    }

    public function hasStock() {
        $stock = true;
        foreach ($this->getProducts() as $product) {
            if (!$product['stock']) {
                $stock = false;
            }
        }

        return $stock;
    }

}

?>