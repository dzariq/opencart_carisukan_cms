<?php

class ModelCatalogProduct extends Model {

    public function saveQuantity($product_id, $quantity) {
        $this->db->query("UPDATE " . DB_PREFIX . "product SET quantity = '" . (int) $quantity . "', date_modified = NOW() WHERE product_id = '" . $product_id . "'");

        $query = $this->db->query("SELECT quantity FROM " . DB_PREFIX . "product WHERE product_id = '" . $product_id . "'");
        $row = $query->row;

        return $row['quantity'];
    }

    public function addProduct($data, $copy = false) {

        foreach ($data['section1_image'] as $key => $item) {
            $data['section1_image'][$key]['description2'] = $this->createRate($item['rate1a'], $item['rate2a']);
            $data['section1_image'][$key]['price2'] = $this->createRate($item['rate1b'], $item['rate2b']);
            $data['section1_image'][$key]['price3'] = $this->createRate($item['rate1c'], $item['rate2c']);
        }

        $this->db->query("INSERT INTO " . DB_PREFIX . "product SET section1 = '" . $this->db->escape($data['section1']) . "',section2 = '" . $this->db->escape($data['section2']) . "',section3 = '" . $this->db->escape($data['section3']) . "',section4 = '" . $this->db->escape($data['section4']) . "',section5 = '" . $this->db->escape($data['section5']) . "', model = '', sku = '" . $this->db->escape($data['sku']) . "', upc = '" . $this->db->escape($data['upc']) . "', ean = '" . $this->db->escape($data['ean']) . "', jan = '" . $this->db->escape($data['jan']) . "', isbn = '" . $this->db->escape($data['isbn']) . "', mpn = '" . $this->db->escape($data['mpn']) . "', location = '" . $this->db->escape($data['location']) . "', quantity = '" . (int) $data['quantity'] . "', minimum = '" . (int) $data['minimum'] . "', subtract = '" . (int) $data['subtract'] . "', stock_status_id = '5', date_available = '" . $this->db->escape($data['date_available']) . "', manufacturer_id = '" . (int) $data['manufacturer_id'] . "', shipping = '" . (int) $data['shipping'] . "', price_rate = '" . $data['price_rate'] . "',price = '" . (float) $data['price'] . "', points = '" . (int) $data['points'] . "', weight = '" . $data['weight'] . "', weight_class_id = '" . (int) $data['weight_class_id'] . "', length = '" . $data['length'] . "', width = '" . (float) $data['width'] . "', height = '" . (float) $data['height'] . "', length_class_id = '" . (int) $data['length_class_id'] . "', status = '" . (int) $data['status'] . "', tax_class_id = '" . $this->db->escape($data['tax_class_id']) . "', sort_order = '" . (int) $data['sort_order'] . "', date_added = NOW()");

        $product_id = $this->db->getLastId();

        if (isset($data['image'])) {
            $this->db->query("UPDATE " . DB_PREFIX . "product SET image = '" . $this->db->escape(html_entity_decode($data['image'], ENT_QUOTES, 'UTF-8')) . "' WHERE product_id = '" . (int) $product_id . "'");
        }

        foreach ($data['product_description'] as $language_id => $value) {
            $this->db->query("INSERT INTO " . DB_PREFIX . "product_description SET product_id = '" . (int) $product_id . "', language_id = '" . (int) $language_id . "', name = '" . $this->db->escape($value['name']) . "', meta_keyword = '" . $this->db->escape($value['meta_keyword']) . "', meta_description = '" . $this->db->escape($value['meta_description']) . "', description = '" . $this->db->escape($value['description']) . "', tag = '" . $this->db->escape($value['tag']) . "'");
            $this->db->query("UPDATE " . DB_PREFIX . "product SET model = '" . $this->db->escape($value['name']) . "' WHERE product_id = '" . (int) $product_id . "'");
        }

        if (isset($data['product_store'])) {
            foreach ($data['product_store'] as $store_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_to_store SET product_id = '" . (int) $product_id . "', store_id = '" . (int) $store_id . "'");
            }
        }


        $actual_quantity = 0;
        $sub_quantity = false;


//        $this->db->query("INSERT INTO " . DB_PREFIX . "product_option SET product_id = '" . (int) $product_id . "', option_id = '31', option_value = '', required = '1'");
//        $this->db->query("INSERT INTO " . DB_PREFIX . "product_option SET product_id = '" . (int) $product_id . "', option_id = '41', option_value = '', required = '1'");
//        $this->db->query("INSERT INTO " . DB_PREFIX . "product_option SET product_id = '" . (int) $product_id . "', option_id = '38', option_value = '', required = '1'");
//        $this->db->query("INSERT INTO " . DB_PREFIX . "product_option SET product_id = '" . (int) $product_id . "', option_id = '35', option_value = '', required = '1'");
//

        if ($sub_quantity == true) {
            $this->db->query("UPDATE " . DB_PREFIX . "product SET quantity = '" . (int) $actual_quantity . "' WHERE product_id = '" . (int) $product_id . "'");
        }

        if (isset($data['image'])) {
            $this->db->query("UPDATE " . DB_PREFIX . "product SET image = '" . $this->db->escape(html_entity_decode($data['image'], ENT_QUOTES, 'UTF-8')) . "' WHERE product_id = '" . (int) $product_id . "'");
        }

        if (($this->db->escape($data["pmLat"]) == '') || ($this->db->escape($data["pmLng"]) == '') || ($this->db->escape($data["pmAddress"]) == '')) {
            $this->db->query('DELETE FROM `' . DB_PREFIX . 'productmaps`
				WHERE id_product = ' . (int) $product_id);
        } else {
            $this->db->query('
			REPLACE INTO `' . DB_PREFIX . 'productmaps`
				(p,zoom,address,width,height,id_product)
			VALUES (
				PointFromText("POINT(' . $this->db->escape($data["pmLat"]) . ' ' . $this->db->escape($data["pmLng"]) . ')"),
				' . $data["pmZoom"] . ',
				"' . $this->db->escape($data["pmAddress"]) . '",
 				' . $data["pmWidth"] . ',
				' . $data["pmHeight"] . ',
				' . (int) $product_id . ')');
        }

//remove product_discount with priority 999
        if (isset($data['customer_group_discount'])) {
            if (isset($data['product_discount'])) {
                $temp = $data['product_discount'];
                $data['product_discount'] = array();

                foreach ($temp as $id => $product_discount) {
                    if ((int) $product_discount['quantity'] == 1 && (int) $product_discount['priority'] == 999 && ($product_discount['date_start'] == '' || $product_discount['date_start'] == '0000-00-00') && ($product_discount['date_end'] == '' || $product_discount['date_end'] == '0000-00-00')) {
//skip this discount
                    } else {
                        $data['product_discount'][] = $product_discount;
                    }
                }
            } else {
                $data['product_discount'] = array();
            }

//add product discount using customer_group_discount data
            foreach ($data['customer_group_discount'] as $customer_group_discount) {
                if (is_numeric($customer_group_discount['price']))
                    $data['product_discount'][] = array
                        (
                        'customer_group_id' => $customer_group_discount['customer_group_id'],
                        'quantity' => 1,
                        'priority' => 999,
                        'price' => $customer_group_discount['price'],
                        'date_start' => '',
                        'date_end' => '',
                        'points' => 0,
                        'points_special' => 0
                    );
            }
        }

//remove product_special with priority 999
        if (isset($data['customer_group_special'])) {
            if (isset($data['product_special'])) {
                $temp = $data['product_special'];
                $data['product_special'] = array();

                foreach ($temp as $id => $product_special) {
                    if ((int) $product_special['priority'] == 999 && ($product_special['date_start'] == '' || $product_special['date_start'] == '0000-00-00') && ($product_special['date_end'] == '' || $product_special['date_end'] == '0000-00-00')) {
//skip this discount
                    } else {
                        $data['product_special'][] = $product_special;
                    }
                }
            } else {
                $data['product_special'] = array();
            }

//add product special using customer_group_special data
            foreach ($data['customer_group_special'] as $customer_group_special) {
                if (is_numeric($customer_group_special['price']))
                    $data['product_special'][] = array
                        (
                        'customer_group_id' => $customer_group_special['customer_group_id'],
                        'priority' => 999,
                        'price' => $customer_group_special['price'],
                        'date_start' => '',
                        'date_end' => '',
                        'points' => 0,
                        'points_special' => 0
                    );
            }
        }

        if (isset($data['product_discount'])) {
            foreach ($data['product_discount'] as $product_discount) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_discount SET product_id = '" . (int) $product_id . "', customer_group_id = '" . (int) $product_discount['customer_group_id'] . "', quantity = '" . (int) $product_discount['quantity'] . "', priority = '" . (int) $product_discount['priority'] . "', price = '" . (float) $product_discount['price'] . "', date_start = '" . $this->db->escape($product_discount['date_start']) . "', date_end = '" . $this->db->escape($product_discount['date_end']) . "'");
            }
        }

        if (isset($data['product_special'])) {
            foreach ($data['product_special'] as $product_special) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_special SET product_id = '" . (int) $product_id . "', customer_group_id = '" . (int) $product_special['customer_group_id'] . "', priority = '" . (int) $product_special['priority'] . "', price = '" . (float) $product_special['price'] . "', date_start = '" . $this->db->escape($product_special['date_start']) . "', date_end = '" . $this->db->escape($product_special['date_end']) . "'");
            }
        }

        if (isset($data['product_image'])) {
            foreach ($data['product_image'] as $product_image) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_image SET product_id = '" . (int) $product_id . "', image = '" . $this->db->escape(html_entity_decode($product_image['image'], ENT_QUOTES, 'UTF-8')) . "', sort_order = '" . (int) $product_image['sort_order'] . "'");
            }
        }

        if (isset($data['section1_image'])) {
            foreach ($data['section1_image'] as $item) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_section1_image SET product_id = '" . (int) $product_id . "',caption = '" . $this->db->escape($item['caption']) . "',description = '" . $this->db->escape($item['description']) . "',description2 = '" . $this->db->escape($item['description2']) . "',price2 = '" . $this->db->escape($item['price2']) . "',price3 = '" . $this->db->escape($item['price3']) . "',price4 = '" . $this->db->escape($item['price4']) . "',price5 = '" . $this->db->escape($item['price5']) . "', image = '" . $this->db->escape(html_entity_decode($item['image'], ENT_QUOTES, 'UTF-8')) . "', sort_order = '" . (int) $item['sort_order'] . "'");
            }
        }

        if (isset($data['section2_image'])) {
            foreach ($data['section2_image'] as $item) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_section2_image SET product_id = '" . (int) $product_id . "',caption = '" . $this->db->escape($item['caption']) . "',description = '" . $this->db->escape($item['description']) . "',description2 = '" . $this->db->escape($item['description2']) . "',price2 = '" . $this->db->escape($item['price2']) . "',price3 = '" . $this->db->escape($item['price3']) . "',price4 = '" . $this->db->escape($item['price4']) . "',price5 = '" . $this->db->escape($item['price5']) . "', image = '" . $this->db->escape(html_entity_decode($item['image'], ENT_QUOTES, 'UTF-8')) . "', sort_order = '" . (int) $item['sort_order'] . "'");
            }
        }

        if (isset($data['section3_image'])) {
            foreach ($data['section3_image'] as $item) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_section3_image SET product_id = '" . (int) $product_id . "',caption = '" . $this->db->escape($item['caption']) . "',description = '" . $this->db->escape($item['description']) . "',description2 = '" . $this->db->escape($item['description2']) . "',price2 = '" . $this->db->escape($item['price2']) . "',price3 = '" . $this->db->escape($item['price3']) . "',price4 = '" . $this->db->escape($item['price4']) . "', image = '" . $this->db->escape(html_entity_decode($item['image'], ENT_QUOTES, 'UTF-8')) . "', sort_order = '" . (int) $item['sort_order'] . "'");
            }
        }

        if (isset($data['section4_image'])) {
            foreach ($data['section4_image'] as $item) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_section4_image SET product_id = '" . (int) $product_id . "',caption = '" . $this->db->escape($item['caption']) . "',description = '" . $this->db->escape($item['description']) . "',description2 = '" . $this->db->escape($item['description2']) . "', image = '" . $this->db->escape(html_entity_decode($item['image'], ENT_QUOTES, 'UTF-8')) . "', sort_order = '" . (int) $item['sort_order'] . "'");
            }
        }

        if (isset($data['section5_image'])) {
            foreach ($data['section5_image'] as $item) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_section5_image SET product_id = '" . (int) $product_id . "',caption = '" . $this->db->escape($item['caption']) . "',description = '" . $this->db->escape($item['description']) . "',description2 = '" . $this->db->escape($item['description2']) . "', image = '" . $this->db->escape(html_entity_decode($item['image'], ENT_QUOTES, 'UTF-8')) . "', sort_order = '" . (int) $item['sort_order'] . "'");
            }
        }


        if (isset($data['product_category'])) {
            foreach ($data['product_category'] as $category_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_to_category SET product_id = '" . (int) $product_id . "', category_id = '" . (int) $category_id . "'");
            }
        }

        if (isset($data['product_related'])) {
            foreach ($data['product_related'] as $related_id) {
                $this->db->query("DELETE FROM " . DB_PREFIX . "product_related WHERE product_id = '" . (int) $product_id . "' AND related_id = '" . (int) $related_id . "'");
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_related SET product_id = '" . (int) $product_id . "', related_id = '" . (int) $related_id . "'");
                $this->db->query("DELETE FROM " . DB_PREFIX . "product_related WHERE product_id = '" . (int) $related_id . "' AND related_id = '" . (int) $product_id . "'");
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_related SET product_id = '" . (int) $related_id . "', related_id = '" . (int) $product_id . "'");
            }
        }

        if (isset($data['product_layout'])) {
            foreach ($data['product_layout'] as $store_id => $layout) {
                if ($layout['layout_id']) {
                    $this->db->query("INSERT INTO " . DB_PREFIX . "product_to_layout SET product_id = '" . (int) $product_id . "', store_id = '" . (int) $store_id . "', layout_id = '" . (int) $layout['layout_id'] . "'");
                }
            }
        }

        if ($copy == false) {
            if ($data['keyword']) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "url_alias SET query = 'product_id=" . (int) $product_id . "', keyword = '" . $this->db->escape($data['keyword']) . "'");
            } else {
                foreach ($data['product_description'] as $language_id => $value) {
                    $newkeyword = strtolower(preg_replace('/\s+/', '-', $value['name']));
                    $newkeyword = str_replace(array('/', ' '), array('-', ''), $newkeyword);
                    $newkeyword = preg_replace("/&#?[a-z0-9]{2,8};/i", "", $newkeyword);
                    $this->db->query("INSERT INTO " . DB_PREFIX . "url_alias SET query = 'product_id=" . (int) $product_id . "', keyword = '" . $this->db->escape($newkeyword) . "'");
                }
            }
        }

        $this->cache->delete('product');
    }

    public function createRate($rate1, $rate2) {
        $data = "";
        $data .= "18:" . $rate1 . ";23:" . $rate2;

        return $data;
    }

    public function editProduct($product_id, $data) {

        if (isset($data['section1_image'])) {
            foreach ($data['section1_image'] as $key => $item) {
                $data['section1_image'][$key]['description2'] = $this->createRate($item['rate1a'], $item['rate2a']);
                $data['section1_image'][$key]['price2'] = $this->createRate($item['rate1b'], $item['rate2b']);
                $data['section1_image'][$key]['price3'] = $this->createRate($item['rate1c'], $item['rate2c']);
            }
        }
        if (($this->db->escape($data["pmLat"]) == '') || ($this->db->escape($data["pmLng"]) == '') || ($this->db->escape($data["pmAddress"]) == '')) {
            $this->db->query('DELETE FROM `' . DB_PREFIX . 'productmaps`
				WHERE id_product = ' . (int) $product_id);
        } else {
            $this->db->query('
			REPLACE INTO `' . DB_PREFIX . 'productmaps`
				(p,zoom,address,width,height,id_product)
			VALUES (
				PointFromText("POINT(' . $this->db->escape($data["pmLat"]) . ' ' . $this->db->escape($data["pmLng"]) . ')"),
				' . $data["pmZoom"] . ',
				"' . $this->db->escape($data["pmAddress"]) . '",
 				' . $data["pmWidth"] . ',
				' . $data["pmHeight"] . ',
				' . (int) $product_id . ')');
        }

            $this->db->query("UPDATE " . DB_PREFIX . "product SET section1 = '" . $this->db->escape($data['section1']) . "',section2 = '" . $this->db->escape($data['section2']) . "',section3 = '" . $this->db->escape($data['section3']) . "',section4 = '" . $this->db->escape($data['section4']) . "',section5 = '" . $this->db->escape($data['section5']) . "',model = '', sku = '" . $this->db->escape($data['sku']) . "', upc = '" . $this->db->escape($data['upc']) . "', ean = '" . $this->db->escape($data['ean']) . "', jan = '" . $this->db->escape($data['jan']) . "', isbn = '" . $this->db->escape($data['isbn']) . "', mpn = '" . $this->db->escape($data['mpn']) . "', location = '" . $this->db->escape($data['location']) . "', quantity = '" . (int) $data['quantity'] . "', minimum = '" . (int) $data['minimum'] . "', subtract = '" . (int) $data['subtract'] . "', stock_status_id = '5', date_available = '" . $this->db->escape($data['date_available']) . "', manufacturer_id = '" . (int) $data['manufacturer_id'] . "', shipping = '" . (int) $data['shipping'] . "', price = '" . (float) $data['price'] . "', price_rate = '" . $data['price_rate'] . "', points = '" . (int) $data['points'] . "', weight = '" . $data['weight'] . "', weight_class_id = '" . (int) $data['weight_class_id'] . "', length = '" . $data['length'] . "', width = '" . (float) $data['width'] . "', height = '" . (float) $data['height'] . "', length_class_id = '" . (int) $data['length_class_id'] . "', status = '" . (int) $data['status'] . "', tax_class_id = '" . $this->db->escape($data['tax_class_id']) . "', sort_order = '" . (int) $data['sort_order'] . "', date_modified = NOW() WHERE product_id = '" . (int) $product_id . "'");

            if (isset($data['image'])) {
            $this->db->query("UPDATE " . DB_PREFIX . "product SET image = '" . $this->db->escape(html_entity_decode($data['image'], ENT_QUOTES, 'UTF-8')) . "' WHERE product_id = '" . (int) $product_id . "'");
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "product_description WHERE product_id = '" . (int) $product_id . "'");

        foreach ($data['product_description'] as $language_id => $value) {
            $this->db->query("INSERT INTO " . DB_PREFIX . "product_description SET product_id = '" . (int) $product_id . "', language_id = '" . (int) $language_id . "', name = '" . $this->db->escape($value['name']) . "', meta_keyword = '" . $this->db->escape($value['meta_keyword']) . "', meta_description = '" . $this->db->escape($value['meta_description']) . "', description = '" . $this->db->escape($value['description']) . "', tag = '" . $this->db->escape($value['tag']) . "'");
            $this->db->query("UPDATE " . DB_PREFIX . "product SET model = '" . $this->db->escape($value['name']) . "' WHERE product_id = '" . (int) $product_id . "'");
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "product_to_store WHERE product_id = '" . (int) $product_id . "'");

        if (isset($data['product_store'])) {
            foreach ($data['product_store'] as $store_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_to_store SET product_id = '" . (int) $product_id . "', store_id = '" . (int) $store_id . "'");
            }
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "product_discount WHERE product_id = '" . (int) $product_id . "'");

//remove product_discount with priority 999
        if (isset($data['customer_group_discount'])) {
            if (isset($data['product_discount'])) {
                $temp = $data['product_discount'];
                $data['product_discount'] = array();

                foreach ($temp as $id => $product_discount) {
                    if ((int) $product_discount['quantity'] == 1 && (int) $product_discount['priority'] == 999 && ($product_discount['date_start'] == '' || $product_discount['date_start'] == '0000-00-00') && ($product_discount['date_end'] == '' || $product_discount['date_end'] == '0000-00-00')) {
//skip this discount
                    } else {
                        $data['product_discount'][] = $product_discount;
                    }
                }
            } else {
                $data['product_discount'] = array();
            }

//add product discount using customer_group_discount data
            foreach ($data['customer_group_discount'] as $customer_group_discount) {
                if (is_numeric($customer_group_discount['price']))
                    $data['product_discount'][] = array
                        (
                        'customer_group_id' => $customer_group_discount['customer_group_id'],
                        'quantity' => 1,
                        'priority' => 999,
                        'price' => $customer_group_discount['price'],
                        'date_start' => '',
                        'date_end' => '',
                        'points' => 0,
                        'points_special' => 0
                    );
            }
        }

        if (isset($data['product_discount'])) {
            foreach ($data['product_discount'] as $product_discount) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_discount SET product_id = '" . (int) $product_id . "', customer_group_id = '" . (int) $product_discount['customer_group_id'] . "', quantity = '" . (int) $product_discount['quantity'] . "', priority = '" . (int) $product_discount['priority'] . "', price = '" . (float) $product_discount['price'] . "', date_start = '" . $this->db->escape($product_discount['date_start']) . "', date_end = '" . $this->db->escape($product_discount['date_end']) . "'");
            }
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "product_special WHERE product_id = '" . (int) $product_id . "'");

        if (isset($data['product_special'])) {
            foreach ($data['product_special'] as $product_special) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_special SET product_id = '" . (int) $product_id . "', customer_group_id = '" . (int) $product_special['customer_group_id'] . "', priority = '" . (int) $product_special['priority'] . "', price = '" . (float) $product_special['price'] . "', date_start = '" . $this->db->escape($product_special['date_start']) . "', date_end = '" . $this->db->escape($product_special['date_end']) . "'");
            }
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "product_image WHERE product_id = '" . (int) $product_id . "'");

        if (isset($data['product_image'])) {
            foreach ($data['product_image'] as $product_image) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_image SET product_id = '" . (int) $product_id . "', image = '" . $this->db->escape(html_entity_decode($product_image['image'], ENT_QUOTES, 'UTF-8')) . "', sort_order = '" . (int) $product_image['sort_order'] . "'");
            }
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "product_section1_image WHERE product_id = '" . (int) $product_id . "'");

        if (isset($data['section1_image'])) {
            foreach ($data['section1_image'] as $item) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_section1_image SET product_id = '" . (int) $product_id . "',caption = '" . $this->db->escape($item['caption']) . "',description = '" . $this->db->escape($item['description']) . "',description2 = '" . $this->db->escape($item['description2']) . "',price2 = '" . $this->db->escape($item['price2']) . "',price3 = '" . $this->db->escape($item['price3']) . "',price4 = '" . $this->db->escape($item['price4']) . "',price5 = '" . $this->db->escape($item['price5']) . "', image = '" . $this->db->escape(html_entity_decode($item['image'], ENT_QUOTES, 'UTF-8')) . "', sort_order = '" . (int) $item['sort_order'] . "'");
            }
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "product_section2_image WHERE product_id = '" . (int) $product_id . "'");

        if (isset($data['section2_image'])) {
            foreach ($data['section2_image'] as $item) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_section2_image SET product_id = '" . (int) $product_id . "',caption = '" . $this->db->escape($item['caption']) . "',description = '" . $this->db->escape($item['description']) . "',description2 = '" . $this->db->escape($item['description2']) . "',price2 = '" . $this->db->escape($item['price2']) . "',price3 = '" . $this->db->escape($item['price3']) . "',price4 = '" . $this->db->escape($item['price4']) . "',price5 = '" . $this->db->escape($item['price5']) . "', image = '" . $this->db->escape(html_entity_decode($item['image'], ENT_QUOTES, 'UTF-8')) . "', sort_order = '" . (int) $item['sort_order'] . "'");
            }
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "product_section3_image WHERE product_id = '" . (int) $product_id . "'");

        if (isset($data['section3_image'])) {
            foreach ($data['section3_image'] as $item) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_section3_image SET product_id = '" . (int) $product_id . "',caption = '" . $this->db->escape($item['caption']) . "',description = '" . $this->db->escape($item['description']) . "',description2 = '" . $this->db->escape($item['description2']) . "',price2 = '" . $this->db->escape($item['price2']) . "',price3 = '" . $this->db->escape($item['price3']) . "',price4 = '" . $this->db->escape($item['price4']) . "',price5 = '" . $this->db->escape($item['price5']) . "', image = '" . $this->db->escape(html_entity_decode($item['image'], ENT_QUOTES, 'UTF-8')) . "', sort_order = '" . (int) $item['sort_order'] . "'");
            }
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "product_section4_image WHERE product_id = '" . (int) $product_id . "'");

        if (isset($data['section4_image'])) {
            foreach ($data['section4_image'] as $item) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_section4_image SET product_id = '" . (int) $product_id . "',caption = '" . $this->db->escape($item['caption']) . "',description = '" . $this->db->escape($item['description']) . "',description2 = '" . $this->db->escape($item['description2']) . "', image = '" . $this->db->escape(html_entity_decode($item['image'], ENT_QUOTES, 'UTF-8')) . "', sort_order = '" . (int) $item['sort_order'] . "'");
            }
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "product_section5_image WHERE product_id = '" . (int) $product_id . "'");

        if (isset($data['section5_image'])) {
            foreach ($data['section5_image'] as $item) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_section5_image SET product_id = '" . (int) $product_id . "',caption = '" . $this->db->escape($item['caption']) . "',description = '" . $this->db->escape($item['description']) . "',description2 = '" . $this->db->escape($item['description2']) . "', image = '" . $this->db->escape(html_entity_decode($item['image'], ENT_QUOTES, 'UTF-8')) . "', sort_order = '" . (int) $item['sort_order'] . "'");
            }
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "product_to_category WHERE product_id = '" . (int) $product_id . "'");

        if (isset($data['product_category'])) {
            foreach ($data['product_category'] as $category_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_to_category SET product_id = '" . (int) $product_id . "', category_id = '" . (int) $category_id . "'");
            }
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "product_related WHERE product_id = '" . (int) $product_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "product_related WHERE related_id = '" . (int) $product_id . "'");

        if (isset($data['product_related'])) {
            foreach ($data['product_related'] as $related_id) {
                $this->db->query("DELETE FROM " . DB_PREFIX . "product_related WHERE product_id = '" . (int) $product_id . "' AND related_id = '" . (int) $related_id . "'");
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_related SET product_id = '" . (int) $product_id . "', related_id = '" . (int) $related_id . "'");
                $this->db->query("DELETE FROM " . DB_PREFIX . "product_related WHERE product_id = '" . (int) $related_id . "' AND related_id = '" . (int) $product_id . "'");
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_related SET product_id = '" . (int) $related_id . "', related_id = '" . (int) $product_id . "'");
            }
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "product_to_layout WHERE product_id = '" . (int) $product_id . "'");

        if (isset($data['product_layout'])) {
            foreach ($data['product_layout'] as $store_id => $layout) {
                if ($layout['layout_id']) {
                    $this->db->query("INSERT INTO " . DB_PREFIX . "product_to_layout SET product_id = '" . (int) $product_id . "', store_id = '" . (int) $store_id . "', layout_id = '" . (int) $layout['layout_id'] . "'");
                }
            }
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "url_alias WHERE query = 'product_id=" . (int) $product_id . "'");

        if ($data['keyword']) {
            $this->db->query("INSERT INTO " . DB_PREFIX . "url_alias SET query = 'product_id=" . (int) $product_id . "', keyword = '" . $this->db->escape($data['keyword']) . "'");
        } else {
            foreach ($data['product_description'] as $language_id => $value) {
                $newkeyword = strtolower(preg_replace('/\s+/', '-', $value['name']));
                $newkeyword = str_replace(array('/', ' '), array('-', ''), $newkeyword);
                $newkeyword = preg_replace("/&#?[a-z0-9]{2,8};/i", "", $newkeyword);
                $this->db->query("INSERT INTO " . DB_PREFIX . "url_alias SET query = 'product_id=" . (int) $product_id . "', keyword = '" . $this->db->escape($newkeyword) . "'");
            }
        }

        $this->cache->delete('product');
    }

    public function copyProduct($product_id) {
        $query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) WHERE p.product_id = '" . (int) $product_id . "' AND pd.language_id = '" . (int) $this->config->get('config_language_id') . "'");

        if ($query->num_rows) {
            $data = array();

            $data = $query->row;

            $data['sku'] = '';
            $data['upc'] = '';
            $data['viewed'] = '0';
            $data['keyword'] = '';
            $data['status'] = '0';

            $data = array_merge($data, array('product_description' => $this->getProductDescriptions($product_id)));
            $data = array_merge($data, array('product_discount' => $this->getProductDiscounts($product_id)));
            $data = array_merge($data, array('product_image' => $this->getProductImages($product_id)));
            $data = array_merge($data, array('product_related' => $this->getProductRelated($product_id)));
            $data = array_merge($data, array('product_special' => $this->getProductSpecials($product_id)));
            $data = array_merge($data, array('product_category' => $this->getProductCategories($product_id)));
            $data = array_merge($data, array('product_layout' => $this->getProductLayouts($product_id)));
            $data = array_merge($data, array('product_store' => $this->getProductStores($product_id)));

            $this->addProduct($data, 1);
        }
    }

    public function deleteProduct($product_id) {
        $this->db->query('DELETE FROM `' . DB_PREFIX . 'productmaps`
				WHERE id_product = ' . (int) $product_id);
        $this->db->query("DELETE FROM " . DB_PREFIX . "product WHERE product_id = '" . (int) $product_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "product_description WHERE product_id = '" . (int) $product_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "product_discount WHERE product_id = '" . (int) $product_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "product_image WHERE product_id = '" . (int) $product_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "product_related WHERE product_id = '" . (int) $product_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "product_related WHERE related_id = '" . (int) $product_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "product_special WHERE product_id = '" . (int) $product_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "product_to_category WHERE product_id = '" . (int) $product_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "product_to_layout WHERE product_id = '" . (int) $product_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "product_to_store WHERE product_id = '" . (int) $product_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "review WHERE product_id = '" . (int) $product_id . "'");

        $this->db->query("DELETE FROM " . DB_PREFIX . "url_alias WHERE query = 'product_id=" . (int) $product_id . "'");

        $this->cache->delete('product');
    }

    public function getProduct($product_id) {
        $query = $this->db->query("SELECT DISTINCT *, (SELECT keyword FROM " . DB_PREFIX . "url_alias WHERE query = 'product_id=" . (int) $product_id . "' LIMIT 1) AS keyword FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) WHERE p.product_id = '" . (int) $product_id . "' AND pd.language_id = '" . (int) $this->config->get('config_language_id') . "'");
        return $query->row;
    }

    public function getProducts($data = array()) {
        if ($data) {
            $sql = "SELECT * FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id)";
// renamed filter_category_id to filter_category
            if (!empty($data['filter_category'])) {
                $sql .= " LEFT JOIN " . DB_PREFIX . "product_to_category p2c ON (p.product_id = p2c.product_id)";
            }

            $sql .= " WHERE pd.language_id = '" . (int) $this->config->get('config_language_id') . "'";

            if (!empty($data['filter_name'])) {
                $sql .= " AND LCASE(pd.name) LIKE '" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "%'";
            }

            if (!empty($data['filter_model'])) {
                $sql .= " AND LCASE(p.model) LIKE '" . $this->db->escape(utf8_strtolower($data['filter_model'])) . "%'";
            }

            if (!empty($data['filter_sku'])) {
                $sql .= " AND LCASE(p.sku) LIKE '" . $this->db->escape(utf8_strtolower($data['filter_sku'])) . "%'";
            }

            if (!empty($data['filter_price'])) {
                $sql .= " AND p.price LIKE '" . $this->db->escape($data['filter_price']) . "%'";
            }

            if (isset($data['filter_quantity']) && !is_null($data['filter_quantity'])) {
                $sql .= " AND p.quantity = '" . $this->db->escape($data['filter_quantity']) . "'";
            }

            if (isset($data['filter_status']) && !is_null($data['filter_status'])) {
                $sql .= " AND p.status = '" . (int) $data['filter_status'] . "'";
            }



// renamed filter_category_id to filter_category
            if (!empty($data['filter_category'])) {
                if (!empty($data['filter_sub_category'])) {
                    $implode_data = array();

                    $implode_data[] = "category_id = '" . (int) $data['filter_category'] . "'";

                    $this->load->model('catalog/category');

                    $categories = $this->model_catalog_category->getCategories($data['filter_category']);

                    foreach ($categories as $category) {
                        $implode_data[] = "p2c.category_id = '" . (int) $category['category_id'] . "'";
                    }

                    $sql .= " AND (" . implode(' OR ', $implode_data) . ")";
                } else {
                    $sql .= " AND p2c.category_id = '" . (int) $data['filter_category'] . "'";
                }
            }


            $sql .= " GROUP BY p.product_id";

            $sort_data = array(
                'pd.name',
                'p.model',
                'p.price',
                'convert(p.jan, decimal)',
                // add
                'p2c.category_id',
                // end
                'p.quantity',
                'p.status',
                'p.sort_order'
            );

            if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
                $sql .= " ORDER BY " . $data['sort'];
            } else {
                $sql .= " ORDER BY pd.name";
            }

            if (isset($data['order']) && ($data['order'] == 'DESC')) {
                $sql .= " DESC";
            } else {
                $sql .= " ASC";
            }

            if (isset($data['start']) || isset($data['limit'])) {
                if ($data['start'] < 0) {
                    $data['start'] = 0;
                }

                if ($data['limit'] < 1) {
                    $data['limit'] = 20;
                }

                $sql .= " LIMIT " . (int) $data['start'] . "," . (int) $data['limit'];
            }
            $query = $this->db->query($sql);

            return $query->rows;
        } else {
            $product_data = $this->cache->get('product.' . (int) $this->config->get('config_language_id') . '.' . (int) $this->config->get('config_store_id'));

            if (!$product_data) {
                $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) WHERE pd.language_id = '" . (int) $this->config->get('config_language_id') . "' ORDER BY pd.name ASC");

                $product_data = $query->rows;

                $this->cache->set('product.' . (int) $this->config->get('config_language_id') . '.' . (int) $this->config->get('config_store_id'), $product_data);
            }
            return $product_data;
        }
    }

    public function getProductsByCategoryId($category_id) {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) LEFT JOIN " . DB_PREFIX . "product_to_category p2c ON (p.product_id = p2c.product_id) WHERE pd.language_id = '" . (int) $this->config->get('config_language_id') . "' AND p2c.category_id = '" . (int) $category_id . "' ORDER BY pd.name ASC");

        return $query->rows;
    }

    public function getProductDescriptions($product_id) {
        $product_description_data = array();

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_description WHERE product_id = '" . (int) $product_id . "'");

        foreach ($query->rows as $result) {
            $product_description_data[$result['language_id']] = array(
                'name' => $result['name'],
                'description' => $result['description'],
                'meta_keyword' => $result['meta_keyword'],
                'meta_description' => $result['meta_description'],
                'tag' => $result['tag']
            );
        }

        return $product_description_data;
    }

    public function getProductImages($product_id) {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_image WHERE product_id = '" . (int) $product_id . "'");

        return $query->rows;
    }

    public function getProductDiscounts($product_id) {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_discount WHERE product_id = '" . (int) $product_id . "' ORDER BY quantity, priority, price");

        return $query->rows;
    }

    public function getProductSpecials($product_id) {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_special WHERE product_id = '" . (int) $product_id . "' ORDER BY priority, price");

        return $query->rows;
    }

    public function getProductStores($product_id) {
        $product_store_data = array();

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_to_store WHERE product_id = '" . (int) $product_id . "'");

        foreach ($query->rows as $result) {
            $product_store_data[] = $result['store_id'];
        }

        return $product_store_data;
    }

    public function getProductLayouts($product_id) {
        $product_layout_data = array();

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_to_layout WHERE product_id = '" . (int) $product_id . "'");

        foreach ($query->rows as $result) {
            $product_layout_data[$result['store_id']] = $result['layout_id'];
        }

        return $product_layout_data;
    }

    public function getProductCategories($product_id) {
        $product_category_data = array();

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_to_category WHERE product_id = '" . (int) $product_id . "'");

        foreach ($query->rows as $result) {
            $product_category_data[] = $result['category_id'];
        }

        return $product_category_data;
    }

    public function getProductRelated($product_id) {
        $product_related_data = array();

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_related WHERE product_id = '" . (int) $product_id . "'");

        foreach ($query->rows as $result) {
            $product_related_data[] = $result['related_id'];
        }

        return $product_related_data;
    }

    public function getTotalProducts($data = array()) {
        $sql = "SELECT COUNT(DISTINCT p.product_id) AS total FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id)";

// renamed
        if (!empty($data['filter_category'])) {
            $sql .= " LEFT JOIN " . DB_PREFIX . "product_to_category p2c ON (p.product_id = p2c.product_id)";
        }

        $sql .= " WHERE pd.language_id = '" . (int) $this->config->get('config_language_id') . "'";

        if (!empty($data['filter_name'])) {
            $sql .= " AND LCASE(pd.name) LIKE '" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "%'";
        }

        if (!empty($data['filter_model'])) {
            $sql .= " AND LCASE(p.model) LIKE '" . $this->db->escape(utf8_strtolower($data['filter_model'])) . "%'";
        }

        if (!empty($data['filter_price'])) {
            $sql .= " AND p.price LIKE '" . $this->db->escape($data['filter_price']) . "%'";
        }

        if (isset($data['filter_quantity']) && !is_null($data['filter_quantity'])) {
            $sql .= " AND p.quantity = '" . $this->db->escape($data['filter_quantity']) . "'";
        }

        if (isset($data['filter_status']) && !is_null($data['filter_status'])) {
            $sql .= " AND p.status = '" . (int) $data['filter_status'] . "'";
        }

// renamed
        if (!empty($data['filter_category'])) {
            if (!empty($data['filter_sub_category'])) {
                $implode_data = array();

                $implode_data[] = "p2c.category_id = '" . (int) $data['filter_category'] . "'";

                $this->load->model('catalog/category');

                $categories = $this->model_catalog_category->getCategories($data['filter_category']);

                foreach ($categories as $category) {
                    $implode_data[] = "p2c.category_id = '" . (int) $category['category_id'] . "'";
                }

                $sql .= " AND (" . implode(' OR ', $implode_data) . ")";
            } else {
                $sql .= " AND p2c.category_id = '" . (int) $data['filter_category_id'] . "'";
            }
        }

        $query = $this->db->query($sql);

        return $query->row['total'];
    }

    public function getTotalProductsByTaxClassId($tax_class_id) {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "product WHERE tax_class_id = '" . (int) $tax_class_id . "'");

        return $query->row['total'];
    }

    public function getTotalProductsByStockStatusId($stock_status_id) {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "product WHERE stock_status_id = '" . (int) $stock_status_id . "'");

        return $query->row['total'];
    }

    public function getTotalProductsByLayoutId($layout_id) {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "product_to_layout WHERE layout_id = '" . (int) $layout_id . "'");

        return $query->row['total'];
    }

    public function getSection1Images($product_id) {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_section1_image WHERE product_id = '" . (int) $product_id . "' ORDER BY sort_order ASC");

        return $query->rows;
    }

    public function getSection2Images($product_id) {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_section2_image WHERE product_id = '" . (int) $product_id . "'");

        return $query->rows;
    }

    public function getSection3Images($product_id) {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_section3_image WHERE product_id = '" . (int) $product_id . "'");

        return $query->rows;
    }

    public function getSection4Images($product_id) {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_section4_image WHERE product_id = '" . (int) $product_id . "'");

        return $query->rows;
    }

    public function getSection5Images($product_id) {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_section5_image WHERE product_id = '" . (int) $product_id . "'");

        return $query->rows;
    }

}

?>