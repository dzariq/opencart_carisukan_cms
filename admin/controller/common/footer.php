<?php

class ControllerCommonFooter extends Controller {

    protected function index() {
        $this->load->language('common/footer');

        $this->data['text_footer'] = sprintf($this->language->get('text_footer'), VERSION);
        $this->data['text_footer'] = "<a href='http://potatoapp.com.my'><b>potatoapp.com.my</b></a><br /> " . $this->data['text_footer'];


        $this->template = 'common/footer.tpl';

        $this->render();
    }

}

?>