<?php

/*
 *  Pgc product video translation
 *  Translation
 */
$_['heading_title'] = 'Pgc Product Video setting';
$_['text_video_embed_link'] = 'Video embed link';
$_['text_module'] = 'Modules';

$_['text_product_video']        = 'Product video';
$_['text_product_video_gallery']       = 'Product video gallery';

$_['text_success'] = 'Video settings has been modified';
$_['text_width']        = 'Default product video width';
$_['text_height']       = 'Default product video height';

$_['text_videos_position']       = 'Videos position';
$_['text_videos_position_belowt']       = 'Video below title';
$_['text_videos_position_pleft']       = 'Video in left section';
$_['text_videos_position_pright']       = 'Video in right section';
$_['text_videos_position_udesc']       = 'Video above product description';
$_['text_videos_position_atags']       = 'Video above tags';

$_['text_width_thumb']        = 'Default video galery thumbnail width';
$_['text_height_thumb']       = 'Default video galery thumbnail height';

$_['text_video_gallery_position']       = 'Video gallery position';
$_['text_video_gallery_position_udesc']       = 'Video gallery above product description';
$_['text_video_gallery_position_atags']       = 'Video gallery above tags';
$_['text_video_gallery_position_tub']       = 'Video gallery in video tab';

$_['text_video_gallery_custom_thumb']       = 'Video custom thumbnail';
?>