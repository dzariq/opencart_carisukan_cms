<?php
// Heading
$_['heading_title']       = 'Instagram';

// Text
$_['text_module']         = 'Modules';
$_['text_success']        = 'Success: You have modified module Instagram!';
$_['text_content_top']    = 'Content Top';
$_['text_content_bottom'] = 'Content Bottom';
$_['text_column_left']    = 'Column Left';
$_['text_column_right']   = 'Column Right';

// Entry
$_['entry_layout']        = 'Layout:';
$_['entry_position']      = 'Position:';
$_['entry_status']        = 'Status:';
$_['entry_sort_order']    = 'Sort Order:';
$_['entry_client_id']    = 'Instagram Client ID:';
$_['entry_client_secret']    = 'Instagram Client Secret:';
$_['entry_client_access_token']    = 'Instagram Client Access Token:';
$_['entry_client_username']    = 'Instagram Client username:';
$_['entry_client_bio']    = 'Instagram Client Bio:';
$_['entry_client_website']    = 'Instagram Client Website:';
$_['entry_client_profile_picture']    = 'Instagram Client Profile Picture:';
$_['entry_client_full_name']    = 'Instagram Client Full Name:';
$_['entry_client_igid']    = 'Instagram Client IG ID:';

// Error
$_['error_permission']    = 'Warning: You do not have permission to modify module account!';
?>