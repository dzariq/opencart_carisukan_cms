<?php
// Heading
$_['heading_title']      = 'Items';

// Text
$_['text_success']       = 'Success: You have modified items!';
$_['text_default']       = 'Default';
$_['text_image_manager'] = 'Image Manager';
$_['text_browse']        = 'Browse Files';
$_['text_clear']         = 'Clear Image';

// Column
$_['column_name']        = 'Item Name';
$_['column_status']      = 'Status';
$_['column_action']      = 'Action';

// Entry
$_['entry_name']         = 'Item Name:';
$_['entry_title']        = 'Title:';
$_['entry_link']         = 'Link:';
$_['entry_image']        = 'Image:';
$_['entry_status']       = 'Status:';

// Error
$_['error_permission']   = 'Warning: You do not have permission to modify items!';
$_['error_name']         = 'Item Name must be between 3 and 64 characters!';
$_['error_title']        = 'Item Title must be between 2 and 64 characters!';
$_['error_default']      = 'Warning: This layout cannot be deleted as it is currently assigned as the default store layout!';
$_['error_product']      = 'Warning: This layout cannot be deleted as it is currently assigned to %s products!';
$_['error_category']     = 'Warning: This layout cannot be deleted as it is currently assigned to %s categories!';
$_['error_information']  = 'Warning: This layout cannot be deleted as it is currently assigned to %s information pages!';
?>