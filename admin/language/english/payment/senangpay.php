<?php
// Heading
$_['heading_title'] = 'senangPay';

// Text
$_['text_payment']  	= 'Payment';
$_['text_success']  	= 'You have successfully modified senangPay setting. ';
$_['text_edit']     	= 'Edit senangPay';
$_['text_senangpay'] 	= '<a onclick="window.open(\'http://senangpay.my/\');"><img src="https://app.senangpay.my/public/img/logo_senangpay.png" style="border: 1px solid #EEEEEE;" /></a>';

// Entry
$_['entry_order_status']    		= 'Order Status';
$_['entry_order_fail_status']    	= 'Order Fail Status';
$_['entry_status']          		= 'Status';
$_['entry_merchant_id']     		= 'Merchant ID';
$_['entry_secret_key']      		= 'Secret Key';

// Help
$_['help_order_status']     		= 'Order Status to be set after the payment was done. ';
$_['help_order_fail_status']     	= 'Order Status to be set after the payment was done but payment fail. ';
$_['help_merchant_id']      		= 'Merchant ID as displayed in senangPay Dashboard.';
$_['help_secret_key']       		= 'Secret key as displayed in senangPay Dashboard.';

// Error
$_['error_permission']  			= 'Warning: You do not have permission to modify senangPay setting. ';