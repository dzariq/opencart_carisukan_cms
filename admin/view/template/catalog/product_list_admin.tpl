<?php echo $header; ?>
<div id="content">
  <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <?php if ($success) { ?>
  <div class="success"><?php echo $success; ?></div>
  <?php } ?>
  <div class="box">
    <div class="heading">
      <h1><img src="view/image/product.png" alt="" /> Center</h1>
    </div>
    <div class="content">
      <form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form">
        <table class="list">
          <thead>
            <tr>
              <td class="center"><?php echo $column_image; ?></td>
              <td class="left"><?php if ($sort == 'pd.name') { ?>
                <a href="<?php echo $sort_name; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_name; ?></a>
                <?php } else { ?>
                <a href="<?php echo $sort_name; ?>"><?php echo $column_name; ?></a>
                <?php } ?></td>
              <td style="display:none" class="left"><?php if ($sort == 'p.model') { ?>
                <a href="<?php echo $sort_model; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_model; ?></a>
                <?php } else { ?>
                <a href="<?php echo $sort_model; ?>"><?php echo $column_model; ?></a>
                <?php } ?></td>
             
			 
              <td class="left"><?php if ($sort == 'p.status') { ?>
                <a href="<?php echo $sort_status; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_status; ?></a>
                <?php } else { ?>
                <a href="<?php echo $sort_status; ?>"><?php echo $column_status; ?></a>
                <?php } ?></td>
              <td class="right"><?php echo $column_action; ?></td>
            </tr>
          </thead>
          <tbody>
            <?php if ($products) { ?>
            <?php foreach ($products as $product) { ?>
            <tr>
            
              <td class="center"><img src="<?php echo $product['image']; ?>" alt="<?php echo $product['name']; ?>" style="padding: 1px; border: 1px solid #DDDDDD;" /></td>
              <td class="left"><?php echo $product['name']; ?></td>
              <td  style="display:none" class="left"><?php echo $product['model']; ?></td>
             
              <td class="left"><?php echo $product['status']; ?></td>
              <td class="right"><?php foreach ($product['action'] as $action) { ?>
                [ <a href="<?php echo $action['href']; ?>"><?php echo $action['text']; ?></a> ]
                <?php } ?></td>
            </tr>
            <?php } ?>
            <?php } else { ?>
            <tr>
              <td class="center" colspan="8"><?php echo $text_no_results; ?></td>
            </tr>
            <?php } ?>
          </tbody>
        </table>
      </form>
      <div class="pagination"><?php echo $pagination; ?></div>
    </div>
  </div>
</div>
<?php if ($this->user->hasPermission('modify', 'catalog/product')) {?>
<style>
.ajax-edit {
   display:none;
   float:right;
   margin-right: 5px;
   clear:right;
}
.ajax-edit input,.ajax-edit-option input {
   width:100%;
   text-align: right;
   cursor: text;
}
.ajax-edit +span,.ajax-edit-option +span {
   cursor: pointer;
}

</style>
<?php } ?>
<script type="text/javascript"><!--
    
    <?php if ($this->user->hasPermission('modify', 'catalog/product')) {?>
    	
                                $(document).ready(function() {
                                   $('.ajax-edit').each(function(index, wrapper) {
                                        $(this).siblings().click(function() {
                                            $(wrapper).show();
                                            $(wrapper).siblings().hide();
                                        })
                                   });
                                   $('.ajax-edit-option').each(function(index, wrapper) {
                                        $(this).siblings().click(function() {
                                            $(wrapper).show();
                                            $(wrapper).siblings().hide();
                                        })
                                   });
                                })

                                function save_quantity(id) {
                                    var input_quantity = $('#quantity-'+id+' input');
                                    var quantity = $(input_quantity).val();
                                    $(quantity).css('cursor','progress');
                                    $.ajax({
                                      url: 'index.php?route=catalog/product/saveQuantity&product_id='+id+'&quantity='+quantity+'&token=<?php echo $token; ?>',
                                      dataType: 'html',
                                      data: {},
                                      success: function(quantity) { 
                                         $('#quantity-'+id).next().html(quantity);
                                         close_quantity(id);
                                      }
                                    });
                                    $(input_quantity).css('cursor','default');
                                }

                                function close_quantity(id) {
                                     $('.ajax-edit input').blur();
                                     $('#quantity-'+id).siblings().show();
                                     $('#quantity-'+id).hide(500);
                                }
                                
                                function save_quantity_option(product_id,subtract,id) {
                                    var input_quantity = $('#quantity-option-'+id+' input');
                                    var original_qty = $('#quantity-hidden-'+id).val();
                                    
                                    var quantity = $(input_quantity).val();
                                    $(quantity).css('cursor','progress');
                                    $.ajax({
                                      url: 'index.php?route=catalog/product/saveQuantityOption&product_id='+product_id+'&original_qty='+original_qty+'&subtract='+subtract+'&product_option_id='+id+'&quantity='+quantity+'&token=<?php echo $token; ?>',
                                      dataType: 'json',
                                      data: {},
                                      success: function(json) {
                                          console.log(json)
                                         $('#quantity-option-'+id).next().html(json.quantity);
                                         $('#quantity-'+product_id).next().html(json.quantity_total);
                                         $('#quantity-hidden-'+id).val(json.quantity);
                                      }
                                    });
                                    $(input_quantity).css('cursor','default');
                                }

                             
                             
<?php } ?>
    
function filter() {
	url = 'index.php?route=catalog/product&token=<?php echo $token; ?>';

	var filter_name = $('input[name=\'filter_name\']').attr('value');

	if (filter_name) {
		url += '&filter_name=' + encodeURIComponent(filter_name);
	}

	var filter_model = $('input[name=\'filter_model\']').attr('value');

	if (filter_model) {
		url += '&filter_model=' + encodeURIComponent(filter_model);
	}

	var filter_price = $('input[name=\'filter_price\']').attr('value');

	if (filter_price) {
		url += '&filter_price=' + encodeURIComponent(filter_price);
	}

	var filter_category = $('select[name=\'filter_category\']').attr('value');

    if (filter_category != '*') {
		url += '&filter_category=' + encodeURIComponent(filter_category);
	}

	var filter_quantity = $('input[name=\'filter_quantity\']').attr('value');

	if (filter_quantity) {
		url += '&filter_quantity=' + encodeURIComponent(filter_quantity);
	}

	var filter_status = $('select[name=\'filter_status\']').attr('value');

	if (filter_status != '*') {
		url += '&filter_status=' + encodeURIComponent(filter_status);
	}

	location = url;
}
//--></script>
<script type="text/javascript"><!--
$('#form input').keydown(function(e) {
	if (e.keyCode == 13) {
		filter();
	}
});
//--></script>
<script type="text/javascript"><!--
$('input[name=\'filter_name\']').autocomplete({
	delay: 0,
	source: function(request, response) {
		$.ajax({
			url: 'index.php?route=catalog/product/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request.term),
			dataType: 'json',
			success: function(json) {
				response($.map(json, function(item) {
					return {
						label: item.name,
						value: item.product_id
					}
				}));
			}
		});
	},
	select: function(event, ui) {
		$('input[name=\'filter_name\']').val(ui.item.label);

		return false;
	},
	focus: function(event, ui) {
      	return false;
   	}
});

$('input[name=\'filter_model\']').autocomplete({
	delay: 0,
	source: function(request, response) {
		$.ajax({
			url: 'index.php?route=catalog/product/autocomplete&token=<?php echo $token; ?>&filter_model=' +  encodeURIComponent(request.term),
			dataType: 'json',
			success: function(json) {
				response($.map(json, function(item) {
					return {
						label: item.model,
						value: item.product_id
					}
				}));
			}
		});
	},
	select: function(event, ui) {
		$('input[name=\'filter_model\']').val(ui.item.label);

		return false;
	},
	focus: function(event, ui) {
      	return false;
   	}
});
//--></script>
<?php echo $footer; ?>