<?php echo $header; ?>
<div id='content'>
    <table>
        <thead>
            <tr>
                <td>Product</td><td>Type</td><td>Quantity</td>
            </tr>
        </thead>
        <tbody>
            <?php foreach($products as $product){ ?>
            <?php if(count($product['child']) != 0){ ?>
            <?php $i=0; foreach($product['child'] as $child){ ?>
            <tr>
                <?php if($i == 0){ ?>
                <td><?php echo $product['name'] ?></td>
                <?php }else{ ?>
                <td>&nbsp;</td>
                <?php } ?>
                <td><?php echo $child['option'] ?></td><td><?php echo $child['quantity'] ?></td>
            </tr>
            <?php $i++; } ?>
            <?php } ?>
            <?php } ?>
        </tbody>
    </table>
</div>
<?php echo $footer; ?>