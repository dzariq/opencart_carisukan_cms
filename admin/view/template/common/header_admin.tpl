<!DOCTYPE html>
<?php
$url = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";



$nowww = ereg_replace('www\.', '', $url);

$domain = parse_url($nowww);

if (!empty($domain["host"])) {

$domain_name =  $domain["host"];

} else {

$domain_name = $domain["path"];

}
?>
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
    <head>
        <meta charset="UTF-8" />
        <title><?php echo $title; ?></title>
        <base href="<?php echo $base; ?>" />
        <?php if ($description) {
        ?>
        <meta name="description" content="<?php echo $description; ?>" />
        <?php } ?>
        <?php if ($keywords) {
        ?>
        <meta name="keywords" content="<?php echo $keywords; ?>" />
        <?php } ?>
        <?php foreach ($links as $link) {
        ?>
        <link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" />
        <?php } ?>
        <?php foreach ($styles as $style) {
        ?>
        <link rel="<?php echo $style['rel']; ?>" type="text/css" href="<?php echo $style['href']; ?>" media="<?php echo $style['media']; ?>" />
        <?php } ?>
        <script type="text/javascript" src="view/javascript/jquery/jquery-1.7.1.min.js"></script>
        <script src="../catalog/view/theme/theme4/js/jquery.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>
        <script type="text/javascript" src="view/javascript/jquery/tabs.js"></script>
        <!--        <script type="text/javascript" src="view/javascript/jquery/jquery-ui-timepicker-addon.js"></script>-->
        <script type="text/javascript" src="view/javascript/jquery/jquery.cookie.js"></script>
        <script type="text/javascript" src="view/javascript/jquery/jquery.treeview.js"></script>
        <script type="text/javascript" src="view/javascript/syntaxhighlighter/shCore.js"></script>
        <script type="text/javascript" src="view/javascript/syntaxhighlighter/shBrushXml.js"></script>
        <script type="text/javascript" src="view/javascript/syntaxhighlighter/shBrushJScript.js"></script>
        <script type="text/javascript" src="view/javascript/bootstrap.min.js"></script>
        <script type="text/javascript" src="view/javascript/jquery.loadmask.min.js"></script>
        <script type="text/javascript" src="../catalog/view/theme/theme4/js/jquery/ui/jquery.datetimepicker.js"></script>
        <script type="text/javascript" src="view/js/jquery-ui.multidatespicker.js"></script>

        <link href="view/javascript/jquery/ui/themes/ui-lightness/jquery-ui-1.8.16.custom.css" rel="stylesheet" type="text/css">
        <link href="view/stylesheet/shCore.css" rel="stylesheet" type="text/css">
        <link href="view/css/jquery.loadmask.css" rel="stylesheet" type="text/css">
        <link href="view/stylesheet/shThemeDefault.css" rel="stylesheet" type="text/css">
        <link href="view/stylesheet/jquery.treeview.css" rel="stylesheet" type="text/css">
        <link href="view/stylesheet/bootstrap3.min.css" rel="stylesheet">
        <!--  <link href="view/stylesheet/bootstrap.min.css" rel="stylesheet">
          <link href="view/stylesheet/bootstrap-responsive.min.css" rel="stylesheet">-->
        <link href="view/stylesheet/font-awesome.css" rel="stylesheet">
        <link href="view/stylesheet/notifications.css" rel="stylesheet" type="text/css">
        <link href="view/stylesheet/seraphine.css" rel="stylesheet" type="text/css">
        <link href="view/stylesheet/admindesign.css" rel="stylesheet">
        <link rel="stylesheet" media="screen" type="text/css" href="view/css/colorpicker.css" />
        <link href="../catalog/view/theme/theme4/js/jquery/ui/jquery.datetimepicker.css" rel="stylesheet">

        <script type="text/javascript" src="view/js/colorpicker.js"></script>
        <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600' rel='stylesheet' type='text/css'>    <!-- Firefox  -->

        <!--[if lt IE 9]>
        <script src="http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE9.js"></script>
        <![endif]-->

        <script type="text/javascript">



            function closeNotification() {
                $('#notification').animate({marginTop:-$('#notification').height()+'px'}, function () {
                    $('#notification').remove();
                });
            }



        </script>
        <?php foreach ($scripts as $script) {
        ?>
        <script type="text/javascript" src="<?php echo $script; ?>"></script>
        <?php } ?>
        <script type="text/javascript">
            //-----------------------------------------
            // Confirm Actions (delete, uninstall)
            //-----------------------------------------
            $(document).ready(function () {
                // Confirm Delete
                $('#form').submit(function () {
                    if ($(this).attr('action').indexOf('delete', 1) != -1) {
                        if (!confirm('<?php echo $text_confirm; ?>')) {
                            return false;
                        }
                    }
                });
                // Confirm Uninstall
                $('a').click(function () {
                    if ($(this).attr('href') != null && $(this).attr('href').indexOf('uninstall', 1) != -1) {
                        if (!confirm('<?php echo $text_confirm; ?>')) {
                            return false;
                        }
                    }
                });
            });</script>
        <style>
            .success{
                color: white;
                background:  seagreen;
                padding: 6px;
                margin-top: 10px;
            }

            .selected{
                color: white !Important;
                background:  seagreen !Important;
            }

            #notification{
                /*                    height:100px !important*/
            }

            .breadcrumb{
                background-image:none;
                background: #62953c;
                color:white !Important;
                border:none !Important
            }

            .breadcrumb a{
                color:white !Important
            }

            .navbar-inner{
                padding-bottom:6px;
            }

            .vtabs-content{
                border:none;
            }

            table{
            }

            table td{
                font-size:11px !Important
            }

            h1{
                font-size:21px !Important
            }

            a.button{
                padding-left:2px !Important
                    padding-right:2px !Important
            }

            a{
                font-size:12px
            }
        </style>
    </head>

    <body>
        <div id="notification" style="margin-top: 0px; ">
            <div style="position:absolute;top:7px;right:40px;"><a href="javascript:closeNotification();"><img id="closenotimg" src="view/image/notifications/closenotification.png" style="display: none;"></a></div>
        </div>

        <style>

            .ads{
                float:left;
                padding:20px;
                padding-top:10px;
                padding-bottom:10px;
                font-size:14px;
                background:transparent;
                margin-right:8px
            }

            .ads a{
                color:white
            }

            #content{
                padding-top:0px
            }

            .breadcrumb{
                margin-bottom:0px
            }

            .overview{
                margin-top:0px;
            }

        </style>

        <?php if ($logged) { ?>
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav">
                            <li class=""><a href="<?php echo $home; ?>"><h1 style='color:black;font-size:20px'>CARI SUKAN ADMIN PANEL&nbsp;</h1>
                                </a></li>
                            <li class=""><a href="index.php?route=catalog/product/update&token=<?php echo $this->session->data['token'] ?>&product_id=<?php echo $this->user->getusertype(); ?>">SETTINGS</a></li>
                            <li><a href="<?php echo $logout; ?>">LOG OUT</a></li>
                            <li style="visibility: hidden" class=""><a href="index.php?route=catalog/review&token=<?php echo $this->session->data['token'] ?>&product_id=<?php echo $this->user->getusertype(); ?>">COMMENTS</a></li>
                        </ul>

                    </div><!-- /.navbar-collapse -->
                </div><!-- /.container-fluid -->
        </nav>
        <?php } ?>

        <div class="container" >

            <script>
                if ('<?php echo $_GET['firsttime'] ?>' == 1){
                    $(document).ready(function () {
                        $("#presetting").trigger("click");
                        $('#myModal a[href="#product"]').tab('show');
                    });
                }
            </script>