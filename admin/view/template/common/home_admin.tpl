<?php echo $header; ?>
<div id="content">
  
    <?php if ($error_install) {
    ?>
    <div class="warning"><?php echo $error_install; ?></div>
    <?php } ?>
    <?php if ($error_image) {
    ?>
    <div class="warning"><?php echo $error_image; ?></div>
    <?php } ?>
    <?php if ($error_image_cache) {
    ?>
    <div class="warning"><?php echo $error_image_cache; ?></div>
    <?php } ?>
    <?php if ($error_cache) {
    ?>
    <div class="warning"><?php echo $error_cache; ?></div>
    <?php } ?>
    <?php if ($error_download) {
    ?>
    <div class="warning"><?php echo $error_download; ?></div>
    <?php } ?>
    <?php if ($error_logs) {
    ?>
    <div class="warning"><?php echo $error_logs; ?></div>
    <?php } ?>
    <div class="box">
        <div class="content container">
            <div style="display:none" class=" col-sm-4">
                <div class="dashboard-heading"><?php echo 'Today\'s Summary'; ?></div>
                <div class="dashboard-content">
                    <table class="list">
                        <tr style="cursor: pointer;" onclick="location = '<?php echo $this->url->link('report / sale_order', 'token = ' . $this->session->data['token'], 'SSL'); ?>'">
                            <td class="right"><?php echo $text_total_sale; ?></td>
                            <td class="left"><b><font color="green"><?php echo $total_sale; ?></font></b></td>
                        </tr>
                        <tr style="cursor: pointer;" onclick="location = '<?php echo $this->url->link('sale / order', 'token = ' . $this->session->data['token'], 'SSL'); ?>'">
                            <td class="right"><?php echo $text_total_order; ?></td>
                            <td class="left"><b><?php echo $total_order; ?></b></td>
                        </tr>
                        <tr style="display:none;cursor: pointer;" onclick="location = '<?php echo $this->url->link('sale / customer', 'token = ' . $this->session->data['token'], 'SSL'); ?>'">
                            <td class="right"><?php echo $text_total_customer; ?></td>
                            <td class="left"><b><?php echo $total_customer; ?></b></td>
                        </tr>
                        <?php if ($total_review_approval) {
                        ?><tr style="cursor: pointer;" onclick="location = '<?php echo $this->url->link('catalog / review', 'token = ' . $this->session->data['token'], 'SSL'); ?>'">
                            <td class="right"><?php echo $text_total_review_approval; ?></td>
                            <td class="left"><b><font color="red"><?php echo $total_review_approval; ?></font></b></td>
                        </tr><?php } ?>
                    </table>
                </div>
            </div>
            <div class=" col-sm-12">
                <?php require "tablebooking.tpl" ?>
            </div>
        </div>

    </div>
</div>
</div>
<!--[if IE]>
<script type="text/javascript" src="view/javascript/jquery/flot/excanvas.js"></script>
<![endif]-->
<script type="text/javascript" src="view/javascript/jquery/flot/jquery.flot.js"></script>

<?php echo $footer; ?>