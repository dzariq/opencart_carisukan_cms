<table class='tablemain' style='width:710px;margin-top:10px' >
    <thead  >
        <tr>
            <td style='text-align:left'>
                Time
            </td>
            <?php foreach($section1_images as $room){ ?>
            <td style='text-align:left'>
                <span class='text-center'><?php echo $room['caption'] ?></span>
            </td>
            <?php } ?>
        </tr>
        <tr>
            <td style="background:white !Important" colspan="<?php echo count($section1_images)+1 ?>">
                <label style="color:#333" for="dates">Choose Date:&nbsp;&nbsp; </label><input style="width:30%" type='text' placeholder="Choose Date" id='checkin' name='dates' class='date checkin ' value='<?php echo date("d/m/Y") ?>' />
                <h2 style="display: none;margin-top:10px;background:white;color:black;" id="dateselected" class='text-center'></h2>
            </td>
        </tr>
    </thead>
    <tbody id='timetable' >

    </tbody>
</table> 


<script>
    $(function () {

        var dates = '<?php echo $holidays ?>';
        var peaks = '<?php echo $peaks ?>';
        var offs = '<?php echo $offs ?>';
        var weekend = '<?php echo $weekends ?>';
        var school = '<?php echo $school ?>';

        $('#checkin').datepicker({
            stepMinute: '60',
            showMinute: false,
            dateFormat: 'dd/mm/yy',
            onSelect: function (dateText, inst) {
                checkRoomAvailable();
            },
            beforeShowDay: function (date) {

                var y = date.getFullYear().toString(); // get full year
                var m = (date.getMonth() + 1).toString(); // get month.
                var d = date.getDate().toString(); // get Day
                if (m.length == 1) {
                    m = '0' + m;
                } // append zero(0) if single digit
                if (d.length == 1) {
                    d = '0' + d;
                } // append zero(0) if single digit
                var currDate = y + '-' + m + '-' + d;
                if (dates.indexOf(currDate) >= 0) {
                    return [true, "ui-highlight"];
                } else if (peaks.indexOf(currDate) >= 0) {
                    return [true, "ui-highlightpeak"];
                } else if (offs.indexOf(currDate) >= 0) {
                    return false
                } else if (school.indexOf(currDate) >= 0) {
                    return [true, "ui-highlightschool"];
                } else {
                    if (weekend == 2) {
                        if (date.getDay() == 5 || date.getDay() == 6) {
                            return [true, "ui-highlightweekend"];
                        }
                    } else {
                        if (date.getDay() == 6 || date.getDay() == 0) {
                            return [true, "ui-highlightweekend"];
                        }
                    }
                }
                return [true];
            }
        });



        checkRoomAvailable();



    });


    function checkRoomAvailable() {
        var data = {
            'product_id': '<?php echo $this->user->getusertype() ?>',
            'checkin': $('.checkin').val(),
            'quantity': 1,
            'mpn': $('#mpn').val()
        }

        $.ajax({
            url: '../index.php?route=product/product/checkRoomAvailable',
            type: 'post',
            dataType: 'json',
            data: data,
            success: function (data) {
                var html = "";
                html += "<tr>";
                z = 1;
                $.each(data, function (key, value) {
                    if (value.constructor === Array) {
                        if (z == 1) {
                            html += '<td  class="headertd">';
                            for (i = 9; i <= 23; i++) {
                                html += '<table style="width:100%">';
                                html += '<tr>';
                                html += '<td >';
                                html += i + ':00 - ' + (i + 1) + ':00';
                                html += '</td>';
                                html += '</tr>';
                                html += '</table>';
                            }
                            html += '</td>';
                        }

                        html += "<td class='headertd'>";
                        $.each(value, function (key2, value2) {
                            $.each(value2, function (key3, value3) {
                                if (value3['status'] == 0) {
                                    var classdiv = "noroom";
                                    var clickable = "onclick=\"bookingdetails(\'" + value3.bookingid + "\',\'" + value3.name + "\',\'" + value3.date + "\',\'" + value3.email + "\',\'" + value3.telephone + "\',\'" + value3.court + "\')\" ";
                                } else {
                                    var classdiv = "slot";
                                    var dateselected = data['date'];
                                    var clickable = "onclick=\"addBooking(\'" + key + "\',\'" + key3 + "\',\'" + $('.checkin').val() + "\')\" ";

                                }
                                if (value3['dayType'] == 0) {
                                    var daytype = "normalday";
                                } else if (value3['dayType'] == 1) {
                                    var daytype = "weekendday";
                                } else if (value3['dayType'] == 2) {
                                    var daytype = "phday";
                                } else if (value3['dayType'] == 20) {
                                    var daytype = "";
                                }
                                html += '<table style="width:100%">';
                                html += '<tr id="" ' + clickable + ' class=" ' + daytype + ' ' + classdiv + ' slotdiv">';
                                html += '<td style="border:1px solid black">';
                                html += value3['price'];
                                html += '</td>';
                                html += '</tr>';
                                html += '</table>';
                            });
                        });
                        html += "</td>";
                        z++;
                    }
                });
                html += "</tr>";
                $('#timetable').html(html);
            }


        });

    }

    var court_id_glo;
    var time_glo;
    var date_glo;

    function addBooking(courtid,time,date) {
        alert(courtid)
        alert(time)
        alert(date)

        court_id = courtid;
        time_glo = time;
        date_glo = date;

        $('#bookingModal').modal()
       
    }
    function submitBooking() {
       //ajax api
       var data = {
            'booking_name': document.getElementById('bookingname').value,
            'booking_phone': document.getElementById('bookingphone').value,
            'booking_email': document.getElementById('bookingemail').value,
            'booking_court_id' : court_id,
            'booking_time' : time_glo,
            'booking_date' : date_glo
        }

        alert("submit booking");
		
	$.ajax({
            url: 'index.php?route=api/adminbooking/adminBook&token=<?php echo $this->session->data[token] ?>',
            type: 'post',
            dataType: 'json',
            data: data,
            success: function (data) {
			alert(data);
		     }
	});
    }
    
    function bookingdetails(bookingid, name, date, email, telephone, court) {
        $('#modalBody').empty()
        $('#myModal').modal()
        $('#modalBody').append('<button type="button" class="close" data-dismiss="modal">&times;</button>')
        $('#modalBody').append('BOOKING ID: ' + bookingid)
        $('#modalBody').append('<hr/><br/>COURT: ' + court)
        $('#modalBody').append('<br/>DATE: ' + date)
        $('#modalBody').append('<hr/><br/>NAME: ' + name)
        $('#modalBody').append('<br/>PHONE: ' + telephone)
        $('#modalBody').append('<br/>EMAIL: ' + email)
    }

</script>


<!-- Modal -->
<div  id="myModal"  class=" modal fade" role="dialog">
    <div class="modal-dialog" >
        <!-- Modal content-->
        <div class="modal-content">
            <div  class="flexcroll modal-body" id="modalBody" style="text-align: center">
                <button type="button" class="close" data-dismiss="modal">&times;</button>

            </div>

        </div>

    </div>
</div>

<!-- Modal -->
<div  id="bookingModal"  class=" modal fade" role="dialog">
    <div class="modal-dialog" >
        <div class="modal-content">
            <div  class="flexcroll modal-body" id="bookingBody" style="text-align: center">
                <h2>Add Booking</h2>
                <h4></h4>
                Name: <input type="text" id="bookingname" class="form-control" />
                <br/>
                Contact: <input type="text" id="bookingphone" class="form-control" />
                <br/>
                Email: <input type="text" id="bookingemail" class="form-control" />
                <br/>
                <br/>
                <input onclick="submitBooking()" type="button" value="Submit" />
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

        </div>

    </div>
</div>