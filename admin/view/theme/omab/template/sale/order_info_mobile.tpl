<?php echo $header; ?>
<div id="details" role="main">
	<h3><?php echo $tab_order; ?></h3>
	<div class="details-content">
		<table>
			<tr>
				<th><?php echo $text_order_id; ?></th>
				<td><?php echo $order_id; ?></td>
			</tr>
			<?php if ($customer) { ?>
			<tr>
				<th><?php echo $text_customer; ?></th>
				<td><a><?php echo $firstname; ?> <?php echo $lastname; ?></a></td>
			</tr>
      <?php } else { ?>
			<tr>
				<th><?php echo $text_customer; ?></th>
				<td><?php echo $firstname; ?> <?php echo $lastname; ?></td>
			</tr>
			<?php } ?>
      <?php if ($customer_group) { ?>
			<tr>
				<th><?php echo $text_customer_group; ?></th>
				<td><?php echo $customer_group; ?></td>
			</tr>
			<?php } ?>
			<tr>
				<th><?php echo $text_email; ?></th>
				<td><?php echo $email; ?></td>
			</tr>
			<tr>
				<th><?php echo $text_telephone; ?></th>
				<td><?php echo $telephone; ?></td>
			</tr>
			<?php if ($fax) { ?>
			<tr>
				<th><?php echo $text_fax; ?></th>
				<td><?php echo $fax; ?></td>
			</tr>
			<?php } ?>
			<tr>
				<th><?php echo $text_total; ?></th>
				<td><?php echo $total; ?>
					<?php if ($credit && $customer) { ?>
					<?php if (!$credit_total) { ?>
					<span id="credit"><b>[</b> <a id="credit-add"><?php echo $text_credit_add; ?></a> <b>]</b></span>
					<?php } else { ?>
					<span id="credit"><b>[</b> <a id="credit-remove"><?php echo $text_credit_remove; ?></a> <b>]</b></span>
					<?php } ?>
					<?php } ?>
				</td>
			</tr>
			<?php if ($reward && $customer) { ?>
			<tr>
				<th><?php echo $text_reward; ?></th>
				<td><?php echo $reward; ?>
					<?php if (!$reward_total) { ?>
					<span id="reward"><b>[</b> <a id="reward-add"><?php echo $text_reward_add; ?></a> <b>]</b></span>
					<?php } else { ?>
					<span id="reward"><b>[</b> <a id="reward-remove"><?php echo $text_reward_remove; ?></a> <b>]</b></span>
					<?php } ?>
				</td>
			</tr>
			<?php } ?>
			<?php if ($order_status) { ?>
			<tr>
				<th><?php echo $text_order_status; ?></th>
				<td id="order-status"><?php echo $order_status; ?></td>
			</tr>
			<?php } ?>
			<?php if ($comment) { ?>
			<tr>
				<th><?php echo $text_comment; ?></th>
				<td><?php echo $comment; ?></td>
			</tr>
			<?php } ?>
			<?php if ($affiliate) { ?>
			<tr>
				<th><?php echo $text_affiliate; ?></th>
				<td><a><?php echo $affiliate_firstname; ?> <?php echo $affiliate_lastname; ?></a></td>
			</tr>
			<tr>
				<th><?php echo $text_commission; ?></th>
				<td><?php echo $commission; ?>
					<?php if (!$commission_total) { ?>
					<span id="commission"><b>[</b> <a id="commission-add"><?php echo $text_commission_add; ?></a> <b>]</b></span>
					<?php } else { ?>
					<span id="commission"><b>[</b> <a id="commission-remove"><?php echo $text_commission_remove; ?></a> <b>]</b></span>
					<?php } ?>
				</td>
			</tr>
			<?php } ?>
			<?php /*if ($ip) { ?>
			<tr>
				<th><?php echo $text_ip; ?></th>
				<td><?php echo $ip; ?></td>
			</tr>
			<?php } ?>
			<?php if ($forwarded_ip) { ?>
			<tr>
				<th><?php echo $text_forwarded_ip; ?></th>
				<td><?php echo $forwarded_ip; ?></td>
			</tr>
			<?php } ?>
			<?php if ($user_agent) { ?>
			<tr>
				<th><?php echo $text_user_agent; ?></th>
				<td><?php echo $user_agent; ?></td>
			</tr>
			<?php } ?>
			<?php if ($accept_language) { ?>
			<tr>
				<th><?php echo $text_accept_language; ?></th>
				<td><?php echo $accept_language; ?></td>
			</tr>
			<?php } */?>
			<tr>
				<th><?php echo $text_date_added; ?></th>
				<td><?php echo $date_added; ?></td>
			</tr>
			<tr>
				<th><?php echo $text_date_modified; ?></th>
				<td><?php echo $date_modified; ?></td>
			</tr>
		</table>
	</div>
	<h3><?php echo $tab_payment; ?></h3>
	<div class="details-content">
				<table>
          <tr>
            <th><?php echo $text_firstname; ?></th>
            <td><?php echo $payment_firstname; ?></td>
          </tr>
          <tr>
            <th><?php echo $text_lastname; ?></th>
            <td><?php echo $payment_lastname; ?></td>
          </tr>
          <?php if ($payment_company) { ?>
          <tr>
            <th><?php echo $text_company; ?></th>
            <td><?php echo $payment_company; ?></td>
          </tr>
          <?php } ?>
          <?php if ($payment_company_id) { ?>
          <tr>
            <th><?php echo $text_company_id; ?></th>
            <td><?php echo $payment_company_id; ?></td>
          </tr>
          <?php } ?>          
          <?php if ($payment_tax_id) { ?>
          <tr>
            <th><?php echo $text_tax_id; ?></th>
            <td><?php echo $payment_tax_id; ?></td>
          </tr>
          <?php } ?>            
          <tr>
            <th><?php echo $text_address_1; ?></th>
            <td><?php echo $payment_address_1; ?></td>
          </tr>
          <?php if ($payment_address_2) { ?>
          <tr>
            <th><?php echo $text_address_2; ?></th>
            <td><?php echo $payment_address_2; ?></td>
          </tr>
          <?php } ?>
          <tr>
            <th><?php echo $text_city; ?></th>
            <td><?php echo $payment_city; ?></td>
          </tr>
          <?php if ($payment_postcode) { ?>
          <tr>
            <th><?php echo $text_postcode; ?></th>
            <td><?php echo $payment_postcode; ?></td>
          </tr>
          <?php } ?>
          <tr>
            <th><?php echo $text_zone; ?></th>
            <td><?php echo $payment_zone; ?></td>
          </tr>
          <?php if ($payment_zone_code) { ?>
          <tr>
            <th><?php echo $text_zone_code; ?></th>
            <td><?php echo $payment_zone_code; ?></td>
          </tr>
          <?php } ?>
          <tr>
            <th><?php echo $text_country; ?></th>
            <td><?php echo $payment_country; ?></td>
          </tr>
          <tr>
            <th><?php echo $text_payment_method; ?></th>
            <td><?php echo $payment_method; ?></td>
          </tr>
  			</table>
	</div>
	<?php if ($shipping_method) { ?>
	<h3><?php echo $tab_shipping; ?></h3>
	<div class="details-content">
				<table>
          <tr>
            <th><?php echo $text_firstname; ?></th>
            <td><?php echo $shipping_firstname; ?></td>
          </tr>
          <tr>
            <th><?php echo $text_lastname; ?></th>
            <td><?php echo $shipping_lastname; ?></td>
          </tr>
          <?php if ($shipping_company) { ?>
          <tr>
            <th><?php echo $text_company; ?></th>
            <td><?php echo $shipping_company; ?></td>
          </tr>
          <?php } ?>
          <tr>
            <th><?php echo $text_address_1; ?></th>
            <td><?php echo $shipping_address_1; ?></td>
          </tr>
          <?php if ($shipping_address_2) { ?>
          <tr>
            <th><?php echo $text_address_2; ?></th>
            <td><?php echo $shipping_address_2; ?></td>
          </tr>
          <?php } ?>
          <tr>
            <th><?php echo $text_city; ?></th>
            <td><?php echo $shipping_city; ?></td>
          </tr>
          <?php if ($shipping_postcode) { ?>
          <tr>
            <th><?php echo $text_postcode; ?></th>
            <td><?php echo $shipping_postcode; ?></td>
          </tr>
          <?php } ?>
          <tr>
            <th><?php echo $text_zone; ?></th>
            <td><?php echo $shipping_zone; ?></td>
          </tr>
          <?php if ($shipping_zone_code) { ?>
          <tr>
            <th><?php echo $text_zone_code; ?></th>
            <td><?php echo $shipping_zone_code; ?></td>
          </tr>
          <?php } ?>
          <tr>
            <th><?php echo $text_country; ?></th>
            <td><?php echo $shipping_country; ?></td>
          </tr>
          <?php if ($shipping_method) { ?>
          <tr>
            <th><?php echo $text_shipping_method; ?></th>
            <td><?php echo $shipping_method; ?></td>
          </tr>
          <?php } ?>
        </table>
	</div>
	<?php } ?>
	<h3><?php echo $tab_product; ?></h3>
	<div class="details-content">
		<?php foreach ($products as $product) { ?>
		<table>
			<caption><?php echo $product['name']; ?></caption>
			<?php foreach ($product['option'] as $option) { ?>
                        <tr>
            <th>Options</th>
                        <td>
                <?php if ($option['type'] != 'file') { ?>

                &nbsp;<small> - <?php echo $option['name']; ?>: <?php echo $option['value']; ?></small>

                <?php } else { ?>

                &nbsp;<small> - <?php echo $option['name']; ?>: <a href="<?php echo $option['href']; ?>"><?php echo $option['value']; ?></a></small>

                <?php } ?>
                        </td>
                </tr>
                <?php } ?>
			<tr>
				<th><?php echo $column_quantity; ?></th>
				<td class="left"><?php echo $product['quantity']; ?></td>
			</tr>
			<tr>
				<th><?php echo $column_price; ?></th>
				<td><?php echo $product['price']; ?></td>
			</tr>
			<tr>
				<th><?php echo $column_total; ?></th>
				<td><?php echo $product['total']; ?></td>
			</tr>
		</table>
		<?php } ?>
		<?php foreach ($vouchers as $voucher) { ?>
		<table>
			<caption>
				<h4><?php echo $voucher['description']; ?></h4>
			</caption>
			<tr>
				<th><?php echo $column_quantity; ?></th>
				<td class="left">1</td>
			</tr>
			<tr>
				<th><?php echo $column_price; ?></th>
				<td><?php echo $voucher['amount']; ?></td>
			</tr>
			<tr>
				<th><?php echo $column_total; ?></th>
				<td><?php echo $voucher['amount']; ?></td>
			</tr>
		</table>
		<?php } ?>
		<table class="total">
			<?php foreach ($totals as $totals) { ?>
			<tr>
				<th><?php echo $totals['title']; ?>:</th>
				<td><?php echo $totals['text']; ?></td>
			</tr>
			<?php } ?>
		</table>
	</div>
	<h3><?php echo $tab_order_history; ?></h3>
	<div class="details-content">
	<section id="history"></section>
	<hr />
	<form id="add-history">
		<ul>
			<li>
				<label><?php echo $entry_order_status; ?></label>
				<select name="order_status_id">
					<?php foreach ($order_statuses as $order_statuses) { ?>
					<?php if ($order_statuses['order_status_id'] == $order_status_id) { ?>
					<option value="<?php echo $order_statuses['order_status_id']; ?>" selected="selected"><?php echo $order_statuses['name']; ?></option>
					<?php } else { ?>
					<option value="<?php echo $order_statuses['order_status_id']; ?>"><?php echo $order_statuses['name']; ?></option>
					<?php } ?>
					<?php } ?>
				</select>
			</li>
			<li>
				<label><?php echo $entry_notify; ?></label>
				<input type="checkbox" name="notify" value="1" />
			</li>
			<li>
				<label><?php echo $entry_comment; ?></label>
				<textarea name="comment" cols="40" rows="8"></textarea>
			</li>
			<li>
				<input type="button" id="button-history" class="button" value = "<?php echo $button_add_history; ?>">
			</li>
		</ul>
	</form>
	</div>
</div>
<script src="view/theme/oma/js/jq.mobi.min.js" type="text/javascript" ></script>		
<script>window.$ = window.jq;</script>
<script>
    $.get("index.php?route=sale/order/history&token=<?php echo $token; ?>&order_id=<?php echo $order_id; ?>", function(data){
			$('#history').html(data);
		});
		
		$('#button-history').bind('click', function() {
			$.ajax({
				url: 'index.php?route=sale/order/history&token=<?php echo $token; ?>&order_id=<?php echo $order_id; ?>',
				type: 'post',
				dataType: 'html',
				data: 'order_status_id=' + encodeURIComponent($('select[name=\'order_status_id\']').val()) + '&notify=' + encodeURIComponent($('input[name=\'notify\']').attr('checked') ? 1 : 0) + '&append=' + encodeURIComponent($('input[name=\'append\']').attr('checked') ? 1 : 0) + '&comment=' + encodeURIComponent($('textarea[name=\'comment\']').val()),
				beforeSend: function() {
					$('.success, .warning').remove();
					$('#button-history').attr('disabled', true);
					//$('<div class="attention"><img src="view/image/loading.gif" alt="" /> <?php echo $text_wait; ?></div>').insertBefore($('#history'));
				},
				complete: function() {
					$('#button-history').removeAttr('disabled');
					$('.attention').remove();
				},
				success: function(html) {
					$('#history').html(html);
					
					$('textarea[name=\'comment\']').val('');
					
					//$('#order-status').html($('select[name=\'order_status_id\'] option:selected').text());
				}
			});
		});
</script>
<?php echo $footer; ?>