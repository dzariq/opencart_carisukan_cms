<?php echo $header; ?>
<div id="main" role="main">
    <table style="width:100%">
        <thead>
            <tr>
                <td style="width:40%;">Product</td><td style="width:30%;">Type</td><td style="width:30%;">Quantity</td>
            </tr>
        </thead>
        <tbody>
            <?php foreach($products as $product){ ?>
            <?php if(count($product['child']) != 0){ ?>
            <?php $i=0; foreach($product['child'] as $child){ ?>
            <tr >
                <?php if($i == 0){ ?>
                <td style="width:40%;font-size:8px"><?php echo $product['name'] ?></td>
                <?php }else{ ?>
                <td style="width:40%;">&nbsp;</td>
                <?php } ?>
                <td style="width:30%;font-size:8px"><?php echo $child['option'] ?></td><td style="width:30%;"><?php echo $child['quantity'] ?></td>
            </tr>
            <?php $i++; } ?>
            <?php } ?>
            <?php } ?>
        </tbody>
    </table>
</div>
<?php echo $footer; ?>