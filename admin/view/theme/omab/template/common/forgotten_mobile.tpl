<?php echo $header; ?>
<div id="main" role="main">
  <h1><?php echo $text_login_oma; ?></h1> 
  <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <h2><?php echo $heading_title; ?></h2>
  <p><?php echo $text_email_oma; ?></p>
  <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="forgotten">
    <ul>
	  <li>
        <input type="text" name="email" placeholder = "E-Mail" value="<?php echo $email; ?>" />
      </li>
	  <li>
	    <input type="button"  class="button" value = "<?php echo $button_cancel; ?>" onclick="location = '<?php echo $cancel; ?>';">
	  	<input type="submit" class="button" value = "<?php echo $button_reset; ?>" onclick = "success('An email with a confirmation link has been sent')")/>
	  </li>
    </ul>  
  </form>
</div>
<?php echo $footer; ?>