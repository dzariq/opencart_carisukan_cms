<?php echo $header; ?>
<div id="main" role="main">
	<?php if ($error_install) { ?>
 	<div class="warning"><?php echo $error_install; ?></div>
	<?php } ?>
	<?php if ($error_image) { ?>
	<div class="warning"><?php echo $error_image; ?></div>
	<?php } ?>
	<?php if ($error_image_cache) { ?>
	<div class="warning"><?php echo $error_image_cache; ?></div>
	<?php } ?>
	<?php if ($error_cache) { ?>
	<div class="warning"><?php echo $error_cache; ?></div>
	<?php } ?>
	<?php if ($error_download) { ?>
	<div class="warning"><?php echo $error_download; ?></div>
	<?php } ?>
	<?php if ($error_logs) { ?>
	<div class="warning"><?php echo $error_logs; ?></div>
	<?php } ?>
	<section id="overview" class="dashboard-panel">
		<h3><?php echo $text_overview; ?></h3>
		<div class="dashboard-content">
			<table>
				<tr>
					<th><?php echo $text_total_sale; ?></th>
					<td><?php echo $total_sale; ?></td>
				</tr>
				
				<tr>
					<th><?php echo $text_total_order; ?></th>
					<td><?php echo $total_order; ?></td>
				</tr>
				<tr>
					<th><?php echo $text_total_customer; ?></th>
					<td><?php echo $total_customer; ?></td>
				</tr>
<!--				<tr>
					<th><?php //echo $text_total_customer_approval; ?></th>
					<td><?php //echo $total_customer_approval; ?></td>
				</tr>
				<tr>
					<th><?php //echo $text_total_review_approval; ?></th>
					<td><?php //echo $total_review_approval; ?></td>
				</tr>
				<tr>
					<th><?php //echo $text_total_affiliate; ?></th>
					<td><?php //echo $total_affiliate; ?></td>
				</tr>
				<tr>
					<th><?php //echo $text_total_affiliate_approval; ?></th>
					<td><?php //echo $total_affiliate_approval; ?></td>
				</tr>-->
			</table>
		</div>
	</section>
	<section id="latest" class="dashboard-panel">
		<h3><?php echo 'To do List'; ?></h3>
		<div class="dashboard-content">
		<?php if ($orders) { ?>
			<?php $id = 0;?>
			<?php foreach ($orders as $order) { ?>
			<ol id = "order<?php echo $id ?>" class="order">
			<?php foreach ($order['action'] as $action) { ?>
				<li><a href="<?php echo $action['href']; ?>"><?php echo $order['customer']; ?><span style="float:right;color:red"><?php echo $order['status']; ?></span></a></li>
			<?php } /*?> <!-- Waiting for next release -->
			<div class="order-content">
				<table>
					<tr>
						<th><?php echo $column_order; ?></th>
						<td><?php echo $order['order_id']; ?></td>
					</tr>
					<tr>
						<th><?php echo $column_status; ?></th>
						<td><?php echo $order['status']; ?></td>
					</tr>
					<tr>
						<th><?php echo $column_date_added; ?></th>
						<td class="right"><?php echo $order['date_added']; ?></td>
					</tr>
					<tr>
						<th><?php echo $column_total; ?></th>
						<td><?php echo $order['total']; ?></td>
					</tr>
				</table>
				<a href="<?php echo $action['href']; ?>"  class="button"><?php echo $action['text']; ?></a>
			</div> <?php */ ?>
			</ol>
			<?php $id++; ?>
			<?php } ?>
		<?php } else { ?>		
			<h4><?php echo $text_no_results; ?></h4>
		<?php } ?>
		</div>
	</section>
</div>
<?php echo $footer; ?>