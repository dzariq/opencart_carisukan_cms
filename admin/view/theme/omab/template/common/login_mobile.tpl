<?php echo $header; ?>
<div id="main" role="main">
  <h1><?php echo $text_login_oma; ?></h1> 
  <?php if ($success) { ?>
  <div class="success"><?php echo $success; ?></div>
  <?php } ?>
  <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="login">
    <ul>          
      <li>
        <input type="text" name="username" placeholder = "Username" value="<?php echo $username; ?>" />
      </li>
      <li>  
        <input type="password" name="password" placeholder = "Password" value="<?php echo $password; ?>" />
      </li>
      <li>
        <input type="submit" class="button" value="<?php echo $button_login; ?>" />
      </li>
      <li>
        <a href="<?php echo $forgotten; ?>"><?php echo $text_forgotten; ?></a>
      </li>      
    </ul>
    <?php if ($redirect) { ?>
    <input type="hidden" name="redirect" value="<?php echo $redirect; ?>" />
    <?php } ?>
  </form>
</div>
<?php echo $footer; ?>