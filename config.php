<?php

//setting
$url = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";


$parse = parse_url($url);
$domain_name = $parse['host'];

define('DOMAIN_NAME', $domain_name);
define('SERVERURL', '/home/potatoapp/public_html/web/');


// HTTP

define('HTTP_SERVER', 'http://' . $domain_name . '/');

define('HTTP_IMAGE', 'http://' . $domain_name . '/image/');

define('HTTP_ADMIN', 'http://' . $domain_name . '/admin/');



// HTTPS

define('HTTPS_SERVER', 'http://' . $domain_name . '/');

define('HTTPS_IMAGE', 'http://' . $domain_name . '/image/');



// DIR

define('DIR_APPLICATION', SERVERURL . 'catalog/');

define('DIR_SYSTEM', SERVERURL . 'system/');

define('DIR_DATABASE', SERVERURL . 'system/database/');

define('DIR_LANGUAGE', SERVERURL . 'catalog/language/');

define('DIR_TEMPLATE', SERVERURL . 'catalog/view/theme/');

define('DIR_CONFIG', SERVERURL . 'system/config/');

define('DIR_IMAGE', SERVERURL . 'image/');

define('DIR_CACHE', SERVERURL . 'system/cache/');

define('DIR_DOWNLOAD', SERVERURL . 'download/');

define('DIR_LOGS', SERVERURL . 'system/logs/');



$temp = explode('.', $domain_name);
if ($temp[0] == 'www') {
    unset($temp[0]);
}

$is_subdomain = false;

foreach ($temp as $key => $val) {

    if (isset($temp[$key + 1])) {
        if ($val . '.' . $temp[$key + 1] == 'cuticuti.com') {
            unset($temp[$key]);
            unset($temp[$key + 1]);
            $is_subdomain = true;
        }
    }
}

$temp = array_values($temp);

if (count($temp) == 3 && ($temp[2] == 'my')) {
    $db_name = $temp[0] . $temp[1] . $temp[2];
} else if (count($temp) == 2) {
    $db_name = $temp[0] . $temp[1];
} else if (count($temp) == 3) {
    $db_name = $temp[0];
} else {
    $db_name = $temp[0] . $temp[1];
}


// DB
define('DB_DRIVER', 'mysqliz');

define('DB_HOSTNAME', 'localhost');

define('DB_USERNAME', 'potatoap_admin');

define('DB_PASSWORD', 'ubikentang123');


define('DB_DATABASE', 'potatoap_demoweb');


define('DB_NAME', 'potatoap_demoweb');

define('DB_PREFIX', '');

