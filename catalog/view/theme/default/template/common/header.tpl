<!DOCTYPE html>

<!--[if IE 7]>                  <html class="ie7 no-js" lang="<?php echo $lang; ?>" dir="<?php echo $direction; ?>">     <![endif]-->
<!--[if lte IE 8]>              <html class="ie8 no-js" lang="<?php echo $lang; ?>" dir="<?php echo $direction; ?>">     <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--> <html class="not-ie no-js" lang="<?php echo $lang; ?>" dir="<?php echo $direction; ?>">  <!--<![endif]-->

    <head>

        <!-- Charset
    ================================================== -->
        <meta charset="UTF-8">

        <!-- Always force latest IE rendering engine (even in intranet) & Chrome Frame.
        Remove meta X-UA-Compatible if you use the .htaccess
        ================================================== -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

        <!-- Mobile Specific Metas
        ================================================== -->
        <?php if($this->config->get('config_responsive') == 1){ ?>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <?php } ?>

        <!-- Basic Page Needs
        ================================================== -->
        <title><?php echo $title; ?></title>

        <base href="<?php echo $base; ?>" >

        <?php if ($description) {
        ?>
        <meta name="description" content="<?php echo $description; ?>" />
        <?php } ?>
        <?php if ($keywords) {
        ?>
        <meta name="keywords" content="<?php echo $keywords; ?>" />
        <?php } ?>
        <meta name="author" content="<?php echo $name; ?>">
        <!--    <meta name="DC.creator" content="Neil Smart - http://the1path.com" />-->

        <!-- Favicons
        ================================================== -->
        <?php if ($icon) { ?>
        <link href="<?php echo $icon; ?>" rel="icon" />
        <link rel="shortcut icon" href="<?php echo $icon; ?>">
        <link rel="apple-touch-icon" href="<?php echo $icon; ?>">
        <?php } ?>

        <?php foreach ($links as $link) { ?>
        <link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" />
        <?php } ?>

        <!-- Fonts -->

        <link href='http://fonts.googleapis.com/css?family=Open+Sans+Condensed:300' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,400,600italic,600' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Oswald' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Droid+Sans' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Damion' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Droid+Serif' rel='stylesheet' type='text/css'>
        <!-- Stylesheets
        ================================================== -->

            <link rel="stylesheet" type="text/css" href="catalog/view/css/module.css" media="screen, projection" />
        <link rel="stylesheet" type="text/css" href="catalog/view/theme/<?php echo $this->config->get('config_template'); ?>/stylesheet/bootstrap.min.css" media="screen, projection" />
        <link rel="stylesheet" type="text/css" href="catalog/view/theme/<?php echo $this->config->get('config_template'); ?>/stylesheet/animate.min.css" media="screen, projection" />
        <link rel="stylesheet" type="text/css" href="catalog/view/theme/<?php echo $this->config->get('config_template'); ?>/stylesheet/ddlevelsmenu-base.css" media="screen, projection" />
        <link rel="stylesheet" type="text/css" href="catalog/view/theme/<?php echo $this->config->get('config_template'); ?>/stylesheet/ddlevelsmenu-topbar.css" media="screen, projection" />
        <link rel="stylesheet" type="text/css" href="catalog/view/theme/<?php echo $this->config->get('config_template'); ?>/stylesheet/jquery.countdown.css" media="screen, projection" />
        <link rel="stylesheet" type="text/css" href="catalog/view/theme/<?php echo $this->config->get('config_template'); ?>/stylesheet/font-awesome.min.css" media="screen, projection" />
        <link rel="stylesheet" type="text/css" href="catalog/view/theme/<?php echo $this->config->get('config_template'); ?>/stylesheet/style.css" media="screen, projection" />
        <link rel="stylesheet" type="text/css" href="catalog/view/theme/<?php echo $this->config->get('config_template'); ?>/stylesheet/custom.css" media="screen, projection" />
        <link rel="stylesheet" type="text/css" href="catalog/view/css/font-awesome.css" media="screen, projection" />
        <link rel="stylesheet" type="text/css" href="catalog/view/css/component.css" media="screen, projection" />

        <script type="text/javascript" src="catalog/view/theme/<?php echo $this->config->get('config_template'); ?>/js/animations.js"></script>
        <link href="catalog/view/javascript/bxslider/jquery.bxslider.css" rel="stylesheet">


        <?php foreach ($styles as $style) { ?>
        <link rel="<?php echo $style['rel']; ?>" type="text/css" href="<?php echo $style['href']; ?>" media="<?php echo $style['media']; ?>" />
        <?php } ?>

        <link href="catalog/view/css/blog_custom.css" rel="stylesheet">


        <!-- Wheres all the JS? Check out the footer :)
        ================================================== -->
        <script type="text/javascript" src="catalog/view/javascript/jquery/jquery-1.7.1.min.js"></script>
        <script src="catalog/view/javascript/bxslider/jquery.bxslider.min.js"></script>
        <script type="text/javascript" src="catalog/view/javascript/jarallax.js"></script>

        <script type="text/javascript" src="catalog/view/javascript/jquery/jquery.jcarousel.min.js"></script>
        <script type="text/javascript" src="catalog/view/javascript/blog_search.js"></script>

        <?php foreach ($scripts as $script) { ?>
        <script type="text/javascript" src="<?php echo $script; ?>"></script>
        <?php } ?>

        <!--[if IE 7]>
                <link rel="stylesheet" type="text/css" href="catalog/view/theme/<?php echo $this->config->get('config_template'); ?>/stylesheet/ie7.css" />
        <![endif]-->
        <!--[if lt IE 7]>
                <link rel="stylesheet" type="text/css" href="catalog/view/theme/<?php echo $this->config->get('config_template'); ?>/stylesheet/ie6.css" />
                <script type="text/javascript" src="catalog/view/javascript/DD_belatedPNG_0.0.8a-min.js"></script>
                <script type="text/javascript">
                        DD_belatedPNG.fix('#logo img');
                </script>
        <![endif]-->
        <!--[if lt IE 9]>
                <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
        <!--[if lt IE 10]>
            <link rel="stylesheet" type="text/css" href="catalog/view/theme/<?php echo $this->config->get('config_template'); ?>/stylesheet/ieglobal.css" />
        <![endif]-->
        <?php if ($stores) { ?>
        <script type="text/javascript"><!--
            $(document).ready(function() {
            <?php foreach ($stores as $store) { ?>
                    $('body').prepend('<iframe src="<?php echo $store; ?>" style="display: none;"></iframe>');
                    <?php } ?>
            });
                    //--></script>
        <?php } ?>
        <?php echo $google_analytics; ?>

        <?php
        $layout = 'container';
        if($this->config->get('config_package') == 1){
        $layout = 'container-full';
        }

        define('layout', $layout );



        ?>

        <!-- customer css -->
        <style>

            .product-snippet{
                margin: 0px;
                display:none;
            }
            
            .snippet-desc{
                line-height:1.2;
                font-size:11px
            }
            
            .snippet-inside{
                background:rgba(255,255,255,1.0);
                color:black !important;
                font-size:14px;
                padding:20px;
            }
            
            .tile{
                background-size: auto auto;
                height: <?php echo $this->config->get("config_image_product_height") ?>px;
                cursor: pointer;

                background-repeat: no-repeat;
                background-position:center center;
            //    border:1px solid #e0e0e0;
                padding:0px;
            }
       
            
              .colored > .overlay {
                width:100%;
                height:100%;
                position:absolute;
            }
              .colored:hover > .overlay {
                width:100%;
                height:100%;
                position:absolute;
                
            }
            
              .tile:hover > .overlay {
                width:100%;
                height:100%;
            
                position:absolute;
                background-color:white
              //  background-color:#<?php echo $this->config->get('config_main_theme') ?>;
            }


            .metromenu-inactive{
                color : <?php echo $this->config->get('theme_menu_color') ?> !important;
                background-color:#<?php echo $this->config->get('config_main_theme') ?> !important;
                cursor:pointer;
                height:29px;
                opacity:0.3;
                line-height: 2.3;
            }
            .metromenu{
                line-height: 2.3;
                color : <?php echo $this->config->get('theme_menu_color') ?> !important;
                background-color:#<?php echo $this->config->get('config_main_theme') ?> !important;
                cursor:pointer;
                height:29px;
            }



            .logo{
                font-family:"Kaushan Script";
                font-size:32px;
                line-height:1.6
            }

            #filterpro_box .option_box .option_name{
                color : <?php echo $this->config->get('theme_menu_color') ?> !important;
                background-color:#<?php echo $this->config->get('config_main_theme') ?> !important;

            }

            .date-article span{
                color : <?php echo $this->config->get('theme_menu_color') ?> !important;
                font-family: "Oswald", sans-serif !important
            }

            .tile-border{
                // border-bottom:1px solid #e0e0e0
            }

            .btn-info, .btn-primary{
                color : <?php echo $this->config->get('theme_menu_color') ?> !important;
                border-radius: 0px
            }

            .pagination>.active>span{
             background-color:#<?php echo $this->config->get('config_main_theme') ?> !important;
                color : <?php echo $this->config->get('theme_menu_color') ?> !important;
                border:none
            }


            .darken > .overlay{
                width:100%;
                height:100%;
                position:absolute;
                        background-color:#<?php echo $this->config->get('config_main_theme') ?>;
opacity:0.1;
            }

            .darken-active > .overlay {
                width:100%;
                height:100%;
                position:absolute;
                background-color:#<?php echo $this->config->get('config_main_theme') ?>;
                opacity:0.3;
            }
            .darken:hover > .overlay {
                width:100%;
                height:100%;
                position:absolute;
                background-color:#<?php echo $this->config->get('config_main_theme') ?>;
                opacity:0.3;
            }

            .top > .overlay{
                background-color:transparent;
                opacity:1.0 ;
            }
            .top:hover > .overlay{
                background-color:transparent;
            }

            #cboxLoadedContent p,#cboxLoadedContent h1{
                padding:20px !important
            }

            <?php if($this->config->get('config_responsive') == 0){ ?>
                                                                    .container,.container-full{
                                                                        /* Margin/padding copied from Bootstrap */
                                                                        margin-left: auto !important;
                                                                        margin-right: auto !important;
                                                                        padding-left: 15px !important;
                                                                        padding-right: 15px !important;

                                                                        /* Set width to your desired site width */
                                                                        max-width:none !important;
                                                                        width: 1020px !important;
                                                                    }                                                

                                                                    <?php } ?>


            body, .header, footer{
                background: <?php echo $this->config->get('theme_background_color') ?>;
                color: <?php echo $this->config->get('theme_links_color') ?>;

            }
            .nav-tabs{
                border-bottom: <?php echo $this->config->get('theme_background_color') ?> !important;

            }
            a, h1, h2,h3,h4,h5,h6,p,span,div{
                color: <?php echo $this->config->get('theme_links_color') ?> !important;

            }


            .carousel .carousel-control span{
                background:#<?php echo $this->config->get('config_main_theme') ?>
            }

            video#bgvid {
                right: 0; bottom: 0;
                min-width: 100%; height: 100%;
                width: auto; height: auto; z-index: -100;
                background: url(image/data/greenbilling/section1.png) no-repeat;
                background-size: cover;
            }


            @media (max-width:991px){
                video#bgvid {
                    width: 100% !important; height: 100%  !important;
                }
            }

            <!--[if lt IE 9]>
            <script>
            document.createElement('video');
            </script>
            <![endif]-->

            video { display: block; }

            a:hover{
                border:none;
            }
            .kart-links{
                margin-top:20px
            }
            .header{
                color:white;
                border:none;
                min-height:50px
            }
            .kart-links a,.btn-danger{
                text-transform:uppercase;
                background : #<?php echo $this->config->get('config_main_theme') ?>  !important;;
                color : <?php echo $this->config->get('theme_menu_color') ?> !important;
            }
            .kart-links a:hover{
                background :  #8b8b8b  !important;;
                color : black !important;
            }


            .shop-items .item-price{
                background : <?php echo $this->config->get('config_main_theme') ?>  !important;;
                color : <?php echo $this->config->get('theme_menu_color') ?> ;
            }

            .btn-info, .btn-danger , .checkout-heading {
                color : <?php echo $this->config->get('theme_buttonstext_color') ?> !important;
            }

            .carousel .carousel-caption a.btn{
                border:none;
            }
            .btn-info:hover{
                background:<?php echo $this->config->get('theme_buttons_color') ?>
            }
            .btn-info{
                background:#6E6E6E
            }
            .shop-items .item-price{
                background:#6E6E6E
            }
            .btn-danger{
                background :  <?php echo $this->config->get('theme_buttons_color') ?>;
                border-color: <?php echo $this->config->get('theme_buttons_color') ?>;
            }
            .btn-danger:hover{
                background :  <?php echo $this->config->get('theme_buttons_color') ?>;
                border-color: <?php echo $this->config->get('theme_buttons_color') ?>;
            }
            .cta .btn-danger{
                border-color: <?php echo $this->config->get('theme_buttons_color') ?> !important
            }
                #column-left,#column-right{
            padding:10px
        }
            .sidey .nav > li > a{
                background :  white;
                border:none;
                color:#666
            }
            .sidey .nav{
                border-color:#e0e0e0;
            }
            .sidey .nav a:hover{
                background :  #e0e0e0;
            }
            .btn-info{
                background: <?php echo $this->config->get('theme_buttons_color') ?>
            }
            a{
                color :black
            }
            @media (min-width:768px) and (max-width: 991px){
                .navis select{
                    margin-top: 14px !important;
                }
            }
            .carousel-indicators .active{
                border:none;
                background: <?php echo $this->config->get('theme_buttons_color') ?>
            }
            .carousel-indicators li:hover{
                border:none;
                background: #6E6E6E
            }
            .carousel-indicators li{
                border:none;
                background: #6E6E6E
            }
            .error{
                color:red !important
            }
            a:hover{
                color :#6E6E6E
            }
            .breadcrumb>li+li:before{
                color:#333
            }
            #myTab li.active a{
                background: #<?php echo $this->config->get('config_main_theme') ?> !important;
                color: <?php echo $this->config->get('theme_menu_color') ?> !important;
                border:none;
                border-radius:0px

            }

            .hero p{
                border-color: <?php echo $this->config->get('theme_buttons_color') ?> !important;
            }

            .btn-info:hover{
                border:none
            }
            .btn-info{
                border:none
            }

            .ddsubmenustyle li a:hover{
                background: #6E6E6E !important
            }

            #ddsubmenu1 a{
                color: <?php echo $this->config->get('theme_buttons_color') ?> !important;
            }

            .navis{
                margin-top:-10px
            }
            .carousel-inner{
                background: transparent
            }

            .navis select{
                width: 100%;
                height: 35px;
                padding: 6px 12px;
                font-size: 12px;
                line-height: 1.428571429;
                color: #555;
                vertical-align: middle;
                background-color: #fff;
                border: 1px solid #ccc;

                -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,0.075);
                box-shadow: inset 0 1px 1px rgba(0,0,0,0.075);
                -webkit-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
                transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            }

            .container-full {
                margin: 0 auto;
                width: 100%;
                padding-left:20px;
                padding-right:20px;
            }

            .container-full .row {
                margin-left: 0px;
                margin-right: 0px;
            }

            .shop-items{
                margin : 0 !important
            }

            .shop-items .breadcrumb{
                border:none;
                padding:10px;

            }
            .shop-items .breadcrumb a, .breadcrumb>li+li:before{
                color:#5d717e !important;
                font-size:12px
            }

            .ddsubmenustyle ul{
                top:-1px !important
            }

            .box-heading{
                text-align:center;
                text-transform:uppercase
            }

            footer{
                border-top:1px solid #e0e0e0;
                background: #ededed
            }

            footer h4{
                color:<?php echo $this->config->get('theme_links_color') ?> !important
            }
            footer a,span,p,.copy{
                color:<?php echo $this->config->get('theme_links_color') ?> !important
            }

            #headercat{
                background : transparent;
                border-top:1px solid #e0e0e0;
            }
            .mattblackmenu li a:hover{
                background : #<?php echo $this->config->get('config_main_theme') ?> !important;
                color:<?php echo $this->config->get('theme_menu_color') ?> !important
            }

            .mattblackmenu li a{
                text-transform:uppercase;
                color : #8b8b8b !Important
            }

            .cool-block-bor {
                background : <?php echo $this->config->get('theme_blockbg_color') ?> !important;
                border:none;
            }
            .item-details a,.cool-block-bor h3,.cool-block-bor h1,.cool-block-bor h2, .cool-block-bor h4,.cool-block-bor h5,.cool-block-bor h6,.cool-block-bor p,.cool-block-bor a,.cool-block-bor label{
                color : <?php echo $this->config->get('theme_blocktext_color') ?> !important;
            }

            @media(max-width:767px) {
                .kart-links{
                    text-align:center !important;
                    margin-top:60px;
                }
                .logomain{
                    margin: 0 auto
                }
            }

            @media (max-width:480px){
                .kart-links{
                    margin-top:30px !important
                }
            }

            .ddsubmenustyle{
                z-index:99999 !Important
            }



            #search-box-wrap{
                background-color: #e68b2b;
                height: 50px;
            }

            #searchpdtsubmit{
                color : <?php echo $this->config->get('theme_menu_color') ?> !important;
                background-color:#<?php echo $this->config->get('config_main_theme') ?> !important;
                border: 0;

                text-transform: uppercase;
                font-weight: bold;
                padding: 5px;
                padding-left: 10px;
                padding-right: 10px;
            }

            #search-box #close-x {
                float: right;
                width: 23px;
                height: 23px;
                color : <?php echo $this->config->get('theme_menu_color') ?> !important;
                background-color:#<?php echo $this->config->get('config_main_theme') ?> !important;
                font-family: "Droid Sans", Arial, Helvetica, sans-serif;
                font-weight: 700;
                font-size: 14px;
                text-transform: uppercase;
                text-align: center;
                line-height: 21px;
                margin-top: 15px;
                margin-right: 0;
                margin-left: 20px;
                cursor: pointer;
            }

            @media (max-width: 580px){
                #spdt{
                    width: 132px !important;
                    min-width: 100px  !important;
                }
                #close-x{
                    margin-left:6px !Important
                }
            }


            #spdt{
                height: 30px;
                font-size: 1.2em;
                background: url(image/icon-search.png) no-repeat 7px 7px #fff;
                padding: 5px 7px 5px 30px;
                border: 0;
                box-shadow: 3px 3px 10px #d0d0d0 inset;
                min-width: 400px;
                outline: none;
                margin-right: -4px;
            }

            #search-box-wrap {
                display: none;
            }

            .screen-reader-text, .assistive-text {
                position: absolute !important;
                clip: rect(1px 1px 1px 1px);
                clip: rect(1px, 1px, 1px, 1px);
            }
            #search-box {
                position: relative;
                width: 96%;
                margin: 0 auto;
                padding: 0 2%;
                max-width: 1062px;
                -webkit-box-sizing: border-box;
                -moz-box-sizing: border-box;
                -o-box-sizing: border-box;
                box-sizing: border-box;
            }

            #search-box #searchform {
                float: right;
                margin-top: 10px;
            }
        </style>
    </head>

    <body onload="init()">
        <!-- Logo & Navigation starts -->
        <div id="search-box-wrap" >
            <div id="search-box">
                <div id="close-x">x</div>
                <div id="searchform" class="searchform" >
                    <div >
                        <label class="screen-reader-text" for="s">Search for:</label>
                        <input  type="text" value="" name="spdt" id="spdt">
                        <input type="button" id="searchpdtsubmit" value="Search">
                    </div>
                </div>        
            </div>
        </div>
        <div class="header">
            <div class="<?php echo layout ?>">
                <div class="row">
                    <div class="col-sm-4 col-xs-12">
                        <!-- Logo -->
                        <div class="logo" style='padding-top:10px;height:60px'>
                            <?php if($logo != ''){ ?>
                            <a href="<?php echo $home ?>"><img style="height:60px" class="logomain img-responsive" src="<?php echo $logo ?>" /></a>
                            <?php }else{ ?>
                            <a class="logo" href="<?php echo $home ?>"><?php echo $this->config->get('config_name') ?></a>
                            <?php } ?>
                        </div>
                    </div>

                    <div <?php  if (count($languages) > 1 || count($currencies) > 1) { ?>class=" col-sm-4 col-xs-12" <?php }else{ ?>  class="col-md-8 col-sm-8 col-xs-12" <?php } ?> >
                        <div style='text-align:right' class="kart-links">
                            <?php if($this->request->get['firsttime'] == 1){ ?>
                            <a style='background:green;color:white' target='_blank' href="http://<?php echo $domain_name.'/admin/index.php?route=setting/setting&firsttime=1' ?>">Edit my Website</a>
                            <?php } ?>
                            <?php if(!$this->customer->isLogged()){ ?>
                            <a href="<?php echo 'index.php?route=account/login' ?>"><?php echo $text_login ?></a>
                            <a href="<?php echo 'index.php?route=account/register' ?>"><?php echo $text_register ?></a>
                            <?php }else{ ?>
                            <a href="<?php echo 'index.php?route=account/logout' ?>"><?php echo $text_logout ?></a>
                            <a href="<?php echo 'index.php?route=account/account' ?>"><?php echo $text_account ?></a>
                            <?php } ?>
                            <?php if($this->config->get('config_shoppingcart')){ ?>
                            <a  href="index.php?route=checkout/cart"><i class="icon-shopping-cart"></i>  Cart</a>
                            <?php } ?>
                            <a style='background:transparent !important' class='blogs' href='javascript:void(0)'  >
                                <img class='img-circle' style='background:#8b8b8b' src='image/icon-search-w.png' />
                            </a>
                        </div>
                    </div>
                    <div style="padding-top:11px" class='col-sm-2 col-xs-6  text-center'>
                        <?php echo $language ?>
                    </div>
                    <?php if($this->config->get('config_shoppingcart')){ ?>
                    <div  style="padding-top:11px" class='col-sm-2 col-xs-6  text-center'>
                        <?php echo $currency ?>
                    </div>
                    <?php } ?>

                </div>
            </div>
        </div>
    </div>

    <div class='header <?php echo layout ?>' id='headercat' style=';height:40px;min-height:40px'>
        <!-- Navigation menu -->
        <div class="navi" style='padding:0px'>
            <div id="ddtopmenubar" class="mattblackmenu" style='float:left'>
                <ul>
                    <li><a href="<?php echo $home ?>"><?php echo $text_home ?></a></li>
                    <?php if($categories && $this->config->get('config_shoppingcart')){ ?>
                    <?php foreach($categories as $category){ ?>
                    <li><a href="<?php echo $category['href'] ?>" <?php if($category['children']){ ?> rel="ddsubmenu1" <?php } ?>><?php echo $category['name'] ?></a>
                        <?php if($category['children']){ ?>
                        <ul style="z-index:99999" id="ddsubmenu1" class="ddsubmenustyle">
                            <?php  foreach($category['children'] as $child){ ?>
                            <li><a href="<?php echo $child['href'] ?>"><?php echo $child['name'] ?></a></li>
                            <?php } ?>
                        </ul>
                        <?php } ?>
                    </li>
                   
                    <?php } ?>
                    <?php } ?>
                     <?php if($this->config->get('simple_blog_status') == 1){ ?>
                    <li><a href="simple-blog">Blog</a></li>
                    <?php } ?>
                </ul>
            </div>

        </div>

        <!-- Dropdown NavBar -->
        <div class="navis"></div>

    </div>
    <div class='clearfix'></div>
    <div id="notification"></div>

    <script>
                        $(document).ready(function() {

                $("video").prop('muted', true);
                });
    </script>
    
    <div class="container travelbox">