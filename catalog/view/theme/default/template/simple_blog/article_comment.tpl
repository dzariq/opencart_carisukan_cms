<?php  if ($comments) { ?>
<?php foreach ($comments as $comment) { ?>
<div class="article-reply">
    <div class="author"><b><?php echo $comment['author']; ?></b></div>
    <div class="comment-date"> <?php echo $comment['date_added']; ?></div>
    <div class="comment-text text"><?php echo $comment['comment']; ?></div><br />
    <hr/>
    <div><a style="float: left;cursor:pointer" onclick="setArticleId( <?php echo $comment['simple_blog_comment_id']; ?> )"><?php echo $text_reply_comment; ?></a></div>
    <div class="clearfix"></div>
    <div class='reply-div' id='reply-me<?php echo $comment['simple_blog_comment_id']; ?>'></div>
    <br/>
    <?php if($comment['comment_reply']) { ?>
    <?php foreach($comment['comment_reply'] as $comment_reply) { ?>
    <div style='margin-left:40px;border:none' class="article-reply">
        <div class="author"><b><?php echo ucwords($comment_reply['author']); ?></b></div>
        <div class="comment-date"><?php echo date('F jS, Y g:i A', strtotime($comment_reply['date_added'])); ?></div>
        <div class="comment-text text"><?php echo $comment_reply['comment']; ?></div>
    </div>
    <?php } ?>
    <?php } ?>

</div>
<?php } ?>
<div class="pagination"><?php echo $pagination; ?></div>
<?php } else { ?>
<div class="content"><?php echo $text_no_blog; ?></div>
<?php } ?>

<script type="text/javascript">
    function setArticleId(article_id) {
        //$("#blog-reply-id").val(article_id);

        var html = '';
        html += '<div class="reply-div-inner" >';
        html += '<div id="review-title-reply" ></div>';
        html += '<input type="hidden" name="blog_article_reply_id" value="' + article_id + '" id="blog-reply-id"/>';
        html += '<div class="comment-left">';
        html += '<input placeholder="<?php echo $entry_name; ?>" class="form-control" type="text" name="name" value="" />';
        html += '<br />';
        html += '<textarea placeholder="<?php echo $entry_review; ?>" class="form-control" name="text" cols="40" rows="4" style="width: 98%;"></textarea>';
        html += '<span style="font-size: 11px;"><?php echo $text_note; ?></span>';
        html += '<br /><br />';
        html += '<input placeholder="<?php echo $entry_captcha; ?>" class="form-control" type="text" name="captcha" style="" value="" /><br />';
        html += '<img src="index.php?route=product/product/captcha" alt="" id="captcha" />';
        html += '</div>';
        html += '<br />';
        html += '<div class="buttons">';
        html += '<div class="right"><button id="button-comment-reply" onclick="reply_go()" class="btn-info"><span><?php echo $button_submit; ?></span></button></div>';
        html += '</div>';
        html += '</div>';

        $(".reply-div").slideUp()
        $("#reply-me" + article_id).html(html).slideDown();
        //$("#reply-remove").css('display', 'inline');
    }

 
</script> 


