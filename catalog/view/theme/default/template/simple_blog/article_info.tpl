<?php if($this->config->get('blog_manager_custom_theme')) { ?>
<?php echo $blog_header; ?>


<div class="row">
    <div class="col-lg-2 visible-lg"></div>
    <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12">

    </div>
    <div class="col-lg-2 visible-lg"></div>
</div>


<?php echo $blog_footer; ?>	
<?php } else { ?>

<?php echo $header; ?>
<div class='col-md-8 col-xs-12'  style="padding:0px">
    <div id="content" class='row  col-md-12'>
        <?php echo $content_top; ?>
        <?php if(isset($article_info_found)) { ?>
        <div class="article-info ">
           <div class="article-title" style="margin-top:0px">
                <div style="padding:0" class=" col-sm-3 col-xs-7">
                <span class="blog-date "></span><time ><?php echo date('F jS, Y', strtotime($article_info['date_modified'])); ?></time>
                </div>
                <div style="padding:0"  class="col-sm-9 hidden-xs">
                <span class="blog-author "></span><time style="cursor: pointer;" onclick="window.location='<?php echo $article_info['author_href']; ?>'" ><?php echo $article_info['author_name']; ?></time>
                </div>
               <div class="clearfix"></div>
                <h2><?php echo $article_info['article_title']; ?></h2>
                        </div>

                        <div class="article-sub-title">
                            <span class="article-share" style="float: left;">
                                <!-- ShareThis Button BEGIN -->
                                <script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
                                <script type="text/javascript">stLight.options({publisher: "ur-d825282d-618f-598d-fca6-d67ef9e76731", doNotHash: true, doNotCopy: true, hashAddressBar: false});</script>
                                <span class='st_facebook' displayText=''></span>
                                <span class='st_twitter' displayText=''></span>
                                <span class='st_linkedin' displayText=''></span>
                                <span class='st_googleplus' displayText=''></span>
                                <span class='st_pinterest' displayText=''></span>
                                <!-- ShareThis Button END -->
                            </span>
                        </div>
                        <div class='clearfix'></div>
                        <?php if($image) { ?>
                        <?php if(isset($featured_found)) { ?>
                        <div class="article-image">
                            <img class='img-responsive' style='margin:0 auto' src="<?php echo $image; ?>" alt="<?php echo $article_info['article_title']; ?>" height="500" />
                        </div>
                        <?php } else { ?>
                        <div class="article-thumbnail-image">
                            <img class='img-responsive' style='margin:0 auto' src="<?php echo $image; ?>" alt="<?php echo $article_info['article_title']; ?>" height="100" width="100" />
                            <span class="article-description">
                                <?php echo html_entity_decode($article_info['description'], ENT_QUOTES, 'UTF-8'); ?>
                            </span>
                        </div>
                        <?php } ?>
                        <?php } ?>
                        <?php if(isset($featured_found)) { ?>
                        <div class="article-description">
                            <?php echo html_entity_decode($article_info['description'], ENT_QUOTES, 'UTF-8'); ?>
                        </div>
                        <?php } else { ?>
                        <div class="article-description">
                            <?php echo html_entity_decode($article_info['description'], ENT_QUOTES, 'UTF-8'); ?>
                        </div>
                        <?php } ?>

                        <?php if($article_additional_description) { ?>
                        <?php foreach($article_additional_description as $description) { ?>
                        <div class="article-description">
                            <?php echo html_entity_decode($description['additional_description'], ENT_QUOTES, 'UTF-8'); ?>
                        </div>
                        <?php } ?>
                        <?php } ?>
                        <br/>
                        <?php if ($products) { ?>
                        <div class="box">
                            <div class="box-heading" ><h4><?php echo $text_related_product; ?></h4></div>
                            <div class="box-content">
                                <div class="box-product">
                                    <?php foreach ($products as $product) { ?>
                                    <div class="col-sm-3 col-xs-6">
                                        <?php if ($product['thumb']) { ?>
                                        <div class="image"><a href="<?php echo $product['href']; ?>"><img class="img-responsive" style="margin:0 auto" src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" /></a></div>
                                        <?php } ?>
                                        <div class="name"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></div>

                                        <?php if ($product['rating']) { ?>
                                        <div class="img-responsive" style="margin:0 auto" class="rating"><img src="catalog/view/theme/default/image/stars-<?php echo $product['rating']; ?>.png" alt="<?php echo $product['reviews']; ?>" /></div>
                                        <?php } ?>
                                    </div>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                        <?php } ?>
                        <div class="clearfix" ></div>
                        <?php if($this->config->get('blog_related_articles')) { ?>
                        <?php if($related_articles) { ?>
                        <div class="box">
                            <div class="box-heading">Related Article</div>
                            <div class="box-content">
                                <div class="related-article">
                                    <?php $i=0; ?>
                                    <?php foreach($related_articles as $related_article) { ?>
                                    <?php if(($i%2) == 0) { ?>
                                    <div class="<?php if($column_left || $column_right) { ?> related-article-blok-11 <?php } else { ?> related-article-blok-1 <?php } ?>">
                                        <?php } else { ?>
                                        <div class="<?php if($column_left || $column_right) { ?> related-article-blok-22 <?php } else { ?> related-article-blok-2 <?php } ?>">
                                            <?php } ?>

                                            <?php $url = $this->url->link('simple_blog/article/view', 'simple_blog_article_id=' . $related_article['simple_blog_article_id'], 'SSL'); ?>

                                            <div class="name">
                                                <a href="<?php echo $url; ?>"><?php echo $related_article['article_title']; ?></a>
                                            </div>

                                            <div class="related-article-meta">
                                                <?php $author_href = $this->url->link('simple_blog/author', 'simple_blog_author_id=' . $related_article['simple_blog_author_id'], 'SSL'); ?>
                                                <?php echo $text_posted_by; ?> <a href="<?php echo $author_href; ?>"><?php echo $related_article['author_name']; ?></a> | <?php echo $text_on; ?> <?php echo $related_article['date_added']; ?> | <?php echo $text_updated; ?> <?php echo $related_article['date_modified']; ?> |
                                            </div>

                                            <div class="related-article-description">
                                                <div class="left">
                                                    <img src="<?php echo $related_article['image']; ?>" height="150" width="150" />	
                                                </div>

                                                <div class="right">
                                                    <?php if($column_left || $column_right) { ?>
                                                    <?php echo utf8_substr(strip_tags(html_entity_decode($related_article['description'], ENT_QUOTES, 'UTF-8')), 0, 100) . '...'; ?>
                                                    <?php } else { ?>
                                                    <?php echo utf8_substr(strip_tags(html_entity_decode($related_article['description'], ENT_QUOTES, 'UTF-8')), 0, 350) . '...'; ?>
                                                    <?php } ?>
                                                </div>
                                            </div>

                                            <div class="related-article-button">
                                                <a href="<?php echo $url; ?>" class="button"><?php echo $button_continue_reading; ?></a>
                                            </div>

                                            <div class="related-article-footer">
                                                <?php echo $related_article['total_comment']; ?><?php echo $text_comment_on_article; ?> <a href="<?php echo $url; ?>#comment-section"><?php echo $text_view_comment; ?></a>
                                            </div>

                                            <?php $i++; ?>
                                        </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                            <?php } ?>
                            <?php } ?>						

                            <?php if($this->config->get('blog_author_information')) { ?>
                            <?php if(isset($author_image)) { ?>
                            <div class="box">
                                <div class="box-heading"><?php echo $author_name; ?> <?php echo $text_author_information; ?></div>
                                <div class="box-content">
                                    <div class="author-info">
                                        <div class="left">
                                            <img src="<?php echo $author_image; ?>" alt="<?php echo $article_info['article_title']; ?>" style="border: 1px solid #cccccc; padding: 5px; border-radius: 5px;" />
                                        </div>
                                        <div class="right">
                                            <?php echo $author_description; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php } ?>
                            <?php } ?>

                            <?php if($article_info['allow_comment']) { ?>
                            <div class="box">
                                <div class="box-heading" style='font-size:16px'><?php echo $total_comment; ?>:</div>
                                <br/>
                                <div class="box-content">
                                    <div id="comments" class="blog-comment-info">
                                        <div id="comment-section"></div>
                                        <div id="comment-list"></div>
                                        <div id='your-comment'>
                                            <h4 id="review-title">
                                                <?php echo $text_write_comment; ?>
                                                <img src="<?php echo HTTP_SERVER; ?>catalog/view/theme/default/image/remove.png" alt="Remove" id="reply-remove" style="display:none;" onclick="removeCommentId();" />
                                            </h4>							
                                            <input type="hidden" name="blog_article_reply_id" value="0" id="blog-reply-id"/>

                                            <div class="comment-left">
                                                <input placeholder='<?php echo $entry_name; ?>' class='form-control' type="text" name="name" value="" />								
                                                <br />
                                                <textarea placeholder='<?php echo $entry_review; ?>' class='form-control' name="text" cols="40" rows="4" style="width: 98%;"></textarea>
                                                <span style="font-size: 11px;"><?php echo $text_note; ?></span>
                                                <br /><br />
                                                <input placeholder='<?php echo $entry_captcha; ?>' class='form-control' type="text" name="captcha" style="" value="" /><br />
                                                <img src="index.php?route=product/product/captcha" alt="" id="captcha" />
                                            </div>
                                            <br />
                                            <div class="buttons">
                                                <div class="right"><button id="button-comment" class="btn-info"><span><?php echo $button_submit; ?></span></button></div>					    
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php } ?>
                        </div>
                        <?php } else { ?>
                        <div class="buttons">
                            <div class="center">
                                <?php echo $text_no_found; ?>
                            </div>
                        </div>
                        <?php } ?>

                        <?php echo $content_bottom; ?>
                        </div>
                        </div>
                        <div class='col-md-4 col-xs-12'>
                            <?php echo $column_right; ?>

                        </div>

                        <script type="text/javascript">
                            function removeCommentId() {
                                $("#blog-reply-id").val(0);
                                $("#reply-remove").css('display', 'none');
                            }
                        </script>

                        <script type="text/javascript">
                            $('#comment-list .pagination a').click(function() {
                                $('#comment-list').fadeOut('slow');

                                $('#comment-list').load(this.href);

                                $('#comment-list').fadeIn('slow');

                                return false;
                            });

                            $('#comment-list').load('index.php?route=simple_blog/article/comment&simple_blog_article_id=<?php echo $simple_blog_article_id; ?>');

                        </script>		


                        <script type="text/javascript">
                            $('#button-comment').bind('click', function() {
                                $.ajax({
                                    type: 'POST',
                                    url: 'index.php?route=simple_blog/article/writeComment&simple_blog_article_id=<?php echo $simple_blog_article_id; ?>',
                                    dataType: 'json',
                                    data: 'name=' + encodeURIComponent($('input[name=\'name\']').val()) + '&text=' + encodeURIComponent($('textarea[name=\'text\']').val()) + '&captcha=' + encodeURIComponent($('input[name=\'captcha\']').val()) + '&reply_id=' + encodeURIComponent($('input[name=\'blog_article_reply_id\']').val()),
                                    beforeSend: function() {
                                        $('.success, .warning').remove();
                                        $('#button-comment').attr('disabled', true);
                                        $('#review-title').after('<div class="attention"><img src="catalog/view/theme/default/image/loading.gif" alt="" /> <?php echo $text_wait; ?></div>');
                                    },
                                    complete: function() {
                                        $('#button-comment').attr('disabled', false);
                                        $('.attention').remove();
                                    },
                                    success: function(data) {
                                        if (data['error']) {
                                            $('#review-title').after('<div class="warning">' + data['error'] + '</div>');
                                        }

                                        if (data['success']) {
                                            $('#review-title').after('<div class="success">' + data['success'] + '</div>');

                                            $('input[name=\'name\']').val('');
                                            $('textarea[name=\'text\']').val('');
                                            $('input[name=\'captcha\']').val('');
                                            $("#blog-reply-id").val(0);
                                            $("#reply-remove").css('display', 'none');

                                            $('#comment-list').load('index.php?route=simple_blog/article/comment&simple_blog_article_id=<?php echo $simple_blog_article_id; ?>');
                                        }
                                    }
                                });
                            });
                            
                               function reply_go(){
                                    $.ajax({
                                        type: 'POST',
                                        url: 'index.php?route=simple_blog/article/writeComment&simple_blog_article_id=<?php echo $simple_blog_article_id; ?>',
                                        dataType: 'json',
                                        data: 'name=' + encodeURIComponent($('input[name=\'name\']').val()) + '&text=' + encodeURIComponent($('textarea[name=\'text\']').val()) + '&captcha=' + encodeURIComponent($('input[name=\'captcha\']').val()) + '&reply_id=' + encodeURIComponent($('input[name=\'blog_article_reply_id\']').val()),
                                        beforeSend: function() {
                                            $('.success, .warning').remove();
                                            $('#review-title-reply').after('<div class="attention"><img src="catalog/view/theme/default/image/loading.gif" alt="" /> <?php echo $text_wait; ?></div>');
                                        },
                                        complete: function() {
                                            $('#button-comment-reply').attr('disabled', false);
                                            $('.attention').remove();
                                        },
                                        success: function(data) {
                                            if (data['error']) {
                                                $('#review-title-reply').after('<div class="warning">' + data['error'] + '</div>');
                                            }

                                            if (data['success']) {
                                                $('#review-title-reply').after('<div class="success">' + data['success'] + '</div>');

                                                $('input[name=\'name\']').val('');
                                                $('textarea[name=\'text\']').val('');
                                                $('input[name=\'captcha\']').val('');

                                                $('#comment-list').load('index.php?route=simple_blog/article/comment&simple_blog_article_id=<?php echo $simple_blog_article_id; ?>');
                                            }
                                        }
                                    })
                            }
                        </script> 
</div>
                        <?php echo $footer; ?>	
                        <?php } ?>