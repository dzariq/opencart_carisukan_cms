<!--
//-----------------------------------------
// Author: Qphoria@gmail.com
// Web: http://www.OpenCartGuru.com/
//-----------------------------------------
-->

<?php if ($testmode) { ?>
<div class="warning"><?php echo $text_testmode; ?></div>
<?php } ?>

<div class="buttons" style="text-align: right;min-height:20px;">
  <div class="right">
    <a id="button-confirm" class="button"><span><?php echo $button_continue; ?></span></a></td>
  </div>
</div>


<script type="text/javascript"><!--
$('#button-confirm').bind('click', function() {
	$.ajax({
		type: 'GET',
		datatype: 'html',
		url: 'index.php?route=payment/<?php echo $classname; ?>/send',
		success: function(html) {
			document.write(html);
		},
		error: function(xhr, ajaxOptions, thrownError) {
			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	});
});
//--></script>