

<?php echo $header; ?><?php echo $column_left; ?><?php echo $column_right; ?>
<div class="container" id="content"><?php echo $content_top; ?>

    <h1><?php echo $text_payment_title; ?></h1>
    <div class="content"><div style="color: <?php echo $color; ?>"><?php echo $text_payment_status; ?></div></div>
    <div class="buttons">
        <div class="right"><a href="<?php echo $continue; ?>" class="btn btn-primary"><?php echo $button_continue; ?></a></div>
    </div>
    <?php echo $content_bottom; ?>
</div>
<br/>
<br/>
<?php echo $footer; ?>