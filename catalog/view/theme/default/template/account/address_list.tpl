<?php echo $header; ?>
<?php if ($success) { ?>
<div class="alert alert-success">
    <?php echo $success; ?>
    <button type="button" class="close" data-dismiss="alert">&times;</button>
</div>
<?php } ?>
<?php if ($error_warning) { ?>
<div class="alert alert-danger">
    <?php echo $error_warning; ?>
    <button type="button" class="close" data-dismiss="alert">&times;</button>
</div>
<?php } ?>
<div class="<?php echo layout ?>">
    <div class="row">
        <div class="col-md-12">
            <ol class="breadcrumb hidden-xs">
                <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
                <?php } ?>
            </ol>
        </div>
        <div class="col-md-3">
            <?php echo $column_left ?>
        </div>
        <div id="content" class="col-md-9">
            <?php echo $content_top; ?>
            <hgroup>
                <h1><?php echo $heading_title; ?></h1>
            </hgroup>
            <?php foreach ($addresses as $result) { ?>
            <table style="width: 100%; margin-bottom: 15px;">
                <tr>
                    <td><?php echo $result['address']; ?></td>
                    <td style="text-align: right;" class="btn-group">

                        &nbsp;&nbsp;
                        <!--<a style="margin-left:20px" href="<?php echo $result['delete']; ?>" type="button" class="btn btn-info"><?php echo $button_delete; ?></a>-->
                    </td>
                </tr>
            </table>
            <div class="pull-left">
                <a href="<?php echo $result['update']; ?>" type="button" class="btn btn-info"><?php echo $button_edit; ?></a> 
                </div>
                    <?php } ?>
                    <div style="margin-left:20px" class="pull-left">
                        <a href="<?php echo $back; ?>" class="btn btn-info"><?php echo $button_back; ?></a>
                    </div>
                    <!--  <div class="pull-right">
                          <a href="<?php echo $insert; ?>" class="btn btn-info"><?php echo $button_new_address; ?></a>
                      </div> -->
                    <?php echo $content_bottom; ?>
                </div>
            </div>
        </div>
        <?php echo $footer; ?>