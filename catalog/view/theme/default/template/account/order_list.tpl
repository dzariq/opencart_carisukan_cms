<?php echo $header; ?>
<div class="row">

      <!-- Page title -->
      <div class="page-title">
         <div class="<?php echo layout ?>">
            <h2><i class="icon-desktop color"></i> <?php echo $heading_title ?></h2>
            <hr />
         </div>
      </div>
      <!-- Page title -->

      <!-- Page content -->

      <div class="account-content">
         <div class="<?php echo layout ?>">

            <div class="row">
                <div class="col-md-3">
                  <?php echo $column_left ?>
               </div>
               <div class="col-md-9">
                  <h3><i class="icon-user color"></i> <?php echo $heading_title ?></h3>
                  <table class="table table-striped tcart">
                    <thead>
                      <tr>
                        <th><?php echo $text_order_id; ?></th>
                        <th><?php echo $text_status; ?></th>
                        <th><?php echo $text_date_added; ?></th>
                        <th><?php echo $text_products; ?></th>
                        <th>Pos Laju Tracking</th>
                        <th><?php echo $text_total; ?></th>
                        <th>&nbsp;</th>
                      </tr>
                    </thead>
                    <tbody>
                    <?php if ($orders) { ?>
                        <?php $i=0; foreach ($orders as $order) { ?>
                      <tr>
                        <td>#<?php echo $order['order_id']; ?></td>
                        <td><?php echo $order['status']; ?></td>
                        <td><?php echo $order['date_added']; ?></td>
                        <td><?php echo $order['products']; ?></td>
                        <td>
                            <?php echo $order['tracking_number']; ?>
                            <?php if($order['tracking_number'] != ''){ ?>
                            <p id='<?php echo $i ?>-track'>
                                
                            </p>
                            <?php } ?>
                        </td>
                        <td><?php echo $order['total']; ?></td>
                        <td><a href="<?php echo $order['href']; ?>" style="color:white !important" class="btn btn-primary" data-toggle="tooltip" title="<?php echo $button_view; ?>">
                            	<?php echo $button_view; ?>
                            </a>
                        </td>
                      </tr>
                      <?php $i++; } } ?>
                    </tbody>
                  </table>

               </div>
            </div>

            <div class="sep-bor"></div>
         </div>
      </div>
</div>
<script>
    
     <?php if ($orders) { ?>
         <?php $i=0; foreach ($orders as $order) { ?>
            $.ajax({
                        url: 'http://poslajutracking.herokuapp.com/track/<?php echo $order[tracking_number] ?>/json',
                        type: 'GET',
                        dataType: 'jsonp',
                        error: function(xhr, status, error) {
                            console.log("error");
                        },
                        success: function(json) {
                            var result = jQuery.parseJSON(json);
                            var html = result.data[0].process
                            html += '<br/>'
                            html += result.data[0].office
                            html += '<br/>'
                            html += result.data[0].date
                            html += '<br/>'
                            html += result.data[0].time
                            $('#<?php echo $i ?>-track').html(html)
                            console.log(result);
                        }
                    }); 
            
            <?php $i++; } ?>
       <?php } ?>
    </script>
<?php echo $footer; ?>