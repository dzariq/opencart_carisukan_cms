<?php echo $header; ?>

<div class="clear"></div>
<div class="box carousel featured">
  <div class="c_header">
    <div class="grid_10 alpha">
        <h2><?php echo $heading_title; ?></h2>
    </div><!-- .grid_10 -->

    <div class="grid_2 omega">
        <a class="next arows" href="#"><span>Next</span></a>
        <a class="prev arows" href="#"><span>Prev</span></a>
    </div><!-- .grid_2 -->
  </div><!-- .c_header -->

  <div class="list_carousel negative-grid box-content">
    <ul class="list_product">
      <?php foreach ($categories as $product) { ?>
      <li>
        <div  class="grid_3 product">
          <div class="prev">
          <?php if ($product['thumb']) { ?>
              <?php if ($product['special']) { ?>
                <div class="sale-label"></div>
              <?php } ?>
              <a href="<?php echo $product['href']; ?>">
                  <img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" />
              </a>
          <?php } ?>
          </div><!-- .prev -->
          <div class="name"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></div>


          <?php if ($product['rating']) { ?>
          <div class="rating"><img src="catalog/view/theme/<?php echo $this->config->get('config_template') ?>/image/stars-<?php echo $product['rating']; ?>.png" alt="<?php echo $product['reviews']; ?>" /></div>
          <?php } ?>
        </div><!-- .grid_3.product -->
      </li>
      <?php } ?>
    </ul><!-- .list_product -->
  </div><!-- .list_carousel -->
  <div class="clear"></div>
</div><!-- .carousel -->
<div class="clear"></div>
<?php echo $footer; ?>
