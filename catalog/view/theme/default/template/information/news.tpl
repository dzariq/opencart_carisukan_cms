<?php echo $header; ?><?php echo $column_left; ?><?php echo $column_right; ?>
<?php if($news_id != 0){ ?>
<div class="<?php echo layout ?>">
    <div class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
        <?php } ?>
    </div>
    <div class="row">
        <div class="col-md-8 col-sm-8 col-xs-12">
            <div class = "thumbnail">
                <?php if($image != ''){ ?>
                <img src="<?php echo $image ?>" class="img-responsive img-rounded "  />
                <?php } ?>
                <div class="caption">
                    <h3><?php echo $heading_title ?></h3>
                    <p>
                        <?php echo $description ?>
                    </p>
                </div>
            </div>
        </div>
        <div class="col-md-4 col-sm-4 col-xs-12">
            <?php $i=1; foreach ($news_data as $story) { if($i < 4 && $story['news_id'] != $news_id){ ?>
            <div class="col-md-12">
                <a href="<?php echo $story['href'] ?>">
                    <div class = "thumbnail">
                        <?php if($story['image'] != ''){ ?>
                        <img src="<?php echo $story['image'] ?>" class="img-responsive img-rounded "  />
                        <?php } ?>
                        <div class="caption">
                            <h3><?php echo $story['title'] ?></h3>
                            <p>
                                <?php echo $story['description'] ?>
                            </p>
                            <br/>
                            <p>
                                <b>Date: </b>&nbsp;<?php echo $story['date_added']; ?>
                            </p>
                        </div>
                    </div>
                </a>
            </div>
            <?php $i++; } } ?>
            <a style='float:right;margin-top:10px' class='btn btn-danger btn-sm' href='index.php?route=information/news' >More News</a>
        </div>
    </div>
</div>
<br/>
<br/>
<?php }else{ ?>
<br/>
<br/>
<div class="<?php echo layout ?>">
    <div class='row'>
        <div class='col-md-12'>
            <h1>Blog News</h1>
        </div>
    </div>
    <div class='row'>
        <?php  foreach ($news_data as $story) {  ?>
        <div class="col-md-3 col-sm-3 col-xs-6">
            <a href="<?php echo $story['href'] ?>">
                <div class = "thumbnail">
                    <?php if($story['image'] != ''){ ?>
                    <img src="<?php echo $story['image'] ?>" class="img-responsive img-rounded "  />
                    <?php } ?>
                    <div class="caption">
                        <h3><?php echo $story['title'] ?></h3>
                        <p>
                            <?php echo $story['description'] ?>
                        </p>
                        <br/>
                        <p>
                            <b>Date: </b>&nbsp;<?php echo $story['date_added']; ?>
                        </p>
                    </div>
                </div>
            </a>
        </div>
        <?php  } ?>
    </div>
</div>
<br/>
<br/>
<?php } ?>
<?php echo $footer; ?>
