<?php echo $header; ?>
<div class="<?php echo layout ?>">

<section id="main">
    <div id="content" style="margin-top:0px" class="<?php echo strlen(trim($column_left . $column_right)) ? 'grid_9' : 'grid_12' ?>"><?php echo $content_top; ?>

        <div id="notification"></div>

        <h1  class="category_title"><?php echo $heading_title; ?></h1>

        <div style="<?php
if ($this->config->get('config_event') == 0) {
    echo 'display:none;';
}
?>margin-top:20px" class="latest">
            <style>    table.calendar    { border-left:1px solid #999;margin-top:-100px;width:100% }tr.calendar-row  {  }td.calendar-day  { vertical-align:top;min-height:80px; font-size:11px; position:relative; } * html div.calendar-day { height:80px; }td.calendar-day:hover  { background:#eceff5; }td.calendar-day-np  { background:#eee; min-height:80px; } * html div.calendar-day-np { height:80px; }td.calendar-day-head { background:#ccc; font-weight:bold; text-align:center; width:120px; padding:5px; border-bottom:1px solid #999; border-top:1px solid #999; border-right:1px solid #999; }div.day-number    { background:#999; padding:5px; color:#fff; font-weight:bold; float:right; margin:-5px -5px 0 0; width:20px; text-align:center; }/* shared */
                td.calendar-day, td.calendar-day-np {
                    width:120px; padding:5px; border-bottom:1px solid #999; border-right:1px solid #999;
                    height:100px
                }
            </style>
            <?php
            if ($calendar_month == 1) {
                $calendar_month = 'January';
            } else if ($calendar_month == 2) {
                $calendar_month = 'February';
            } else if ($calendar_month == 3) {
                $calendar_month = 'March';
            } else if ($calendar_month == 4) {
                $calendar_month = 'April';
            } else if ($calendar_month == 5) {
                $calendar_month = 'May';
            } else if ($calendar_month == 6) {
                $calendar_month = 'June';
            } else if ($calendar_month == 7) {
                $calendar_month = 'July';
            } else if ($calendar_month == 8) {
                $calendar_month = 'August';
            } else if ($calendar_month == 9) {
                $calendar_month = 'September';
            } else if ($calendar_month == 10) {
                $calendar_month = 'October';
            } else if ($calendar_month == 11) {
                $calendar_month = 'November';
            } else if ($calendar_month == 12) {
                $calendar_month = 'December';
            }
            ?>
            <h1 id="event_title" style="font-size:28px">Event Calendar</h1>
            <div class="dashboard-content"  style="text-align:center">
                <table style="margin-top:-55px;">
                    <thead>
                        <tr>&nbsp;</tr>
                    </thead>
                    <tr>
                        <td>
                            <div style='margin-left:30%'>
                                <span style="font-weight:bold;cursor:pointer;font-size:20px;" onclick="window.location=location.href+'&&date=<?php echo $calendar_prev ?>'"><img title="Previous Month" src="../image/arrow-left.png"/></span>
                                <span style="line-height: 0.2;font-size:20px;color:green"><?php echo ' ' . $calendar_month ?> <?php echo $calendar_year . ' ' ?></span>
                                <span style="font-weight:bold;cursor:pointer;font-size:20px;" onclick="window.location=location.href+'&date=<?php echo $calendar_next ?>'"><img title="Next Month" src="../image/arrow-right.png"/></span>
                            </div>
                            <br/>
                            <span style="color:red;font-size:14px;"><?php echo $text_calendar_info ?></span>
                        </td>
                    </tr>
                </table>
                <div style="clear:both"></div>

<?php echo $calendar ?>
            </div>
        </div>
        <br/>
        <br/>
        <br/>
        <br/>
        <div style="display:none">
            <h3>List of Events</h3>
<?php if ($products) { ?>
            <div class="options">
                <div class="display">
                    <a class="grid" onclick="display('grid');"><span><?php echo $text_grid; ?></span></a><div class="list"><span><?php echo $text_list; ?></span></div>
                    </div>

                    <div class="product-compare <?php echo isset($this->session->data['compare']) && count($this->session->data['compare']) > 0 ? '' : 'empty' ?>">
                        <a href="<?php echo $compare; ?>" id="compare-total"></a>
                    </div>

                    <div class="show"><span><?php echo $text_limit; ?></span>
                        <select onchange="location = this.value;">
<?php foreach ($limits as $limits) { ?>
<?php if ($limits['value'] == $limit) { ?>
                                    <option value="<?php echo $limits['href']; ?>" selected="selected"><?php echo $limits['text']; ?></option>
                        <?php } else {
                        ?>
                        <option value="<?php echo $limits['href']; ?>"><?php echo $limits['text']; ?></option>
                        <?php } ?>
<?php } ?>
                    </select>
                </div>
                <div class="sort"><span class="sort-by"><?php echo $text_sort; ?></span>
                    <select onchange="location = this.value;">
                        <?php foreach ($sorts as $sorts) { ?>
<?php if ($sorts['value'] == $sort . '-' . $order) { ?>
                                <option value="<?php echo $sorts['href']; ?>" selected="selected"><?php echo $sorts['text']; ?></option>
<?php } else { ?>
                                <option value="<?php echo $sorts['href']; ?>"><?php echo $sorts['text']; ?></option>
                        <?php } ?>
                        <?php } ?>
                    </select>
                </div>
            </div><!-- .options -->
            <div class="product-list">
                        <?php foreach ($products as $product) { ?>
                    <div>
                        <?php if ($product['thumb']) {
 ?>
                            <div class="prev">
<?php if ($product['special']) { ?>
                                    <div class="sale-label"></div>
<?php } ?>
                                <a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" title="<?php echo $product['name']; ?>" alt="<?php echo $product['name']; ?>" /></a></div>
                <?php } ?>
                                <div class="name"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a>
                    <?php if ($product['price']) {
 ?>
                                    <div class="price">
                                        <div class="vert">
<?php if (!$product['special']) {
?>
<?php echo $product['price']; ?>
                    <?php } else {
 ?>

                                                <div class="price_new"><?php echo $product['special']; ?>&nbsp;&nbsp;<span class="price_old"><?php echo $product['price']; ?></span></div>

                                <?php } ?>
                                <?php if ($product['tax']) {
 ?>
                                    <div class="price-tax"><span><?php echo $text_tax; ?></span> <?php echo $product['tax']; ?></div>
<?php } ?>
                            </div>
                        </div>
                                <?php } ?>
                    </div>
                    <div class="description"><?php echo $product['description']; ?></div>
                                <?php if ($product['price']) {
 ?>
                        <div class="price">
                            <div class="vert">
<?php if (!$product['special']) { ?>
<?php echo $product['price']; ?>
                    <?php } else { ?>

                                                <div class="price_new"><?php echo $product['special']; ?></div>
                                                <div class="price_old"><?php echo $product['price']; ?></div>

                            <?php } ?>
                            <?php if ($product['tax']) {
                            ?>
                                        <div class="price-tax"><span><?php echo $text_tax; ?></span> <?php echo $product['tax']; ?></div>
<?php } ?>
                                </div>
                            </div>
                            <?php } ?>
                            <?php if ($product['rating']) {
                            ?>
                            <div class="rating"><img src="catalog/view/theme/<?php echo $this->config->get('config_template') ?>/image/stars-<?php echo $product['rating']; ?>.png" alt="<?php echo $product['reviews']; ?>" /></div>
                            <?php } ?>

                    </div>

                    <?php } ?>
                        <div class="clear"></div>
                    </div>
                    <div class="pagination">
<?php
                            echo strtr($pagination, array(
                                '|&lt;' => '<span class="first">|&lt;</span>',
                                '&gt;|' => '<span class="last">&gt;|</span>',
                                '&lt;' => '<span class="prev">←</span>',
                                '&gt;' => '<span class="next">→</span>'));
?>
                        </div>
                <?php } ?>
                <?php if (!$categories && !$products) {
 ?>
                        <div class="content"><?php echo $text_empty; ?></div>
                        <div class="buttons">
                            <div class="right"><a href="<?php echo $continue; ?>" class="button"><?php echo $button_continue; ?></a></div>
                        </div>
            <?php } ?>
                    </div>

<?php echo $content_bottom; ?></div>

<?php echo $column_left; ?>
            <?php echo $column_right; ?>

                <script type="text/javascript"><!--
                    function display(view) {
                        if (view == 'list') {
                            $('.product-grid').attr('class', 'product-list');

                            $('.product-list > div').each(function(index, element) {

                                html = '<div class="product_li">';

                                html += '<div class="grid_3">';

                                var image = $(element).find('.prev').html();

                                if (image != null) {
                                    html += '<div class="prev">' + image + '</div>';
                                } else {
                                    html += '<div class="prev"></div>';
                                }

                                html += '</div>';

                                html += '<div class="grid_4">';
                                html += '<div class="entry_content">';

                                html += '  <div class="name">' + $(element).find('.name').html() + '</div>';

                                var rating = $(element).find('.rating').html();

                                if (rating != null) {
                                    html += '<div class="rating">' + rating + '</div>';
                                }

                                html += '  <div class="description">' + $(element).find('.description').html() + '</div>';

                                html += '</div>';//<!-- .entry_content -->
                                html += '</div>';//<!-- .grid_4 -->

                                html += '<div class="grid_2">';
                                html += '<div  style="display:none" class="cart">';
                                var price = $(element).find('.price').html();

                                if (price != null) {
                                    html += '<div class="price">' + price  + '</div>';
                                }
                                html += '  <div class="bay">' + $(element).find('.bay').html() + '</div>';

                                html += '  <div class="compare">' + $(element).find('.compare').html() + '</div>';
                                html += '  <div class="wishlist">' + $(element).find('.wishlist').html() + '</div>';

                                html += '</div>';//<!-- .cart -->
                                html += '</div>';//<!-- .grid_2 -->

                                html += '</div>';//<!-- .product_li -->

                                $(element).html(html);
                            });

                            $('.display').html('<a class="grid" onclick="display(\'grid\');"><span><?php echo $text_grid; ?></span></a><div class="list"><span><?php echo $text_list; ?></span></div>');

                                    $.totalStorage('display', 'list');
                                } else {
                                    $('.product-list').attr('class', 'product-grid');

                                    $('.product-grid > div').each(function(index, element) {
                                        html = '<div class="grid_3 product">';

                                        var image = $(element).find('.prev').html();

                                        if (image != null) {
                                            html += '<div class="prev">' + image + '</div>';
                                        } else {
                                            html += '<div class="prev"></div>';
                                        }

                                        html += '<div class="name">' + $(element).find('.name').html() + '</div>';
                                        html += '<div class="description">' + $(element).find('.description').html() + '</div>';

                                        html += '<div style="display:none" class="cart">';

                                        var price = $(element).find('.price').html();

                                        if (price != null) {
                                            html += '<div class="price">' + price  + '</div>';
                                        }

                                        var rating = $(element).find('.rating').html();

                                        if (rating != null) {
                                            html += '<div class="rating">' + rating + '</div>';
                                        }

                                        html += '<div class="bay">' + $(element).find('.bay').html() + '</div>';
                                        html += '<div class="wishlist">' + $(element).find('.wishlist').html() + '</div>';
                                        html += '<div class="compare">' + $(element).find('.compare').html() + '</div>';

                                        html += '</div>';//<!-- .cart -->

                                        html += '</div>';

                                        $(element).html(html);
                                    });

                                    $('.display').html('<div class="grid"><span><?php echo $text_grid; ?></span></div><a class="list" onclick="display(\'list\');"><span><?php echo $text_list; ?></span></a>');

                                    $.totalStorage('display', 'grid');
                                }
                            }

                            //    view = $.totalStorage('display');

                            //    if (view) {
                            display('grid');
                            //    } else {
                            //            display('list');
                            //    }
                            //--></script>

                        <script type="text/javascript"><!--

                            $(document).ready(function(){
//                                $('#main').css('background','transparent')
                            });

                            function getFormattedDate() {
                                var date = new Date();
                                var str = date.getFullYear() + "-" + getFormattedPartTime(date.getMonth()) + "-" + getFormattedPartTime(date.getDate()) + " " +  getFormattedPartTime(date.getHours()) + ":" + getFormattedPartTime(date.getMinutes()) + ":" + getFormattedPartTime(date.getSeconds());

                                return str;
                            }

                            function getFormattedPartTime(partTime){
                                if (partTime<10)
                                    return "0"+partTime;
                                return partTime;
                            }


                            function join_event(){

                                if(parseInt($('#participants').html()) >= parseInt($('#event_quantity').html())){
                                    $( "#warning_popup" ).dialog({ width: 300 });
                                    $( "#warning_popup" ).dialog({ height: 140 });
                                    $( "#warning_popup" ).dialog( "open" );
                                    return 0;
                                }
                                addToCart($('#product_id').val())
                            }

                            function open_dialog(product_id){
                                var data = {
                                    'product_id' : product_id,
                                }

                                $.ajax({
                                    type: 'post',
                                    url: 'index.php?route=product/product/getEvent',
                                    dataType: 'json',
                                    data: data,
                                    success: function(json) {
                                        $('#product_id').val(json['product_id'])
                                        $('#event_name').html(json['name'])
                                        $('#event_start').html(json['start'])
                                        $('#event_end').html(json['end'])
                                        $('#event_start_time').html(json['jan'])
                                        $('#event_end_time').html(json['isbn'])
                                        $('#event_location').html(json['mpn'])
                                        $('#event_trainer').html(json['sku'])
                                        $('#event_price').html(json['price'])
                                        $('#event_quantity').html(json['maximum'])
                                        $('#event_description').html(json['description'])
                                        $('#participants').html(json['participant'])

                                                                   $("#create_event").dialog({
    width: 'auto', // overcomes width:'auto' and maxWidth bug
    maxWidth: 600,
    height: 'auto',
    modal: true,
    fluid: true, //new option
    resizable: false
});


                                    }
                                });


                            }

                            function close_dialog(id){
                                $( "#"+id ).dialog( "close");

                            }


                            function addToCart(product_id, quantity) {
                                quantity = typeof(quantity) != 'undefined' ? quantity : 1;

                                $.ajax({
                                    url: 'index.php?route=checkout/cart/clearCart',
                                    type: 'post',
                                    dataType: 'json',
                                    success: function(json) {
                                        if (json['success']) {
                                            $.ajax({
                                                url: 'index.php?route=checkout/cart/add',
                                                type: 'post',
                                                data: 'product_id=' + product_id + '&quantity=' + quantity,
                                                dataType: 'json',
                                                success: function(json) {

                                                    if (json['redirect']) {
                                                        location = json['redirect'];
                                                    }

                                                    if (json['success']) {
                                                        window.location="index.php?route=checkout/cart"
                                                    }
                                                }
                                            });
                                        }
                                    }
                                });


                            }

                            //--></script>
                        <style>
                            .dialog_table{
                                width:100%
                            }
                            .dialog_table td{
                                padding:5px;
                                background: white;
                                border:1px solid #e0e0e0;
                            }
                            #bar_cal:hover{
                                background:  #e0e0e0;
                                color:black
                            }
                            #bar_cal{
                                font-size:13px;max-width:180px;float:left;text-align: left;margin-top:2px;color: white;cursor: pointer;padding: 4px;
                             background: #3f4c6b; /* Old browsers */
background: -moz-linear-gradient(top,  #3f4c6b 0%, #3f4c6b 100%); /* FF3.6+ */
background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#3f4c6b), color-stop(100%,#3f4c6b)); /* Chrome,Safari4+ */
background: -webkit-linear-gradient(top,  #3f4c6b 0%,#3f4c6b 100%); /* Chrome10+,Safari5.1+ */
background: -o-linear-gradient(top,  #3f4c6b 0%,#3f4c6b 100%); /* Opera 11.10+ */
background: -ms-linear-gradient(top,  #3f4c6b 0%,#3f4c6b 100%); /* IE10+ */
background: linear-gradient(to bottom,  #3f4c6b 0%,#3f4c6b 100%); /* W3C */
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#3f4c6b', endColorstr='#3f4c6b',GradientType=0 ); /* IE6-9 */

                            }
                            #create_event{
                                background:white;
                                color:#333;
                            }

                            .ui-widget-header{
                                background:#333;
                                color:white
                            }
                            .my_table td{
                                text-align: left;
                                padding-left: 20px;
                                font-size:16px;
                                border:1px solid #eaeaea

                            }
                        </style>

                        <div style="background:white;text-align:center;display:none" id="warning_popup"  title="Message">
                            Maaf. Seminar Telah Penuh. Sila Pilih yang lain.
                            <br/>
                            <input style="margin-top:20px;background-color:#f7b64a;border:0px;font-size:14px;cursor:pointer;color:white;width:80px;height:30px;" type="button" onclick="close_dialog('warning_popup')" value="Close" />
                        </div>
                        <div style="display:none" id="create_event"  title="Event Details">
                            <div id="error_event"></div>
                            <input id="product_id" type="hidden" value="" />
                            <table class="my_table" style="width:100%">
                                <tr>
                                    <td style="font-weight:bold">Event Name</td>
                                    <td><span  id="event_name"></span></td>
                                </tr>
                                <tr>
                                    <td style="font-weight:bold">Date</td>
                                    <td><span id="event_start"></span> <b>to</b> <span  id="event_end" ></span></td>
                                </tr>
                                <tr>
                                    <td style="font-weight:bold">Time</td>
                                    <td><span  id="event_start_time" readonly="readonly"   type="text" ></span> <b>to</b> <span id="event_end_time" ></span></td>
                                </tr>
                                <tr>
                                    <td style="font-weight:bold">Location</td>
                                    <td><span id="event_location" ></span> </td>
                                </tr>
                                <tr>
                                    <td style="font-weight:bold">Trainer Name</td>
                                    <td><span id="event_trainer"  > </span></td>
                                </tr>
                                <tr>
                                    <td style="font-weight:bold">Price</td>
                                    <td><span  id="event_price" ></span></td>
                                </tr>
                                <tr  style="display:none">
                                    <td style="font-weight:bold">Maximum Seat</td>
                                    <td><span  id="event_quantity"  ></span></td>
                                </tr>
                                <tr>
                                    <td style="font-weight:bold">Description</td>
                                    <td><span  id="event_description"></span></td>
                                </tr>
                                <tr style="display:none">
                                    <td style="font-weight:bold">Participants</td>
                                    <td><span id="participants" ></span></td>
                                </tr>

                            </table>
                            <input style="float:right;background-color:#f7b64a;border:0px;font-size:14px;cursor:pointer;color:white;width:80px;height:30px;" type="button" onclick="close_dialog('create_event')" value="<?php echo $text_close ?>" />
                            <input style="float:left;background-color:#f7b64a;border:0px;font-size:14px;cursor:pointer;margin-right:10px;color:white;width:150px;height:30px;" type="button" onclick="join_event()" value="<?php echo $text_join_event ?>" />
                        </div>

                        <script>
                            $('.date').datepicker({dateFormat: 'yy-mm-dd'});
                        </script>
                        <div class="clear"></div>
                    </section><!-- #main -->
                    <script type="text/javascript">
                        function DoubleScroll(element) {
                            var scrollbar= document.createElement('div');
                            scrollbar.appendChild(document.createElement('div'));
                            scrollbar.style.overflow= 'auto';
                            scrollbar.style.overflowY= 'hidden';
                            scrollbar.firstChild.style.width= element.scrollWidth+'px';
                            scrollbar.firstChild.style.paddingTop= '1px';
                            scrollbar.firstChild.appendChild(document.createTextNode('\xA0'));
                            scrollbar.onscroll= function() {
                                element.scrollLeft= scrollbar.scrollLeft;
                            };
                            element.onscroll= function() {
                                scrollbar.scrollLeft= element.scrollLeft;
                            };
                            element.parentNode.insertBefore(scrollbar, element);
                        }

                        DoubleScroll(document.getElementById('doublescroll'));
                    </script>
</div>
<?php echo $footer; ?>