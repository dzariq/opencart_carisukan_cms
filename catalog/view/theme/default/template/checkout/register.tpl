<div class="col-xs-12 col-sm-6 col-md-6">
    <h2><?php echo $text_your_details; ?></h2>
    <div class="form-group">
        <label for="firstname" class="required"><?php echo $entry_firstname; ?></label>
        <input class="form-control" type="text" id="firstname" name="firstname" value="" />
    </div>
    <div class="form-group">
        <label for="lastname" class="required"><?php echo $entry_lastname; ?></label>
        <input class="form-control" type="text" id="lastname" name="lastname" value="" />
    </div>
    <div class="form-group">
        <label for="email" class="required"><?php echo $entry_email; ?></label>
        <input class="form-control" type="email" id="email" name="email" value="" />
    </div>
    <div class="form-group">
        <label for="telephone" class="required"><?php echo $entry_telephone; ?></label>
        <input class="form-control" type="tel" id="telephone" name="telephone" value="" />
    </div>
    <h2><?php echo $text_your_password; ?></h2>
    <div class="form-group">
        <label for="password" class="required"><?php echo $entry_password; ?></label>
        <input class="form-control" type="password" id="password" name="password" value="" />
    </div>
    <div class="form-group">
        <label for="confirm" class="required"><?php echo $entry_confirm; ?></label>
        <input class="form-control" type="password" id="confirm" name="confirm" value="" />
    </div>
</div>
<div class="col-xs-12 col-sm-6 col-md-6">
    <h2><?php echo $text_your_address; ?></h2>
    <div style="display: <?php echo (count($customer_groups) > 1 ? 'table-row' : 'none'); ?>;">
        <div class="form-group">
            <label for="customer_group_id"><?php echo $entry_customer_group; ?></label>
            <?php foreach ($customer_groups as $customer_group) { ?>
                <?php if ($customer_group['customer_group_id'] == $customer_group_id) { ?>
                    <div class="radio">
                        <label>
                            <input type="radio" id="customer_group_id" name="customer_group_id" value="<?php echo $customer_group['customer_group_id']; ?>" id="customer_group_id<?php echo $customer_group['customer_group_id']; ?>" checked="checked" />
                            <?php echo $customer_group['name']; ?>
                        </label>
                    </div>
                <?php } else { ?>
                    <div class="radio">
                        <label for="customer_group_id<?php echo $customer_group['customer_group_id']; ?>">
                            <input type="radio" id="customer_group_id" name="customer_group_id" value="<?php echo $customer_group['customer_group_id']; ?>" id="customer_group_id<?php echo $customer_group['customer_group_id']; ?>" />
                            <?php echo $customer_group['name']; ?>
                        </label>
                    </div>
                <?php } ?>
            <?php } ?>
        </div>
    </div>
    <div class="form-group">
        <label for="address_1" class="required">Address</label>
         <textarea class="form-control" id="address_1b" name="address_1">
<?php echo $address_1; ?>
        </textarea>
        <input style='visibility: hidden' type="text" class="form-control" id="address_1" name="address_1" value="<?php echo $address_1 ?>" />

    </div>
   
    <div class="form-group">
        <label for="city" class="required"><?php echo $entry_city; ?></label>
        <input type="text" class="form-control" id="city" name="city" value="" />
    </div>
    <div class="form-group">
        <span id="shipping-postcode-required" class="required"></span>
        <label for="postcode"><?php echo $entry_postcode; ?></label>
        <input type="text" class="form-control" id="postcode" name="postcode" value="<?php echo $postcode; ?>" />
    </div>
    <div class="form-group">
        <label for="country_id" class="required"><?php echo $entry_country; ?></label>
        <select class="form-control" id="country_id" name="country_id">
            <option value=""><?php echo $text_select; ?></option>
            <?php foreach ($countries as $country) { ?>
                <?php if ($country['country_id'] == $country_id) { ?>
                    <option value="<?php echo $country['country_id']; ?>" selected="selected"><?php echo $country['name']; ?></option>
                <?php } else { ?>
                    <option value="<?php echo $country['country_id']; ?>"><?php echo $country['name']; ?></option>
            <?php } } ?>
        </select>
    </div>
    <div class="form-group">
        <label for="zone_id" class="required"><?php echo $entry_zone; ?></label>
        <select class="form-control" id="zone_id" name="zone_id"></select>
    </div>
</div>
<div class="pull-left">
	<div class="checkbox">
        <label>
        	<input type="checkbox" name="newsletter" value="1" id="newsletter" />
            <?php echo $entry_newsletter; ?>
        </label>
    </div>
    <?php if ($shipping_required) { ?>
        <div class="checkbox">
            <label>
                <input type="checkbox" name="shipping_address" value="1" id="shipping" checked="checked" />
                <?php echo $entry_shipping; ?>
            </label>
        </div>
    <?php } ?>
</div>
<div class="pull-right">
	<?php if ($text_agree) { 
        echo $text_agree; ?>
        <input style="margin: 4px 15px 0 10px;" type="checkbox" name="agree" value="1" /> 
        <input type="button" value="<?php echo $button_continue; ?>" id="button-register" class="btn btn-info" />
    <?php } else { ?>
        <input type="button" value="<?php echo $button_continue; ?>" id="button-register" class="btn btn-info" />
    <?php } ?>
</div>
<script type="text/javascript"><!--
    
    $('#address_1b').on('keyup', function() {
    $('#address_1').val(this.value);
});
    
    
$(document).on('change', '#payment-address input[name=\'customer_group_id\']:checked', function() {
	var customer_group = [];
	
<?php foreach ($customer_groups as $customer_group) { ?>
	customer_group[<?php echo $customer_group['customer_group_id']; ?>] = [];
	customer_group[<?php echo $customer_group['customer_group_id']; ?>]['company_id_display'] = '<?php echo $customer_group['company_id_display']; ?>';
	customer_group[<?php echo $customer_group['customer_group_id']; ?>]['company_id_required'] = '<?php echo $customer_group['company_id_required']; ?>';
	customer_group[<?php echo $customer_group['customer_group_id']; ?>]['tax_id_display'] = '<?php echo $customer_group['tax_id_display']; ?>';
	customer_group[<?php echo $customer_group['customer_group_id']; ?>]['tax_id_required'] = '<?php echo $customer_group['tax_id_required']; ?>';
<?php } ?>	

	if (customer_group[this.value]) {
		if (customer_group[this.value]['company_id_display'] == '1') {
			$('#company-id-display').show();
		} else {
			$('#company-id-display').hide();
		}
		
		if (customer_group[this.value]['company_id_required'] == '1') {
			$('#company-id-required').show();
		} else {
			$('#company-id-required').hide();
		}
		
		if (customer_group[this.value]['tax_id_display'] == '1') {
			$('#tax-id-display').show();
		} else {
			$('#tax-id-display').hide();
		}
		
		if (customer_group[this.value]['tax_id_required'] == '1') {
			$('#tax-id-required').show();
		} else {
			$('#tax-id-required').hide();
		}	
	}
});

$('#payment-address input[name=\'customer_group_id\']:checked').trigger('change');
//--></script> 
<script type="text/javascript"><!--
$('#payment-address select[name=\'country_id\']').bind('change', function() {
	if (this.value == '') return;
	$.ajax({
		url: 'index.php?route=checkout/checkout/country&country_id=' + this.value,
		dataType: 'json',
		beforeSend: function() {
			$('#payment-address select[name=\'country_id\']').after('<span class="wait">&nbsp;<img src="catalog/view/theme/default/image/loading.gif" alt="" /></span>');
		},
		complete: function() {
			$('.wait').remove();
		},			
		success: function(json) {
			if (json['postcode_required'] == '1') {
				$('#payment-postcode-required').show();
			} else {
				$('#payment-postcode-required').hide();
			}
			
			html = '<option value=""><?php echo $text_select; ?></option>';
			
			if (json['zone'] != '') {

				for (i = 0; i < json['zone'].length; i++) {
        			html += '<option value="' + json['zone'][i]['zone_id'] + '"';
	    			
					if (json['zone'][i]['zone_id'] == '<?php echo $zone_id; ?>') {
	      				html += ' selected="selected"';
	    			}
	
	    			html += '>' + json['zone'][i]['name'] + '</option>';
				}
			} else {
				html += '<option value="0" selected="selected"><?php echo $text_none; ?></option>';
			}
			
			$('#payment-address select[name=\'zone_id\']').html(html);
		},
		error: function(xhr, ajaxOptions, thrownError) {
			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	});
});
$('#payment-address select[name=\'country_id\']').trigger('change');
//--></script> 
<script type="text/javascript"><!--
$(document).ready(function() {
	$('.colorbox').colorbox({
		overlayClose: true,
		opacity: 0.5,
                width:"80%", height:"80%",
		rel: "colorbox"
	});
});
//--></script> 