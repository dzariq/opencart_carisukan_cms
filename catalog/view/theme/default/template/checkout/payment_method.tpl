<?php if ($error_warning) { ?>
    <div class="alert alert-danger">
    	<?php echo $error_warning; ?>
    	<button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
<?php } ?>
<form role="form">
    <?php if ($payment_methods) { ?>
        <p><?php echo $text_payment_method; ?></p>
            <table class="table table-hover">
                <?php foreach ($payment_methods as $payment_method) { ?>
                    <tr>
                        <div class="form-group">
                            <div class="radio">
                                <label for="<?php echo $payment_method['code']; ?>">
                                    <?php if ($payment_method['code'] == $code || !$code) { ?>
                                        <?php $code = $payment_method['code']; ?>
                                        <input type="radio" name="payment_method" value="<?php echo $payment_method['code']; ?>" id="<?php echo $payment_method['code']; ?>" checked="checked" />
                                    <?php } else { ?>
                                        <input type="radio" name="payment_method" value="<?php echo $payment_method['code']; ?>" id="<?php echo $payment_method['code']; ?>" />
                                    <?php } 
                                    echo $payment_method['title']; ?>
                                </label>
                            </div>
                        </div>
                    </tr>
                <?php } ?>
            </table>
    <?php } ?>
    <div class="form-group">
        <label for="comment"><?php echo $text_comments; ?></label>
        <textarea class="form-control" id="comment" name="comment" rows="8"><?php echo $comment; ?></textarea>
    </div>
    <div class="pull-right">
        <?php if ($text_agree) { ?>
            <?php echo $text_agree; ?>
            <?php if ($agree) { ?>
                <input style="margin: 4px 15px 0 10px;" type="checkbox" name="agree" value="1" checked="checked" /> 
            <?php } else { ?>
                <input style="margin: 4px 15px 0 10px;" type="checkbox" name="agree" value="1" /> 
            <?php } ?>
            <input type="button" value="<?php echo $button_continue; ?>" id="button-payment-method" class="btn btn-info" />
        <?php } else { ?>
            <input type="button" value="<?php echo $button_continue; ?>" id="button-payment-method" class="btn btn-info" />
        <?php } ?>
    </div>
</form>
<script type="text/javascript"><!--
$(document).ready(function() {
	$('.colorbox').colorbox({
		overlayClose: true,
		opacity: 0.5,
                width:"80%", height:"80%",
		rel: "colorbox"
	});
});
//--></script> 