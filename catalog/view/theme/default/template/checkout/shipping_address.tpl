<?php if ($addresses) { ?>
    <form role="form">
        <div class="form-group">
            <div class="radio">
                <label for="shipping-address-existing">
                    <input type="radio" name="shipping_address" value="existing" id="shipping-address-existing" checked="checked" />
                    <?php echo $text_address_existing; ?>
                </label>
            </div>
        </div>
        <div id="shipping-existing" class="form-group">
            <select class="form-control" name="address_id">
                <?php foreach ($addresses as $address) { ?>
                    <?php if ($address['address_id'] == $address_id) { ?>
                        <option value="<?php echo $address['address_id']; ?>" selected="selected"><?php echo $address['firstname']; ?> <?php echo $address['lastname']; ?>, <?php echo $address['address_1']; ?>, <?php echo $address['city']; ?>, <?php echo $address['zone']; ?>, <?php echo $address['country']; ?></option>
                    <?php } else { ?>
                        <option value="<?php echo $address['address_id']; ?>"><?php echo $address['firstname']; ?> <?php echo $address['lastname']; ?>, <?php echo $address['address_1']; ?>, <?php echo $address['city']; ?>, <?php echo $address['zone']; ?>, <?php echo $address['country']; ?></option>
                <?php } } ?>
            </select>
        </div>
        <div class="form-group">
            <div class="radio">
                <label for="shipping-address-new">
                    <input type="radio" name="shipping_address" value="new" id="shipping-address-new" />
                    <?php echo $text_address_new; ?>
                </label>
            </div>
        </div>
    </form>
<?php } ?>
<div id="shipping-new" style="display: <?php echo ($addresses ? 'none' : 'block'); ?>;">
    <form role="form">
        <div class="form-group">
            <label for="firstname" class="required"><?php echo $entry_firstname; ?></label>
            <input class="form-control" type="text" id="firstname" name="firstname" value="" />
        </div>
        <div class="form-group">
            <label for="lastname" class="required"><?php echo $entry_lastname; ?></label>
            <input class="form-control" type="text" id="lastname" name="lastname" value="" />
        </div>
        <div class="form-group">
            <label for="company"><?php echo $entry_company; ?></label>
            <input class="form-control" type="text" id="company" name="company" value="" />
        </div>
        <div class="form-group">
            <label for="address_1" class="required"><?php echo $entry_address_1; ?></label>
            <input class="form-control" type="text" id="address_1" name="address_1" value="" />
        </div>
        <div class="form-group">
            <label for="address_2" class="required"><?php echo $entry_address_2; ?></label>
            <input class="form-control" type="text" id="address_2" name="address_2" value="" />
        </div>
        <div class="form-group">
            <label for="city" class="required"><?php echo $entry_city; ?></label>
            <input class="form-control" type="text" id="city" name="city" value="" />
        </div>
        <div class="form-group">
            <span id="shipping-postcode-required" class="required"></span>
            <label for="postcode"><?php echo $entry_postcode; ?></label>
            <input type="text" class="form-control" id="postcode" name="postcode" value="<?php echo $postcode; ?>" />
        </div>
        <div class="form-group">
            <label for="country_id" class="required"><?php echo $entry_country; ?></label>
            <select class="form-control" id="country_id" name="country_id">
                <option value=""><?php echo $text_select; ?></option>
                <?php foreach ($countries as $country) { ?>
                    <?php if ($country['country_id'] == $country_id) { ?>
                        <option value="<?php echo $country['country_id']; ?>" selected="selected"><?php echo $country['name']; ?></option>
                    <?php } else { ?>
                        <option value="<?php echo $country['country_id']; ?>"><?php echo $country['name']; ?></option>
                <?php } } ?>
            </select>
        </div>
        <div class="form-group">
            <label for="zone_id" class="required"><?php echo $entry_zone; ?></label>
            <select class="form-control" id="zone_id" name="zone_id"></select>
        </div>
    </form>
</div>
<div class="pull-right">
    <input type="button" value="<?php echo $button_continue; ?>" id="button-shipping-address" class="btn btn-info" />
</div>
<script type="text/javascript"><!--
$(document).on('change','#shipping-address input[name=\'shipping_address\']', function() {
	if (this.value == 'new') {
		$('#shipping-existing').hide();
		$('#shipping-new').show();
	} else {
		$('#shipping-existing').show();
		$('#shipping-new').hide();
	}
});
//--></script> 
<script type="text/javascript"><!--
$('#shipping-address select[name=\'country_id\']').bind('change', function() {
	if (this.value == '') return;
	$.ajax({
		url: 'index.php?route=checkout/checkout/country&country_id=' + this.value,
		dataType: 'json',
		beforeSend: function() {
			$('#shipping-address select[name=\'country_id\']').after('<span class="wait">&nbsp;<img src="catalog/view/theme/default/image/loading.gif" alt="" /></span>');
		},
		complete: function() {
			$('.wait').remove();
		},			
		success: function(json) {
			if (json['postcode_required'] == '1') {
				$('#shipping-postcode-required').show();
			} else {
				$('#shipping-postcode-required').hide();
			}
			
			html = '<option value=""><?php echo $text_select; ?></option>';
			
			if (json['zone'] != '') {
				for (i = 0; i < json['zone'].length; i++) {
        			html += '<option value="' + json['zone'][i]['zone_id'] + '"';
	    			
					if (json['zone'][i]['zone_id'] == '<?php echo $zone_id; ?>') {
	      				html += ' selected="selected"';
	    			}
	
	    			html += '>' + json['zone'][i]['name'] + '</option>';
				}
			} else {
				html += '<option value="0" selected="selected"><?php echo $text_none; ?></option>';
			}
			
			$('#shipping-address select[name=\'zone_id\']').html(html);
		},
		error: function(xhr, ajaxOptions, thrownError) {
			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	});
});

$('#shipping-address select[name=\'country_id\']').trigger('change');
//--></script>