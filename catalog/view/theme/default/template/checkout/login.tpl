        <?php if(!$no_register){ ?>
<div class="col-xs-12 col-sm-6 col-md-6">
	<section>
        <h2><?php echo $text_new_customer; ?></h2>
        <p><?php echo $text_checkout; ?></p>
    </section>
    <form role="form>">
    	<div class="radio">
            <label>
                <?php if ($account == 'register') { ?>
                    <input type="radio" id="account" name="account" value="register" id="register" checked="checked" />
                <?php } else { ?>
                    <input type="radio" id="account" name="account" value="register" id="register" />
                <?php } ?>
                <?php echo $text_register; ?>
            </label>
        </div>
        <?php if ($guest_checkout) { ?>
        	<div class="radio">
                <label>
                    <?php if ($account == 'guest') { ?>
                        <input type="radio" id="account" name="account" value="guest" id="guest" checked="checked" />
                    <?php } else { ?>
                        <input type="radio" id="account" name="account" value="guest" id="guest" />
                    <?php } ?>
                    <?php echo $text_guest; ?>
                </label>
            </div>
        <?php } ?>
        <p><?php echo $text_register_account; ?></p>
        <input type="button" value="<?php echo $button_continue; ?>" id="button-account" class="btn btn-info" />
    </form>
</div>
                <?php } ?>

<div id="login" class="col-xs-12 col-sm-6 col-md-6">
	<section>
        <h2><?php echo $text_returning_customer; ?></h2>
        <p><?php echo $text_i_am_returning_customer; ?></p>
    </section>
    <form role="form">
        <div class="form-group">
            <label for="email"><?php echo $entry_email; ?></label>
            <input class="form-control" type="email" id="email" name="email" placeholder="Enter email" />
        </div>
        <div class="form-group">
            <label for="password"><?php echo $entry_password; ?></label>
            <input class="form-control" type="password" id="password" name="password" placeholder="Password" />
        </div>
        <a href="<?php echo $forgotten; ?>"><?php echo $text_forgotten; ?></a>
        <input type="button" value="<?php echo $button_login; ?>" id="button-login" class="btn btn-info pull-right" />
    </form>
</div>