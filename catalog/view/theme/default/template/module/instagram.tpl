<div class="box">
  <div class="box-heading"><?php echo $heading_title; ?></div>
  <div class="box-content">
      <div id="instagram">
      <?php 
      foreach($images as $k=>$data){
        $nimages=$data['images'];
        echo '<a href="'.$nimages['standard_resolution']['url'].'"  class="instagram"><img rel="instagram" src="'.$nimages['thumbnail']['url'].'" /></a>';
      }
      ?>
      </div>
      <a href="#" id="prev"><?php echo $prev;?></a> |<a href="#" id="next"><?php echo $next;?></a>
      
<script type="text/javascript"><!--
$(document).ready(function() {
	$('.instagram').colorbox({
		overlayClose: true,
		opacity: 0.5,
		rel: "instagram"
	});
            $('#instagram').cycle({ 
    prev:   '#prev', 
    next:   '#next', 
    timeout: 0 
});
});
//--></script> 
  </div>
</div>
