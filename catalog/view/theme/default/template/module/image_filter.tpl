<!-- Portfolio --> 
<style>
    @media (max-width:1199px) {
        .works p{
            font-size:14px !important;
        }

        .work:hover .work-img .zoom {
            left: 64.65% !important;
            top: 69% !important;
        }

        .overlay-div{
            font-size:14px !important;
        }

        .zoom{
            display:none !important
        }

        .work-head{
            font-size:26px !important

        }

        .filters a{
            font-size:13px !important
        }

        .filters li a{
            padding: 7px 20px !Important;
        }
    }

    @media (max-width:560px){
        .work-head{
            font-size:18px !important

        }

        .works p{
            font-size:12px !important;
        }

        .filters a{
         //   font-size:18px !important
        }
    }

    .filter{
        cursor:pointer
    }

    .filters a{
        font-size:15px 
    }


    #filters li{
        float:left;
        margin-left:25px;
    }
    #filters li a{
        color:#bcbcbc ;
        font-weight: 600;
        font-size:13px
    }
    
    
    .filters .active{
        color: #666 !important
    }

    .works{
        margin-bottom:100px;
    }
</style>
<div class="row" style="margin-bottom:20px">
    <h3 class="text-left" ><?php echo $title ?></h3>
<p ><?php echo $description ?></p>
<section  id="portfolio" class="main-content">
    <!-- Content -->
    <div class="content">

        <!-- Works -->
        <div class="works col-xs-12">
            <!-- Filter Navigation -->
            <div id="menu-filter-wrap" style="border:none;border-bottom:1px solid #666" class="filter-menu inline animated" data-animation="fadeInUp" data-animation-delay="500">
                <div id="menu-item-icon">
                    <ul id="filters" class="menu-filters filters option-set" data-option-key="filter">
                        <li><a onclick="filter_images(this, 1, 'filterall')" id="filterall" class="menu-filter-link filter active" data-option-value="*" > View all</a></li>
                        <?php $i=0; foreach($taggingArray as $tagging){ ?>
                        <li><a onclick="filter_images(this, 0, 'filterall<?php echo $i ?>')"  id="filterall<?php echo $i ?>" class="menu-filter-link filter " data-option-value=".<?php echo $tagging['tag'] ?>"> <?php echo $tagging['tagname'] ?></a></li>
                        <?php $i++; } ?>
                    </ul>
                    <div class="clearfix"></div>
                </div>
            </div>
            <br/>
            <!-- End Filter Navigation -->

            <!-- Filter -->
            <div class="items animated pretty-lightbox-a" data-animation="fadeInUp" data-animation-delay="700">

                <?php $i = 1; foreach ($banners as $banner){ ?>
                <!-- Item -->
                <div   class="work col-xs-12 col-sm-6 col-md-3 col-lg-3 <?php foreach($banner['tagging'] as $tag){ echo $tag['tag'].' '; } ?>">
                    <div class="work-inner">
                        <div class="work-img ">
                            <img class="img-responsive" style="margin:0 auto" src="<?php echo $banner['image'] ?>" />
                            <div class='desc-container'>
                                <h4 class='text-left'><?php echo $banner['title'] ?></h4>
                              
                            </div>

                        </div>
                    </div>
                </div>
                <!-- End Item -->
                <?php $i++; } ?>


                <div class="clear"></div>

            </div>
            <!-- End Filter -->
        </div>
        <!-- End Works -->
    </div>	
    <!-- End Content -->
</section>
</div>
<!-- End Portfolio Section -->
<script>

   function filter_images(item, all, id){
            var newclass = $(item).attr('data-option-value');
                    if (all == 1){
            $('.work').show("slow")

                    $('#' + id).addClass('active')
            } else{

                   $(newclass).show("slow")
                $('.work').not(newclass).hide('slow');
            //$('.work').hide()
            }
            $('.filter').removeClass('active')

                    $('#' + id).addClass('active')

            }








</script>