<?php if ($news) { ?>
 <?php
        $layout = 'container';
            if($this->config->get('config_package') == 1){
                $layout = 'container-full';
            }
            

            
        
        ?>
<div class='<?php echo $layout ?>'>
    <?php $i=1; foreach($news as $story){ ?>
    <?php if($i < 4){ ?>
    <div class='col-md-4 col-sm-6 col-xs-12'>
        <a href="<?php echo $story['href'] ?>"><div class = "thumbnail">
                <?php if($story['image'] != ''){ ?>
                <img src="<?php echo $story['image'] ?>" class="img-rounded "  />
                <?php } ?>
                <div class="caption">
                    <h3><?php echo $story['title'] ?></h3>
                    <p>
                        <?php echo $story['description'] ?>
                    </p>
                </div>
            </div>
        </a>
    </div>
    <?php } ?>
    <?php $i++; } ?>
</div>
<?php }else{ ?>

<?php } ?>
