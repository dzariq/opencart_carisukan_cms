<style>
    .articlerow{
        padding:10px;
        border:1px solid #e0e0e0
    }

    .article-cont{
        font-size:12px;
        color:#8b8b8b;
        font-weight:bold
    }
    
    .article-author a, article-author p{
        color:#8b8b8b !Important
    }
    
    .article-cont a{
        font-size:12px !important
    }
    
    .date-article{
        height:80px;
        background-color:#<?php echo $this->config->get('config_main_theme') ?> !important;
        color:<?php echo $this->config->get('theme_menu_color') ?> !important;
        line-height:1.6;
    }

    .day{
        font-size:24px;
        font-weight:bold
    }

    .month{
        font-size:18px;
    }
</style>
<div class="box">
    <br/>
    <div class="box-heading"><?php echo $heading_title; ?></div>
    <div class="box-content" style='margin-top: 10px;'>
        <?php  if($articles) { ?>
        <?php foreach ($articles as $article) { ?>
        <div class="articlerow row">
            <div style='padding:0px'  class='text-center date-article col-sm-2 col-xs-2'>
                <span class='day'><?php echo $article['day_added']; ?></span>
                <br/>
                <span class='month'><?php echo $article['month_added']; ?></span>
            </div>
            <div style='padding:0px;' class='col-sm-2 hidden-xs'>
                <img style='margin-left:10px;margin-right:10px;border-radius:0px' class='img-responsive thumbnail' src='<?php echo $article[image]; ?>' />
            </div>
            <div class="col-sm-8 col-xs-8 article-author">
                <a href="<?php echo $article['href']; ?>"><?php echo strtoupper($article['article_title']) ?></a>
                <br/>
                <p class="article-cont">
                    <?php echo $text_posted ?> <a href="<?php echo $article['author_href']; ?>"><?php echo $article['author_name']; ?></a> / <a href="<?php echo $article['comment_href']; ?>"><?php echo $article['total_comment']; ?></a>
                </p>
            </div>
        </div>
        <br/>
        <?php } ?>
        <?php } else { ?>
        <div class="buttons">
            <div class="center"><?php echo $text_no_found; ?></div>
        </div>
        <?php } ?>
    </div>
</div>