<?php $i=0; foreach($products as $product){ ?>
<!-- Item #1 -->
<div onclick="window.location='<?php echo $product["href"] ?>'"  class="tile col-sm-3 col-xs-12 <?php if ($i % 2 == 0) { ?> colored <?php } ?>" style="background-image:url('<?php echo $product[thumb] ?>')" >
    <div class="overlay">
        <div  class='row text-center product-snippet'>
                <div class='col-xs-12  snippet-inside'>
                    <?php echo $product['name'] ?>
                    <br/>
                    <p class='snippet-desc'>
                        <?php echo $product['description'] ?>
                    </p>
                         <div class="item-price  "  style="margin:0;text-align: center;color:black !important;background:transparent !important;width:100%;font-size:16px;">
                                        <?php if (!$product['special']) { ?><?php echo $product['price'] ?><?php }else{ ?><span style="text-decoration:line-through"><?php echo $product['price']; ?></span>&nbsp;&nbsp;<?php echo $product['special']; ?><?php } ?>
                                    </div>
                </div>
        </div>
    </div>
                    <?php if ($product['special']) { ?>
                    <span class="sale_product"></span>
                    <?php } ?>
</div> 
<?php $i++; } ?>

<script>
    $('.tile').hover(
            function() {
                $(this).find('.product-snippet').slideDown(250); //.fadeIn(250)
            },
            function() {
                $(this).find('.product-snippet').slideUp(250); //.fadeOut(205)
            }
    );

</script>