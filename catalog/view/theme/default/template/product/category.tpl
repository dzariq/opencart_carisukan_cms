<?php echo $header; ?>



    <!-- Page content -->

    <div class="shop-items">

            <div class="row">
          <div class="col-md-2" style='padding: 0px;'>
                                   <?php echo $column_left ?>

          </div>
                <div class="col-md-10 ">

                    <style>
                        .shop-items .item-image{
                            max-height:400px
                        }
                        .shop-items .item{
                            max-height:600px

                        }

                        .shop-items .item, .shop-items .item-image{
                            max-height: none;
                            max-width: none
                        }
                        #hover-cap-4col .thumbnail {
                            position:relative;
                            overflow:hidden;    
                        }
                        .caption {
                            display: none;
                            position: absolute;
                            top: 150px;
                            left: 0;
                            padding-top:0px;
                            background: rgba(0,0,0,0.3);
                            width: 100%;
                            height: 30%;
                            color:#fff !important;
                        }
                        .captionnohide {
                            display: block;
                            position: absolute;
                            padding-top:0px;
                            top: 150px;
                            left: 0;
                            background: rgba(0,0,0,0.3);
                            width: 100%;
                            height: 30%;
                            color:#fff !important;
                        }

                        .caption h4,.caption h3, .caption p{
                            color:white !important; 
                            margin-top:0 !Important
                        }

                        .thumbnail{
                            border:none;
                        }

                        @media(max-width:767px) {
                            .sold-out{
                                line-height: 3.5 !important;
                                font-size: 18px !important
                            }

                            .caption, .captionnohide{
                                top:120px;
                                height:30%;
                            }

                            .caption h4,.caption h3{
                                font-size:18px !Important
                            }
                            .caption p{
                                color:white !important; 
                                font-size:14px !Important
                            }
                        }
                        @media(max-width:425px) {
                            .caption h4,.caption h3{
                                font-size:15px !Important
                            }

                            .caption, .captionnohide{
                                top:90px;
                                height:45%;
                            }

                            .sold-out{
                                line-height: 2.5 !important;
                            }
                            .caption p{
                                color:white !important; 
                                font-size:10px !Important
                            }
                        }
                        
                        .shop-items .item{
                            border:none;
                        }

                        @media(min-width:768px) and (max-width:879px) {
                            .sold-out{
                                line-height: 3.0 !important;
                                font-size: 15px !important
                            }

                            .caption, .captionnohide{
                                top:65px;
                                height:65%;
                            }

                            .caption h4,.caption h3{
                                font-size:13px !Important
                            }
                            .caption p{
                                color:white !important; 
                                font-size:10px !Important
                            }
                        }

                        @media(min-width:767px) and (max-width:1020px) {
                            .sold-out{
                                line-height: 3.5 !important;
                                font-size: 15px !important
                            }




                            .caption h4,.caption h3{
                                font-size:14px !Important
                            }
                            .caption p{
                                color:white !important; 
                                font-size:12px !Important
                            }
                        }

.pagination{
    margin-top:0px !Important
}
                    </style>
                    <!-- Items List starts -->
                    <div style='margin-top:8px' class="row">

                        <div style='float:left;color:#595959 !Important;margin-bottom:10px' class="sort col-md-2 col-xs-12 pull-left product-filter">

                            <select style='padding:0px;height:29px' class='form-control' onchange="location = this.value;">
                                <?php foreach ($sorts as $sorts) { ?>
                                <?php if ($sorts['value'] == $sort . '-' . $order) { ?>
                                <option value="<?php echo $sorts['href']; ?>" selected="selected"><?php echo $sorts['text']; ?></option>
                                <?php } else { ?>
                                <option value="<?php echo $sorts['href']; ?>"><?php echo $sorts['text']; ?></option>
                                <?php } ?>
                                <?php } ?>
                            </select>
                        </div>
                        <div style='float:right;color:#595959 !Important' class="paginationdiv col-md-10 col-xs-12 text-left "><?php echo $pagination; ?></div>
                    </div>
                    
                    <style>
                   
                        .sale_product{
                        display: block;
                        position: absolute;
                        z-index: 4;
                        width: 64px;
                        height: 165px;
                        right: -5px;
                        top: 0;
                        background: transparent url(image/SALE-ICON.png) center center no-repeat;
                        }
                        </style>
                    <div class="row product-list ">
                       <?php $i=0; foreach($products as $product){ ?>
<!-- Item #1 -->
<div onclick="window.location='<?php echo $product["href"] ?>'"  class="tile <?php if(empty($column_left)){ ?> col-sm-4 <?php }else{ ?> col-sm-3 <?php } ?> col-xs-12 <?php if ($i % 2 == 0) { ?> colored <?php } ?>" style=" height:250px;background-image:url('<?php echo $product[thumb] ?>')" >
    <div class="overlay">
        <div  class='row text-center product-snippet'>
                <div class='col-xs-12  snippet-inside'>
                    <?php echo $product['name'] ?>
                    <br/>
                    <p class='snippet-desc'>
                        <?php echo $product['description'] ?>
                    </p>
                         <div class="item-price  "  style="margin:0;text-align: center;color:black !important;background:transparent !important;width:100%;font-size:16px;">
                                        <?php if (!$product['special']) { ?><?php echo $product['price'] ?><?php }else{ ?><span style="text-decoration:line-through"><?php echo $product['price']; ?></span>&nbsp;&nbsp;<?php echo $product['special']; ?><?php } ?>
                                    </div>
                </div>
        </div>
    </div>
                    <?php if ($product['special']) { ?>
                    <span class="sale_product"></span>
                    <?php } ?>
</div> 
<?php $i++; } ?>
                    </div>

                    <div class="row">
                        <div class="paginationdiv col-md-12">
                                <?php echo $pagination; ?>
                                

                        </div>
                    </div>

                </div>


               
            </div>

            <div class="sep-bor"></div>
    </div>



<?php echo $footer; ?>
<script>
    function addToCart2(product_id, quantity) {
        quantity = typeof (quantity) != 'undefined' ? quantity : 1;

        $.ajax({
            url: 'index.php?route=checkout/cart/add',
            type: 'post',
            data: 'product_id=' + product_id + '&quantity=' + quantity,
            dataType: 'json',
            success: function(json) {

                if (json['redirect']) {
                    location = json['redirect'];
                }

                if (json['success']) {
                    location = 'index.php?route=checkout/cart';
                }
            }
        });
    }

    $(document).ready(function() {

        $("[rel='tooltip']").tooltip();

        $('#hover-cap-4col .thumbnail').hover(
                function() {
                    $(this).find('.caption').slideDown(250); //.fadeIn(250)
                },
                function() {
                    $(this).find('.caption').slideUp(250); //.fadeOut(205)
                }
        );

    });




</script>

<script>
    $('.tile').hover(
            function() {
                $(this).find('.product-snippet').slideDown(250); //.fadeIn(250)
            },
            function() {
                $(this).find('.product-snippet').slideUp(250); //.fadeOut(205)
            }
    );

</script>