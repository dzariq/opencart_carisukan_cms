<?php echo $header ?>
<!-- Page title -->
<script>
    //global variable quantity of product (stock) //
    var option_quantity = '';
            var option_required = false;</script>

<style>
    .tab-pane{
        border:1px solid #e0e0e0;
    }

    h1,h2,h3{
        margin-top:5px !Important;
        margin-bottom:5px !Important
    }
    
      ul.sizes .active {
        color: white

    }

    .nav-tabs>li>a{
        background: #e0e0e0 !Important
    }

    .form-group{
        margin-bottom:-10px !important;
    }

    @media (max-width:767px) {
        .breadcrumb{
            display:none
        }
    }

    .col-sm-12,.col-xs-12{
        //   text-align: center
    }

    .col-xs-12{
     //   margin-top:0px !important;
    }
    ul{
        list-style: none;
    }

    ul.sizes .active {
        background: <?php echo $this->config->get('theme_buttons_color') ?>

    }

    ul.sizes li {
        background-color: #F2F2F2;
        font-size: 13px;
        height: 40px;
        text-align: center;
        cursor:pointer;
        margin:3px;
        color:black;
        float:left;
        line-height: 30px;
        font-size:14px;
        padding-top:5px;
              width: auto;
padding-right: 10px;
padding-left: 10px;
        border: 1px solid #e0e0e0;
        /* background-image: url(../img/product-options/generic_single.png); */
    }

    .number {
        background-color: #F2F2F2;
        border: 1px solid #e0e0e0;
        color: #000000 !important;
        float: left;
        font-size: 20px;
        height: 40px;
        line-height: 40px;
        text-align: center;
        width: 40px;
        margin-right: 4px;
        margin-top: 10px;
        margin-bottom: 15px;
        cursor: pointer;
    }
    .quant{
        float: left;
        margin-top: 23px;
    }

    @media (max-width:991px){
        .last-table{
            margin-top:40px !important;
        }
    }
</style>
<!-- Page title -->
<div class="shop-items">
    <div class=" product-info col-md-12">
        <div class="row">
            <!-- Breadcrumb -->
            <div class="col-md-12 hidden-phone">
                <ul class="breadcrumb">
                    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a> <span class="divider"></span></li>
                    <?php } ?>
                </ul>
                                         <h4 style="margin-top:10px"><?php echo $heading_title ?></h4>

            </div>

            <div class="single-item ">
                <div class="row" style='height: 100%;'>
                    <?php if ($images && $thumb) { ?>
                    <div class="col-md-5 col-sm-12 col-xs-12">
                        <div style="padding:0px" class="col-md-3 col-sm-3 col-xs-3">
                            <?php foreach ($images as $image) { ?>
                            <a style="padding:0px;width:100%;height:100%" href="<?php echo $image['popup']; ?>" title="<?php echo $heading_title; ?>" class="colorbox col-xs-5 col-sm-5 col-md-5">
                                <img class="img-responsive" src="<?php echo $image['thumb']; ?>" title="<?php echo $heading_title; ?>" alt="<?php echo $heading_title; ?>" />
                            </a>
                            <br/>
                            <?php } ?>
                        </div>

                        <div  class="col-md-9 col-sm-9 col-xs-9"  >
                            <a id='main-popup' href="<?php echo $popup; ?>" title="<?php echo $heading_title; ?>" class="colorbox">
                                <img class="img-responsive"  src="<?php echo $thumb; ?>" alt="<?php echo $heading_title; ?>" id="image" />
                            </a>
                        </div>
                              <?php if ($products) { ?>
                            <h5><?php echo $tab_related ?></h5>
                        <div class="row tab-content">
                            <div class="box-product">
                                <?php foreach ($products as $product) { ?>
                                <div class="col-xs-4">
                                    <?php if ($product['thumb']) { ?>
                                    <div class="image"><a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" /></a></div>
                                    <?php } ?>
                                    <div class="name"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></div>
                                    <?php if ($product['price']) { ?>
                                    <div class="price">
                                        <?php if (!$product['special']) { ?>
                                        <span style="font-size:11px;"><?php echo $product['price']; ?></span>
                                        <?php } else { ?>
                                        <span style="font-size:10px;text-decoration:line-through"><?php echo $product['price']; ?></span> <span style="font-size:11px;"><?php echo $product['special']; ?></span>
                                        <?php } ?>
                                    </div>
                                    <?php } ?>
                                    <?php if ($product['rating']) { ?>
                                    <div class="rating"><img src="catalog/view/theme/default/image/stars-<?php echo $product['rating']; ?>.png" alt="<?php echo $product['reviews']; ?>" /></div>
                                    <?php } ?>
                                  <!--  <a onclick="addToCart('<?php echo $product['product_id']; ?>');" class="button"><?php echo $button_cart; ?></a>-->
                                </div>
                                <?php } ?>
                            </div>
                        </div>
                        <?php } ?>


                    </div>
                    <?php } ?>
                    <div style="border: 0px solid #e0e0e0;" <?php if ($images && $thumb) { ?> class="col-md-3  col-sm-12 col-xs-12 <?php }else{ ?> class="col-md-6  col-sm-12 col-xs-12 <?php } ?>">
                         <!-- Title -->
                        <h4><strong><?php if (!$special) { ?><?php echo $price; ?><?php }else{ ?><span style="text-decoration:line-through"><?php echo $price; ?></span>&nbsp;&nbsp;<?php echo $special; ?><?php } ?></strong></h4>

                        <div class="form-group">
                            <?php if ($options) { ?>
                            <div class="options">
                                <br />
                                <?php foreach ($options as $option) { ?>

                                <?php if ( $option['type'] == 'select' || $option['type'] == 'radio') { ?>
                                <div id="option-<?php echo $option['product_option_id']; ?>" class="option">

                                    <label><?php if ($option['required']) { ?>
                                        <script>
                                                    var option_required = true;                                        </script>
                                        <span class="required">*</span>
                                        <?php } ?><?php echo $option['name']; ?></label><br />
                                    <ul class="sizes">
                                        <?php  foreach ($option['option_value'] as $option_value) { ?>
                                        <li class="radio-box <?php echo $option['product_option_id']; ?>" id="<?php echo $option_value['product_option_value_id']; ?>" onclick="radio_click( <?php echo $option_value['quantity'] ?> , <?php echo $option['product_option_id']; ?> , <?php echo $option_value['product_option_value_id']; ?> ,'<?php echo $option_value[image_option_value]; ?>','<?php echo $option_value[image_option_value_popup]; ?>')" value="<?php echo $option_value['product_option_value_id']; ?>">
                                            <?php echo $option_value['name']; ?> 
                                            <?php if ($option_value['price']) { ?>
                                            (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>) 
                                            <?php } ?>
                                        </li>
                                        <?php } ?>    
                                    </ul>
                                  
                                        
                                    <?php foreach ($option['option_value'] as $option_value) { ?>
                                    <input type="radio" style="visibility: hidden" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" id="option-value-<?php echo $option_value['product_option_value_id']; ?>" />
                                    <?php } ?>
                                </div>
                                <br />
                                <?php } ?>
                               
                                <?php if ($option['type'] == 'checkbox') { ?>
                                <div id="option-<?php echo $option['product_option_id']; ?>" class="option">
                                    <?php if ($option['required']) { ?>
                                    <span class="required">*</span>
                                    <?php } ?>
                                    <b><?php echo $option['name']; ?>:</b><br />
                                    <?php foreach ($option['option_value'] as $option_value) { ?>
                                    <input type="checkbox" name="option[<?php echo $option['product_option_id']; ?>][]" value="<?php echo $option_value['product_option_value_id']; ?>" id="option-value-<?php echo $option_value['product_option_value_id']; ?>" />
                                    <label for="option-value-<?php echo $option_value['product_option_value_id']; ?>"><?php echo $option_value['name']; ?>
                                        <?php if ($option_value['price']) { ?>
                                        (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                                        <?php } ?>
                                    </label>
                                    <br />
                                    <?php } ?>
                                </div>
                                <br />
                                <?php } ?>
                                <?php if ($option['type'] == 'image') { ?>
                                <div id="option-<?php echo $option['product_option_id']; ?>" class="option">
                                    <?php if ($option['required']) { ?>
                                    <span class="required">*</span>
                                    <?php } ?>
                                    <b><?php echo $option['name']; ?>:</b><br />
                                    <table class="option-image">
                                        <?php foreach ($option['option_value'] as $option_value) { ?>
                                        <tr>
                                            <td style="width: 1px;"><input type="radio" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" id="option-value-<?php echo $option_value['product_option_value_id']; ?>" /></td>
                                            <td><label for="option-value-<?php echo $option_value['product_option_value_id']; ?>"><img src="<?php echo $option_value['image']; ?>" alt="<?php echo $option_value['name'] . ($option_value['price'] ? ' ' . $option_value['price_prefix'] . $option_value['price'] : ''); ?>" /></label></td>
                                            <td><label for="option-value-<?php echo $option_value['product_option_value_id']; ?>"><?php echo $option_value['name']; ?>
                                                    <?php if ($option_value['price']) { ?>
                                                    (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                                                    <?php } ?>
                                                </label></td>
                                        </tr>
                                        <?php } ?>
                                    </table>
                                </div>
                                <br />
                                <?php } ?>
                                <?php if ($option['type'] == 'text') { ?>
                                <div id="option-<?php echo $option['product_option_id']; ?>" class="option">
                                    <?php if ($option['required']) { ?>
                                    <span class="required">*</span>
                                    <?php } ?>
                                    <b><?php echo $option['name']; ?>:</b><br />
                                    <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['option_value']; ?>" />
                                </div>
                                <br />
                                <?php } ?>
                                <?php if ($option['type'] == 'textarea') { ?>
                                <div id="option-<?php echo $option['product_option_id']; ?>" class="option">
                                    <?php if ($option['required']) { ?>
                                    <span class="required">*</span>
                                    <?php } ?>
                                    <b><?php echo $option['name']; ?>:</b><br />
                                    <textarea name="option[<?php echo $option['product_option_id']; ?>]" cols="40" rows="5"><?php echo $option['option_value']; ?></textarea>
                                </div>
                                <br />
                                <?php } ?>
                                <?php if ($option['type'] == 'file') { ?>
                                <div id="option-<?php echo $option['product_option_id']; ?>" class="option">
                                    <?php if ($option['required']) { ?>
                                    <span class="required">*</span>
                                    <?php } ?>
                                    <b><?php echo $option['name']; ?>:</b><br />
                                    <input type="button" value="<?php echo $button_upload; ?>" id="button-option-<?php echo $option['product_option_id']; ?>" class="button">
                                    <input type="hidden" name="option[<?php echo $option['product_option_id']; ?>]" value="" />
                                </div>
                                <br />
                                <?php } ?>
                                <?php if ($option['type'] == 'date') { ?>
                                <div id="option-<?php echo $option['product_option_id']; ?>" class="option">
                                    <?php if ($option['required']) { ?>
                                    <span class="required">*</span>
                                    <?php } ?>
                                    <b><?php echo $option['name']; ?>:</b><br />
                                    <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['option_value']; ?>" class="date" />
                                </div>
                                <br />
                                <?php } ?>
                                <?php if ($option['type'] == 'datetime') { ?>
                                <div id="option-<?php echo $option['product_option_id']; ?>" class="option">
                                    <?php if ($option['required']) { ?>
                                    <span class="required">*</span>
                                    <?php } ?>
                                    <b><?php echo $option['name']; ?>:</b><br />
                                    <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['option_value']; ?>" class="datetime" />
                                </div>
                                <br />
                                <?php } ?>
                                <?php if ($option['type'] == 'time') { ?>
                                <div id="option-<?php echo $option['product_option_id']; ?>" class="option">
                                    <?php if ($option['required']) { ?>
                                    <span class="required">*</span>
                                    <?php } ?>
                                    <b><?php echo $option['name']; ?>:</b><br />
                                    <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['option_value']; ?>" class="time" />
                                </div>
                                <br />
                                <?php } ?>
                                <?php } ?>
                            </div>
                            <?php } ?>
                        </div>
                        <div class="row">
                            <div class="col-md-12 col-xs-12 col-sm-12">

                                <input type="hidden" name="product_id" size="2" value="<?php echo $product_id; ?>" />
                                <?php if($this->config->get('config_shoppingcart')){ ?>
                                <!-- Quantity and add to cart button -->
                                <div class="quant">
                                    <label><span class="required">*</span> <?php echo $text_quantity ?></label>
                                    <br/>
                                    <div style="margin-left:18px" id="resta1" class="number">-</div><div id="number1" class="number">1</div><div id="suma1" class="number">+</div>
                                </div>

                                <select style="visibility:hidden" id="quantity" name="quantity" class="form-control">
                                    <?php for($i=1;$i<=200;$i++){ ?> 
                                    <option <?php if($minimum == $i){ ?> selected="selected" <?php } ?> value="<?php echo $i ?>" ><?php echo $i ?></option>
                                    <?php  } ?>
                                </select>
                                <?php if($stockstatus == 'SOLD OUT'){ ?>
                                <button style="margin-top:-10px;margin-bottom:10px;color:white;width:100%;border:none;background:#a50025 !important" type="button" class="btn btn-info" >SOLD OUT</button>
                                <?php }else{ ?>
                                <button style="margin-top:-10px;height:44px;width:100%;font-size:22px;line-height:1.0;font-weight:bold;margin-bottom:10px" type="button" id="button-cart" class="btn btn-info" ><?php echo $button_cart; ?></button>
                                <?php } ?>
                                <?php } ?>
<br/>
<br/>
                            </div>
                            <div style="display:none"  class="col-md-12 col-xs-12 col-sm-12">
                                <!-- Share button -->
                                <div class="addthis_default_style"><a class="addthis_button_compact"><?php echo $text_share; ?></a> <a class="addthis_button_email"></a><a class="addthis_button_print"></a> <a class="addthis_button_facebook"></a> <a class="addthis_button_twitter"></a></div>
                                <script type="text/javascript" src="//s7.addthis.com/js/250/addthis_widget.js"></script>
                                <!-- AddThis Button END -->
                            </div>
                        </div>
                    </div>
                    <div <?php if ($images && $thumb) { ?> class="last-table col-md-4  col-sm-12 col-xs-12 <?php }else{ ?> class=" last-table col-md-6  col-sm-12 col-xs-12 <?php } ?>" >
                        <!-- Description, specs and review -->
                        <ul id="myTab" class="nav nav-tabs">
                            <!-- Use uniqe name for "href" in below anchor tags -->
                            <li class="active"><a href="#tab1" data-toggle="tab">Description</a></li>
                            <?php if ($attribute_groups) { ?>
                            <li><a href="#tab2" data-toggle="tab">Size Chart</a></li>
                            <?php } ?>
                            <?php if ($review_statustemp) { ?>
                            <li><a href="#tab3" data-toggle="tab">Review (5)</a></li>
                            <?php } ?>
                        </ul>
                        <!-- Tab Content -->
                        <div id="myTabContent" class="tab-content">
                            <!-- Description -->
                            <div style='text-align:left;padding:10px;padding-top:20px;line-height: 1.5 !important' class="tab-pane fade in active" id="tab1">
                                <?php echo $description; ?>
                            </div>

                            <!-- Sepcs -->
                            <?php if ($attribute_groups) { ?>
                            <div class="tab-pane fade" id="tab2">
                                <table class="table table-striped">
                                    <?php foreach ($attribute_groups as $attribute_group) { ?>
                                    <thead>

                                    </thead>
                                    <?php } ?>
                                    <tbody>
                                        <?php foreach ($attribute_group['attribute'] as $attribute) { ?>
                                        <tr>
                                            <td><strong><?php echo $attribute['name']; ?></strong></td>
                                            <td><?php echo $attribute['text']; ?></td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                            <?php } ?>
                            <!-- Review -->
                            <?php if ($review_statustemp) { ?>
                            <div class="tab-pane fade" id="tab3">
                                <h5><strong>Product Reviews :</strong></h5>
                                <hr />
                                <div class="item-review">
                                    <h5>Ravi Kumar - <span class="color">4/5</span></h5>
                                    <p class="rmeta">27/1/2012</p>
                                    <p>Suspendisse potenti. Morbi ac felis nec mauris imperdiet fermentum. Aenean sodales augue ac lacus hendrerit sed rhoncus erat hendrerit. Vivamus vel ultricies elit. Curabitur lacinia nulla vel tellus elementum non mollis justo aliquam.</p>
                                </div>

                                <hr />
                                <h5><strong>Write a Review</strong></h5>
                                <hr />
                                <form role="form">
                                    <div class="form-group">
                                        <label for="name">Your Name</label>
                                        <input type="text" class="form-control" id="name" placeholder="Enter Name">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Email address</label>
                                        <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email">
                                    </div>
                                    <div class="form-group">
                                        <label for="rating">Rating</label>
                                        <!-- Dropdown menu -->
                                        <select class="form-control">
                                            <option>Rating</option>
                                            <option>1</option>
                                            <option>2</option>
                                            <option>3</option>
                                            <option>4</option>
                                            <option>5</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Review</label>
                                        <textarea class="form-control" rows="3"></textarea>
                                    </div>
                                    <button type="submit" class="btn btn-info">Send</button>
                                    <button type="reset" class="btn btn-default">Reset</button>
                                </form>

                            </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="sep-bor"></div>
    </div>
</div>
<script type="text/javascript"><!--
$(document).ready(function() {
    $('.colorbox').colorbox({
    overlayClose: true,
            opacity: 0.5,
            rel: "colorbox"
    });
    });
//--></script> 
<script type="text/javascript"><!--

            $('#button-cart').bind('click', function() {
    $.ajax({
    url: 'index.php?route=checkout/cart/add',
            type: 'post',
            data: $('.product-info input[type=\'text\'], .product-info input[type=\'hidden\'], .product-info input[type=\'radio\']:checked, .product-info input[type=\'checkbox\']:checked, .product-info select, .product-info textarea'),
            dataType: 'json',
            success: function(json) {
            $('.success, .warning, .attention, information, .error').remove();
                    if (json['error']) {
            if (json['error']['option']) {
            for (i in json['error']['option']) {
            $('#option-' + i).after('<span class="error">' + json['error']['option'][i] + '</span>');
            }
            }
            }

            if (json['success']) {
            window.location = "index.php?route=checkout/cart";
            }
            }
    });
    });
//--></script>
<?php if ($options) { ?>
<script type="text/javascript" src="catalog/view/javascript/jquery/ajaxupload.js"></script>
<?php foreach ($options as $option) { ?>
<?php if ($option['type'] == 'file') { ?>
<script type="text/javascript"><!--
new AjaxUpload('#button-option-<?php echo $option['product_option_id']; ?>', {
            action: 'index.php?route=product/product/upload',
                    name: 'file',
                    autoSubmit: true,
                    responseType: 'json',
                    onSubmit: function(file, extension) {
                    $('#button-option-<?php echo $option['product_option_id']; ?>').after('<img src="catalog/view/theme/default/image/loading.gif" class="loading" style="padding-left: 5px;" />');
                            $('#button-option-<?php echo $option['product_option_id']; ?>').attr('disabled', true);
                    },
                    onComplete: function(file, json) {
                    $('#button-option-<?php echo $option['product_option_id']; ?>').attr('disabled', false);
                            $('.error').remove();
                            if (json['success']) {
                    alert(json['success']);
                            $('input[name=\'option[<?php echo $option['product_option_id']; ?>]\']').attr('value', json['file']);
                    }

                    if (json['error']) {
                    $('#option-<?php echo $option['product_option_id']; ?>').after('<span class="error">' + json['error'] + '</span>');
                    }

                    $('.loading').remove();
                    }
            });
//--></script>
<?php } ?>
<?php } ?>
<?php } ?>
<script type="text/javascript"><!--
$('#review .pagination a').bind('click', function() {
    $('#review').fadeOut('slow');
            $('#review').load(this.href);
            $('#review').fadeIn('slow');
            return false;
    });
            $('#review').load('index.php?route=product/product/review&product_id=<?php echo $product_id; ?>');
            $('#button-review').bind('click', function() {
    $.ajax({
    url: 'index.php?route=product/product/write&product_id=<?php echo $product_id; ?>',
            type: 'post',
            dataType: 'json',
            data: 'name=' + encodeURIComponent($('input[name=\'name\']').val()) + '&text=' + encodeURIComponent($('textarea[name=\'text\']').val()) + '&rating=' + encodeURIComponent($('input[name=\'rating\']:checked').val() ? $('input[name=\'rating\']:checked').val() : '') + '&captcha=' + encodeURIComponent($('input[name=\'captcha\']').val()),
            beforeSend: function() {
            $('.success, .warning').remove();
                    $('#button-review').attr('disabled', true);
                    $('#review-title').after('<div class="attention"><img src="catalog/view/theme/default/image/loading.gif" alt="" /> <?php echo $text_wait; ?></div>');
            },
            complete: function() {
            $('#button-review').attr('disabled', false);
                    $('.attention').remove();
            },
            success: function(data) {
            if (data['error']) {
            $('#review-title').after('<div class="warning">' + data['error'] + '</div>');
            }

            if (data['success']) {
            $('#review-title').after('<div class="success">' + data['success'] + '</div>');
                    $('input[name=\'name\']').val('');
                    $('textarea[name=\'text\']').val('');
                    $('input[name=\'rating\']:checked').attr('checked', '');
                    $('input[name=\'captcha\']').val('');
            }
            }
    });
    });
            function radio_click(qty, name, id,image,popup){
                
               if(image != ''){
                    $('#image').attr('src',image)
                    $('#main-popup').attr('href',popup)
            }
                
            option_quantity = qty;
                    if ($('#quantity').val() > option_quantity){
                        var total_dec = $('#quantity').val() - option_quantity;
                        for(i=0;i<total_dec;i++){
                            decrement();
                        }
            }
            $(".radio-box").removeClass("active");
                    $("#" + id).addClass("active");
                    $("#option-value-" + id).prop('checked', true);
            }

    $('#resta1').click(function() {
    decrement();
    });
            $('#suma1').click(function() {
    increment();
    });
            function increment() {
            if (option_required == true && option_quantity == ''){
            alert('<?php echo $text_require_option ?>');
                    return 0;
            }
            if (option_quantity != ''){
            var max_qty = option_quantity
            } else{
            var max_qty = <?php echo $quantity ?> ;
            }

            $('#quantity').val(function(i, oldval) {
            var old = oldval;
                    var newval = ++oldval;
                    if (oldval > max_qty){
            return old;
            } else{
            return newval;
            }
            });
                    $('#number1').html($('#quantity').val())
            }

    function decrement() {
    $('#quantity').val(function(i, oldval) {
    var old = oldval;
            var newval = --oldval;
            if (oldval < <?php echo $minimum ?> ){
    return old;
    } else{
    return newval;
    }
    });
            $('#number1').html($('#quantity').val())

    }
//--></script>


<?php echo $footer ?>
