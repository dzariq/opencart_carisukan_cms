 <div class="form-group">
                            <?php if ($options) { ?>
                            <div class="options">
                                <br />
                                <?php foreach ($options as $option) { ?>

                                <?php if ( $option['type'] == 'select' || $option['type'] == 'radio') { ?>
                                <div id="option-<?php echo $option['product_option_id']; ?>" class="option">

                                   <?php if ($option['required']) { ?>
                                        <script>
                                                    var option_required = true;                                        </script>
                                        <span class="required">*</span>
                                        <?php } ?><?php echo $option['name']; ?><br />
                                    <ul class="sizes">
                                        <?php  foreach ($option['option_value'] as $option_value) { ?>
                                        <li class="radio-box <?php echo $option['product_option_id']; ?>" id="<?php echo $option_value['product_option_value_id']; ?>" onclick="radio_click( <?php echo $option_value['quantity'] ?> , <?php echo $option['product_option_id']; ?> , <?php echo $option_value['product_option_value_id']; ?> ,'<?php echo $option_value[image_option_value]; ?>','<?php echo $option_value[image_option_value_popup]; ?>')" value="<?php echo $option_value['product_option_value_id']; ?>">
                                            <?php echo $option_value['name']; ?> 
                                            <?php if ($option_value['price']) { ?>
                                            (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>) 
                                            <?php } ?>
                                        </li>
                                        <?php } ?>    
                                    </ul>
                                  
                                        
                                    <?php foreach ($option['option_value'] as $option_value) { ?>
                                    <input type="radio" style="visibility: hidden" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" id="option-value-<?php echo $option_value['product_option_value_id']; ?>" />
                                    <?php } ?>
                                </div>
                                <br />
                                <?php } ?>
                               
                                <?php if ($option['type'] == 'text') { ?>
                                <div id="option-<?php echo $option['product_option_id']; ?>" class="option">
                                    <?php if ($option['required']) { ?>
                                    <span class="required">*</span>
                                    <?php } ?>
                                    <b><?php echo $option['name']; ?>:</b><br />
                                    <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['option_value']; ?>" />
                                </div>
                                <br />
                                <?php } ?>
                                <?php if ($option['type'] == 'textarea') { ?>
                                <div id="option-<?php echo $option['product_option_id']; ?>" class="option">
                                    <?php if ($option['required']) { ?>
                                    <span class="required">*</span>
                                    <?php } ?>
                                    <b><?php echo $option['name']; ?>:</b><br />
                                    <textarea name="option[<?php echo $option['product_option_id']; ?>]" cols="40" rows="5"><?php echo $option['option_value']; ?></textarea>
                                </div>
                                <br />
                                <?php } ?>
                                
                               
                                <?php if ($option['type'] == 'date') { ?>
                                <div id="option-<?php echo $option['product_option_id']; ?>" class="option">
                                    <?php if ($option['required']) { ?>
                                    <span class="required">*</span>
                                    <?php } ?>
                                    <?php echo $option['name']; ?>:<br />
                                    <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['option_value']; ?>" class="date" />
                                </div>
                                <br />
                                <?php } ?>
                             
                                <?php } ?>
                            </div>
                            <?php } ?>
                        </div>


