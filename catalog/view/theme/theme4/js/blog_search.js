/**
 * @author Apcrat
 */

$(document).ready(function () {
    $('#search-box input[name=\'s\']').keydown(function (e) {
        if (e.keyCode == 13) {
            $('#searchsubmit').trigger('click');
        }
    });
    $('#searchpdtsubmit').on('click', function () {
        url = $('base').attr('href') + 'index.php?route=product/search';

        var search = $('#spdt').val();

        if (search) {
            url += '&filter_name=' + encodeURIComponent(search);
        }

        location = url;
    });
   
    $('#searchsubmit').bind('click', function () {
        url = 'index.php?route=simple_blog/search';
        var article_search = $('#search-box input[name=\'s\']').val();
        if (article_search) {
            url += '&blog_search=' + encodeURIComponent(article_search);
        }
        window.location = url;
    });

    $('.blogs').bind('click', function () {
        if ($('#search-box-wrap').is(':visible')) {
            // Code
            $('#search-box-wrap').slideUp();
        } else {
            $('#search-box-wrap').slideDown();


        }

    });
    $('#close-x').bind('click', function () {
        if ($('#search-box-wrap').is(':visible')) {
            // Code
            $('#search-box-wrap').slideUp()
        }

    });



});