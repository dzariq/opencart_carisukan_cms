$(document).ready(function() {
	/* Search */
    $('#searchpdtsubmit').on('click', function() {
        url = $('base').attr('href') + 'index.php?route=product/search';
                 
        var search = $('#spdt').val();
        
        if (search) {
            url += '&filter_name=' + encodeURIComponent(search);
        }
        
        location = url;
    });
//    $('header input[name=\'search\']').parent().find('span').on('click', function() {
//        url = $('base').attr('href') + 'index.php?route=product/search';
//                 
//        var search = $('header input[name=\'search\']').val();
//        
//        if (search) {
//            url += '&search=' + encodeURIComponent(search);
//        }
//        
//        location = url;
//    });

    $('header input[name=\'search\']').on('keydown', function(e) {
        if (e.keyCode == 13) {
            $('header input[name=\'search\']').parent().find('span').trigger('click');
        }
    });
	
	
	/* Ajax Cart */
	$('#cart button').on('click', function() {
		$('#cart').load('index.php?route=module/cart #cart > *');
	});
	
	// Navigation - Columns
    $('.main-navbar .dropdown-menu').each(function(){

        var menu = $('.main-navbar').offset();
        var dropdown = $(this).parent().offset();

        var i = (dropdown.left + $(this).outerWidth()) - (menu.left + $('.main-navbar').outerWidth());

        if (i > 0) {
            $(this).css('margin-left', '-' + (i + 5) + 'px');
        }

    });

	// change product-grid to product-list
    $('#list-view').click(function() {
        $('.product-grid').removeClass('product-grid').addClass('product-list');
        $('.product-thumb').addClass('clearfix');

    });
    // change product-list to product-grid
    $('#grid-view').click(function() {
        $('.product-list').removeClass('product-list').addClass('product-grid');
        $('.product-thumb').removeClass('clearfix');
    });

	// tooltips on hover
//	$('[data-toggle=\'tooltip\']').tooltip();
});

function getURLVar(key) {
	var value = [];
	
	var query = String(document.location).split('?');
	
	if (query[1]) {
		var part = query[1].split('&');

		for (i = 0; i < part.length; i++) {
			var data = part[i].split('=');
			
			if (data[0] && data[1]) {
				value[data[0]] = data[1];
			}
		}
		
		if (value[key]) {
			return value[key];
		} else {
			return '';
		}
	}
} 

function addToCart(product_id, quantity) {
    quantity = typeof(quantity) != 'undefined' ? quantity : 1;

    $.ajax({
        url: 'index.php?route=checkout/cart/add',
        type: 'post',
        data: 'product_id=' + product_id + '&quantity=' + quantity,
        dataType: 'json',
        success: function(json) {
            $('.alert, .error').remove();
            
            if (json['redirect']) {
                location = json['redirect'];
            }
            
            if (json['success']) {
                $('#notification').html('<div class="alert alert-success">' + json['success'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
                
                $('.alert').fadeIn('slow');
				
				$('#cart-total').html(json['total']);
                
                $('html, body').animate({ scrollTop: 0 }, 'slow'); 
            }   
        }
    });
}
function addToWishList(product_id) {
	$.ajax({
		url: 'index.php?route=account/wishlist/add',
		type: 'post',
		data: 'product_id=' + product_id,
		dataType: 'json',
		success: function(json) {
            $('.alert').remove();
						
			if (json['success']) {
                $('#notification').html('<div class="alert alert-success">' + json['success'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
				
				$('.alert').fadeIn('slow');
				
				$('#wishlist-total').html(json['total']);
				
				$('html, body').animate({ scrollTop: 0 }, 'slow');
			}	
		}
	});
}

function addToCompare(product_id) { 
	$.ajax({
		url: 'index.php?route=product/compare/add',
		type: 'post',
		data: 'product_id=' + product_id,
		dataType: 'json',
		success: function(json) {
            $('.alert').remove();
						
			if (json['success']) {
                $('#notification').html('<div class="alert alert-success">' + json['success'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
				
				$('.alert').fadeIn('slow');
				
				$('#compare-total').html(json['total']);
				
				$('html, body').animate({ scrollTop: 0 }, 'slow'); 
			}	
		}
	});
}

   