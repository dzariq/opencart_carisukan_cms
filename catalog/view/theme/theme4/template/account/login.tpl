<?php echo $header; ?>
<?php if ($success) { ?>
    <div class="alert alert-success">
    	<?php echo $success; ?>
        <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
<?php } ?>
<?php if ($error_warning) { ?>
    <div class="alert alert-danger">
    	<?php echo $error_warning; ?>
    	<button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
<?php } ?>
 <!-- Page content -->
      <section>
         <div class="container">
            <div class="row">
   
               <div class="col-md-6 col-md-offset-3">
                  <div class="register-login" style="    background: wheat;
    padding: 15px;
    padding-bottom: 38px;">
                     <div class="cool-block">
                        <div class="cool-block-bor">

                           <h2 style="text-align: center"><?php echo $heading_title; ?></h2>
                           <br/>
                           <form class="form-horizontal" role="form" action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="login">
                             <div class="form-group">
                               <label for="inputEmail1" class="col-lg-2 control-label">
                                  Email: 
                               </label>
                               <div class="col-lg-10">
                                 <input type="email" class="form-control" value="<?php echo $email ?>" name="email" id="email" placeholder="Email">
                               </div>
                             </div>
                               <br/>
                             <div class="form-group">
                               <label for="inputPassword1" class="col-lg-2 control-label"><?php echo $entry_password; ?></label>
                               <div class="col-lg-10">
                                 <input type="password" value="<?php echo $password ?>" class="form-control" name="password" id="password" placeholder="Password">
                               </div>
                             </div>
<!--                             <div class="form-group">
                               <div class="col-lg-offset-2 col-lg-10">
                                 <div class="checkbox">
                                   <label>
                                     <input type="checkbox"> Remember me
                                   </label>
                                 </div>
                               </div>
                             </div>-->
                             <div class="form-group">
                               <div class="col-lg-offset-2 col-lg-10">
                                 <button onclick="$('#login').submit();" class="btn btn-primary"><?php echo $button_login ?></button>
                                    <a href="<?php echo $forgotten; ?>"><?php echo $text_forgotten; ?></a>
                               </div>
                             </div>
                           </form>

                        </div>
                     </div>
                  </div>
               </div>
                            <div class="col-md-6">
<!--                  <div class="reg-login-info">
                     <h2>Login to Access Amazing Benefits <span class="color">!!!</span></h2>
                     <img src="img/back1.jpg" alt="" class="img-responsive img-thumbnail" />
                     <p>Duis leo risus, vehicula luctus nunc. Quiue rhoncus, a sodales enim arcu quis turpis. Duis leo risus, condimentum ut posuere ac, vehicula luctus nunc. Quisque rhoncus, a sodales enim arcu quis turpis.</p>
                  </div>-->
               </div>
            </div>
            <div class="sep-bor"></div>
         </div>
      </section>
<script type="text/javascript"><!--
$('#login input').keydown(function(e) {
	if (e.keyCode == 13) {
		$('#login').submit();
	}
});
//--></script>  
<?php echo $footer; ?>