<?php echo $header; ?>
<section>
    <div class='container'>
        <?php if ($error_warning) { ?>
        <div class="warning"><?php echo $error_warning; ?></div>
        <?php } ?>
        <div id="content"><?php echo $content_top; ?>
            <h1><?php echo $heading_title; ?></h1>
            <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
                <p><?php echo $text_email; ?></p>
                <div class="no-pad col-sm-2">
                    <?php echo $entry_email; ?>
                </div>
                <div class="no-pad col-sm-5 pull-left">
                    <input class="form-control" type="text" name="email" value="" />
                </div>
                <div class="clearfix"></div>
                <div class="buttons">
                    <a href="<?php echo $back; ?>" class="btn btn-primary"><?php echo $button_back; ?></a>
                    <input type="submit" value="<?php echo $button_continue; ?>" class="btn btn-primary" />
                </div>
            </form>
        </div>
    </div>
</section>
<?php echo $footer; ?>