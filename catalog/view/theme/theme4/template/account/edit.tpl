<?php echo $header; ?>
<section>
    <div class="container">
        <h2>Edit <?php echo $heading_title ?></h2>
        <?php if ($error_warning) { ?>
        <div class="alert alert-danger">
            <?php echo $error_warning; ?>
            <button type="button" class="close" data-dismiss="alert">&times;</button>
        </div>
        <?php } ?>

        <div class="row">


            <!-- Page content -->
            <div class="account-content">

                <div class="col-md-3">
                    <?php echo $column_left ?>
                </div>
                <div class="col-md-9 co-box">
                    <div>
                        <!-- Your details -->
                        <form role="form" action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="edit">
                            <div class="form-group">
                                <label for="firstname" class="required"><?php echo $entry_firstname; ?></label>
                                <input type="text" class="form-control" id="firstname" name="firstname" value="<?php echo $firstname; ?>" autofocus />
                                <?php if ($error_firstname) { ?>
                                <span class="error"><?php echo $error_firstname; ?></span>
                                <?php } ?>
                            </div>
                            <div class="form-group">
                                <label for="email" class="required"><?php echo $entry_email; ?></label>
                                <input type="email" class="form-control" id="email" name="email" value="<?php echo $email; ?>" />
                                <?php if ($error_email) { ?>
                                <span class="error"><?php echo $error_email; ?></span>
                                <?php } ?>
                            </div>
                            <div class="form-group">
                                <label for="telephone" class="required"><?php echo $entry_telephone; ?></label>
                                <input type="tel" class="form-control" id="telephone" name="telephone" value="<?php echo $telephone; ?>" />
                                <?php if ($error_telephone) { ?>
                                <span class="error"><?php echo $error_telephone; ?></span>
                                <?php } ?>
                            </div>
                                <a href="<?php echo $back; ?>" class="btn btn-primary"><?php echo $button_back; ?></a>
                                <a style='background:palegreen;color:black;font-weight: bold' onclick="$('#edit').submit();" class="btn btn-primary">Save</a>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php echo $footer; ?>