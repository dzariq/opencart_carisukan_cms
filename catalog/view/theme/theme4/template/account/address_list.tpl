<?php echo $header; ?>
<section>
    <div class="container">
        <h2><?php echo $heading_title; ?></h2>

        <div class="row">
            <?php if ($success) { ?>
            <div class="alert alert-success">
                <?php echo $success; ?>
                <button type="button" class="close" data-dismiss="alert">&times;</button>
            </div>
            <?php } ?>
            <?php if ($error_warning) { ?>
            <div class="alert alert-danger">
                <?php echo $error_warning; ?>
                <button type="button" class="close" data-dismiss="alert">&times;</button>
            </div>
            <?php } ?>

            <div class="col-md-3">
                <?php echo $column_left ?>
            </div>
            <div id="content" class="col-md-9 co-box">
                <div>
                    <?php echo $content_top; ?>

                    <?php foreach ($addresses as $result) { ?>
                    <table style="width: 100%; margin-bottom: 15px;">
                        <tr>
                            <td style="text-align: left;"><?php echo $result['address']; ?></td>
                           
                        </tr>
                    </table>
                    <a href="<?php echo $back; ?>" class="btn btn-primary"><?php echo $button_back; ?></a>
                    <a style='background:palegreen;color:black;font-weight: bold' href="<?php echo $result['update']; ?>" type="button" class="btn btn-primary"><?php echo $button_edit; ?></a> 
                    <?php } ?>
                    <!--  <div class="pull-right">
                          <a href="<?php echo $insert; ?>" class="btn btn-primary"><?php echo $button_new_address; ?></a>
                      </div> -->
                    <?php echo $content_bottom; ?>
                </div>
            </div>
        </div>
    </div>
</section>
<?php echo $footer; ?>