<?php echo $header; ?>
<section>
    <div class="container">
        <h2><?php echo $heading_title; ?></h2>

        <div class="row">
            <div class="col-md-3">
                <?php echo $column_left ?>
            </div>
            <div id="content" class="col-md-9 co-box">
                <div>
                    <?php echo $content_top; ?>
                    <form role="form" action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="address">
                        <h2><?php echo $text_edit_address; ?></h2>
                        <div class="form-group">
                            <label for="firstname" class="required"><?php echo $entry_firstname; ?></label>
                            <input type="text" class="form-control" id="firstname" name="firstname" value="<?php echo $firstname; ?>" autofocus />
                            <?php if ($error_firstname) { ?>
                            <span class="error"><?php echo $error_firstname; ?></span>
                            <?php } ?>
                        </div>
                        <br/>
                        <div class="form-group">
                            <label for="address_1" class="required"><?php echo $entry_address_1; ?></label>
                            <input type="text" class="form-control" id="address_1" name="address_1" value="<?php echo $address_1; ?>" />
                            <?php if ($error_address_1) { ?>
                            <span class="error"><?php echo $error_address_1; ?></span>
                            <?php } ?>
                        </div>
                        <br/>

                        <div class="form-group">
                            <label for="city" class="required"><?php echo $entry_city; ?></label>
                            <input type="text" class="form-control" id="city" name="city" value="<?php echo $city; ?>" />
                            <?php if ($error_city) { ?>
                            <span class="error"><?php echo $error_city; ?></span>
                            <?php } ?>
                        </div>
                        <br/>

                        <div class="form-group">
                            <label id="postcode-required" for="postcode" class="required"><?php echo $entry_postcode; ?></label>
                            <input type="text" class="form-control" id="postcode" name="postcode" value="<?php echo $postcode; ?>" />
                            <?php if ($error_postcode) { ?>
                            <span class="error"><?php echo $error_postcode; ?></span>
                            <?php } ?>
                        </div>
                        <div class="form-group">
                            <label for="countr_id" class="required"><?php echo $entry_country; ?></label>
                            <select class="form-control" id="country_id" name="country_id">
                                <option value=""><?php echo $text_select; ?></option>
                                <?php foreach ($countries as $country) { ?>
                                <?php if ($country['country_id'] == $country_id) { ?>
                                <option value="<?php echo $country['country_id']; ?>" selected="selected"><?php echo $country['name']; ?></option>
                                <?php } else { ?>
                                <option value="<?php echo $country['country_id']; ?>"><?php echo $country['name']; ?></option>
                                <?php } ?>
                                <?php } ?>
                            </select>
                            <?php if ($error_country) { ?>
                            <span class="error"><?php echo $error_country; ?></span>
                            <?php } ?>
                        </div>
                        <br/>

                        <div class="form-group">
                            <label for="zone_id" class="required"><?php echo $entry_zone; ?></label>
                            <select class="form-control" id="zone_id" name="zone_id"></select>
                            <?php if ($error_zone) { ?>
                            <span class="error"><?php echo $error_zone; ?></span>
                            <?php } ?>
                        </div>

                        <input style='visibility: hidden' type="radio" id="default" name="default" value="1" checked="checked" />

                </div>
                <a href="<?php echo $back; ?>" class="btn btn-primary"><?php echo $button_back; ?></a>
                <a style='background:palegreen;color:black;font-weight: bold' onclick="$('#address').submit();" class="btn btn-primary">Save</a>
            </div>
            </form>
            <?php echo $content_bottom; ?>
        </div>
    </div>
</div>
<script type="text/javascript"><!--
$('select[name=\'country_id\']').bind('change', function () {
        $.ajax({
            url: 'index.php?route=account/address/country&country_id=' + this.value,
            dataType: 'json',
            beforeSend: function () {
                $('select[name=\'country_id\']').after('<span class="wait">&nbsp;<img src="catalog/view/theme/default/image/loading.gif" alt="" /></span>');
            },
            complete: function () {
                $('.wait').remove();
            },
            success: function (json) {
                if (json['postcode_required'] == '1') {
                    $('#postcode-required').show();
                } else {
                    $('#postcode-required').hide();
                }

                html = '<option value=""><?php echo $text_select; ?></option>';

                if (json['zone'] != '') {
                    for (i = 0; i < json['zone'].length; i++) {
                        html += '<option value="' + json['zone'][i]['zone_id'] + '"';

                        if (json['zone'][i]['zone_id'] == '<?php echo $zone_id; ?>') {
                            html += ' selected="selected"';
                        }

                        html += '>' + json['zone'][i]['name'] + '</option>';
                    }
                } else {
                    html += '<option value="0" selected="selected"><?php echo $text_none; ?></option>';
                }

                $('select[name=\'zone_id\']').html(html);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    });

    $('select[name=\'country_id\']').trigger('change');
    //--></script>
</div>
</section>
<?php echo $footer; ?>