<!DOCTYPE html>

<!--[if IE 7]>                  <html class="ie7 no-js" lang="<?php echo $lang; ?>" dir="<?php echo $direction; ?>">     <![endif]-->
<!--[if lte IE 8]>              <html class="ie8 no-js" lang="<?php echo $lang; ?>" dir="<?php echo $direction; ?>">     <![endif]-->
<html class="not-ie no-js" lang="<?php echo $lang; ?>" dir="<?php echo $direction; ?>" xmlns="http://www.w3.org/1999/xhtml"
      xmlns:fb="http://ogp.me/ns/fb#" >  <!--<![endif]-->
    <head>
        <!-- Always force latest IE rendering engine (even in intranet) & Chrome Frame.
        Remove meta X-UA-Compatible if you use the .htaccess
        ================================================== -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>

        <meta property="og:title" content="<?php echo $title ?>"/>
        <meta property="og:type" content="article" />
        <meta property="og:image" content="<?php echo $pageimage ?>" />
        <meta property="og:site_name" content="<?php echo $this->config->get('config_name') ?>"/>
        <meta property="og:description" content="<?php echo $description ?>" />
        <meta property="og:url" content="<?php echo $currentUrl ?>" />
        <meta property="og:site_name" content="<?php echo $this->config->get('config_name') ?>" />
        <meta property="fb:app_id" content="347794171954162" />
        <!-- Mobile Specific Metas
        ================================================== -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <!-- Basic Page Needs
        ================================================== -->
        <title><?php echo $title; ?></title>

        <base href="<?php echo $base; ?>" />

        <?php if ($description) {
        ?>
        <meta name="description" content="<?php echo $description; ?>" />
        <?php } ?>
        <?php if ($keywords) {
        ?>
        <meta name="keywords" content="<?php echo $keywords; ?>" />
        <?php } ?>
        <meta name="author" content="<?php echo $name; ?>"/>

        <!-- Favicons
        ================================================== -->
        <?php if ($icon) { ?>
        <link href="<?php echo $icon; ?>" rel="icon" />
        <link rel="shortcut icon" href="<?php echo $icon; ?>" />
        <link rel="apple-touch-icon" href="<?php echo $icon; ?>" />
        <?php } ?>

        <?php echo $google_analytics; ?>

        <!-- !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! COMMON Stylesheets !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
================================================== -->

        <link rel="stylesheet" type="text/css" href="<?php echo $theme_path ?>css/bootstrap.min.css" media="screen, projection" />

        <!-- !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! COMMON Stylesheets !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ================================================== -->

        <!-- !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! COMMON JS !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ================================================== -->

        <script src="<?php echo $theme_path ?>/js/jquery.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>
        <script src="<?php echo $theme_path ?>/js/bootstrap.min.js"></script>
        <script src="<?php echo $theme_path ?>/js/jquery.prettyPhoto.js"></script>
        <script src="<?php echo $theme_path ?>/js/jquery.isotope.min.js"></script>
        <script src="<?php echo $theme_path ?>/js/main.js"></script>
        <script src="<?php echo $theme_path ?>/js/wow.min.js"></script>
        <script src="<?php echo $theme_path ?>/js/pinterest_grid.js"></script>
        <script src="<?php echo $theme_path ?>/js/jquery.auto-complete.js"></script>
        <script src="<?php echo $theme_path ?>/js/timepicker/jquery.timepicker.min.js"></script>
        <script src="<?php echo $theme_path ?>/js/jpreloader.min.js"></script>
        <script src="<?php echo $theme_path ?>/js/responsiveslide.js"></script>
        <script src="<?php echo $theme_path ?>/js/jquery.newsTicker.min.js"></script>

        <script src="http://maps.google.com/maps/api/js?sensor=false&libraries=geometry&v=3.7">
        </script>
        <script src="<?php echo $theme_path ?>/js/blog_search.js"></script>
        <script src="<?php echo $theme_path ?>/js/jquery/colorbox/jquery.colorbox-min.js"></script>
        <script src="<?php echo $theme_path ?>/js/common.js"></script>
        <script src="<?php echo $theme_path ?>/js/bxslider/jquery.bxslider.min.js"></script>
        <script src="<?php echo $theme_path ?>/js/mapplace.js"></script>
        <script type="text/javascript" src="<?php echo $theme_path ?>/js/jquery/ui/jquery.datetimepicker.js"></script>
        <script type="text/javascript" src="<?php echo $theme_path ?>/js/jquery/ui/jquery.slider.js"></script>
        <!-- !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! COMMON JS !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ================================================== -->

        <!-- core CSS -->
        <link href="<?php echo $theme_path ?>/css/font-awesome.min.css" rel="stylesheet" />
        <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/themes/smoothness/jquery-ui.css" />
        <link href="<?php echo $theme_path ?>/css/animate.min.css" rel="stylesheet"/>
        <link href="<?php echo $theme_path ?>/css/prettyPhoto.css" rel="stylesheet"/>
        <link href="<?php echo $theme_path ?>/css/main.css" rel="stylesheet"/>
        <link href="<?php echo $theme_path ?>/css/custom.css" rel="stylesheet"/>
        <link href="<?php echo $theme_path ?>/css/responsive.css" rel="stylesheet"/>
        <link href="<?php echo $theme_path ?>/css/gallerystyle.css" rel="stylesheet"/>
        <link href="<?php echo $theme_path ?>/css/jquery.auto-complete.css" rel="stylesheet"/>
        <link href="<?php echo $theme_path ?>/js/timepicker/jquery.timepicker.min.css" rel="stylesheet"/>
        <link href="<?php echo $theme_path ?>/css/jpreloader.css" rel="stylesheet"/>
        <link href="<?php echo $theme_path ?>/js/jquery/colorbox/colorbox.css" rel="stylesheet"/>
        <link href="<?php echo $theme_path ?>/js/bxslider/jquery.bxslider.css" rel="stylesheet">
            <link href="<?php echo $theme_path ?>/js/jquery/ui/jquery.datetimepicker.css" rel="stylesheet">
                <link href="<?php echo $theme_path ?>/css/fractionslider.css" rel="stylesheet"/>

                <style>
                    .navbar, .recent-work-wrap .overlay,#publish_date{
                        background:#<?php echo $this->config->get('config_main_theme') ?> !Important;
                    }

                    #searchengine_submit{
                        background:white !Important
                    }

                    .inner-search{
                        background:#252122 !Important
                    } 

                    .inner-search h4{
                        color:white !important
                    }

                    .btn-primary,.headertd{
                        //     background:#<?php echo $this->config->get('config_main_theme') ?> !Important;
                    }


                    #bottom {
                        border-bottom: 5px solid #<?php echo $this->config->get('config_main_theme') ?> !Important;
                    }

                    table thead td,.nav-tabs>li.active>a, .nav-tabs>li.active>a:hover, .nav-tabs>li.active
                    {
                        background-color:#<?php echo $this->config->get('config_main_theme') ?> !Important;
                    }
                    .nav-tabs>li.active>a:after{
                        border-color:transparent transparent transparent #<?php echo $this->config->get('config_main_theme') ?> !Important;
                    }

                    h2,#bottom a{
                        color:#333 !Important
                    }

                    .flexcroll{
                        scrollbar-face-color: #367CD2;
                        scrollbar-shadow-color: #FFFFFF;
                        scrollbar-highlight-color: #FFFFFF;
                        scrollbar-3dlight-color: #FFFFFF;
                        scrollbar-darkshadow-color: #FFFFFF;
                        scrollbar-track-color: #FFFFFF;
                        scrollbar-arrow-color: #FFFFFF;
                    }

                    /* Let's get this party started */
                    .flexcroll::-webkit-scrollbar {
                        width: 12px;
                    }

                    /* Track */
                    .flexcroll::-webkit-scrollbar-track {
                        -webkit-box-shadow: inset 0 0 6px rgba(250, 158, 37, 0.3) 
                            -webkit-border-radius: 10px;
                        border-radius: 10px;
                    }

                    /* Handle */
                    .flexcroll::-webkit-scrollbar-thumb {
                        -webkit-border-radius: 10px;
                        border-radius: 10px;
                        background: rgba(250, 158, 37, 0.8);
                        -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.5); 
                    }
                </style>
                </head><!--/head-->

                <body class="flexcroll homepage">

                    <div id="fb-root"></div>
                    <script>(function (d, s, id) {
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id))
                    return;
                js = d.createElement(s);
                js.id = id;
                js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&appId=347794171954162&version=v2.3";
                fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));</script>
                    <header  id="header">
                        <nav class="hidden-sm hidden-md hidden-lg navbar navbar-inverse " role="banner">
                            <div style='padding:0px' class="container">
                                <div  class="navbar-header">
                                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                        <span class="sr-only">Toggle navigation</span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                    </button>
                                </div>

                                <div class="collapse navbar-collapse navbar-left">
                                    <ul class="nav navbar-nav">
                                        <?php  foreach($categories as $category){ ?>
                                        <li><a href="<?php echo $category['href'] ?>"><?php echo $category['name'] ?></a></li>                        
                                        <?php } ?>
                                        <li><a href="simple-blog">What's Hot</a></li>
                                    </ul>
                                </div>
                                <div style="display:none" class="col-sm-3 animated fadeInRight" id="search-box">
                                    <div id="searchform" class="searchform" >
                                        <div style="margin:10px" class="text-center  pull-right input-group">
                                            <input  placeholder="Search" class="form-control" type="text" value="" name="spdt" id="spdt">
                                                <span id="searchpdtsubmit" style="cursor: pointer" class="input-group-addon">
                                                    <i   class="fa fa-search"></i>
                                                </span>
                                        </div>
                                    </div>        
                                </div>
                                <div  class="collapse navbar-collapse navbar-right">
                                    <ul class="nav navbar-nav">

                                        <?php if(!$this->customer->isLogged()){ ?>
                                        <li><a href="<?php echo 'index.php?route=account/login' ?>"><?php echo $text_login ?></a></li>
                                        <li><a href="<?php echo 'index.php?route=account/register' ?>"><?php echo $text_register ?></a></li>
                                        <?php }else{ ?>
                                        <li><a href="<?php echo 'index.php?route=account/logout' ?>"><?php echo $text_logout ?></a></li>
                                        <li><a href="<?php echo 'index.php?route=account/account' ?>"><?php echo $text_account ?></a></li>
                                        <?php } ?>
                                    </ul>

                                </div>



                            </div><!--/.container-->
                        </nav><!--/nav-->
                        <br/>
                        <h1 style="background:#fa9e25;font-weight:normal;text-align: center;margin-top:0px;margin-bottom:0px;color:#252122;font-size:110px;font-family:'stop';cursor:pointer" onclick="window.location = ''" >C<span style="color:#ffffff !Important">S</span><span style="font-size:32px;color:#252122 !Important">.COM</span></h1>
                        <div class="hidden-xs " style="float:right;
                             position:relative;
                             left:-50%;
                             text-align:left;">
                            <ul style='position:relative;left:50%;margin-bottom:20px' class="csmenu text-center nav navbar-nav centered ">
                                <?php  foreach($categories as $category){ ?>
                                <li><a class='btn btn-primary' href="<?php echo $category['href'] ?>"><?php echo $category['name'] ?></a></li>                        
                                <?php } ?>
                                <li><a class='btn btn-primary' href="simple-blog">What's Hot</a></li>
                                <?php if(!$this->customer->isLogged()){ ?>
                                <li><a class='btn btn-primary' href="<?php echo 'index.php?route=account/login' ?>"><?php echo $text_login ?></a></li>
                                <li><a class='btn btn-primary' href="<?php echo 'index.php?route=account/register' ?>"><?php echo $text_register ?></a></li>
                                <?php }else{ ?>
                                <li><a class='btn btn-primary' href="<?php echo 'index.php?route=account/logout' ?>"><?php echo $text_logout ?></a></li>
                                <li><a class='btn btn-primary' href="<?php echo 'index.php?route=account/account' ?>"><?php echo $text_account ?></a></li>
                                <?php } ?>
                            </ul>
                        </div>
                        <div class='clearfix'></div>
                    </header><!--/header-->

                    <script>
                        jQuery(function ($) {
                            $('#maindate').datepicker({
                                minDate: "<?php echo date('d/m/Y', strtotime(' +5 day')) ?>",
                                dateFormat: 'dd/mm/yy'

                            });

                            $('.colorbox').colorbox({
                                overlayClose: true,
                                opacity: 0.5,
                                width: "80%", height: "80%",
                                rel: "colorbox"
                            });

                        });


                    </script>

                    <!-- This section is for Splash Screen -->
                    <section id="jSplash" style="background:white !important">
                        <h1 style="background:#fa9e25;font-weight:normal;text-align: center;margin-top:70px;margin-bottom:0px;color:#252122;font-size:110px;font-family:'stop';cursor:pointer" onclick="window.location = ''" >C<span style="color:#ffffff !Important">S</span><span style="font-size:32px;color:#252122 !Important">.COM</span></h1>
                    </section>