<?php echo $header; ?>
<?php if(isset($this->session->data['last_order_id'])){ ?>
<script>
    window.location = 'http://carisukan.com/index.php?route=account/order/info&order_id=<?php echo $this->session->data["last_order_id"] ?>';
</script>
<?php } ?>
<section>
    <div class="container">
        <div class="row">
            <div id="content" class="col-md-12">
                <h1><?php echo $heading_title ?></h1>
                <?php echo $content_top; ?>
                <section>
                    <?php echo $text_message; ?>
                </section>
                <div class="pull-right">
                    <a href="<?php echo $continue; ?>" class="btn btn-primary"><?php echo $button_continue; ?></a>
                </div>
                <?php echo $content_bottom; ?>
            </div>
        </div>
    </div>
</section>
<?php echo $footer; ?>