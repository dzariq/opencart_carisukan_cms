<?php echo $header ?>
<div class=" container ">
    <div  class='wow fadeInRight col-sm-8 '  >
        <?php echo $column_left ?>
    </div>
    <div class='wow fadeInLeft col-sm-4'  >
        <?php echo $column_right ?>
    </div>
    <div style="margin-top:22px" class='wow fadeInRight col-sm-12 '  >
        <?php echo $content_bottom ?>
    </div>
</div>
<?php echo $footer ?>
