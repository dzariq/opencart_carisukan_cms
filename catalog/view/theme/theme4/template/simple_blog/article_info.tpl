<?php echo $header; ?>
<section id="blog" class="container">
                                    <h2><?php echo $article_info['article_title']; ?></h2>

    <div class="blog">
        <div class="row">
            <div class='col-md-8 col-xs-12' >
                <?php if(isset($article_info_found)) { ?>
                <div class="blog-item">
                    <script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
                    <script type="text/javascript">stLight.options({publisher: "ur-d825282d-618f-598d-fca6-d67ef9e76731", doNotHash: true, doNotCopy: true, hashAddressBar: false});</script>
                    <span class='st_facebook' displayText=''></span>
                    <span class='st_twitter' displayText=''></span>
                    <span class='st_linkedin' displayText=''></span>
                    <span class='st_googleplus' displayText=''></span>
                    <span class='st_pinterest' displayText=''></span>

                    <div class="row">  
                        <div class="col-xs-12 col-sm-2 text-center">
                            <div class="entry-meta">
                                <span id="publish_date"><?php echo date('F jS, Y', strtotime($article_info['date_modified'])); ?></span>
                                <span><i class="fa fa-user"></i> <a href="<?php echo $author_href ?>"> <?php echo $article_info[author_name] ?></a></span>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-10 co-box blog-content">
                            <div>
                                <img style='margin:0 auto' class='img-responsive img-blog' src="<?php echo $imagereal; ?>" alt="<?php echo $article_info['article_title']; ?>" height="500" />
                                <br/>
                                <br/>
                                <?php echo html_entity_decode($article_info['description'], ENT_QUOTES, 'UTF-8'); ?>
                            </div>
                        </div>
                    </div>
                </div><!--/.blog-item-->

                <?php if($article_info['allow_comment']) { ?>
                <div class="box">
                    <div class="box-heading" style='font-size:16px'><?php echo $total_comment; ?>:</div>
                    <br/>
                    <div class="box-content">
                        <div id="comments" class="blog-comment-info">
                            <div id="comment-section"></div>
                            <div id="comment-list"></div>
                            <div id='your-comment'>
                                <h4 id="review-title">
                                    <?php echo $text_write_comment; ?>
                                    <img src="<?php echo HTTP_SERVER; ?>catalog/view/theme/default/image/remove.png" alt="Remove" id="reply-remove" style="display:none;" onclick="removeCommentId();" />
                                </h4>							
                                <input type="hidden" name="blog_article_reply_id" value="0" id="blog-reply-id"/>

                                <div class="row">
                                    <div class="col-sm-5">
                                        <div class="form-group">
                                            <label><?php echo $entry_name; ?> *</label>
                                            <input class='form-control' type="text" name="name" value="" />								
                                        </div>
                                        <div class="form-group">
                                            <label><?php echo $entry_captcha; ?> *</label>
                                            <input class='form-control' type="text" name="captcha" style="" value="" /><br />
                                            <img src="index.php?route=product/product/captcha" alt="" id="captcha" />
                                        </div>
                                    </div>
                                    <div class="col-sm-7">
                                        <div class="form-group">
                                            <label><?php echo $entry_review; ?> *</label>
                                            <textarea  class='form-control' name="text" rows="8" ></textarea>
                                            <span style="font-size: 11px;"><?php echo $text_note; ?></span>
                                        </div>
                                        <div class="form-group">
                                            <button id="button-comment" class="btn btn-lg btn-primary">
                                                <span><?php echo $button_submit; ?></span>
                                            </button>
                                        </div>

                                    </div>
                                </div>

                            </div>

                        </div>
                    </div>
                    <?php } ?>
                </div>
                <?php } else { ?>
                <div class="buttons">
                    <div class="center">
                        <?php echo $text_no_found; ?>
                    </div>
                </div>
                <?php } ?>

            </div>
            <div class='col-md-4 col-xs-12'>
                <?php echo $column_right; ?>

            </div>
        </div>
    </div>
</section>


<script type="text/javascript">
    function removeCommentId() {
        $("#blog-reply-id").val(0);
        $("#reply-remove").css('display', 'none');
    }
</script>

<script type="text/javascript">
    $('#comment-list .pagination a').click(function () {
        $('#comment-list').fadeOut('slow');

        $('#comment-list').load(this.href);

        $('#comment-list').fadeIn('slow');

        return false;
    });

    $('#comment-list').load('index.php?route=simple_blog/article/comment&simple_blog_article_id=<?php echo $simple_blog_article_id; ?>');

</script>		


<script type="text/javascript">
    $('#button-comment').bind('click', function () {
        $.ajax({
            type: 'POST',
            url: 'index.php?route=simple_blog/article/writeComment&simple_blog_article_id=<?php echo $simple_blog_article_id; ?>',
            dataType: 'json', data: 'name=' + encodeURIComponent($('input[name=\'name\']').val()) + '&text=' + encodeURIComponent($('textarea[name=\'text\']').val()) + '&captcha=' + encodeURIComponent($('input[name=\'captcha\']').val()) + '&reply_id=' + encodeURIComponent($('input[name=\'blog_article_reply_id\']').val()),
            beforeSend: function () {
                $('.success, .warning').remove();
                $('#button-comment').attr('disabled', true);
                $('#review-title').after('<div class="attention"><img src="catalog/view/theme/default/image/loading.gif" alt="" /> <?php echo $text_wait; ?></div>');
            },
            complete: function () {
                $('#button-comment').attr('disabled', false);
                $('.attention').remove();
            },
            success: function (data) {
                if (data['error']) {
                    $('#review-title').after('<div class="warning">' + data['error'] + '</div>');
                }

                if (data['success']) {
                    $('#review-title').after('<div class="success">' + data['success'] + '</div>');

                    $('input[name=\'name\']').val('');
                    $('textarea[name=\'text\']').val('');
                    $('input[name=\'captcha\']').val('');
                    $("#blog-reply-id").val(0);
                    $("#reply-remove").css('display', 'none');

                    $('#comment-list').load('index.php?route=simple_blog/article/comment&simple_blog_article_id=<?php echo $simple_blog_article_id; ?>');
                }
            }
        });
    });

    function reply_go() {
        $.ajax({
            type: 'POST',
            url: 'index.php?route=simple_blog/article/writeComment&simple_blog_article_id=<?php echo $simple_blog_article_id; ?>',
            dataType: 'json', data: 'name=' + encodeURIComponent($('input[name=\'name\']').val()) + '&text=' + encodeURIComponent($('textarea[name=\'text\']').val()) + '&captcha=' + encodeURIComponent($('input[name=\'captcha\']').val()) + '&reply_id=' + encodeURIComponent($('input[name=\'blog_article_reply_id\']').val()),
            beforeSend: function () {
                $('.success, .warning').remove();
                $('#review-title-reply').after('<div class="attention"><img src="catalog/view/theme/default/image/loading.gif" alt="" /> <?php echo $text_wait; ?></div>');
            },
            complete: function () {
                $('#button-comment-reply').attr('disabled', false);
                $('.attention').remove();
            },
            success: function (data) {
                if (data['error']) {
                    $('#review-title-reply').after('<div class="warning">' + data['error'] + '</div>');
                }

                if (data['success']) {
                    $('#review-title-reply').after('<div class="success">' + data['success'] + '</div>');

                    $('input[name=\'name\']').val('');
                    $('textarea[name=\'text\']').val('');
                    $('input[name=\'captcha\']').val('');

                    $('#comment-list').load('index.php?route=simple_blog/article/comment&simple_blog_article_id=<?php echo $simple_blog_article_id; ?>');
                }
            }
        })
    }
</script> 

<?php echo $footer; ?>	