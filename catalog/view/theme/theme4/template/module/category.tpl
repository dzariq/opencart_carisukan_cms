<div class='clearfix'></div>
<div class="sidey">
    <ul class="nav">
        <?php foreach($categories as $category){ ?>
        <li><a style='font-size:21px;color:black' href="<?php echo $category['href']; ?>">&nbsp;<?php echo $category['name']; ?></a>
            <?php if ($category['children']) { ?>
            <ul>
                <?php  foreach ($category['children'] as $child) { ?>
                <li><a href="<?php echo $child['href']; ?>"><?php echo $child['name']; ?></a></li>
                <?php } ?>
            </ul>
            <?php } ?>
        </li>
        <?php } ?>
    </ul>
</div>
<div class='clearfix'></div>
