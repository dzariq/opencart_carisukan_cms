<div class="clearfix"></div>
<style>

    @media(max-width:530px){

        .downbtn{
            margin:75% auto !Important;
            width:20%
        }
        .desc{
            width:80%;
            margin:-10% auto !Important
        }
        .logotop{
            width:30%;
            margin:20% auto !Important
        }
    }

    .bx-pager-item{
        display:none !Important;
    }

    .icon-sports img:hover{
        opacity: 1
    }
    .icon-sports img{
        cursor:pointer;
        opacity:0.3
    }
    .icon-sports .active{
        opacity:1.0
    }

</style>
<div class='icon-sports text-center'>
    <img title='My Location' onclick='changeType("0",this)' class='active' style='width:10%;margin-right:20px' src='image/icon/icon-my.jpg' />
    <img title='Futsal' onclick='changeType("A",this)' style='width:10%;margin-right:20px' src='image/icon/icon-football.jpg' />
    <img title='Tennis' onclick='changeType("B",this)' style='width:10%;margin-right:20px' src='image/icon/icon-tennis.jpg' />
    <img title='Badminton' onclick='changeType("C",this)' style='width:10%;margin-right:20px' src='image/icon/icon-badminton.jpg' />
</div>
<br/>
<div style="height:520px" id="gmap"></div>
<div id="controls"></div>
<article>
    <p>Finding your location: <span id="status">checking...</span></p>
</article>

<script>


    var Locs0 = <?php echo $productsAll ?> ;
    var LocsA = <?php echo $productsFutsal ?> ;
            var LocsB = <?php echo $productsTennis ?> ;
            var LocsC = <?php echo $productsBadminton ?> ;
            console.log(Locs0)
            console.log(LocsA)
            function changeType(type,item) {
                $(".icon-sports img").removeClass("active");
                $(item).addClass('active')
                $('#gmap').empty()
                $('.controls').empty()
                if (navigator.geolocation) {
                if(type=='A'){
                    navigator.geolocation.getCurrentPosition(mapDrawA, mapDrawError);
                }else if(type == 'B'){
                    navigator.geolocation.getCurrentPosition(mapDrawB, mapDrawError);
                }else if(type == 'C'){
                    navigator.geolocation.getCurrentPosition(mapDrawC, mapDrawError);
                }else{
                    navigator.geolocation.getCurrentPosition(mapDraw0, mapDrawError);
                }
                } else {
                    alert('not supported');
                }
            }

    $(document).ready(function () {

        geocoder = new google.maps.Geocoder();


        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(mapDraw0, mapDrawError);
        } else {
            alert('not supported');
        }

        //  mapDraw();



    });
  function mapDraw0(position) {
        var lat = position.coords.latitude;
        var lng = position.coords.longitude;
        codeLatLng(lat, lng)
        var s = document.querySelector('#status');
        s.innerHTML = typeof msg == 'string' ? msg : "success";
        s.className = 'success';
        new Maplace({
            map_options: {
                set_center: [lat, lng],
                zoom: 11
            },
            locations: Locs0,
            map_div: '#gmap',
            controls_title: 'Choose location:',
            listeners: {
                click: function (map, event) {
                    map.setOptions({scrollwheel: false});
                }
            }
        }).Load();
    }
    function mapDrawA(position) {
        var lat = position.coords.latitude;
        var lng = position.coords.longitude;
        codeLatLng(lat, lng)
        var s = document.querySelector('#status');
        s.innerHTML = typeof msg == 'string' ? msg : "success";
        s.className = 'success';
        new Maplace({
            locations: LocsA,
            map_div: '#gmap',
            controls_title: 'Choose location:',
            listeners: {
                click: function (map, event) {
                    map.setOptions({scrollwheel: false});
                }
            }
        }).Load();
    }
  
    function mapDrawB(position) {
        var lat = position.coords.latitude;
        var lng = position.coords.longitude;
        codeLatLng(lat, lng)
        var s = document.querySelector('#status');
        s.innerHTML = typeof msg == 'string' ? msg : "success";
        s.className = 'success';
        new Maplace({
          
            locations: LocsB,
            map_div: '#gmap',
            controls_title: 'Choose location:',
            listeners: {
                click: function (map, event) {
                    map.setOptions({scrollwheel: false});
                }
            }
        }).Load();
    }
    function mapDrawC(position) {
        var lat = position.coords.latitude;
        var lng = position.coords.longitude;
        codeLatLng(lat, lng)
        var s = document.querySelector('#status');
        s.innerHTML = typeof msg == 'string' ? msg : "success";
        s.className = 'success';
        new Maplace({
           
            locations: LocsC,
            map_div: '#gmap',
            controls_title: 'Choose location:',
            listeners: {
                click: function (map, event) {
                    map.setOptions({scrollwheel: false});
                }
            }
        }).Load();
    }


    function mapDrawError(msg) {
        var s = document.querySelector('#status');
        s.innerHTML = typeof msg == 'string' ? msg : "failed";
        s.className = 'fail';
        alert('failed')

        // console.log(arguments);
    }

    function codeLatLng(lat, lng) {

        var latlng = new google.maps.LatLng(lat, lng);
        geocoder.geocode({'latLng': latlng}, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                console.log(results)
                if (results[1]) {
                    //formatted address
                    //  alert(results[0].formatted_address)
                    //find country name
                    for (var i = 0; i < results[0].address_components.length; i++) {
                        for (var b = 0; b < results[0].address_components[i].types.length; b++) {

                            //there are different types that might hold a city admin_area_lvl_1 usually does in come cases looking for sublocality type will be more appropriate
                            if (results[0].address_components[i].types[b] == "administrative_area_level_1") {
                                //this is the object you are looking for
                                city = results[0].address_components[i];
                                break;
                            }
                        }
                    }
                    //city data
                    //  alert(city.short_name + " " + city.long_name)
                    var s = document.querySelector('#status');
                    s.innerHTML = results[0].formatted_address;
                    s.className = 'success';

                } else {
                    alert("No results found");
                }
            } else {
                alert("Geocoder failed due to: " + status);
            }
        });
    }

</script>