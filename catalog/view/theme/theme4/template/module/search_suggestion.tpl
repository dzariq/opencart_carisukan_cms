<?php

if(isset($this->request->get["time"])){
$time = $this->request->get["time"];
}else{
$time = "";
}
if(isset($this->request->get["checkin"])){
$checkin = $this->request->get["checkin"];
}else{
$checkin = "";
}

?>
<div style="padding:0px" id='searchengine' class="col-sm-12"   >
    <div class="inner-search col-sm-12"   >
        <h2 style="margin:0px;text-align: center;color:white !Important;margin-bottom:20px">Find Me a Court</2>
            <h5 style="color:white;margin-bottom:8px;">Sports</h5>
            <div style="padding-left:0px;margin-bottom:10px"  class="col-md-12">
                <select onchange="javascript:$('#name-search').val('')" class="form-control" style='width:30%' id="category-search" name='category-search' >
                    <?php foreach($categories as $category){ ?>
                    <?php if($this->request->get['filter_category_id'] == $category[category_id]){ ?>
                    <option selected="selected" value='<?php echo $category["category_id"] ?>'><?php echo $category["name"] ?></option>
                    <?php }else{ ?>
                    <option value='<?php echo $category["category_id"] ?>'><?php echo $category["name"] ?></option>
                    <?php } ?>
                    <?php } ?>
                </select>
            </div>
            <div class='clearfix'></div>
            <h5 style="color:white;margin-bottom:4px;">Location/Name</h5>
            <div style="padding-left:0px;margin-bottom:6px" class="col-md-12 ">
                <input placeholder='e.g: Subang Jaya, Ferro Futsal' type="text" id="name-search" class="form-control" name="name" value="<?php echo isset($this->request->get['filter_name']) ?  $this->request->get['filter_name'] : ''; ?>" placeholder="Search" />
                <input type="hidden" id="redirect-hidden" value=""  />
            </div>
            <div class='clearfix'></div>
            <div style="padding-left:0px;margin-bottom:6px"  class="col-md-5 ">
                <h5 style="color:white;margin-bottom:4px;" >Date</h5>
                <input type='text' value='<?php echo $checkin ?>' id='maindate' class='date form-control' name='date' />
            </div>
            <div style="padding-left:0px;margin-bottom:6px"  class="col-md-5 ">
                <h5 style="color:white;margin-bottom:4px;" >Time</h5>
                <select id='time' class='form-control'>
                    <option <?php if($time == 9){ ?> selected="selected" <?php } ?> value='9' >9:00 am</option>
                    <option <?php if($time == 10){ ?> selected="selected" <?php } ?> value='10' >10:00 am</option>
                    <option <?php if($time == 11){ ?> selected="selected" <?php } ?> value='11' >11:00 am</option>
                    <option <?php if($time == 12){ ?> selected="selected" <?php } ?> value='12' >12:00 am</option>
                    <option <?php if($time == 13){ ?> selected="selected" <?php } ?> value='13' >1:00 pm</option>
                    <option <?php if($time == 14){ ?> selected="selected" <?php } ?> value='14' >2:00 pm</option>
                    <option <?php if($time == 15){ ?> selected="selected" <?php } ?> value='15' >3:00 pm</option>
                    <option <?php if($time == 16){ ?> selected="selected" <?php } ?> value='16' >4:00 pm</option>
                    <option <?php if($time == 17){ ?> selected="selected" <?php } ?> value='17' >5:00 pm</option>
                    <option <?php if($time == 18){ ?> selected="selected" <?php } ?> value='18' >6:00 pm</option>
                    <option <?php if($time == 19){ ?> selected="selected" <?php } ?> value='19' >7:00 pm</option>
                    <option <?php if($time == 20){ ?> selected="selected" <?php } ?> value='20' >8:00 pm</option>
                    <option <?php if($time == 21){ ?> selected="selected" <?php } ?> value='21' >9:00 pm</option>
                    <option <?php if($time == 22){ ?> selected="selected" <?php } ?> value='22' >10:00 pm</option>
                    <option <?php if($time == 23){ ?> selected="selected" <?php } ?> value='23' >11:00 pm</option>
                </select>
            </div>
            <div style="padding-left:0px;"  class="col-md-2 ">
                <h5 style="color:white;margin-bottom:4px;margin-top:0px" >&nbsp;</h5>
                <button id="searchengine_submit" style=" padding: 7px;font-weight: bold;font-size:14px;color:black;" class="btn btn-primary"  >Go</button>
            </div>
            <div class="clearfix"></div>
    </div>
</div>
<style type="text/css">

    .ui-menu-item{
        text-align:left;
        z-index: 160;
    }
    .ui-autocomplete{
        text-align:left;
        z-index: 160;
    }
</style>
<script type="text/javascript"><!--
//########################################################################
// Module: Search Autocomplete
//########################################################################


    $('#maindate').datepicker({
        stepMinute: '60',
        showMinute: false,
        minTime: '09:00',
        minDate: "<?php echo date('d/m/Y') ?>",
        dateFormat: 'dd/mm/yy',
    });

    $(document).ready(function () {
        $('#name-search').autocomplete({
            delay: 500,
            source: function (request, response) {
                $.ajax({
                    url: "<?php echo $search_json;?>",
                    dataType: "json",
                    data: {
                        keyword: request.term,
                        category_id: $('#category-search').val()

                    },
                    success: function (data) {
                        response($.map(data.result, function (item) {
                            return {
                                label: item.name,
                                type: item.type,
                                value: item.href
                            }
                        }));
                    }
                });
            },
            minLength: 3,
            focus: function (event, ui) {
                return false;
            },
            select: function (event, ui) {
                if (ui.item.value == "") {
                    return false;
                } else {
                    if (ui.item.type == 'more') {
                        location.href = ui.item.value;
                    }
                    $('#name-search').val(ui.item.label)
                    $('#redirect-hidden').val(ui.item.value)
                    return false;
                }
            },
            open: function () {
                $(this).removeClass("ui-corner-all").addClass("ui-corner-top");
                $('.ui-autocomplete').css("z-index", "99");
            },
            close: function () {
                $(this).removeClass("ui-corner-top").addClass("ui-corner-all");
            }
        }).data("ui-autocomplete")._renderItem = function (ul, item) {
            return $("<li></li>")
                    .data("item.autocomplete", item)
                    .append("<a>" + item.label + "</a>")
                    .appendTo(ul);
        };
    })
//########################################################################
// Module: Search Autocomplete
//########################################################################
//--></script>