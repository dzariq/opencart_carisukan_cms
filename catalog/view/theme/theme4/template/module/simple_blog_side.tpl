<div class="widget categories">
    <h2 style="margin-top:0px"><?php echo $heading_title; ?></h2>
    <?php  if($articles) { ?>
    <div class="row">
        <div class="col-sm-12 co-box">
            <div>
                <?php foreach ($articles as $article) { ?>
                <div style='height:50px' class="single_comments">
                    <div class='col-xs-2' style="padding:0px">
                        <a href="<?php echo $article['href'] ?>">
                            <img class="img-responsive" src="<?php echo $article[image]; ?>" alt=""  />
                        </a>
                    </div>
                    <div class='col-xs-10'>
                        <p>
                            <a href="<?php echo $article['href'] ?>">
                                <?php echo $article['article_title'] ?>
                            </a>
                        </p>
                        <div class="entry-meta small muted">
                            <span style='text-align: left'><?php echo $article['description'] ?></span>
                        </div>
                    </div>
                </div>
                <div class='clearfix'></div>
                <?php } ?>
            </div>
        </div>
    </div>
    <?php } else { ?>
    <div class="center">
        <?php echo $text_no_found; ?>
    </div>
    <?php } ?>
</div>