<?php echo $header;
if(isset($_GET['checkin'])){ 
$datein = $_GET['checkin'];
}else{
$datein = date('d/m/Y');
}
?>
<input type="hidden" id='product_id' name="product_id" size="2" value="<?php echo $product_id; ?>" />
<input type='hidden' value='1' id="quantity" name="quantity"  />
<input type='hidden' value='<?php echo $release  ?>' id="mpn" name="mpn"  />
<section >
    <div class="no-pad container">
        <div class=" product-info col-md-12 no-pad">
            <div class="single-item ">
                <div  class="col-sm-8 col-xs-12">
                    <br/>
                    <div class="row">
                        <div id="product-slide" class="col-sm-6 co-box">
                            <div>
                                <ul class="rslides">
                                    <?php foreach ($images as $banner){ ?>
                                    <li><img class='img-responsive' src="<?php echo $banner['image'] ?>" alt=""></li>
                                    <?php } ?>
                                </ul>
                            </div>
                        </div>
                        <div class="col-sm-6 co-box centered">
                            <div id="news-container">
                                <i class="fa fa-arrow-up" id="news-prev"></i>
                                <ul style="padding-left:0px" id="news">
                                    <li>Etiam imperdiet volutpat libero eu tristique. Aenean, rutrum felis in.</li>
                                    <li>Curabitur porttitor ante eget hendrerit adipiscing. Maecenas at magna.</li>
                                    <li>Praesent ornare nisl lorem, ut condimentum lectus gravida ut.</li>
                                    <li>Nunc ultrices tortor eu massa placerat posuere. Vivamus viverra sagittis.</li>
                                    <li>Morbi sodales tellus sit amet leo congue bibendum. Ut non mauris eu neque.</li>
                                    <li>In pharetra suscipit orci sed viverra. Praesent at sollicitudin tortor, id.</li>
                                    <li>Maecenas nec ligula sed est suscipit aliquet sed eget ipsum, suspendisse.</li>
                                    <li>Onec bibendum consectetur diam, nec euismod urna venenatis eget..</li>
                                </ul>
                                <i class="fa fa-arrow-down" id="news-next"></i>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <?php if(count($courts) != ''){ ?>
                    <img style="margin-bottom: 5px;" src="catalog/view/theme/theme4/images/stepbooking.jpg" class="img-responsive" />
                    <?php } ?>
                    <div class="co-box col-xs-12 no-pad">
                        <div>
                            <img style='margin:0 auto' class="img-responsive" src="<?php echo $thumbpopup ?>" />
                        </div>
                    </div>
                    <?php if(count($courts) != ''){ ?>

                    <div class="co-box col-xs-12 no-pad">
                        <div style="height:1000px">
                            <h2>Choose Court</h2>
                            <ul class='legend' style="margin-left: -56px;">
                                <li class="col-sm-2 col-xs-4">
                                    <div class="input-color">
                                        <input readonly="readonly" type="text" value="Available Slots" />
                                        <div class="color-box" style="border:1px solid black;background-color: white;"></div>
                                    </div>
                                </li>
                                <li class="col-sm-2  col-xs-4">
                                    <div class="input-color">
                                        <input readonly="readonly" type="text" value="Booked" />
                                        <div class="color-box" style="border:1px solid black;background-color: red;"></div>
                                        <!-- Replace "navy" to change the color -->
                                    </div>
                                </li>
                                <li class="col-sm-2  col-xs-4">
                                    <div class="input-color">
                                        <input readonly="readonly" type="text" value="Not Available" />
                                        <div class="color-box" style="border:1px solid black;background-color: grey;"></div>
                                        <!-- Replace "navy" to change the color -->
                                    </div>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                            <div style="overflow-x:scroll;" class=" flexcroll no-pad col-xs-12  pull-left">
                                <?php include "catalog/view/theme/theme4/template/product/product_types.tpl" ?>
                            </div>
                            <span style='font-size:11px;'> By clicking Book Now, you are agree with the</span>
                            <a class="colorbox cboxElement" href="http://carisukan.com/index.php?route=information/information/info&amp;information_id=13" alt="Terms &amp; Conditions"><b>carisukan.com TNC</b></a>
                            &nbsp;&nbsp;<input style='font-size:14px;padding:8px;margin-bottom:15px;color:black;font-weight: bold;background:palegreen !Important' onclick="gocart()" roomid="<?php echo $room['room_id'] ?>" name="<?php echo $room['caption'] ?>" type="button" class=" book-room btn btn-primary" value="Book Now" />
                        </div>
                    </div>
                    <div class="clearfix"></div>

                    <?php } ?>
                    <div class="clearfix"></div>

                    <div class="tab-wrap co-box">
                        <div class="media">
                            <div class="parrent pull-left">
                                <ul class="nav nav-tabs nav-stacked">
                                    <li class="active"><a href="#tab1" data-toggle="tab" class="analistic-01">Information</a></li>
                                    <li class=""><a href="#tab3" data-toggle="tab" class="analistic-03">Facilities</a></li>
                                </ul>
                            </div>
                            <div class="parrent media-body">
                                <div class="tab-content">
                                    <div class="tab-pane active in" id="tab1">
                                        <?php echo $description; ?>
                                    </div>
                                    <!--                        section faci-->
                                    <div class="tab-pane" id="tab3">
                                        <table style='background:white;width:100%' class='table-striped' >

                                            <tbody>
                                                <?php foreach($section2_images as $fac){ ?>
                                                <tr>
                                                    <td style="width:50%">
                                                        <?php if($fac['image'] != 'no_image.jpg'){ ?>
                                                        <a href='<?php echo $fac[image] ?>' rel='prettyPhoto' >
                                                            <img style='margin:0 auto' class='col-xs-12 img-responsive' src="<?php echo $fac['image'] ?>" />
                                                        </a>
                                                        <?php } ?>
                                                    </td>
                                                    <td style="width:50%">
                                                        <h4><?php echo $fac['caption'] ?></h4>
                                                        <p>
                                                            <?php echo $fac['description'] ?>
                                                        </p>
                                                    </td>
                                                </tr>
                                                <?php } ?>
                                            </tbody>
                                            </tr>

                                        </table>
                                    </div>
                                </div> <!--/.tab-content-->  
                            </div> <!--/.media-body--> 
                        </div> <!--/.media-->     
                    </div><!--/.tab-wrap-->   
                </div>
                <div style="padding:0px" class="col-sm-4 col-xs-12">

                    <?php if (isset($pmLocations) && !empty($pmLocations)) { ?>
                    <div class=' col-xs-12 co-box'>
                        <div style="margin-top:20px;text-align: center"  >
                            <h2>
                                <?php echo $heading_title ?> 
                            </h2>
                            <?php 
                            echo "<h4 style='margin-bottom:20px'>Direction Map</h4>";
                            echo "<p style='margin-bottom:20px'>{$pmLocations[0]['address']}</p>";
                            echo "<p style='margin-bottom:5px'>Contact Number: {$contactnumber}</p>";
                            echo "<p style='margin-bottom:20px'>Email Address: {$email}</p>";
                            echo "<div id='pmMapDiv'  >";
                            echo "<div class='gmap' style='margin: 0 auto; width:300px; height:300px;' id=\"pmMap{$pmLocations[0]['id']}\"></div>";
                            echo "</div>";
                            echo "<div id='pmDirDiv' >";
                            echo "<input  id='pmTo' value='{$pmLocations[0]['address']}' type='hidden' />";
                            echo "<input style='margin-top:20px' class='form-control' id='pmFrom' value='Kuala Lumpur, Malaysia' type='text' />";
                            echo "<input id='pmDirectionsBtn' class='btn btn-primary' type='button' value='$button_get_directions'>";
                            echo "<div id='pmDirectionsList'></div>";
                            echo "</div>";

                            ?>

                            <script>
                                        $(function() {

                                        <?php
                                                echo "var map = createMap('pmMap{$pmLocations[0]['id']}', {$pmLocations[0]['lat']}, {$pmLocations[0]['lng']}, {$pmLocations[0]['zoom']}, {$pmLocations[0]['width']}, {$pmLocations[0]['height']});";
                                                ?>
                                                function createMap(e, lat, lng, zoom, width, height) {
                                                var gmLatlng = new google.maps.LatLng(lat, lng);
                                                        var gmOpt = {
                                                        zoom: zoom,
                                                                center: gmLatlng,
                                                                mapTypeId: google.maps.MapTypeId.ROADMAP
                                                        }
                                                var map = new google.maps.Map(document.getElementById(e), gmOpt);
                                                        var marker = new google.maps.Marker({
                                                        position: gmLatlng,
                                                                map: map,
                                                                draggable:true
                                                        });
                                                        return map;
                                                }


                                        function generateRoute(map) {
                                        $("#pmDirWarning").remove();
                                                $("#pmDirectionsList").empty();
                                                var directionsService = new google.maps.DirectionsService();
                                                var directionsDisplay = new google.maps.DirectionsRenderer();
                                                var start = document.getElementById("pmFrom").value;
                                                var end = document.getElementById("pmTo").value;
                                                var request = {
                                                origin:start,
                                                        destination:end,
                                                        travelMode: google.maps.TravelMode.DRIVING
                                                };
                                                directionsService.route(request, function(result, status) {
                                                if (status == google.maps.DirectionsStatus.OK) {
                                                directionsDisplay.setMap(map);
                                                        directionsDisplay.setPanel(document.getElementById("pmDirectionsList"));
                                                        directionsDisplay.setDirections(result);
                                                } else if (status == google.maps.DirectionsStatus.ZERO_RESULTS) {
                                                $("#pmFrom").before('<div class="warning" id="pmDirWarning"><?php echo $error_route_not_found; ?></div>');
                                                } else if (status == google.maps.DirectionsStatus.NOT_FOUND) {
                                                $("#pmFrom").before('<div class="warning" id="pmDirWarning"><?php echo $error_address_not_found; ?></div>');
                                                } else {
                                                $("#pmFrom").before('<div class="warning" id="pmDirWarning">' + status + '</div>');
                                                }
                                                });
                                        }

                                        $('#pmDirectionsBtn').bind('click', function(event, ui) {
                                        <?php
                                                echo "var map = createMap('pmMap{$pmLocations[0]['id']}', {$pmLocations[0]['lat']}, {$pmLocations[0]['lng']}, {$pmLocations[0]['zoom']}, {$pmLocations[0]['width']}, {$pmLocations[0]['height']});";
                                                ?>
                                                generateRoute(map);
                                        });
                                                $('#tab-maps-load').bind('click', function(event, ui) {
                                        <?php
                                                echo "createMap('pmMap{$pmLocations[0]['id']}', {$pmLocations[0]['lat']}, {$pmLocations[0]['lng']}, {$pmLocations[0]['zoom']}, {$pmLocations[0]['width']}, {$pmLocations[0]['height']});";
                                                ?>
                                        });
                                        });
                                        var nt_example1 = $('#news').newsTicker({
                                row_height: 60,
                                        max_rows: 3,
                                        duration: 4000,
                                        prevButton: $('#news-prev'),
                                        nextButton: $('#news-next')
                                });</script>
                        </div>
                    </div>
                    <?php } ?>
                    <div class="clearfix"></div>
                    <div class="col-xs-12 co-box" id="review">
                    </div>
                    <div class="clearfix"></div>




                    <div class="col-xs-12 co-box">
                        <div >
                            <h2 id="review-title"><?php echo $text_write; ?></h2>
                            <br/>
                            <b><?php echo $entry_name; ?></b><br />
                            <input class='form-control' type="text" name="name" value="" />
                            <br />
                            <b><?php echo $entry_review; ?></b>

                            <textarea  class="grey form-control" name="text" cols="40" rows="3" ></textarea>

                            <span style="font-size: 11px;"><?php echo $text_note; ?></span><br />
                            <br />

                            <b><?php echo $entry_rating; ?></b> <span><?php echo $entry_bad; ?></span>&nbsp;

                            <input class="ratingbox" type="radio" name="rating" value="1" />

                            &nbsp;

                            <input class="ratingbox" type="radio" name="rating" value="2" />

                            &nbsp;

                            <input class="ratingbox" type="radio" name="rating" value="3" />

                            &nbsp;

                            <input class="ratingbox" type="radio" name="rating" value="4" />

                            &nbsp;

                            <input class="ratingbox" type="radio" name="rating" value="5" />

                            &nbsp;<span><?php echo $entry_good; ?></span><br />

                            <br />

                            <b><?php echo $entry_captcha; ?></b><br />

                            <input class='form-control' type="text" name="captcha" value="" />

                            <br />

                            <img src="index.php?route=product/product/captcha" alt="" id="captcha" /><br />

                            <br />

                            <div class="buttons">

                                <div class="right"><a id="button-review" style="cursor:pointer" class="btn btn-primary">Submit</a></div>

                            </div>
                        </div>
                    </div>

                    <!--          section faci-->
                </div>


            </div>

        </div>
        <?php include "catalog/view/theme/theme4/template/product/product_footer.tpl" ?>
    </div>
</section>


<?php echo $footer ?>
<script>
            $(".rslides").responsiveSlides({
    auto: true, // Boolean: Animate automatically, true or false
            speed: 500, // Integer: Speed of the transition, in milliseconds
            timeout: 4000, // Integer: Time between slide transitions, in milliseconds
            pager: false, // Boolean: Show pager, true or false
            nav: false, // Boolean: Show navigation, true or false
            random: false, // Boolean: Randomize the order of the slides, true or false
            pause: false, // Boolean: Pause on hover, true or false
            pauseControls: true, // Boolean: Pause when hovering controls, true or false
            prevText: "Previous", // String: Text for the "previous" button
            nextText: "Next", // String: Text for the "next" button
            maxwidth: "", // Integer: Max-width of the slideshow, in pixels
            navContainer: "", // Selector: Where controls should be appended to, default is after the 'ul'
            manualControls: "", // Selector: Declare custom pager navigation
            namespace: "rslides", // String: Change the default namespace used
            before: function(){}, // Function: Before callback
            after: function (){}     // Function: After callback
    });
</script>