<?php if(count($courts) != ''){ ?>
<table class='tablemain' style='width:710px;margin-top:10px' >
    <br/>
    <div class='text-center'>
        <label style="color:#333" for="dates">Choose Date:&nbsp;&nbsp; </label><input type='text' placeholder="Choose Date" id='checkin' name='dates' class='checkin date' value='<?php echo $datein ?>' />
    </div>
    <thead  >
        <tr>
            <td style='text-align:left'>
                Time
            </td>
            <?php foreach($courts as $court){ ?>
            <td style='text-align:center;background-color: palegreen !important;color:black;font-weight: bold'>
                <span class='text-center'><?php echo $court['caption'] ?></span>
            </td>
            <?php } ?>
        </tr>

    </thead>
    <tbody id='timetable' >

    </tbody>
</table> 
<?php } ?>

<script>
    var booking_dates = {};
    function triggerbook(roomId, time, date, obj) {
        var randomId = Math.floor(Math.random() * 89999 + 10000);
        if (obj.id == "") {
            booking_dates[randomId] = {};
            booking_dates[randomId]['roomid'] = roomId
            booking_dates[randomId]['date'] = date + ' ' + time
            obj.id = randomId;
            $('#' + obj.id).addClass('selected')
        } else {
            $('#' + obj.id).removeClass('selected')
            var divId = obj.id
            delete booking_dates[divId];
            obj.id = "";
        }
        console.log(booking_dates)
    }
</script>
<style>
    .slotdiv:hover{
        //     background: #37d200 !Important
    }
    .selected{
        background:  palegreen !important
    }
    .vcenter {
        display: inline-block;
        float: none;
    }
    .timeslot{
        height:20px;
    }
    .slot{
        cursor: pointer;
        height:20px;
    }
    .noroom{
        height:20px;
        background: red;
        color:white


    }

    .header{
        background:black;
        color:white
    }

    .normalday{
        background: white ;
        color:black;
    }
    .weekendday{
        background: white ;
        color:black;
        //  background: green ;
        // color:white;
    }
    .phday{
        background: white ;
        color:black;
        // background: yellow ;
        // color:white;
    }

    .pitchcontainer h4,.vcenter span{
        margin-top:20px;
        font-size: 11px !Important;
        margin-bottom:-25px;
    }
    .pitch1{
        height:60px;
        cursor: pointer;
        background: url('catalog/view/theme/theme4/images/pitch.jpg') no-repeat center;
        background-size:100% 100%
    }
    .pitch2{
        height:120px;
        cursor: pointer;
        background: url('catalog/view/theme/theme4/images/pitch2.jpg') no-repeat center;
        background-size:100% 100%

    }
    .pitch1:hover{
        opacity: 0.8
    }
    .row{
        margin-bottom:10px
    }
</style>

