
<script type="text/javascript">
<?php if(count($courts) != ''){ ?>
    $(function () {

        var dates = '<?php echo $holidays ?>';
        var peaks = '<?php echo $peaks ?>';
        var offs = '<?php echo $offs ?>';
        var weekend = <?php echo $weekends ?>;
        var school = '<?php echo $school ?>';

        $('#checkin').datepicker({
            stepMinute: '60',
            showMinute: false,
            minDate: "<?php echo date('d/m/Y') ?>",
            dateFormat: 'dd/mm/yy',
            onSelect: function (dateText, inst) {
                checkRoomAvailable();
            },
            beforeShowDay: function (date) {

                var y = date.getFullYear().toString(); // get full year
                var m = (date.getMonth() + 1).toString(); // get month.
                var d = date.getDate().toString(); // get Day
                if (m.length == 1) {
                    m = '0' + m;
                } // append zero(0) if single digit
                if (d.length == 1) {
                    d = '0' + d;
                } // append zero(0) if single digit
                var currDate = y + '-' + m + '-' + d;
                if (dates.indexOf(currDate) >= 0) {
                    return [true, "ui-highlight"];
                } else if (peaks.indexOf(currDate) >= 0) {
                    return [true, "ui-highlightpeak"];
                } else if (offs.indexOf(currDate) >= 0) {
                    return false
                } else if (school.indexOf(currDate) >= 0) {
                    return [true, "ui-highlightschool"];
                } else {
                    if (weekend == 2) {
                        if (date.getDay() == 5 || date.getDay() == 6) {
                            return [true, "ui-highlightweekend"];
                        }
                    } else {
                        if (date.getDay() == 6 || date.getDay() == 0) {
                            return [true, "ui-highlightweekend"];
                        }
                    }
                }
                return [true];
            }
        });



        checkRoomAvailable();



    });
<?php } ?>


    function gocart() {
        console.log(JSON.stringify(booking_dates))
       
        if(jQuery.isEmptyObject((booking_dates))){
            alert('Please select a court')
            return false;
        }
            
        $.ajax({
            url: 'index.php?route=checkout/cart/add',
            type: 'post',
            data: {'booking_details': JSON.stringify(booking_dates), 'product_id': $('#product_id').val(),'mpn': $('#mpn').val(),'location': '<?php echo $location ?>'},
            dataType: 'json',
            success: function (json) {
                $('.success, .warning, .attention, information, .error').remove();
                if (json['error']) {
                    if (json['error']['option']) {
                        for (i in json['error']['option']) {
                            alert(json['error']['option'][i]);
                            $('#option-' + i).after('<span class="error">' + json['error']['option'][i] + '</span>');
                        }
                    } else if (json['error']) {
                        alert(json['error'])
                    } else {
                        alert(json['error2'])

                    }
                }

                if (json['success']) {
                    window.location = "index.php?route=checkout/cart";
                }
            }
        });
    }

//--></script>

<script type="text/javascript"><!--
$('#review .pagination a').bind('click', function () {
        $('#review').fadeOut('slow');
        $('#review').load(this.href);
        $('#review').fadeIn('slow');
        return false;
    });
    $('#review').load('index.php?route=product/product/review&product_id=<?php echo $product_id; ?>');
    $('#button-review').bind('click', function () {
        $.ajax({
            url: 'index.php?route=product/product/write&product_id=<?php echo $product_id; ?>',
            type: 'post',
            dataType: 'json',
            data: 'name=' + encodeURIComponent($('input[name=\'name\']').val()) + '&text=' + encodeURIComponent($('textarea[name=\'text\']').val()) + '&rating=' + encodeURIComponent($('input[name=\'rating\']:checked').val() ? $('input[name=\'rating\']:checked').val() : '') + '&captcha=' + encodeURIComponent($('input[name=\'captcha\']').val()),
            beforeSend: function () {
                $('.success, .warning').remove();
                $('#button-review').attr('disabled', true);
                $('#review-title').after('<div class="attention"><img src="catalog/view/theme/default/image/loading.gif" alt="" /> <?php echo $text_wait; ?></div>');
            },
            complete: function () {
                $('#button-review').attr('disabled', false);
                $('.attention').remove();
            },
            success: function (data) {
                if (data['error']) {
                    $('#review-title').after('<div class="warning">' + data['error'] + '</div>');
                }

                if (data['success']) {
                    $('#review-title').after('<div class="success">' + data['success'] + '</div>');
                    $('input[name=\'name\']').val('');
                    $('textarea[name=\'text\']').val('');
                    $('input[name=\'rating\']:checked').attr('checked', '');
                    $('input[name=\'captcha\']').val('');
                }
            }
        });
    });


function getTime(i){
    if(i < 13){
        return (i)+'am';
    }else{
       return (i-12)+'pm';
    }
}


    function checkRoomAvailable() {
        var data = {
            'product_id': '<?php echo $product_id ?>',
            'location': '<?php echo $location ?>',
            'checkin': $('.checkin').val(),
            'quantity': $('#quantity').val(),
            'mpn': $('#mpn').val()
        }

        $.ajax({
            url: 'index.php?route=product/product/checkRoomAvailable',
            type: 'post',
            dataType: 'json',
            data: data,
            success: function (data) {
                var html = "";
                html += "<tr>";
                z = 1;
                $.each(data, function (key, value) {
                    if (value.constructor === Array) {
                        if (z == 1) {
                            html += '<td id="headertime" style="width:8%" class="headertd">';
                            for (i = 0; i <= 23; i++) {
                                html += '<table style="width:100%">';
                                html += '<tr>';
                                html += '<td id="timebox" style="text-align:left" >';
                                html += getTime(i);
                                html += '</td>';
                                html += '</tr>';
                                html += '</table>';
                            }
                            html += '</td>';
                        }

                        html += "<td class='headertd' >";
                        $.each(value, function (key2, value2) {
                            $.each(value2, function (key3, value3) {

                                if (value3['status'] == 0) {
                                    var classdiv = "noroom";
                                    var clickable = "";
                                } else {
                                    var classdiv = "slot";
                                    var dateselected = data['date'];
                                    var clickable = "onclick=\"triggerbook(\'" + key + "\',\'" + key3 + "\',\'" + dateselected + "\',this)\" ";
                                }
                                if (value3['dayType'] == 0) {
                                    var daytype = "normalday";
                                } else if (value3['dayType'] == 1) {
                                    var daytype = "weekendday";
                                } else if (value3['dayType'] == 2) {
                                    var daytype = "phday";
                                }
                                html += '<table style="width:100%">';
                                html += '<tr id="" ' + clickable + ' class=" ' + daytype + ' ' + classdiv + ' slotdiv">';
                                html += '<td id="pricebox" style="border-bottom:1px solid black">';
                                html += value3['price'];
                                html += '</td>';
                                html += '</tr>';
                                html += '</table>';
                            });
                        });
                        html += "</td>";
                        z++;
                    }
                });
                html += "</tr>";
                $('#timetable').html(html);
            }
        });
    }


    $('#review').load('index.php?route=product/product/review&product_id=<?php echo $product_id; ?>');
    $('#button-review').bind('click', function () {
        $.ajax({
            url: 'index.php?route=product/product/write&product_id=<?php echo $product_id; ?>',
            type: 'post',
            dataType: 'json',
            data: 'name=' + encodeURIComponent($('input[name=\'name\']').val()) + '&text=' + encodeURIComponent($('textarea[name=\'text\']').val()) + '&rating=' + encodeURIComponent($('input[name=\'rating\']:checked').val() ? $('input[name=\'rating\']:checked').val() : '') + '&captcha=' + encodeURIComponent($('input[name=\'captcha\']').val()),
            beforeSend: function () {
                $('.success, .warning').remove();
                $('#button-review').attr('disabled', true);
                $('#review-title').after('<div class="attention"><img src="catalog/view/theme/default/image/loading.gif" alt="" /> <?php echo $text_wait; ?></div>');
            },
            complete: function () {
                $('#button-review').attr('disabled', false);
                $('.attention').remove();
            },
            success: function (data) {
                if (data['error']) {
                    $('#review-title').after('<div class="warning">' + data['error'] + '</div>');
                }

                if (data['success']) {
                    $('#review-title').after('<div class="success">' + data['success'] + '</div>');
                    $('input[name=\'name\']').val('');
                    $('textarea[name=\'text\']').val('');
                    $('input[name=\'rating\']:checked').attr('checked', '');
                    $('input[name=\'captcha\']').val('');
                }
            }
        });
    });

//--></script>
<div class='clearfix'></div>

