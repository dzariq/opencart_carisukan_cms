<?php echo $header; ?>
<section style="margin-top:50px">
    <div style='background:#ffffff;' class="container">

        <h2><?php echo $heading_title ?></h2>
        <hr />

        <div style='margin-bottom:50px' id="content" class="col-md-12">
                <?php echo $description; ?>
                <br/>
                <?php echo $content_top; ?>
        </div>
        <?php echo $content_bottom; ?>
    </div>
</section>
<?php echo $footer; ?>