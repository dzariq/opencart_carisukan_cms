<?php echo $header ?>
<style>
    table td{
        text-align: left;
    }
</style>
<!-- Page title -->
<section>
    <div class="container ">
        <?php if ($attention) { ?>
        <div class="alert alert-info">
            <?php echo $attention; ?>
            <button type="button" class="close" data-dismiss="alert">&times;</button>
        </div>
        <?php } ?>
        <?php if ($success) { ?>
        <div class="alert alert-success">
            <?php echo $success; ?>
            <button type="button" class="close" data-dismiss="alert">&times;</button>
        </div>
        <?php } ?>
        <?php if ($error_warning) { ?>
        <div class="alert alert-danger">
            <?php echo $error_warning; ?>
            <button type="button" class="close" data-dismiss="alert">&times;</button>
        </div>
        <?php } ?>
        <div class="page-title">
            <h2> Booking Confirmation </h2>
        </div>
        <!-- Page title -->

        <!-- Page content -->

        <div class="view-cart blocky">
            <div class=" co-box">
                <div class="col-md-12">

                    <!-- Table -->
                    <table class="table ">
                        <thead>
                            <tr>
                                <th class="name">Details</th>
                                <th class="price"><?php echo $column_price; ?></th>
                                <th class="total"><?php echo $column_total; ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($products as $product) { ?>
                            <tr>

                                <td class="name"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a>
                                    <br/>
                                    <?php echo $product['bookingdetails']['room_name'] ?>
                                    <br/>
                                    <?php echo $product['bookingdetails']['date'] ?>
                                </td>
                                <td class="price"><?php echo $product['price']; ?></td>
                                <td class="total"><?php echo $product['total']; ?></td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                    <div class="pull-right">
                        <table class='table' id="total">
                            <?php foreach ($totals as $total) { ?>
                            <tr>
                                <td class="right"><b><?php echo $total['title']; ?>:</b></td>
                                <td class="right"><?php echo $total['text']; ?></td>
                            </tr>
                            <?php } ?>
                        </table>
                    </div>
                    <div class="sep-bor"></div>
                    <!-- Discount Coupen -->
                    <div style="padding:0px" class="col-xs-12">
                        <h5 class="title">Coupon</h5>
                        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" class="form-inline" role="form">
                            <div class="form-group">
                                <input type="hidden" name="next" value="coupon" />
                                <input id="use_coupon" type="discount" class="form-control" name="coupon" value="<?php echo $coupon; ?>" />
                            </div>
                            <button style='margin-top:0px;background:palegoldenrod;color:black;font-weight: bold' type="submit" class="btn btn-primary"><?php echo $button_coupon; ?></button>
                        </form>
                    </div>
                    <!-- Buttons-->
                    <div style="padding:0px" class="col-xs-12">
                        <div class="pull-right">
                            <a href="javascript:window.history.back()" class="btn btn-primary">Back</a>
                            <a style='background:palegreen;color:black;font-weight: bold' href="<?php echo $checkout; ?>" class="btn btn-primary"><?php echo $button_checkout; ?></a>
                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>
</section>
<?php echo $footer ?>