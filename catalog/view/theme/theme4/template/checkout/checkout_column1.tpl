<?php if(!$this->customer->isLogged() && !isset($this->session->data['order_id'])){ ?>
<style>
    .loginarea{
        padding:10px;
        background: #f5f5f5
    }
</style>
<div class="loginarea">
    <h4 >Already a member? Login here..</h4>

    <input type="text" placeholder="Email" value="" id="front_email" class="form-control" />
    <input type="password" placeholder="Password" id="front_password" style="margin-top:10px" class="form-control" />
    <div  style="padding-left:0px">
        <input type="button" value="Login" onclick="front_login_form()" id="front-login" style="margin-top:10px" class="btn btn-info " />
    </div>
    <div style="margin-top: 0px;padding: 0px;margin-bottom: 12px;" class="col-md-12">
        <div class="error" id="front_message"></div>
    </div>
</div>
<h3 class="text-center">-OR-</h3>
<?php } ?>
<h4 >Fill in your details</h4>
<hr/>

<table id="billing-address-display" cellpadding="0" cellspacing="0" width="100%" class="table-form checkout_address">



    <?php

    if(isset($this->session->data['order_id']) || $this->customer->isLogged())

    {?>

      <tr>

      <td valign="top" align="left" width="30%"><?php echo $entry_firstname; ?></td>

      <td valign="top" align="left" width="70%"><b><?php echo $firstname; ?></b>

      <input type="hidden" name="firstname" value="<?php echo $firstname; ?>"/></td>

      </tr>   

  <?php

    } else

    {?>

      <tr>

      <td valign="top" align="left" width="30%">

      <span class="required">*</span> <?php echo $entry_firstname; ?></td>

      <td valign="top" align="left" width="70%"><input type="text" name="firstname" value="<?php echo $firstname; ?>" class="form-control"/></td>

      </tr>

      <?php if ($error_firstname) { ?>

      <tr>

      <td valign="top" align="left" colspan="2">

      <div class="error"><?php echo $error_firstname; ?></div><br />

      </td>

      </tr>

      <?php } ?>

    <?php }?>



    <?php

    if(isset($this->session->data['order_id']) || $this->customer->isLogged())

    {?>

  	  <tr>

  	  <td valign="top" align="left" width="30%"><?php echo $entry_email; ?></td>

      <td valign="top" align="left" width="70%"><b><?php echo $email; ?></b>

      <input type="hidden" name="email" value="<?php echo $email; ?>"/></td>

      </tr>  

  <?php

    }

    else

    {?>

  	  <tr>

      <td valign="top" align="left" width="30%">

      <span class="required">*</span> <?php echo $entry_email; ?></td>

      <td valign="top" align="left" width="70%"><input type="text" name="email" value="<?php echo $email; ?>" class="form-control"/></td>

      </tr>

      <?php if ($error_email) { ?>

      <tr>

      <td valign="top" align="left" colspan="2">

      <div class="error"><?php echo $error_email; ?></div><br />

      </td>

      </tr>

  	  <?php }?>

    <?php }?>



    <?php

    if(isset($this->session->data['order_id']) || $this->customer->isLogged())

    {?>

   	  <tr>

  	  <td valign="top" align="left" width="30%"><?php echo $entry_telephone; ?></td>

      <td valign="top" align="left" width="70%"><b><?php echo $telephone; ?></b>

      <input type="hidden" name="telephone" value="<?php echo $telephone; ?>"/></td>

      </tr>

  <?php

    }

    else

    {?>

  	  <tr>

      <td valign="top" align="left" width="30%">

      <span class="required">*</span> <?php echo $entry_telephone; ?></td>

      <td valign="top" align="left" width="70%"><input type="text" name="telephone" value="<?php echo $telephone; ?>" class="form-control"/></td>

      </tr>

      <?php if ($error_telephone) { ?>

      <tr>

      <td valign="top" align="left" colspan="2">

      <div class="error"><?php echo $error_telephone; ?></div><br />

      </td>

      </tr>

  	  <?php }?>

    <?php }?>









    <tr>

        <td colspan="2">

            <div style="display: <?php echo (count($customer_groups) > 1 ? 'table-row' : 'none'); ?>;"> <?php echo $entry_customer_group; ?><br />

                <?php foreach ($customer_groups as $customer_group) { ?>

                <?php if ($customer_group['customer_group_id'] == $customer_group_id) { ?>

                <input type="radio" name="customer_group_id" value="<?php echo $customer_group['customer_group_id']; ?>" id="customer_group_id<?php echo $customer_group['customer_group_id']; ?>" checked="checked" />

                <label for="customer_group_id<?php echo $customer_group['customer_group_id']; ?>"><?php echo $customer_group['name']; ?></label>

                <br />

                <?php } else { ?>

                <input type="radio" name="customer_group_id" value="<?php echo $customer_group['customer_group_id']; ?>" id="customer_group_id<?php echo $customer_group['customer_group_id']; ?>" />

                <label for="customer_group_id<?php echo $customer_group['customer_group_id']; ?>"><?php echo $customer_group['name']; ?></label>

                <br />

                <?php } ?>

                <?php } ?>

                <br />

            </div>

        </td>

    </tr>







    <?php

    if(isset($this->session->data['order_id']) || $this->customer->isLogged())

    {?>

 	  <tr>

  	  <td valign="top" align="left" width="30%"><?php echo $entry_address_1; ?></td>

      <td valign="top" align="left" width="70%"><b><?php echo $address_1; ?></b>

      <input type="hidden" name="address_1" value="<?php echo $address_1; ?>"/></td>

      </tr>

  <?php

    }

    else

    {?>

  	  <tr>

      <td valign="top" align="left" width="30%">

      <span class="required">*</span> <?php echo $entry_address_1; ?></td>

      <td valign="top" align="left" width="70%"><input type="text" name="address_1" value="<?php echo $address_1; ?>" class="form-control"/></td>

      </tr>

      <?php if ($error_address_1) { ?>

      <tr>

      <td valign="top" align="left" colspan="2">

      <div class="error"><?php echo $error_address_1; ?></div><br />

      </td>

      </tr>

  	  <?php }?>

    <?php } ?>



    <?php

    if(isset($this->session->data['order_id']) || $this->customer->isLogged())

    {?>

      <tr>

  	  <td valign="top" align="left" width="30%"><?php echo $entry_city; ?></td>

      <td valign="top" align="left" width="70%"><b><?php echo $city; ?></b>

      <input type="hidden" name="city" value="<?php echo $city; ?>"/></td>

      </tr>

  <?php

    }

    else

    {?>

  	  <tr>

      <td valign="top" align="left" width="30%">

      <span class="required">*</span> <?php echo $entry_city; ?></td>

      <td valign="top" align="left" width="70%"><input type="text" name="city" value="<?php echo $city; ?>" class="form-control"/></td>

      </tr>

      <?php if ($error_city) { ?>

      <tr>

      <td valign="top" align="left" colspan="2">

      <div class="error"><?php echo $error_city; ?></div><br />

      </td>

      </tr>

  	  <?php }?>

    <?php } ?>



    <?php

    if(isset($this->session->data['order_id']) || $this->customer->isLogged())

    {?>

      <tr>

  	  <td valign="top" align="left" width="30%"><?php echo $entry_postcode; ?></td>

      <td valign="top" align="left" width="70%"><b><?php echo $postcode; ?></b>

      <input type="hidden" name="postcode" value="<?php echo $postcode; ?>"/></td>

      </tr>

  <?php

    }

    else

    {?>

      <tr>

      <td valign="top" align="left" width="30%">

      <span id="payment-postcode-required" class="required">*</span> <?php echo $entry_postcode; ?></td>

      <td valign="top" align="left" width="70%"><input type="text" name="postcode" value="<?php echo $postcode; ?>" class="form-control"/></td>

      </tr>

      <?php if ($error_postcode) { ?>

      <tr>

      <td valign="top" align="left" colspan="2">

      <div class="error"><?php echo $error_postcode; ?></div><br />

      </td>

      </tr>

  	  <?php } ?>

    <?php } ?>



    <?php

    if(isset($this->session->data['order_id']) || $this->customer->isLogged())

    {?>

  	  <tr>

  	  <td valign="top" align="left" width="30%"><?php echo $entry_country; ?></td>

      <td valign="top" align="left" width="70%"><b><?php echo $country_name; ?></b>

      <input id="country_id" type="hidden" name="country_id" value="<?php echo $country_id; ?>"/></td>

      </tr>

      

      <tr>

  	  <td valign="top" align="left" width="30%"><?php echo $entry_zone; ?></td>

      <td valign="top" align="left" width="70%"><b><?php echo $zone_name; ?></b>

      <input id="zone_id" type="hidden" name="zone_id" value="<?php echo $zone_id; ?>"/></td>

      </tr>

  <?php

    }

    else

    {?>

      <tr>

      <td valign="top" align="left" width="30%">

      <span class="required">*</span> <?php echo $entry_country; ?></td>

      <td valign="top" align="left" width="70%">

      <select id="country_id" name="country_id" class="form-control">

        <option value=""><?php echo $text_select; ?></option>

        <?php foreach ($countries as $country) { ?>

        <?php if ($country['country_id'] == $country_id) { ?>

        <option value="<?php echo $country['country_id']; ?>" selected="selected"><?php echo $country['name']; ?></option>

        <?php } else { ?>

    <option value="<?php echo $country['country_id']; ?>"><?php echo $country['name']; ?></option>

    <?php } ?>

    <?php } ?>

</select>

</td>

</tr>

<?php if ($error_country) { ?>

<tr>

    <td valign="top" align="left" colspan="2">

        <div class="error"><?php echo $error_country; ?></div><br />

    </td>

</tr>

<?php }?>



<tr>

    <td valign="top" align="left" width="30%">

        <span class="required">*</span> <?php echo $entry_zone; ?></td>

    <td valign="top" align="left" width="70%">

        <select onchange="getShippingMethod(this.value)" id='zone_id' name="zone_id" class="form-control">

        </select>

    </td>

</tr>

<?php if ($error_zone) { ?>

<tr>

    <td valign="top" align="left" colspan="2">

        <div class="error"><?php echo $error_zone; ?></div><br />

    </td>

</tr>

<?php }?>

<!--NEW ADDED============-->
<?php if(!$guest_checkout){ ?>
<tr>
    <td valign="top" align="left" width="30%">
        <span class="required">*</span> Password</td>
    <td valign="top" align="left" width="70%">
        <input value="<?php echo $password ?>" class="form-control" type="password" name="password" />
    </td>
</tr>
<?php if ($error_password) { ?>
<tr>
    <td valign="top" align="left" colspan="2">
        <div class="error"><?php echo $error_password; ?></div><br />
    </td>
</tr>
<?php }?>
<tr>
    <td valign="top" align="left" width="30%">
        <span class="required">*</span> Confirm Password</td>
    <td valign="top" align="left" width="70%">
        <input type="password" class="form-control" value="<?php echo $password_confirm ?>" name="password_confirm" />
    </td>
</tr>
<?php if ($error_password_confirm) { ?>
<tr>
    <td valign="top" align="left" colspan="2">
        <div class="error"><?php echo $error_password_confirm; ?></div><br />
    </td>
</tr>
<?php }?>
<?php } ?>
<?php } ?>

<!--NEW ADDED============-->


</table>

<br/>

<br/>


<script>
    $(document).ready(function () {
    });

    function getShippingMethod() {
        $('#shipping-container').html('')

        if ($("#check_shipping_address").prop('checked') == true) {
            var zone_id = $('#zone_id').val();
            var country_id = $('#country_id').val();
        } else {
            var zone_id = $('#shipping_zone_id').val();
            var country_id = $('#shipping_country_id').val();
        }


        if (zone_id != '' && country_id != '' && '<?php echo $shipping_required ?>' == 1) {

            var data = {
                'zone_id': zone_id,
                'country_id': country_id
            }
            console.log(data)

            $.ajax({
                url: "index.php?route=checkout/shipping_method/getShippingMethod",
                data: data,
                dataType: 'json',
                type: 'post'
            })
                    .done(function (data) {
                        if (data == 'fail') {
                            $('#shipping-container').html('')
                        } else {
                            console.log(data);
                            var html = '';
                            html += '<h4><?php echo $text_shipping_method; ?></h4>'
                            $.each(data, function (index, itemData) {
                                html += "<div class='col-xs-12'>"
                                $.each(itemData.quote, function (index2, itemData2) {
                                    html += "<div class='row'>"
                                    html += '<input type="radio" name="shipping_method" value="' + itemData2.code + '" id="' + itemData2.code + '"  />'
                                    html += '<label for="' + itemData2.code + '">' + itemData2.title + '</label>'
                                    html += '<label for="' + itemData2.code + '">' + itemData2.text + '</label>'
                                    html += "</div>"
                                });
                                html += "</div>"
                            });
                            console.log(html)
                            $('#shipping-container').html(html)
                        }

                    });
        }
    }

    function resetShipping() {
        $('#shipping-container').html('')

    }

    function front_login_form() {
        var data = {
            'email': $('#front_email').val(),
            'password': $('#front_password').val(),
            'json': 1
        };

        $('#front_message').html('');

        $.ajax({
            url: 'index.php?route=account/login',
            data: data,
            type: 'post',
            dataType: 'json',
            success: function (json) {
                if (json.status == 0) {

                    $('#front_message').html('Invalid email or password')
                } else {
                    window.location = "index.php?route=checkout/checkout";
                }
            },
        });

    }
</script>