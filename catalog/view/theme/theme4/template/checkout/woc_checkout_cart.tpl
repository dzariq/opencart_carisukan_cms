<?php
if($products)

{

?>

<div class="checkout-product">
    <table class="table">
        <thead>
            <tr>
                <td class="name"><?php echo $column_name; ?></td>

                <td class="price"><?php echo $column_price; ?></td>

                <td class="total"><?php echo $column_total; ?></td>
            </tr>
        </thead>
        <tbody>

           <?php foreach ($products as $product) { ?>
                            <tr>
                                <td style="text-align: left" class="name"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a>
                                    <br/>
                                    <?php echo $product['bookingdetails']['room_name'] ?>
                                    <br/>
                                    <?php echo $product['bookingdetails']['date'] ?>
                                </td>
                                <td class="price"><?php echo $product['price']; ?></td>
                                <td class="total"><?php echo $product['total']; ?></td>
                            </tr>
                            <?php } ?>

        </tbody>

        <tfoot>

            <?php foreach ($totals as $total) { ?>

            <tr>

                <td colspan="2" class="price"><b><?php echo $total['title']; ?>:</b></td>

                <td class="total"><?php echo $total['text']; ?></td>

            </tr>

            <?php } ?>

        </tfoot>

    </table>

</div>

<!--

<div class="payment"><?php echo $payment; ?></div>-->

<?php } ?>