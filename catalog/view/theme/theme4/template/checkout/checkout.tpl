<?php echo $header; ?><?php echo $column_left; ?><?php echo $column_right; ?>

<style type="text/css">

    input.small-field, select.small-field {

        width: 180px;

    }

    .checkout_address td {
        text-align: left;
        padding:3px;

    }


    #checkout_submit{

    }




</style>
<section>
    <div class="container" id="content"><?php echo $content_top; ?>

        <h2 class="text-center"><?php echo $heading_title; ?></h2>
        <?php
        if(isset($this->session->data['order_id']))
        {?>
                <div id="checkout_submit"></div>
                
            <?php }?>
        <?php if(!isset($this->session->data['order_id']))
        {?>
    <form id="form-ck" action="<?php echo $action;?>" method="post" enctype="multipart/form-data">

        <div class='row'>
          

            <div class='co-box col-sm-4 col-xs-12'>
<div>
                <?php include('catalog/view/theme/'.$template_checkout.'/template/checkout/checkout_column1.tpl'); ?>

            </div>
            </div>

            <div class='co-box col-sm-4 col-xs-12'>
<div style='min-height:240px;'>
                <?php include('catalog/view/theme/'.$template_checkout.'/template/checkout/checkout_column2.tpl'); ?>

            </div>
            </div>

            <div  class='co-box col-sm-4 col-xs-12'>
<div >
                <?php include('catalog/view/theme/'.$template_checkout.'/template/checkout/checkout_column3.tpl'); ?>

            </div>
            </div>

        </div>

    </form>

<?php }  ?>

        <?php echo $content_bottom; ?>

    </div>
</section>





<script type="text/javascript"><!--

    $('input[name=\'customer_group_id\']:checked').change(function () {

        var customer_group = [];

        <?php foreach ($customer_groups as $customer_group) { ?>
                customer_group[ <?php echo $customer_group['customer_group_id']; ?> ] = [];
                customer_group[ <?php echo $customer_group['customer_group_id']; ?> ]['company_id_display'] = '<?php echo $customer_group["company_id_display"]; ?>';
                customer_group[ <?php echo $customer_group['customer_group_id']; ?> ]['company_id_required'] = '<?php echo $customer_group["company_id_required"]; ?>';
                customer_group[ <?php echo $customer_group['customer_group_id']; ?> ]['tax_id_display'] = '<?php echo $customer_group["tax_id_display"]; ?>';
                customer_group[ <?php echo $customer_group['customer_group_id']; ?> ]['tax_id_required'] = '<?php echo $customer_group["tax_id_required"]; ?>';
                <?php } ?>
                if (customer_group[this.value]) {

            if (customer_group[this.value]['company_id_display'] == '1') {

                $('#company-id-display').show();

            } else {

                $('#company-id-display').hide();

            }



            if (customer_group[this.value]['company_id_required'] == '1') {

                $('#company-id-required').show();

            } else {

                $('#company-id-required').hide();

            }



            if (customer_group[this.value]['tax_id_display'] == '1') {

                $('#tax-id-display').show();

            } else {

                $('#tax-id-display').hide();

            }



            if (customer_group[this.value]['tax_id_required'] == '1') {

                $('#tax-id-required').show();

            } else {

                $('#tax-id-required').hide();

            }

        }

    });



    $('input[name=\'customer_group_id\']:checked').trigger('change');

//--></script> 

<script type="text/javascript"><!--

    $('select[name=\'country_id\']').bind('change', function () {
        if (this.value == '')
            return;

        $.ajax({
            url: 'index.php?route=checkout/checkout/country&country_id=' + this.value,
            dataType: 'json',
            beforeSend: function () {

                $('select[name=\'country_id\']').after('<span class="wait">&nbsp;<img src="catalog/view/theme/default/image/loading.gif" alt="" /></span>');

            },
            complete: function () {

                $('.wait').remove();

            },
            success: function (json) {

                if (json['postcode_required'] == '1') {

                    $('#payment-postcode-required').show();

                } else {

                    $('#payment-postcode-required').hide();

                }



                html = '<option value=""><?php echo $text_select; ?></option>';



                if (json['zone'] != '') {

                    for (i = 0; i < json['zone'].length; i++) {

                        html += '<option value="' + json['zone'][i]['zone_id'] + '"';



                        if (json['zone'][i]['zone_id'] == '<?php echo $zone_id; ?>') {

                            html += ' selected="selected"';

                        }



                        html += '>' + json['zone'][i]['name'] + '</option>';

                    }

                } else {

                    html += '<option value="0" selected="selected"><?php echo $text_none; ?></option>';
                }



                $('select[name=\'zone_id\']').html(html);
               

            },
            error: function (xhr, ajaxOptions, thrownError) {

                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);

            }

        });

    });



    $('select[name=\'country_id\']').trigger('change');

//--></script> 



<script type="text/javascript"><!--

    $('#woc_checkout_cart').load('index.php?route=checkout/woc_checkout_cart');



//--></script> 



<script type="text/javascript"><!--

    $('input[type=radio][name=payment_method]').change(function () {
        $.ajax({
            url: 'index.php?route=checkout/checkout/payment_method',
            type: 'post',
            data: 'payment_method=' + $('input[name=\'payment_method\']:checked').attr('value'),
            dataType: 'json',
            success: function (json) {
                $('#woc_checkout_cart').load('index.php?route=checkout/woc_checkout_cart');
            },
            error: function (xhr, ajaxOptions, thrownError) {

                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);

            }

        });

    })

//--></script> 



<?php

if(isset($this->session->data['order_id']))

{?>

<script type="text/javascript"><!--

$('#woc_checkout_cart').load('index.php?route=checkout/woc_checkout_cart');

$.ajax({

		url: 'index.php?route=checkout/checkout/checkout_submit',

		type: 'post',

		data: '',

		dataType: 'json',				

		success: function(json) {

			$('#checkout_submit').html(json['payment']);

},

error: function(xhr, ajaxOptions, thrownError) {

alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);

}

});	

//--></script> 

<?php }?>



<script type="text/javascript"><!--

    $('[name=\'firstname\']').bind('change', function () {
        $.ajax({
            url: 'index.php?route=checkout/checkout/payment_address',
            type: 'post',
            data: 'firstname=' + $(this).val(),
            dataType: 'json',
            success: function (json) {

                $('#firstname_icon').remove();

                if (json['firstname_error']) {

                    $('input[name=\'firstname\']').after('<div id="firstname_icon" class="error"><img src="catalog/view/theme/default/image/warning.png" alt="" />&nbsp;' + json['firstname_error'] + '</div>');

                } else if (json['firstname_success']) {

                    $('input[name=\'firstname\']').before('<div id="firstname_icon"></div>');

                }

            },
            error: function (xhr, ajaxOptions, thrownError) {

                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);

            }

        });

    });



    $('[name=\'lastname\']').bind('change', function () {

        $.ajax({
            url: 'index.php?route=checkout/checkout/payment_address',
            type: 'post',
            data: 'lastname=' + $(this).val(),
            dataType: 'json',
            success: function (json) {

                $('#lastname_icon').remove();

                if (json['lastname_error']) {

                    $('input[name=\'lastname\']').after('<div id="lastname_icon" class="error"><img src="catalog/view/theme/default/image/warning.png" alt="" />&nbsp;' + json['lastname_error'] + '</div>');

                } else if (json['firstname_success']) {

                    $('input[name=\'lastname\']').before('<div id="lastname_icon"></div>');

                }

            },
            error: function (xhr, ajaxOptions, thrownError) {

                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);

            }

        });

    });



    $('[name=\'email\']').bind('change', function () {

        $.ajax({
            url: 'index.php?route=checkout/checkout/payment_address',
            type: 'post',
            data: 'email=' + $(this).val(),
            dataType: 'json',
            success: function (json) {

                $('#email_icon').remove();

                if (json['email_error']) {

                    $('input[name=\'email\']').after('<div id="email_icon" class="error"><img src="catalog/view/theme/default/image/warning.png" alt="" />&nbsp;' + json['email_error'] + '</div>');

                } else if (json['firstname_success']) {

                    $('input[name=\'email\']').before('<div id="email_icon"></div>');

                }

            },
            error: function (xhr, ajaxOptions, thrownError) {

                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);

            }

        });

    });

//--></script> 

<br/>

<?php echo $footer; ?>