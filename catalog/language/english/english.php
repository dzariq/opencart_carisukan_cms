<?php

// Locale

$_['code'] = 'en';

$_['direction'] = 'ltr';

$_['date_format_short'] = 'd/m/Y';

$_['date_format_long'] = 'l dS F Y';

$_['time_format'] = 'h:i:s A';

$_['decimal_point'] = '.';

$_['thousand_point'] = ',';



// Text
$_['text_enter_website']             = 'Shop Now';

$_['text_home'] = 'Home';
$_['text_close'] = 'Close';
$_['text_join_event'] = 'Join Event';

$_['text_yes'] = 'Yes';
$_['text_about_us'] = 'About Us';

$_['text_no'] = 'No';

$_['text_none'] = ' --- None --- ';

$_['text_select'] = ' --- Please Select --- ';

$_['text_all_zones'] = 'All Zones';

$_['text_pagination'] = 'Showing {start} to {end} of {total} ({pages} Pages)';

$_['text_separator'] = ' &raquo; ';
$_['text_calendar_info'] = 'Klik pada seminar di kalendar(di bawah) untuk mendaftar';



// Buttons

$_['button_add_address'] = 'Add Address';

$_['button_back'] = 'Back';

$_['button_continue'] = 'Continue';

$_['button_view'] = 'View';
$_['button_cart'] = 'Buy';

$_['button_compare'] = 'Add to Compare';

$_['button_wishlist'] = 'Add to Wish List';

$_['button_checkout'] = 'Checkout';

$_['button_confirm'] = 'Confirm Payment';

$_['button_coupon'] = 'Apply Coupon';

$_['button_delete'] = 'Delete';

$_['button_download'] = 'Download';

$_['button_edit'] = 'Edit';

$_['button_new_address'] = 'New Address';

$_['button_change_address'] = 'Change Address';

$_['button_reviews'] = 'Reviews';

$_['button_write'] = 'Write Review';

$_['button_login'] = 'Sign In';

$_['button_update'] = 'Update';

$_['button_remove'] = 'Remove';

$_['button_reorder'] = 'Reorder';

$_['button_return'] = 'Return';

$_['button_shopping'] = 'Continue Shopping';

$_['button_search'] = 'Search';

$_['button_shipping'] = 'Apply Shipping';

$_['button_guest'] = 'New Customer';

$_['button_view'] = 'View';

$_['button_voucher'] = 'Apply Voucher';

$_['button_upload'] = 'Upload File';

$_['button_reward'] = 'Apply Points';

$_['button_quote'] = 'Get Quotes';



// Error

$_['error_upload_1'] = 'Warning: The uploaded file exceeds the upload_max_filesize directive in php.ini!';

$_['error_upload_2'] = 'Warning: The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form!';

$_['error_upload_3'] = 'Warning: The uploaded file was only partially uploaded!';

$_['error_upload_4'] = 'Warning: No file was uploaded!';

$_['error_upload_6'] = 'Warning: Missing a temporary folder!';

$_['error_upload_7'] = 'Warning: Failed to write file to disk!';

$_['error_upload_8'] = 'Warning: File upload stopped by extension!';

$_['error_upload_999'] = 'Warning: No error code available!';

$_['error_name']      = 'Warning: Your name must between 3 to 50 Characters';
$_['error_email']      = 'Warning: Please key-in a valid email address';
$_['existing_email']      = 'Warning: You have already submit your email address';
$_['marketing_success']      = 'Thank you for your interest!. Please check your email for the seminar promo code';
$_['button_send_code']      = 'Send Promo Code';
$_['button_resend_code']      = 'Re-Send Promo Code';
$_['seminar_text']      = 'Click here to register for seminar';

$_['mail_seminar_code_subject']      = 'Free Seminar';
$_['mail_seminar_code_message']      = 'Thank You for your interest. Your Coupon code: %s. Click here to register for seminar: %s';

?>