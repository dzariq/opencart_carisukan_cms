<?php
// Heading
$_['heading_title'] = 'Your Booking Has Been Processed!';
$_['heading_title_customer'] = 'Your Booking <span style=\'color:green\'>#%s</span> is Successful';

// Text
$_['text_customer']    = '<p>Please take note your booking ID for verification purpose upon playing. </p><p>The booking details has been sent to your email address.</p>';
$_['text_guest']    = '<p>Please take note your booking ID for verification purpose upon playing. </p><p>The booking details has been sent to your email address.</p>';
$_['text_basket']   = 'Basket';
$_['text_checkout'] = 'Checkout';
$_['text_success']  = 'Success';
?>