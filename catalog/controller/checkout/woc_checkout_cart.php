<?php

/* * ****************************************************
 * @package Checkout for Opencart 1.5.x
 * @version 1.0
 * @author http://www.webopencart.com
 * @copyright	Copyright (C) Feb 2014 Webopencart.com <@emai:webopencart@gmail.com>.All rights reserved.
 * @license		GNU General Public License version 1
 * ***************************************************** */

class ControllerCheckoutWocCheckoutcart extends Controller {

    public function index() {
        // Validate if payment address has been set.
        $this->load->model('account/address');

        // Validate minimum quantity requirments.			
        $products = $this->cart->getProducts();

        foreach ($products as $product) {
            $product_total = 0;

            foreach ($products as $product_2) {
                if ($product_2['product_id'] == $product['product_id']) {
                    $product_total += $product_2['quantity'];
                }
            }

            if ($product['minimum'] > $product_total) {
                $redirect = $this->url->link('checkout/cart');

                break;
            }
        }

        $total_data = array();
        $total = 0;

        $this->load->model('setting/extension');
        $this->load->model('booking/tools');

        $sort_order = array();

        $results = $this->model_setting_extension->getExtensions('total');

        foreach ($results as $key => $value) {
            $sort_order[$key] = $this->config->get($value['code'] . '_sort_order');
        }

        array_multisort($sort_order, SORT_ASC, $results);

        foreach ($results as $result) {
            if ($this->config->get($result['code'] . '_status')) {
                $this->load->model('total/' . $result['code']);

                $this->{'model_total_' . $result['code']}->getTotal($total_data, $total);
            }
        }

        $sort_order = array();

        foreach ($total_data as $key => $value) {
            $sort_order[$key] = $value['sort_order'];
        }

        array_multisort($sort_order, SORT_ASC, $total_data);

        $this->language->load('checkout/checkout');

        $data = array();

        $data['invoice_prefix'] = $this->config->get('config_invoice_prefix');
        $data['store_id'] = $this->config->get('config_store_id');
        $data['store_name'] = $this->config->get('config_name');

        if ($data['store_id']) {
            $data['store_url'] = $this->config->get('config_url');
        } else {
            $data['store_url'] = HTTP_SERVER;
        }

        if (isset($this->session->data['payment_method']['title'])) {
            $data['payment_method'] = $this->session->data['payment_method']['title'];
        } else {
            $data['payment_method'] = '';
        }

        if (isset($this->session->data['payment_method']['code'])) {
            $data['payment_code'] = $this->session->data['payment_method']['code'];
        } else {
            $data['payment_code'] = '';
        }

        $product_data = array();

        foreach ($this->cart->getProducts() as $product) {

            $product_data[] = array(
                'product_id' => $product['product_id'],
                'name' => $product['name'],
                'bookingdetails' => $product['bookingdetails'],
                'quantity' => $product['quantity'],
                'subtract' => $product['subtract'],
                'price' => $product['price'],
                'rawprice' => $product['price'],
                'total' => $product['total'],
            );
        }


        $data['products'] = $product_data;
        $data['products'] = $this->model_booking_tools->concatBookings($data['products'], $this->currency);

        $data['totals'] = $total_data;
        if (isset($this->session->data['comment']))
            $data['comment'] = $this->session->data['comment'];
        else
            $data['comment'] = '';
        $data['total'] = $total;


        $data['language_id'] = $this->config->get('config_language_id');
        $data['currency_id'] = $this->currency->getId();
        $data['currency_code'] = $this->currency->getCode();
        $data['currency_value'] = $this->currency->getValue($this->currency->getCode());
        $data['ip'] = $this->request->server['REMOTE_ADDR'];

        if (!empty($this->request->server['HTTP_X_FORWARDED_FOR'])) {
            $data['forwarded_ip'] = $this->request->server['HTTP_X_FORWARDED_FOR'];
        } elseif (!empty($this->request->server['HTTP_CLIENT_IP'])) {
            $data['forwarded_ip'] = $this->request->server['HTTP_CLIENT_IP'];
        } else {
            $data['forwarded_ip'] = '';
        }

        if (isset($this->request->server['HTTP_USER_AGENT'])) {
            $data['user_agent'] = $this->request->server['HTTP_USER_AGENT'];
        } else {
            $data['user_agent'] = '';
        }

        if (isset($this->request->server['HTTP_ACCEPT_LANGUAGE'])) {
            $data['accept_language'] = $this->request->server['HTTP_ACCEPT_LANGUAGE'];
        } else {
            $data['accept_language'] = '';
        }

        $this->load->model('checkout/order');

        //$this->session->data['order_id'] = $this->model_checkout_order->addOrder($data);

        $this->data['column_name'] = $this->language->get('column_name');
        $this->data['column_model'] = $this->language->get('column_model');
        $this->data['column_quantity'] = $this->language->get('column_quantity');
        $this->data['column_price'] = $this->language->get('column_price');
        $this->data['column_total'] = $this->language->get('column_total');

        $this->data['text_recurring_item'] = $this->language->get('text_recurring_item');
        $this->data['text_payment_profile'] = $this->language->get('text_payment_profile');

        $this->data['products'] = $this->cart->getProducts();

        $this->data['products'] = $this->model_booking_tools->concatBookings($this->data['products'], $this->currency);

        $this->data['totals'] = $total_data;
      

        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/checkout/woc_checkout_cart.tpl')) {
            $this->template = $this->config->get('config_template') . '/template/checkout/woc_checkout_cart.tpl';
        } else {
            $this->template = 'default/template/checkout/woc_checkout_cart.tpl';
        }

        $this->response->setOutput($this->render());
    }

}

?>