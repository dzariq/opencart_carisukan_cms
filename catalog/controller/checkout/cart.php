<?php

class ControllerCheckoutCart extends Controller {

    private $error = array();

    public function index() {

        $this->language->load('checkout/cart');

        // Update
        if (!empty($this->request->post['quantity'])) {
            foreach ($this->request->post['quantity'] as $key => $value) {
                $this->cart->update($key, $value);
            }

            unset($this->session->data['payment_method']);
            unset($this->session->data['payment_methods']);

            $this->redirect($this->url->link('checkout/cart'));
        }

        // Remove
        if (isset($this->request->get['remove'])) {
            $this->cart->remove($this->request->get['remove']);

            unset($this->session->data['vouchers'][$this->request->get['remove']]);

            $this->session->data['success'] = $this->language->get('text_remove');


            unset($this->session->data['payment_method']);
            unset($this->session->data['payment_methods']);

            $this->redirect($this->url->link('checkout/cart'));
        }

        // Coupon
        if (isset($this->request->post['coupon']) && $this->validateCoupon()) {
            $this->session->data['coupon'] = $this->request->post['coupon'];

            $this->session->data['success'] = $this->language->get('text_coupon');

            $this->redirect($this->url->link('checkout/cart'));
        }

        $this->document->setTitle($this->language->get('heading_title'));

        if ($this->cart->hasProducts()) {
            $this->data['heading_title'] = $this->language->get('heading_title');

            $this->data['text_next'] = $this->language->get('text_next');
            $this->data['text_next_choice'] = $this->language->get('text_next_choice');
            $this->data['text_use_coupon'] = $this->language->get('text_use_coupon');
            $this->data['text_select'] = $this->language->get('text_select');
            $this->data['text_none'] = $this->language->get('text_none');

            $this->data['column_image'] = $this->language->get('column_image');
            $this->data['column_name'] = $this->language->get('column_name');
            $this->data['column_model'] = $this->language->get('column_model');
            $this->data['column_quantity'] = $this->language->get('column_quantity');
            $this->data['column_price'] = $this->language->get('column_price');
            $this->data['column_total'] = $this->language->get('column_total');

            $this->data['entry_coupon'] = $this->language->get('entry_coupon');
            $this->data['entry_country'] = $this->language->get('entry_country');
            $this->data['entry_zone'] = $this->language->get('entry_zone');
            $this->data['entry_postcode'] = $this->language->get('entry_postcode');

            $this->data['button_update'] = $this->language->get('button_update');
            $this->data['button_remove'] = $this->language->get('button_remove');
            $this->data['button_coupon'] = $this->language->get('button_coupon');
            $this->data['button_quote'] = $this->language->get('button_quote');
            $this->data['button_shopping'] = $this->language->get('button_shopping');
            $this->data['button_checkout'] = $this->language->get('button_checkout');
            $this->data['error_stock_warn'] = $this->language->get('error_stock_warn');

            if (isset($this->error['warning'])) {
                $this->data['error_warning'] = $this->error['warning'];
            } elseif (!$this->cart->hasStock() && (!$this->config->get('config_stock_checkout') || $this->config->get('config_stock_warning'))) {
                $this->data['error_warning'] = $this->language->get('error_stock');
            } else {
                $this->data['error_warning'] = '';
            }

            if ($this->config->get('config_customer_price') && !$this->customer->isLogged()) {
                $this->data['attention'] = sprintf($this->language->get('text_login'), $this->url->link('account/login'), $this->url->link('account/register'));
            } else {
                $this->data['attention'] = '';
            }

            if (isset($this->session->data['success'])) {
                $this->data['success'] = $this->session->data['success'];

                unset($this->session->data['success']);
            } else {
                $this->data['success'] = '';
            }

            $this->data['action'] = $this->url->link('checkout/cart');

            $this->load->model('tool/image');
            $this->load->model('booking/tools');

            $this->data['products'] = array();

            $products = $this->cart->getProducts();
            $i = 0;
            foreach ($products as $product) {

                $price = $this->currency->format($product['price']);

                $this->data['products'][] = array(
                    'optionQuantity' => "",
                    'id' => $product['product_id'],
                    'key' => $product['key'],
                    'bookingdetails' => $product['bookingdetails'],
                    'name' => $product['name'],
                    'option' => "",
                    'quantity' => $product['quantity'],
                    'rawprice' => $product['price'],
                    'price' => $price,
                    'total' => $price,
                    'href' => $this->url->link('product/product', 'product_id=' . $product['product_id']),
                    'remove' => $this->url->link('checkout/cart', 'remove=' . $product['key'])
                );
            }

            $this->data['products'] = $this->model_booking_tools->concatBookings($this->data['products'], $this->currency);

            if (isset($this->request->post['next'])) {
                $this->data['next'] = $this->request->post['next'];
            } else {
                $this->data['next'] = '';
            }

            $this->data['coupon_status'] = $this->config->get('coupon_status');

            if (isset($this->request->post['coupon'])) {
                $this->data['coupon'] = $this->request->post['coupon'];
            } elseif (isset($this->session->data['coupon'])) {
                $this->data['coupon'] = $this->session->data['coupon'];
            } else {
                $this->data['coupon'] = '';
            }


            // Totals
            $this->load->model('setting/extension');

            $total_data = array();
            $total = 0;

            $sort_order = array();

            $results = $this->model_setting_extension->getExtensions('total');
            
            foreach ($results as $key => $value) {
                $sort_order[$key] = $this->config->get($value['code'] . '_sort_order');
            }

            array_multisort($sort_order, SORT_ASC, $results);

            foreach ($results as $result) {
                if ($this->config->get($result['code'] . '_status')) {
                    $this->load->model('total/' . $result['code']);

                    $this->{'model_total_' . $result['code']}->getTotal($total_data, $total);
                }

                $sort_order = array();

                foreach ($total_data as $key => $value) {
                    $sort_order[$key] = $value['sort_order'];
                }

                array_multisort($sort_order, SORT_ASC, $total_data);
            }
            
            $this->data['totals'] = $total_data;

            $this->data['continue'] = $this->url->link('common/home');

            $this->data['checkout'] = $this->url->link('checkout/checkout', '', 'SSL');

            if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/checkout/cart.tpl')) {
                $this->template = $this->config->get('config_template') . '/template/checkout/cart.tpl';
            } else {
                $this->template = 'default/template/checkout/cart.tpl';
            }

            $this->children = array(
                'common/column_left',
                'common/column_right',
                'common/content_bottom',
                'common/content_top',
                'common/footer',
                'common/header'
            );
            

            $this->response->setOutput($this->render());
        } else {
            $this->data['heading_title'] = $this->language->get('heading_title');

            $this->data['text_error'] = $this->language->get('text_empty');

            $this->data['button_continue'] = $this->language->get('button_continue');

            $this->data['continue'] = $this->url->link('common/home');

            unset($this->session->data['success']);

            if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/error/not_found.tpl')) {
                $this->template = $this->config->get('config_template') . '/template/error/not_found.tpl';
            } else {
                $this->template = 'default/template/error/not_found.tpl';
            }

            $this->children = array(
                'common/column_left',
                'common/column_right',
                'common/content_top',
                'common/content_bottom',
                'common/footer',
                'common/header'
            );

            $this->response->setOutput($this->render());
        }
    }

    private function validateCoupon() {
        $this->load->model('checkout/coupon');

        $coupon_info = $this->model_checkout_coupon->getCoupon($this->request->post['coupon']);

        if (!$coupon_info) {
            $this->error['warning'] = $this->language->get('error_coupon');
        }

        if (!$this->error) {
            return true;
        } else {
            return false;
        }
    }

    public function add() {
        $this->language->load('checkout/cart');
        $json = array();
        if (isset($this->request->post['product_id'])) {
            $product_id = $this->request->post['product_id'];
        } else {
            $product_id = 0;
        }
        
        $error = false;

        $this->cart->clear();

        $this->load->model('catalog/product');

        $product_info = $this->model_catalog_product->getProduct($product_id);

        if ($product_info) {

            $quantity = 1;

            //get booking tools
            $this->load->model('booking/tools');

            if (!$this->request->post['booking_details'] || count($this->request->post['booking_details']) == 0) {
                $json['error'] = 'Please select booking time';
            }
            $i = 0;
            $booking_details = json_decode(html_entity_decode($this->request->post['booking_details']));
            $test = array();
            foreach ($booking_details as $key => $item) {
                $test[$key]['roomid'] = $item->roomid;
                $test[$key]['date'] = $item->date;
            }
            usort($test, array('ControllerCheckoutCart', 'sortDate'));

            foreach ($test as $item) {
                $start = $item['date'];
                $end = date("d/m/Y H:i", strtotime($start . ' +1 hour'));
                $date_booked = $this->model_booking_tools->createDateRangeArray($start, $end);
                $qty = $quantity;

                $realPriceResult = $this->model_booking_tools->getRealPrice($this->request->post, $date_booked, $product_id, $item['roomid'], $qty);

                if ($realPriceResult['status'] == 0) {
                    $error = $realPriceResult['message'];
                } else if ($realPriceResult['status'] == 3) {
                    $error = $realPriceResult['message'];
                } else {
                    $realprice = $realPriceResult['message'];
                }

                if (!$error) {
                    $dataBooking = array(
                        'product_id' => $this->request->post['product_id'],
                        'room_id' => $item['roomid'],
                        'room_name' => "",
                        'price' => $realprice,
                        'date' => $item['date']
                    );
                    $bookingDetails = $this->model_booking_tools->add($dataBooking);
                    $this->cart->add($this->request->post['product_id'], $quantity, $bookingDetails);
                    $json['success'] = sprintf($this->language->get('text_success'), $this->url->link('product/product', 'product_id=' . $this->request->post['product_id']), $product_info['name'], $this->url->link('checkout/cart'));
                    unset($this->session->data['payment_method']);
                    unset($this->session->data['payment_methods']);

                    // Totals
                    $this->load->model('setting/extension');


                    $json['total'] = sprintf($this->language->get('text_items'), $this->cart->countProducts(), '');
                } else {

                    $json['redirect'] = str_replace('&amp;', '&', $this->url->link('product/product', 'product_id=' . $this->request->post['product_id']));
                }
                $i++;
            }
        }
        $this->response->setOutput(json_encode($json));
    }

    private static function sortDate($a, $b) {
        return strtotime($a['date']) > strtotime($b['date']) ? 1 : -1;
    }

}

?>