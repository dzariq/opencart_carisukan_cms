<?php

class ControllerModuleSlideshow extends Controller {

    protected function index($setting) {
        static $module = 0;

        $this->load->model('design/banner');
        $this->load->model('tool/image');
        $this->load->model('catalog/product');

        //get all products
        $products = $this->model_catalog_product->getProducts();
        $productJson = array();


        $i = 0;

        foreach ($products as $product) {
            $location = null;
            $sql = 'SELECT id, x(p) as lat, y(p) as lng, zoom, address, width, height FROM `' . DB_PREFIX . 'productmaps` g WHERE id_product=' . $product['product_id'];
            if ($res = $this->db->query($sql)) {
                $location = $res->rows;
            }
            if ($location) {
                $productJson[$i]['lat'] = $location[0]['lat'];
                $productJson[$i]['lon'] = $location[0]['lng'];
            } else {
                $productJson[$i]['lat'] = 0;
                $productJson[$i]['lon'] = 0;
            }
            $productJson[$i]['title'] = $product['name'];
            $productJson[$i]['html'] = "<a href='" . $this->url->link('product/product', 'product_id=' . $product['product_id']) . "'><h3>" . $product['name'] . "</h3></a>";
            $i++;
        }
       
        $this->data['products'] = json_encode($productJson);

        if ($this->config->get('config_logo') && file_exists(DIR_IMAGE . $this->config->get('config_logo'))) {
//            $this->data['logo'] = $server . $this->config->get('config_logo');
            $this->data['logo'] = 'image/' . $this->config->get('config_logo');
        } else {
            $this->data['logo'] = '';
        }

        $this->data['width'] = $setting['width'];
        $this->data['height'] = $setting['height'];

        $this->data['banners'] = array();


        $iPod = stripos($_SERVER['HTTP_USER_AGENT'], "iPod");
        $iPhone = stripos($_SERVER['HTTP_USER_AGENT'], "iPhone");
        $iPad = stripos($_SERVER['HTTP_USER_AGENT'], "iPad");
        $Android = stripos($_SERVER['HTTP_USER_AGENT'], "Android");
        $Linux = stripos($_SERVER['HTTP_USER_AGENT'], "Linux");
        $webOS = stripos($_SERVER['HTTP_USER_AGENT'], "webOS");


        if (isset($setting['banner_id'])) {
            $results = $this->model_design_banner->getBanner($setting['banner_id']);

            foreach ($results as $result) {
                if (file_exists(DIR_IMAGE . $result['image'])) {


                    $this->data['banners'][] = array(
                        'title' => $result['title'],
                        'link' => $result['link'],
                        'description' => $result['description'],
                        'link_name' => $result['link_name'],
                        'image' => $this->model_tool_image->resize($result['image'], $setting['width'], $setting['height']),
                        'imageraw' => 'image/' . $result['image']
                    );
                }
            }
        }

        $this->data['module'] = $module++;

        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/slideshow.tpl')) {
            $this->template = $this->config->get('config_template') . '/template/module/slideshow.tpl';
        } else {
            $this->template = 'default/template/module/slideshow.tpl';
        }



        $this->render();
    }

}

?>