<?php

class ControllerModuleCarousel extends Controller {

    protected function index($setting) {
        static $module = 0;

        $this->load->model('design/banner');
        $this->load->model('tool/image');
        $this->load->model('catalog/product');

        $data1 = array(
            'filter_category_id' => 103
        );

        $data2 = array(
            'filter_category_id' => 158
        );

        $data3 = array(
            'filter_category_id' => 157
        );

        //get all products
        $productsAll = $this->model_catalog_product->getProducts();
        $productsFutsal = $this->model_catalog_product->getProducts($data1);
        $productsTennis = $this->model_catalog_product->getProducts($data2);
        $productsBadminton = $this->model_catalog_product->getProducts($data3);
        $productJson = array();


        $i = 0;

        foreach ($productsAll as $product) {
            $location = null;
            $sql = 'SELECT id, x(p) as lat, y(p) as lng, zoom, address, width, height FROM `' . DB_PREFIX . 'productmaps` g WHERE id_product=' . $product['product_id'];
            if ($res = $this->db->query($sql)) {
                $location = $res->rows;
            }
            if ($location) {
                $productJson[$i]['lat'] = $location[0]['lat'];
                $productJson[$i]['lon'] = $location[0]['lng'];
            } else {
                $productJson[$i]['lat'] = 0;
                $productJson[$i]['lon'] = 0;
            }

            $details = $this->model_catalog_product->getCategories($product['product_id']);
            $category = $details[0]['category_id'];
            $productJson[$i]['title'] = $product['name'];
            if ($category == '103') {
                $productJson[$i]['icon'] = 'http://carisukan.com/image/icon/icon-football-small.jpg';
            } else if ($category == '158') {
                $productJson[$i]['icon'] = 'http://carisukan.com/image/icon/icon-tennis-small.jpg';
            } else if ($category == '157') {
                $productJson[$i]['icon'] = 'http://carisukan.com/image/icon/icon-badminton-small.jpg';
            } else {
                
            }

            $productJson[$i]['html'] = "<a href='" . $this->url->link('product/product', 'product_id=' . $product['product_id']) . "'><h3>" . $product['name'] . "</h3></a>";
            $i++;
        }

        $this->data['productsAll'] = json_encode($productJson);
        $productJson = array();
        $i = 0;

        foreach ($productsFutsal as $product) {
            $location = null;
            $sql = 'SELECT id, x(p) as lat, y(p) as lng, zoom, address, width, height FROM `' . DB_PREFIX . 'productmaps` g WHERE id_product=' . $product['product_id'];
            if ($res = $this->db->query($sql)) {
                $location = $res->rows;
            }
            if ($location) {
                $productJson[$i]['lat'] = $location[0]['lat'];
                $productJson[$i]['lon'] = $location[0]['lng'];
            } else {
                $productJson[$i]['lat'] = 0;
                $productJson[$i]['lon'] = 0;
            }
            $productJson[$i]['zoom'] = 11;
            $productJson[$i]['title'] = $product['name'];
            $productJson[$i]['icon'] = 'http://carisukan.com/image/icon/icon-football-small.jpg';
            $productJson[$i]['html'] = "<a href='" . $this->url->link('product/product', 'product_id=' . $product['product_id']) . "'><h3>" . $product['name'] . "</h3></a>";
            $i++;
        }

        $this->data['productsFutsal'] = json_encode($productJson);
        $productJson = array();
        $i = 0;

        foreach ($productsTennis as $product) {
            $location = null;
            $sql = 'SELECT id, x(p) as lat, y(p) as lng, zoom, address, width, height FROM `' . DB_PREFIX . 'productmaps` g WHERE id_product=' . $product['product_id'];
            if ($res = $this->db->query($sql)) {
                $location = $res->rows;
            }
            if ($location) {
                $productJson[$i]['lat'] = $location[0]['lat'];
                $productJson[$i]['lon'] = $location[0]['lng'];
            } else {
                $productJson[$i]['lat'] = 0;
                $productJson[$i]['lon'] = 0;
            }
            $productJson[$i]['zoom'] = 11;
            $productJson[$i]['title'] = $product['name'];
            $productJson[$i]['icon'] = 'http://carisukan.com/image/icon/icon-tennis-small.jpg';
            $productJson[$i]['html'] = "<a href='" . $this->url->link('product/product', 'product_id=' . $product['product_id']) . "'><h3>" . $product['name'] . "</h3></a>";
            $i++;
        }
        $this->data['productsTennis'] = json_encode($productJson);
        $productJson = array();
        $i = 0;

        foreach ($productsBadminton as $product) {
            $location = null;
            $sql = 'SELECT id, x(p) as lat, y(p) as lng, zoom, address, width, height FROM `' . DB_PREFIX . 'productmaps` g WHERE id_product=' . $product['product_id'];
            if ($res = $this->db->query($sql)) {
                $location = $res->rows;
            }
            if ($location) {
                $productJson[$i]['lat'] = $location[0]['lat'];
                $productJson[$i]['lon'] = $location[0]['lng'];
            } else {
                $productJson[$i]['lat'] = 0;
                $productJson[$i]['lon'] = 0;
            }
            $productJson[$i]['zoom'] = 11;

            $productJson[$i]['title'] = $product['name'];
            $productJson[$i]['icon'] = 'http://carisukan.com/image/icon/icon-badminton-small.jpg';
            $productJson[$i]['html'] = "<a href='" . $this->url->link('product/product', 'product_id=' . $product['product_id']) . "'><h3>" . $product['name'] . "</h3></a>";
            $i++;
        }

        $this->data['productsBadminton'] = json_encode($productJson);
        $productJson = array();




        $this->data['module'] = $module++;

        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/carousel.tpl')) {
            $this->template = $this->config->get('config_template') . '/template/module/carousel.tpl';
        } else {
            $this->template = 'default/template/module/carousel.tpl';
        }



        $this->render();
    }

}

?>