<?php

class ControllerAccountAccount extends Controller {

    public function index() {
        if (!$this->customer->isLogged()) {
            $this->session->data['redirect'] = $this->url->link('account/account', '', 'SSL');

            $this->redirect($this->url->link('account/login', '', 'SSL'));
        }

        $this->load->model('account/customer_group');


        $customer_group = $this->model_account_customer_group->getCustomerGroup($this->customer->getCustomerGroupId());

        $this->language->load('account/account');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->data['membership_fee'] = $customer_group['membership_fee'];
        $this->data['membership'] = $customer_group['membership'];
        
        $this->data['breadcrumbs'] = array();

        $this->data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/home'),
            'separator' => false
        );

        $this->data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_account'),
            'href' => $this->url->link('account/account', '', 'SSL'),
            'separator' => $this->language->get('text_separator')
        );

        if (isset($this->session->data['success'])) {
            $this->data['success'] = $this->session->data['success'];

            unset($this->session->data['success']);
        } else {
            $this->data['success'] = '';
        }

        $this->data['heading_title'] = $this->language->get('heading_title');

        $this->data['text_my_account'] = $this->language->get('text_my_account');
        $this->data['text_account_number'] = $this->language->get('text_account_number');
        $this->data['text_account_date'] = $this->language->get('text_account_date');
        $this->data['text_my_orders'] = $this->language->get('text_my_orders');
        $this->data['text_my_newsletter'] = $this->language->get('text_my_newsletter');
        $this->data['text_edit'] = $this->language->get('text_edit');
        $this->data['text_password'] = $this->language->get('text_password');
        $this->data['text_address'] = $this->language->get('text_address');
        $this->data['text_wishlist'] = $this->language->get('text_wishlist');
        $this->data['text_order'] = $this->language->get('text_order');
        $this->data['text_download'] = $this->language->get('text_download');
        $this->data['text_reward'] = $this->language->get('text_reward');
        $this->data['text_return'] = $this->language->get('text_return');
        $this->data['text_transaction'] = $this->language->get('text_transaction');
        $this->data['text_newsletter'] = $this->language->get('text_newsletter');
        $this->data['text_renew'] = $this->language->get('text_renew');

        $this->load->model('account/address');


        $this->data['name'] = $this->customer->getFirstName() . ' ' . $this->customer->getLastName();
        $this->data['number'] = $this->customer->getNumber();
        $this->data['startdate'] = $this->customer->startDate();
        $this->data['enddate'] = $this->customer->endDate();
        $this->data['number'] = $this->customer->getNumber();
        $this->data['telephone'] = $this->customer->getTelephone();
        $this->data['email'] = $this->customer->getEmail();
        $address_id = $this->customer->getAddressId();
        $this->data['my_address'] = $this->model_account_address->getAddress($address_id);

        $this->data['edit'] = $this->url->link('account/edit', '', 'SSL');
        $this->data['password'] = $this->url->link('account/password', '', 'SSL');
        $this->data['address'] = $this->url->link('account/address', '', 'SSL');
        $this->data['wishlist'] = $this->url->link('account/wishlist');
        $this->data['order'] = $this->url->link('account/order', '', 'SSL');
        $this->data['download'] = $this->url->link('account/download', '', 'SSL');
        $this->data['return'] = $this->url->link('account/return', '', 'SSL');
        $this->data['transaction'] = $this->url->link('account/transaction', '', 'SSL');
        $this->data['newsletter'] = $this->url->link('account/newsletter', '', 'SSL');

        if ($this->config->get('reward_status')) {
            $this->data['reward'] = $this->url->link('account/reward', '', 'SSL');
        } else {
            $this->data['reward'] = '';
        }

        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/account/account.tpl')) {
            $this->template = $this->config->get('config_template') . '/template/account/account.tpl';
        } else {
            $this->template = 'default/template/account/account.tpl';
        }

        $this->children = array(
            'common/column_left',
            'common/column_right',
            'common/content_top',
            'common/content_bottom',
            'common/footer',
            'common/header'
        );

        $this->data['text_account_welcome'] = sprintf($this->language->get('text_account_welcome'), $this->customer->getFirstName());
        $this->response->setOutput($this->render());
    }

    public function renew() {
        //do payment insertion (membership_transaction table)
        $this->load->model('localisation/country');
        $this->load->model('localisation/zone');
        $this->load->model('account/address');
        $this->load->model('account/customer_group');
        $this->load->model('checkout/order');

        $customer_group = $this->model_account_customer_group->getCustomerGroup($this->customer->getCustomerGroupId());


        //customer_group_description
        $customer_group_desc = $this->model_account_customer_group->getCustomerGroupDescription($this->customer->getCustomerGroupId());

        $customer_address = $this->model_account_address->getAddress($this->customer->getAddressId());


        $data = array(
            'customer_id' => $this->customer->getId(),
            'customer_group_id' => $this->customer->getCustomerGroupId(),
            'firstname' => $this->customer->getFirstName(),
            'payment_firstname' => $this->customer->getFirstName(),
            'email' => $this->customer->getEmail(),
            'payment_address_1' => $customer_address['address_1'],
            'payment_city' => $customer_address['city'],
            'payment_postcode' => $customer_address['postcode'],
            'payment_country' => $customer_address['country'],
            'payment_country_id' => $customer_address['country_id'],
            'payment_zone' => $customer_address['zone'],
            'payment_zone_id' => $customer_address['zone_id'],
            'telephone' => $this->customer->getTelephone(),
            'status' => 0,
            'type' => 2,
            'comment' => $customer_group_desc['name'],
            'total' => $customer_group['membership_fee'],
        );

        $data['payment_method'] = $this->request->get['payment_method'];

        $data['payment_code'] = $this->request->get['payment_method'];

        $data['invoice_prefix'] = $this->config->get('config_invoice_prefix');
        $data['store_id'] = $this->config->get('config_store_id');
        $data['store_name'] = $this->config->get('config_name');

        if ($data['store_id']) {
            $data['store_url'] = $this->config->get('config_url');
        } else {
            $data['store_url'] = HTTP_SERVER;
        }

        $data['language_id'] = $this->config->get('config_language_id');
        $data['currency_id'] = $this->currency->getId();
        $data['currency_code'] = $this->currency->getCode();
        $data['currency_value'] = $this->currency->getValue($this->currency->getCode());
        $data['ip'] = $this->request->server['REMOTE_ADDR'];

        if (!empty($this->request->server['HTTP_X_FORWARDED_FOR'])) {
            $data['forwarded_ip'] = $this->request->server['HTTP_X_FORWARDED_FOR'];
        } elseif (!empty($this->request->server['HTTP_CLIENT_IP'])) {
            $data['forwarded_ip'] = $this->request->server['HTTP_CLIENT_IP'];
        } else {
            $data['forwarded_ip'] = '';
        }

        if (isset($this->request->server['HTTP_USER_AGENT'])) {
            $data['user_agent'] = $this->request->server['HTTP_USER_AGENT'];
        } else {
            $data['user_agent'] = '';
        }

        if (isset($this->request->server['HTTP_ACCEPT_LANGUAGE'])) {
            $data['accept_language'] = $this->request->server['HTTP_ACCEPT_LANGUAGE'];
        } else {
            $data['accept_language'] = '';
        }

        $order_id = $this->model_checkout_order->addOrder($data);
        $this->session->data['order_id'] = $order_id;

        if ($this->request->get['payment_method'] == 'ipay88') {
            if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/account/register_payment_ipay88.tpl')) {
                $this->template = $this->config->get('config_template') . '/template/account/register_payment_ipay88.tpl';
            } else {
                $this->template = 'default/template/account/register_payment_ipay88.tpl';
            }
        } else {
            if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/account/register_payment_pp_standard.tpl')) {
                $this->template = $this->config->get('config_template') . '/template/account/register_payment_pp_standard.tpl';
            } else {
                $this->template = 'default/template/account/register_payment_pp_standard.tpl';
            }
        }

        $this->response->setOutput($this->render());
    }

}

?>