<?php

class ControllerAccountRegister extends Controller {

    private $error = array();

    public function index() {
        if ($this->customer->isLogged()) {
            $this->redirect($this->url->link('account/account', '', 'SSL'));
        }

        $this->language->load('account/register');
        $this->load->model('account/customer_group');

        // Payment Methods

        $customer_group = $this->model_account_customer_group->getCustomerGroup($this->config->get('config_customer_group_id'));

        $customer_group_desc = $this->model_account_customer_group->getCustomerGroupDescription($this->config->get('config_customer_group_id'));


        $this->data['fee'] = $customer_group['membership_fee'];
        $this->data['group_desc'] = $customer_group_desc['description'];

        $method_data = array();

        if ($this->data['fee'] != 0 || $this->data['fee'] != '') {

            $method_data[0] = array(
                'code' => 'ipay88',
                'title' => 'Online Banking (ipay88)',
            );

            $method_data[1] = array(
                'code' => 'pp_standard',
                'title' => 'Paypal (Credit Card / Debit Card)',
            );
        }


        $this->session->data['payment_methods'] = $method_data;


        $this->data['text_payment_method'] = $this->language->get('text_payment_method');
        $this->data['text_comments'] = $this->language->get('text_comments');

        $this->data['button_continue'] = $this->language->get('button_continue');

        if (empty($this->session->data['payment_methods'])) {
            $this->data['error_warning'] = sprintf($this->language->get('error_no_payment'), $this->url->link('information/contact'));
        } else {
            $this->data['error_warning'] = '';
        }

        if (isset($this->session->data['payment_methods'])) {
            $this->data['payment_methods'] = $this->session->data['payment_methods'];
        } else {
            $this->data['payment_methods'] = array();
        }

        if (isset($this->session->data['payment_method']['code'])) {
            $this->data['code'] = $this->session->data['payment_method']['code'];
        } else {
            $this->data['code'] = '';
        }
        // Payment Methods

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('account/customer');
        $this->load->model('checkout/order');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {

            // Customer Group
//            if (isset($this->request->post['customer_group_id']) && is_array($this->config->get('config_customer_group_display')) && in_array($this->request->post['customer_group_id'], $this->config->get('config_customer_group_display'))) {
            if (isset($this->request->post['customer_group_id'])) {
                $customer_group_id = $this->request->post['customer_group_id'];
            } else {
                $customer_group_id = $this->config->get('config_customer_group_id');
            }

            $customer_group = $this->model_account_customer_group->getCustomerGroup($customer_group_id);

            $domain_created = 0;


            if ($this->request->post['payment_method'] != '') {

                $customer_group = $this->model_account_customer_group->getCustomerGroup($this->request->post['customer_group_id']);
                $customer_group_desc = $this->model_account_customer_group->getCustomerGroupDescription($this->request->post['customer_group_id']);

                //do payment insertion (membership_transaction table)
                $this->load->model('localisation/country');
                $this->load->model('localisation/zone');

                $country = $this->model_localisation_country->getCountry($this->request->post['country_id']);
                $zone = $this->model_localisation_zone->getZone($this->request->post['zone_id']);



                $data = array(
                    'customer_id' => '',
                    'customer_group_id' => $customer_group_id,
                    'firstname' => $this->request->post['firstname'],
                    'payment_firstname' => $this->request->post['firstname'],
                    'email' => $this->request->post['email'],
                    'payment_address_1' => $this->request->post['address_1'],
                    'payment_city' => $this->request->post['city'],
                    'payment_postcode' => $this->request->post['postcode'],
                    'payment_country' => $country['name'],
                    'payment_country_id' => $this->request->post['country_id'],
                    'payment_zone' => $zone['name'],
                    'payment_zone_id' => $this->request->post['zone_id'],
                    'password' => $this->request->post['password'],
                    'telephone' => $this->request->post['telephone'],
                    'status' => 0,
                    'type' => 1,
                    'comment' => $customer_group_desc['name'],
                    'total' => $customer_group['membership_fee'],
                );
                $data['payment_method'] = $this->request->post['payment_method'];

                $data['payment_code'] = $this->request->post['payment_method'];

                $data['invoice_prefix'] = $this->config->get('config_invoice_prefix');
                $data['store_id'] = $this->config->get('config_store_id');
                $data['store_name'] = $this->config->get('config_name');

                if ($data['store_id']) {
                    $data['store_url'] = $this->config->get('config_url');
                } else {
                    $data['store_url'] = HTTP_SERVER;
                }

                $data['language_id'] = $this->config->get('config_language_id');
                $data['currency_id'] = $this->currency->getId();
                $data['currency_code'] = $this->currency->getCode();
                $data['currency_value'] = $this->currency->getValue($this->currency->getCode());
                $data['ip'] = $this->request->server['REMOTE_ADDR'];

                if (!empty($this->request->server['HTTP_X_FORWARDED_FOR'])) {
                    $data['forwarded_ip'] = $this->request->server['HTTP_X_FORWARDED_FOR'];
                } elseif (!empty($this->request->server['HTTP_CLIENT_IP'])) {
                    $data['forwarded_ip'] = $this->request->server['HTTP_CLIENT_IP'];
                } else {
                    $data['forwarded_ip'] = '';
                }

                if (isset($this->request->server['HTTP_USER_AGENT'])) {
                    $data['user_agent'] = $this->request->server['HTTP_USER_AGENT'];
                } else {
                    $data['user_agent'] = '';
                }

                if (isset($this->request->server['HTTP_ACCEPT_LANGUAGE'])) {
                    $data['accept_language'] = $this->request->server['HTTP_ACCEPT_LANGUAGE'];
                } else {
                    $data['accept_language'] = '';
                }

                $order_id = $this->model_checkout_order->addOrder($data);
                $this->session->data['order_id'] = $order_id;

                $redirect = 0;
            } else {
                $this->model_account_customer->addCustomer($this->request->post);
                $this->customer->login($this->request->post['email'], $this->request->post['password']);

                unset($this->session->data['guest']);

                // Default Shipping Address
                if ($this->config->get('config_tax_customer') == 'shipping') {
                    $this->session->data['shipping_country_id'] = $this->request->post['country_id'];
                    $this->session->data['shipping_zone_id'] = $this->request->post['zone_id'];
                    $this->session->data['shipping_postcode'] = $this->request->post['postcode'];
                }

                // Default Payment Address
                if ($this->config->get('config_tax_customer') == 'payment') {
                    $this->session->data['payment_country_id'] = $this->request->post['country_id'];
                    $this->session->data['payment_zone_id'] = $this->request->post['zone_id'];
                }

                //            if webname exist, create it
                if (isset($this->request->post['webname']) && $this->request->post['webname'] != '') {
                    header('Access-Control-Allow-Origin: *');

                    $this->createSubdomain($this->request->post['webname'], $this->request->post['email'], $this->request->post['firstname']);
                    $domain_created = 1;
                }

                $redirect = 1;
            }



            $response = array(
                'status' => 1,
                'domain_created' => $domain_created,
                'redirect' => $redirect,
                'remark' => 'success',
                'payment_method' => $this->request->post['payment_method'],
            );
            echo json_encode($response);
            die;
        } else {
            if ($this->request->server['REQUEST_METHOD'] == 'POST') {
                $response = array(
                    'status' => 0,
                    'remark' => $this->error,
                );

                echo json_encode($response);

                die;
            }
        }

        $this->data['breadcrumbs'] = array();

        $this->data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/home'),
            'separator' => false
        );

        $this->data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_account'),
            'href' => $this->url->link('account/account', '', 'SSL'),
            'separator' => $this->language->get('text_separator')
        );

        $this->data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_register'),
            'href' => $this->url->link('account/register', '', 'SSL'),
            'separator' => $this->language->get('text_separator')
        );

        $this->data['heading_title'] = $this->language->get('heading_title');

        $this->data['text_account_already'] = sprintf($this->language->get('text_account_already'), $this->url->link('account/login', '', 'SSL'));
        $this->data['text_your_details'] = $this->language->get('text_your_details');
        $this->data['text_your_address'] = $this->language->get('text_your_address');
        $this->data['text_your_password'] = $this->language->get('text_your_password');
        $this->data['text_newsletter'] = $this->language->get('text_newsletter');
        $this->data['text_yes'] = $this->language->get('text_yes');
        $this->data['text_no'] = $this->language->get('text_no');
        $this->data['text_select'] = $this->language->get('text_select');
        $this->data['text_none'] = $this->language->get('text_none');

        $this->data['entry_firstname'] = $this->language->get('entry_firstname');
        $this->data['entry_lastname'] = $this->language->get('entry_lastname');
        $this->data['entry_email'] = $this->language->get('entry_email');
        $this->data['entry_telephone'] = $this->language->get('entry_telephone');
        $this->data['entry_fax'] = $this->language->get('entry_fax');
        $this->data['entry_company'] = $this->language->get('entry_company');
        $this->data['entry_customer_group'] = $this->language->get('entry_customer_group');
        $this->data['entry_company_id'] = $this->language->get('entry_company_id');
        $this->data['entry_tax_id'] = $this->language->get('entry_tax_id');
        $this->data['entry_address_1'] = $this->language->get('entry_address_1');
        $this->data['entry_address_2'] = $this->language->get('entry_address_2');
        $this->data['entry_postcode'] = $this->language->get('entry_postcode');
        $this->data['entry_city'] = $this->language->get('entry_city');
        $this->data['entry_country'] = $this->language->get('entry_country');
        $this->data['entry_zone'] = $this->language->get('entry_zone');
        $this->data['entry_newsletter'] = $this->language->get('entry_newsletter');
        $this->data['entry_password'] = $this->language->get('entry_password');
        $this->data['entry_confirm'] = $this->language->get('entry_confirm');

        $this->data['button_continue'] = $this->language->get('button_continue');

        if (isset($this->error['warning'])) {
            $this->data['error_warning'] = $this->error['warning'];
        } else {
            $this->data['error_warning'] = '';
        }

        if (isset($this->error['firstname'])) {
            $this->data['error_firstname'] = $this->error['firstname'];
        } else {
            $this->data['error_firstname'] = '';
        }

        if (isset($this->error['lastname'])) {
            $this->data['error_lastname'] = $this->error['lastname'];
        } else {
            $this->data['error_lastname'] = '';
        }

        if (isset($this->error['email'])) {
            $this->data['error_email'] = $this->error['email'];
        } else {
            $this->data['error_email'] = '';
        }

        if (isset($this->error['telephone'])) {
            $this->data['error_telephone'] = $this->error['telephone'];
        } else {
            $this->data['error_telephone'] = '';
        }

        if (isset($this->error['password'])) {
            $this->data['error_password'] = $this->error['password'];
        } else {
            $this->data['error_password'] = '';
        }

        if (isset($this->error['confirm'])) {
            $this->data['error_confirm'] = $this->error['confirm'];
        } else {
            $this->data['error_confirm'] = '';
        }

        if (isset($this->error['company_id'])) {
            $this->data['error_company_id'] = $this->error['company_id'];
        } else {
            $this->data['error_company_id'] = '';
        }

        if (isset($this->error['tax_id'])) {
            $this->data['error_tax_id'] = $this->error['tax_id'];
        } else {
            $this->data['error_tax_id'] = '';
        }

        if (isset($this->error['address_1'])) {
            $this->data['error_address_1'] = $this->error['address_1'];
        } else {
            $this->data['error_address_1'] = '';
        }

        if (isset($this->error['city'])) {
            $this->data['error_city'] = $this->error['city'];
        } else {
            $this->data['error_city'] = '';
        }

        if (isset($this->error['postcode'])) {
            $this->data['error_postcode'] = $this->error['postcode'];
        } else {
            $this->data['error_postcode'] = '';
        }

        if (isset($this->error['country'])) {
            $this->data['error_country'] = $this->error['country'];
        } else {
            $this->data['error_country'] = '';
        }

        if (isset($this->error['zone'])) {
            $this->data['error_zone'] = $this->error['zone'];
        } else {
            $this->data['error_zone'] = '';
        }

        $this->data['action'] = $this->url->link('account/register', '', 'SSL');

        if (isset($this->request->post['firstname'])) {
            $this->data['firstname'] = $this->request->post['firstname'];
        } else {
            $this->data['firstname'] = '';
        }

        if (isset($this->request->post['lastname'])) {
            $this->data['lastname'] = $this->request->post['lastname'];
        } else {
            $this->data['lastname'] = '';
        }

        if (isset($this->request->post['email'])) {
            $this->data['email'] = $this->request->post['email'];
        } else {
            $this->data['email'] = '';
        }

        if (isset($this->request->post['telephone'])) {
            $this->data['telephone'] = $this->request->post['telephone'];
        } else {
            $this->data['telephone'] = '';
        }

        if (isset($this->request->post['fax'])) {
            $this->data['fax'] = $this->request->post['fax'];
        } else {
            $this->data['fax'] = '';
        }

        if (isset($this->request->post['company'])) {
            $this->data['company'] = $this->request->post['company'];
        } else {
            $this->data['company'] = '';
        }

        $this->load->model('account/customer_group');

        $this->data['customer_groups'] = array();

        $customer_groups = $this->model_account_customer_group->getCustomerGroups();

        foreach ($customer_groups as $customer_group) {
            if (in_array($customer_group['customer_group_id'], $this->config->get('config_customer_group_display'))) {
                $this->data['customer_groups'][] = $customer_group;
            }
        }

        if (isset($this->request->post['customer_group_id'])) {
            $this->data['customer_group_id'] = $this->request->post['customer_group_id'];
        } else {
            $this->data['customer_group_id'] = $this->config->get('config_customer_group_id');
        }

        // Company ID
        if (isset($this->request->post['company_id'])) {
            $this->data['company_id'] = $this->request->post['company_id'];
        } else {
            $this->data['company_id'] = '';
        }

        // Tax ID
        if (isset($this->request->post['tax_id'])) {
            $this->data['tax_id'] = $this->request->post['tax_id'];
        } else {
            $this->data['tax_id'] = '';
        }

        if (isset($this->request->post['address_1'])) {
            $this->data['address_1'] = $this->request->post['address_1'];
        } else {
            $this->data['address_1'] = '';
        }

        if (isset($this->request->post['address_2'])) {
            $this->data['address_2'] = $this->request->post['address_2'];
        } else {
            $this->data['address_2'] = '';
        }

        if (isset($this->request->post['postcode'])) {
            $this->data['postcode'] = $this->request->post['postcode'];
        } elseif (isset($this->session->data['shipping_postcode'])) {
            $this->data['postcode'] = $this->session->data['shipping_postcode'];
        } else {
            $this->data['postcode'] = '';
        }

        if (isset($this->request->post['city'])) {
            $this->data['city'] = $this->request->post['city'];
        } else {
            $this->data['city'] = '';
        }

        if (isset($this->request->post['country_id'])) {
            $this->data['country_id'] = $this->request->post['country_id'];
        } elseif (isset($this->session->data['shipping_country_id'])) {
            $this->data['country_id'] = $this->session->data['shipping_country_id'];
        } else {
            $this->data['country_id'] = $this->config->get('config_country_id');
        }

        if (isset($this->request->post['zone_id'])) {
            $this->data['zone_id'] = $this->request->post['zone_id'];
        } elseif (isset($this->session->data['shipping_zone_id'])) {
            $this->data['zone_id'] = $this->session->data['shipping_zone_id'];
        } else {
            $this->data['zone_id'] = '';
        }

        $this->load->model('localisation/country');

        $this->data['countries'] = $this->model_localisation_country->getCountries();

        if (isset($this->request->post['password'])) {
            $this->data['password'] = $this->request->post['password'];
        } else {
            $this->data['password'] = '';
        }

        if (isset($this->request->post['confirm'])) {
            $this->data['confirm'] = $this->request->post['confirm'];
        } else {
            $this->data['confirm'] = '';
        }

        if (isset($this->request->post['newsletter'])) {
            $this->data['newsletter'] = $this->request->post['newsletter'];
        } else {
            $this->data['newsletter'] = '';
        }

        if ($this->config->get('config_account_id')) {
            $this->load->model('catalog/information');

            $information_info = $this->model_catalog_information->getInformation($this->config->get('config_account_id'));

            if ($information_info) {
                $this->data['text_agree'] = sprintf($this->language->get('text_agree'), $this->url->link('information/information/info', 'information_id=' . $this->config->get('config_account_id'), 'SSL'), $information_info['title'], $information_info['title']);
            } else {
                $this->data['text_agree'] = '';
            }
        } else {
            $this->data['text_agree'] = '';
        }

        if (isset($this->request->post['agree'])) {
            $this->data['agree'] = $this->request->post['agree'];
        } else {
            $this->data['agree'] = false;
        }

        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/account/register.tpl')) {
            $this->template = $this->config->get('config_template') . '/template/account/register.tpl';
        } else {
            $this->template = 'default/template/account/register.tpl';
        }

        $this->children = array(
            'common/column_left',
            'common/column_right',
            'common/content_top',
            'common/content_bottom',
            'common/footer',
            'common/header'
        );



        $this->response->setOutput($this->render());
    }

    private function validate() {



        if ((utf8_strlen($this->request->post['firstname']) < 1) || (utf8_strlen($this->request->post['firstname']) > 32)) {
            $this->error['firstname'] = $this->language->get('error_firstname');
        }

//        if ((utf8_strlen($this->request->post['lastname']) < 1) || (utf8_strlen($this->request->post['lastname']) > 32)) {
//            $this->error['lastname'] = $this->language->get('error_lastname');
//        }

        if ((utf8_strlen($this->request->post['email']) > 96) || !preg_match('/^[^\@]+@.*\.[a-z]{2,6}$/i', $this->request->post['email'])) {
            $this->error['email'] = $this->language->get('error_email');
        }

        if ($this->model_account_customer->getTotalCustomersByEmail($this->request->post['email'])) {
            $this->error['warning'] = $this->language->get('error_exists');
        }

        if ((utf8_strlen($this->request->post['telephone']) < 3) || (utf8_strlen($this->request->post['telephone']) > 32)) {
            $this->error['telephone'] = $this->language->get('error_telephone');
        }

        // Customer Group
        $this->load->model('account/customer_group');

        if (isset($this->request->post['customer_group_id']) && is_array($this->config->get('config_customer_group_display')) && in_array($this->request->post['customer_group_id'], $this->config->get('config_customer_group_display'))) {
            $customer_group_id = $this->request->post['customer_group_id'];
        } else {
            $customer_group_id = $this->config->get('config_customer_group_id');
        }

        $customer_group = $this->model_account_customer_group->getCustomerGroup($customer_group_id);

        if ($customer_group) {
            // Company ID
            if ($customer_group['company_id_display'] && $customer_group['company_id_required'] && empty($this->request->post['company_id'])) {
                $this->error['company_id'] = $this->language->get('error_company_id');
            }

            // Tax ID 
            if ($customer_group['tax_id_display'] && $customer_group['tax_id_required'] && empty($this->request->post['tax_id'])) {
                $this->error['tax_id'] = $this->language->get('error_tax_id');
            }
        }

        if ((utf8_strlen($this->request->post['address_1']) < 3) || (utf8_strlen($this->request->post['address_1']) > 128)) {
            $this->error['address_1'] = $this->language->get('error_address_1');
        }

        if ((utf8_strlen($this->request->post['city']) < 2) || (utf8_strlen($this->request->post['city']) > 128)) {
            $this->error['city'] = $this->language->get('error_city');
        }

        $this->load->model('localisation/country');

        $country_info = $this->model_localisation_country->getCountry($this->request->post['country_id']);

        if ($country_info) {
            if ($country_info['postcode_required'] && (utf8_strlen($this->request->post['postcode']) < 2) || (utf8_strlen($this->request->post['postcode']) > 10)) {
                $this->error['postcode'] = $this->language->get('error_postcode');
            }

            // VAT Validation
            $this->load->helper('vat');

            if ($this->config->get('config_vat') && $this->request->post['tax_id'] && (vat_validation($country_info['iso_code_2'], $this->request->post['tax_id']) == 'invalid')) {
                $this->error['tax_id'] = $this->language->get('error_vat');
            }
        }

        if ($this->request->post['country_id'] == '') {
            $this->error['country'] = $this->language->get('error_country');
        }

        if ($this->request->post['zone_id'] == '') {
            $this->error['zone'] = $this->language->get('error_zone');
        }

        if ((utf8_strlen($this->request->post['password']) < 4) || (utf8_strlen($this->request->post['password']) > 20)) {
            $this->error['password'] = $this->language->get('error_password');
        }

        if ($this->request->post['confirm'] != $this->request->post['password']) {
            $this->error['confirm'] = $this->language->get('error_confirm');
        }

        if ($this->config->get('config_account_id')) {
            $this->load->model('catalog/information');

            $information_info = $this->model_catalog_information->getInformation($this->config->get('config_account_id'));

            if ($information_info && !isset($this->request->post['agree'])) {
                $this->error['warning'] = sprintf($this->language->get('error_agree'), $information_info['title']);
            }
        }


        if (!$this->error) {
            return true;
        } else {
            return false;
        }
    }

    public function country() {
        $json = array();

        $this->load->model('localisation/country');

        $country_info = $this->model_localisation_country->getCountry($this->request->get['country_id']);

        if ($country_info) {
            $this->load->model('localisation/zone');

            $json = array(
                'country_id' => $country_info['country_id'],
                'name' => $country_info['name'],
                'iso_code_2' => $country_info['iso_code_2'],
                'iso_code_3' => $country_info['iso_code_3'],
                'address_format' => $country_info['address_format'],
                'postcode_required' => $country_info['postcode_required'],
                'zone' => $this->model_localisation_zone->getZonesByCountryId($this->request->get['country_id']),
                'status' => $country_info['status']
            );
        }

        $this->response->setOutput(json_encode($json));
    }

    public function payment_pp_standard() {
        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/account/register_payment_pp_standard.tpl')) {
            $this->template = $this->config->get('config_template') . '/template/account/register_payment_pp_standard.tpl';
        } else {
            $this->template = 'default/template/account/register_payment_pp_standard.tpl';
        }

        $this->response->setOutput($this->render());
    }

    public function payment_ipay88() {
        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/account/register_payment_ipay88.tpl')) {
            $this->template = $this->config->get('config_template') . '/template/account/register_payment_ipay88.tpl';
        } else {
            $this->template = 'default/template/account/register_payment_ipay88.tpl';
        }

        $this->response->setOutput($this->render());
    }

    public function createSubdomain($my_domain, $email, $firstname) {

        //register subdomain
        $this->db->query("INSERT INTO " . DB_PREFIX . "domain_name SET name = '" . $my_domain . "'");


        require("xmlapi.php"); // this can be downlaoded

        $temp = explode('.', strtolower($my_domain));

        if (count($temp) == 3 && ($temp[2] == 'my')) {
            $db_name = $temp[0] . $temp[1] . $temp[2];
        } else if (count($temp) == 2) {
            $db_name = $temp[0] . $temp[1];
        } else if (count($temp) == 3) {
            $db_name = $temp[0];
        } else {
            $db_name = $temp[0] . $temp[1];
        }

        if (!file_exists('/home/creativeparttime/public_html/ecommerce/image/data/' . strtolower($this->clean($db_name)) . '/')) {

            mkdir('/home/creativeparttime/public_html/ecommerce/image/data/' . strtolower($this->clean($db_name)) . '/', 0777, true);
        }

        $xmlapi = new xmlapi("localhost");
        $opts['user'] = 'creativeparttime';
        $opts['pass'] = 'dzariqmirza85';


        $xmlapi->set_port(2083);
        $xmlapi->password_auth($opts['user'], $opts['pass']);
        $xmlapi->set_debug(1); //output actions in the error log 1 for true and 0 false


        $cpaneluser = $opts['user'];

        $my_domain = strtolower($this->clean($my_domain));


        $databasename = "creative_ecommerce_" . $db_name;

        $databaseuser = "creative_admin";

        $databasepass = $opts['pass'];

//create database

        $createdb = $xmlapi->api1_query($cpaneluser, "Mysql", "adddb", array($databasename));

//add user to database

        $addusr = $xmlapi->api1_query($cpaneluser, "Mysql", "adduserdb", array($databasename, 'creative_admin', 'all'));

        $mysqli = new mysqli('localhost', 'creative_admin', 'dzariqmirza85', $databasename);


        if (mysqli_connect_error()) {

            die('Connect Error (' . mysqli_connect_errno() . ') '
                    . mysqli_connect_error());
        }


        $sql = file_get_contents('sqlecommerce_test.sql');

        //store client email as user admin email
        $sql = str_replace('{email}', $email, $sql);
        $sql = str_replace('{name}', $firstname, $sql);
        $sql = str_replace('{domain}', $temp[0], $sql);

        if (!$sql) {

            die('Error opening file');
        }




        mysqli_multi_query($mysqli, $sql);



        $mysqli->close();
    }

    public function clean($string) {
        $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
        $string = preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.

        return preg_replace('/-+/', '-', $string); // Replaces multiple hyphens with single one.
    }

}

?>