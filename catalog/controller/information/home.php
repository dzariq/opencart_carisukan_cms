<?php

class ControllerInformationHome extends Controller {

    public function index($setting) {

        $this->data['breadcrumbs'] = array();

        $this->language->load('module/featured');


        $this->data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/home'),
            'separator' => false
        );

        $this->document->setTitle('Home');

        $this->data['text_enter_website'] = $this->language->get('text_enter_website');


        $this->data['breadcrumbs'][] = array(
            'text' => $information_info['title'],
            'href' => $this->url->link('information/home'),
            'separator' => $this->language->get('text_separator')
        );



        $this->data['title'] = $this->document->getTitle();

        if (isset($this->request->server['HTTPS']) && (($this->request->server['HTTPS'] == 'on') || ($this->request->server['HTTPS'] == '1'))) {
            $this->data['base'] = $this->config->get('config_ssl');
        } else {
            $this->data['base'] = $this->config->get('config_url');
        }

        $this->data['description'] = $this->document->getDescription();
        $this->data['keywords'] = $this->document->getKeywords();
        $this->data['links'] = $this->document->getLinks();
        $this->data['styles'] = $this->document->getStyles();
        $this->data['scripts'] = $this->document->getScripts();
        $this->data['lang'] = $this->language->get('code');
        $this->data['direction'] = $this->language->get('direction');
        $this->data['google_analytics'] = html_entity_decode($this->config->get('config_google_analytics'), ENT_QUOTES, 'UTF-8');

        // Whos Online
        if ($this->config->get('config_customer_online')) {
            $this->load->model('tool/online');

            if (isset($this->request->server['REMOTE_ADDR'])) {
                $ip = $this->request->server['REMOTE_ADDR'];
            } else {
                $ip = '';
            }

            if (isset($this->request->server['HTTP_HOST']) && isset($this->request->server['REQUEST_URI'])) {
                $url = 'http://' . $this->request->server['HTTP_HOST'] . $this->request->server['REQUEST_URI'];
            } else {
                $url = '';
            }

            if (isset($this->request->server['HTTP_REFERER'])) {
                $referer = $this->request->server['HTTP_REFERER'];
            } else {
                $referer = '';
            }

            $this->model_tool_online->whosonline($ip, $this->customer->getId(), $url, $referer);
        }

        $this->language->load('common/header');

        if (isset($this->request->server['HTTPS']) && (($this->request->server['HTTPS'] == 'on') || ($this->request->server['HTTPS'] == '1'))) {
            $server = HTTPS_IMAGE;
        } else {
            $server = HTTP_IMAGE;
        }

        if ($this->config->get('config_icon') && file_exists(DIR_IMAGE . $this->config->get('config_icon'))) {
            $this->data['icon'] = $server . $this->config->get('config_icon');
        } else {
            $this->data['icon'] = '';
        }

        $this->data['name'] = $this->config->get('config_name');

        if ($this->config->get('config_logo') && file_exists(DIR_IMAGE . $this->config->get('config_logo'))) {
            $this->data['logo'] = $server . $this->config->get('config_logo');
        } else {
            $this->data['logo'] = '';
        }
		$this->data['powered'] = $this->config->get('config_name') . ' &copy; ' . date('Y', time());// = sprintf($this->language->get('text_powered'), $this->config->get('config_name'), date('Y', time()));

        $this->data['text_categories'] = $this->language->get('text_categories');
        $this->data['text_contact'] = $this->language->get('text_contact');
        $this->data['text_featured'] = $this->language->get('heading_title');
        $this->data['text_home'] = $this->language->get('text_home');
        $this->data['text_wishlist'] = sprintf($this->language->get('text_wishlist'), (isset($this->session->data['wishlist']) ? count($this->session->data['wishlist']) : 0));
        $this->data['text_shopping_cart'] = $this->language->get('text_shopping_cart');
        $this->data['text_search'] = $this->language->get('text_search');
        $this->data['text_welcome'] = sprintf($this->language->get('text_welcome'), $this->url->link('account/login', '', 'SSL'), $this->url->link('account/register', '', 'SSL'));
        $this->data['text_logged'] = sprintf($this->language->get('text_logged'), $this->url->link('account/account', '', 'SSL'), $this->customer->getFirstName(), $this->url->link('account/logout', '', 'SSL'));
        $this->data['text_account'] = $this->language->get('text_account');
        $this->data['text_special'] = $this->language->get('text_special');

        $this->data['text_checkout'] = $this->language->get('text_checkout');

        $this->data['home'] = $this->url->link('common/home');
        $this->data['wishlist'] = $this->url->link('account/wishlist', '', 'SSL');
        $this->data['logged'] = $this->customer->isLogged();
        $this->data['account'] = $this->url->link('account/account', '', 'SSL');
        $this->data['shopping_cart'] = $this->url->link('checkout/cart');
        $this->data['checkout'] = $this->url->link('checkout/checkout', '', 'SSL');

        if (isset($this->request->get['filter_name'])) {
            $this->data['filter_name'] = $this->request->get['filter_name'];
        } else {
            $this->data['filter_name'] = $this->language->get('text_search');
        }

        $this->load->model('catalog/product');



        $this->load->model('tool/image');



        $this->data['products'] = array();



        $products = explode(',', $this->config->get('featured_product'));



        if (empty($setting['limit'])) {

            $setting['limit'] = 5;
        }



        $products = array_slice($products, 0, (int) $setting['limit']);



        foreach ($products as $product_id) {

            $product_info = $this->model_catalog_product->getProduct($product_id);



            if ($product_info) {

                if ($product_info['image']) {

                    $image = $this->model_tool_image->resize($product_info['image'], 368, 368);
                } else {

                    $image = false;
                }



                if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {

                    $price = $this->currency->format($this->tax->calculate($product_info['price'], $product_info['tax_class_id'], $this->config->get('config_tax')));
                } else {

                    $price = false;
                }



                if ((float) $product_info['special']) {

                    $special = $this->currency->format($this->tax->calculate($product_info['special'], $product_info['tax_class_id'], $this->config->get('config_tax')));
                } else {

                    $special = false;
                }



                if ($this->config->get('config_review_status')) {

                    $rating = $product_info['rating'];
                } else {

                    $rating = false;
                }



                $this->data['products'][] = array(
                    'description' => utf8_substr(strip_tags(html_entity_decode($product_info['description'], ENT_QUOTES, 'UTF-8')), 0, 150) . '..',
                    'product_id' => $product_info['product_id'],
                    'thumb' => $image,
                    'name' => $product_info['name'],
                    'viewed' => $product_info['viewed'],
                    'price' => $price,
                    'special' => $special,
                    'rating' => $rating,
                    'reviews' => sprintf($this->language->get('text_reviews'), (int) $product_info['reviews']),
                    'href' => $this->url->link('product/product', 'product_id=' . $product_info['product_id']),
                );
            }
        }

        // Menu
        $this->load->model('catalog/category');

        $this->load->model('catalog/product');

        $this->data['categories'] = array();

        $categories = $this->model_catalog_category->getCategories(0);
        foreach ($categories as $category) {
            if ($category['top']) {
                $children_data = array();

                $children = $this->model_catalog_category->getCategories($category['category_id']);
                foreach ($children as $child) {
                    $data = array(
                        'filter_category_id' => $child['category_id'],
                        'filter_sub_category' => true
                    );

                    $product_total = $this->model_catalog_product->getTotalProducts($data);

                    if (!isset($child['linkto']))
                        $child['linkto'] = '';
                    if ($child['linkto']) {
                        $link = $child['linkto'];
                    } else {
                        if (isset($child['database'])) {

                            $link = $this->url->link('product/category', 'path=' . $category['category_id'] . '_' . $child['category_id'] . '&db=' . $child['database']);
                        } else {
                            $link = $this->url->link('product/category', 'path=' . $category['category_id'] . '_' . $child['category_id']);
                        }
                    }

                    $children_data[] = array(
                        'name' => $child['name'] . ($this->config->get('config_product_count') ? ' (' . $product_total . ')' : ''),
                        'href' => $link,
                        'image' => $child['image'],
                        'description' => $child['description'],
                        'description' => utf8_substr(strip_tags(html_entity_decode($child['description'], ENT_QUOTES, 'UTF-8')), 0, 150) . '..',
                    );
                }

                // Level 1

                if (!isset($category['linkto']))
                    $category['linkto'] = '';
                if ($category['linkto']) {
                    $link = $category['linkto'];
                } else
                if (isset($category['database'])) {
                    $link = $this->url->link('product/category', 'path=' . $category['category_id'] . '&db=' . $category['database']);
                } else {
                    $link = $this->url->link('product/category', 'path=' . $category['category_id']);
                }
            }

            $this->data['categories'][] = array(
                'name' => $category['name'],
                'children' => $children_data,
                'column' => $category['column'] ? $category['column'] : 1,
                'href' => $link,
                'image' => $category['image'],
                'description' => utf8_substr(strip_tags(html_entity_decode($category['description'], ENT_QUOTES, 'UTF-8')), 0, 150),
            );
        }


        $this->load->model('catalog/information');

        $this->data['informations'] = array();

        foreach ($this->model_catalog_information->getInformations() as $result) {
            $this->data['informations'][] = array(
                'name' => $result['title'],
                'description' => utf8_substr(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8')), 0, 150),
                'children' => '',
                'column' => 1,
                'href' => $this->url->link('information/information', 'information_id=' . $result['information_id'])
            );
        }


        $this->data['button_continue'] = $this->language->get('button_continue');



        $this->data['description'] = html_entity_decode($information_info['description'], ENT_QUOTES, 'UTF-8');



        $this->data['continue'] = $this->url->link('common/home');



        $this->template = '../../../theme/'.$this->config->get('config_subtheme').'/index.tpl';






        $this->response->setOutput($this->render());
    }

}

?>