<?php

class ControllerInformationContact extends Controller {

    private $error = array();

    public function index() {
        $this->language->load('information/contact');

        $this->document->setTitle($this->language->get('heading_title'));

        $uploaded_file_name1 = '';
        $uploaded_file_name2 = '';

        if (isset($_FILES["attachment1"]['name'])) {
            $uploaded_file_name1 = $_FILES["attachment1"]['name'];
        }

        if (isset($_FILES["attachment2"]['name'])) {
            $uploaded_file_name2 = $_FILES["attachment2"]['name'];
        }

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
            $mail = new Mail();
            $mail->protocol = $this->config->get('config_mail_protocol');
            $mail->parameter = $this->config->get('config_mail_parameter');
            $mail->hostname = $this->config->get('config_smtp_host');
            $mail->username = $this->config->get('config_smtp_username');
            $mail->password = $this->config->get('config_smtp_password');
            $mail->port = $this->config->get('config_smtp_port');
            $mail->timeout = $this->config->get('config_smtp_timeout');
            $mail->setTo($this->config->get('config_email'));
            if ($uploaded_file_name1 != '') {
                $resultUpload = $this->uploadFile($_FILES["attachment1"]);

                if ($resultUpload['error']) {
                    if ($this->request->post['json'] == 1) {
                        $data = array(
                            'status' => 0,
                            'remark' => $resultUpload['error']
                        );
                        echo json_encode($data);
                        exit();
                    }

                    $this->error['attachment1'] = $resultUpload['error'];
                }
                $mail->addAttachment('image/data/' . DIR_IMAGECLIENT . '/attachment/' . $uploaded_file_name1, $uploaded_file_name1);
            }
            if ($uploaded_file_name2 != '') {
                $resultUpload = $this->uploadFile($_FILES["attachment2"]);

                if ($resultUpload['error']) {
                    if ($this->request->post['json'] == 1) {
                        $data = array(
                            'status' => 0,
                            'remark' => $resultUpload['error']
                        );
                        echo json_encode($data);
                        exit();
                    }
                    $this->error['attachment2'] = $resultUpload['error'];
                }
                $mail->addAttachment('image/data/' . DIR_IMAGECLIENT . '/attachment/' . $uploaded_file_name2, $uploaded_file_name2);
            }
            $mail->setFrom('no-reply@'.$_SERVER['HTTP_HOST']);
            $mail->setSender($this->request->post['name']);
            $mail->setSubject(html_entity_decode(sprintf('Enquiry', $this->request->post['name']), ENT_QUOTES, 'UTF-8'));
            $text = '';
            $text .= '<br/>From: '.$this->request->post['email'].'<br/>';
            $i=0;
            foreach ($this->request->post as $key => $item) {
                $temp = explode('_', $key);
                if ($temp[0] == 'details') {
                    $text .= $temp[1].': '.$item.'<br/>';
                }
                $i++;
            }
            $text .= '<br/><br/>'.$this->request->post['enquiry'];

            $mail->setHtml(html_entity_decode($text, ENT_QUOTES, 'UTF-8'));
            $mail->send();


            if ($uploaded_file_name1 != '') {
                $this->request->post['attachment'] = 'image/data/' . DIR_IMAGECLIENT . '/attachment/' . $uploaded_file_name1;
            }

            if ($uploaded_file_name2 != '') {
                $this->request->post['attachment2'] = 'image/data/' . DIR_IMAGECLIENT . '/attachment/' . $uploaded_file_name2;
            }

            if (isset($this->request->post['json']) && $this->request->post['json'] == 1) {

                $this->load->model('account/contact');
                if (isset($this->request->post['category']) && $this->request->post['category'] != 'contact') {
                    $this->model_account_contact->addContact($this->request->post);
                }

                $data = array(
                    'status' => 1,
                    'remark' => 'Success'
                );
                echo json_encode($data);
                exit();
            } else {
                $this->load->model('account/contact');
                if (isset($this->request->post['category']) && $this->request->post['category'] != 'contact') {

                    $this->model_account_contact->addContact($this->request->post);
                }

                $this->redirect($this->url->link('information/contact/success'));
            }
        }

        if (isset($this->request->post['json']) && $this->request->post['json'] == 1) {


            if (isset($this->error['name'])) {
                $data = array(
                    'status' => 0,
                    'remark' => $this->error['name']
                );
                echo json_encode($data);
                exit();
            }

            if (isset($this->error['email'])) {
                $data = array(
                    'status' => 0,
                    'remark' => $this->error['email']
                );
                echo json_encode($data);
                exit();
            }

            if (isset($this->error['enquiry'])) {
                $data = array(
                    'status' => 0,
                    'remark' => $this->error['enquiry']
                );
                echo json_encode($data);
                exit();
            }
            if (isset($this->error['captcha'])) {
                $data = array(
                    'status' => 0,
                    'remark' => $this->error['captcha']
                );
                echo json_encode($data);
                exit();
            }
        }

        $this->data['breadcrumbs'] = array();

        $this->data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/home'),
            'separator' => false
        );

        $this->data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('information/contact'),
            'separator' => $this->language->get('text_separator')
        );

        $this->data['heading_title'] = $this->language->get('heading_title');

        $this->data['text_location'] = $this->language->get('text_location');
        $this->data['text_contact'] = $this->language->get('text_contact');
        $this->data['text_address'] = $this->language->get('text_address');
        $this->data['text_telephone'] = $this->language->get('text_telephone');
        $this->data['text_fax'] = $this->language->get('text_fax');

        $this->data['entry_name'] = $this->language->get('entry_name');
        $this->data['entry_email'] = $this->language->get('entry_email');
        $this->data['entry_enquiry'] = $this->language->get('entry_enquiry');
        $this->data['entry_captcha'] = $this->language->get('entry_captcha');

        if (isset($this->error['name'])) {
            $this->data['error_name'] = $this->error['name'];
        } else {
            $this->data['error_name'] = '';
        }

        if (isset($this->error['email'])) {
            $this->data['error_email'] = $this->error['email'];
        } else {
            $this->data['error_email'] = '';
        }

        if (isset($this->error['enquiry'])) {
            $this->data['error_enquiry'] = $this->error['enquiry'];
        } else {
            $this->data['error_enquiry'] = '';
        }

        if (isset($this->error['captcha'])) {
            $this->data['error_captcha'] = $this->error['captcha'];
        } else {
            $this->data['error_captcha'] = '';
        }

        $this->data['button_continue'] = $this->language->get('button_continue');

        $this->data['action'] = $this->url->link('information/contact');
        $this->data['store'] = $this->config->get('config_name');
        $this->data['address'] = nl2br($this->config->get('config_address'));
        $this->data['telephone'] = $this->config->get('config_telephone');
        $this->data['fax'] = $this->config->get('config_fax');

        if (isset($this->request->post['name'])) {
            $this->data['name'] = $this->request->post['name'];
        } else {
            $this->data['name'] = $this->customer->getFirstName();
        }

        if (isset($this->request->post['email'])) {
            $this->data['email'] = $this->request->post['email'];
        } else {
            $this->data['email'] = $this->customer->getEmail();
        }

        if (isset($this->request->post['enquiry'])) {
            $this->data['enquiry'] = $this->request->post['enquiry'];
        } else {
            $this->data['enquiry'] = '';
        }

        if (isset($this->request->post['captcha'])) {
            $this->data['captcha'] = $this->request->post['captcha'];
        } else {
            $this->data['captcha'] = '';
        }

        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/information/contact.tpl')) {
            $this->template = $this->config->get('config_template') . '/template/information/contact.tpl';
        } else {
            $this->template = 'default/template/information/contact.tpl';
        }

        $this->children = array(
            'common/column_left',
            'common/column_right',
            'common/content_top',
            'common/content_bottom',
            'common/footer',
            'common/header'
        );

        $this->response->setOutput($this->render());
    }

    public function success() {

        $this->language->load('information/contact');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->data['breadcrumbs'] = array();

        $this->data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/home'),
            'separator' => false
        );

        $this->data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('information/contact'),
            'separator' => $this->language->get('text_separator')
        );

        $this->data['heading_title'] = $this->language->get('heading_title');

        $this->data['text_message'] = $this->language->get('text_message');

        $this->data['button_continue'] = $this->language->get('button_continue');

        $this->data['continue'] = $this->url->link('common/home');

        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/common/success.tpl')) {
            $this->template = $this->config->get('config_template') . '/template/common/success.tpl';
        } else {
            $this->template = 'default/template/common/success.tpl';
        }

        $this->children = array(
            'common/column_left',
            'common/column_right',
            'common/content_top',
            'common/content_bottom',
            'common/footer',
            'common/header'
        );

        $this->response->setOutput($this->render());
    }

    private function validate() {
        if ((utf8_strlen($this->request->post['name']) < 3) || (utf8_strlen($this->request->post['name']) > 32)) {
            $this->error['name'] = $this->language->get('error_name');
        }

        if (!preg_match('/^[^\@]+@.*\.[a-z]{2,6}$/i', $this->request->post['email'])) {
            $this->error['email'] = $this->language->get('error_email');
        }

        if (isset($this->request->post['category']) && $this->request->post['category'] == 'alumni') {
            //check email exist or not..if exist cant submit anymore
            $this->load->model('account/contact');
            if ($this->model_account_contact->getTotalContactsByEmail($this->request->post['email']) > 0) {
                $this->error['email'] = $this->language->get('error_exists');
            }
        }

//        if ((utf8_strlen($this->request->post['enquiry']) < 10) || (utf8_strlen($this->request->post['enquiry']) > 3000)) {
//
//
//            $this->error['enquiry'] = $this->language->get('error_enquiry');
//        }

        if ($this->request->post['json'] == 1) {
//            if (empty($this->session->data['captcha']) || ($this->session->data['captcha'] != $this->request->post['captcha'])) {
//                $this->error['captcha'] = $this->language->get('error_captcha');
//            }
        } else {
            if (empty($this->session->data['captcha']) || ($this->session->data['captcha'] != $this->request->post['captcha'])) {
                $this->error['captcha'] = $this->language->get('error_captcha');
            }
        }
        if (!$this->error) {
            return true;
        } else {
            return false;
        }
    }

    public function captcha() {
        $this->load->library('captcha');

        $captcha = new Captcha();

        $this->session->data['captcha'] = $captcha->getCode();

        $captcha->showImage();
    }

    public function uploadFile($files) {

        $this->load->language('common/filemanager');



        $json = array();



        $filename = basename(html_entity_decode($files['name'], ENT_QUOTES, 'UTF-8'));



        if ((strlen($filename) < 3) || (strlen($filename) > 255)) {

            $json['error'] = $this->language->get('error_filename');
        }

        $directory = rtrim(DIR_IMAGE . 'data/' . DIR_IMAGECLIENT . '/attachment');

        if (!file_exists($directory)) {
            mkdir($directory, 0777, true);
        }


        if (!is_dir($directory)) {

            $json['error'] = $this->language->get('error_directory');
        }



        list($width, $height, $type, $attr) = getimagesize($files['tmp_name']);



        if ($width > 2000) {

            $json['error'] = $this->language->get('Image width must be less than 2000px');
        }

        if ($height > 3000) {

            $json['error'] = $this->language->get('Image height must be less than 3000px');
        }

        if ($files['size'] > 600000) {

            $json['error'] = $this->language->get('File size must be less than 500KB');
        }





        $allowed = array(
            'image/jpeg',
            'image/pjpeg',
            'image/png',
            'image/x-png',
            'image/gif',
            'application/pdf'
        );



        if (!in_array($files['type'], $allowed)) {

            $json['error'] = $this->language->get('error_file_type');
        }



        $allowed = array(
            '.jpg',
            '.jpeg',
            '.gif',
            '.pdf',
            '.png',
        );



        if (!in_array(strtolower(strrchr($filename, '.')), $allowed)) {

            $json['error'] = $this->language->get('error_file_type');
        }



        if ($files['error'] != UPLOAD_ERR_OK) {

            $json['error'] = 'error_upload_' . $files['error'];
        }



        if (!isset($json['error'])) {

            if (@move_uploaded_file($files['tmp_name'], $directory . '/' . $filename)) {

                $json['success'] = $this->language->get('text_uploaded');
            } else {

                $json['error'] = $this->language->get('error_uploaded');
            }
        }



        return $json;
    }

}

?>