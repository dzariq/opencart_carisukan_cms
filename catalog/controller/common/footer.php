<?php

class ControllerCommonFooter extends Controller {

    protected function index() {
        $this->language->load('common/footer');

        $this->data['text_information'] = $this->language->get('text_information');
        $this->data['text_service'] = $this->language->get('text_service');
        $this->data['text_extra'] = $this->language->get('text_extra');
        $this->data['text_contact'] = $this->language->get('text_contact');
        $this->data['text_customer_service'] = $this->language->get('text_customer_service');
        $this->data['text_about'] = $this->language->get('text_about');
        $this->data['text_information'] = $this->language->get('text_information');
        $this->data['text_return'] = $this->language->get('text_return');
        $this->data['text_sitemap'] = $this->language->get('text_sitemap');
        $this->data['text_manufacturer'] = $this->language->get('text_manufacturer');
        $this->data['text_voucher'] = $this->language->get('text_voucher');
        $this->data['text_affiliate'] = $this->language->get('text_affiliate');
        $this->data['text_special'] = $this->language->get('text_special');
        $this->data['text_account'] = $this->language->get('text_account');
        $this->data['text_order'] = $this->language->get('text_order');
        $this->data['text_wishlist'] = $this->language->get('text_wishlist');
        $this->data['text_newsletter'] = $this->language->get('text_newsletter');
        $this->data['text_about_us'] = $this->language->get('text_about_us');
        $this->data['scripts'] = $this->document->getScripts();
        $this->data['theme_path'] = 'catalog/view/theme/' . $this->config->get('config_template') . '/';
        if (isset($this->request->server['HTTPS']) && (($this->request->server['HTTPS'] == 'on') || ($this->request->server['HTTPS'] == '1'))) {
            $server = HTTPS_IMAGE;
        } else {
            $server = HTTP_IMAGE;
        }
        if ($this->config->get('config_logo') && file_exists(DIR_IMAGE . $this->config->get('config_logo'))) {
            $this->data['logo'] = $server . $this->config->get('config_logo');
            //     $this->data['logo'] = $this->model_tool_image->resize($this->config->get('config_logo'), 160, 50);
        } else {
            $this->data['logo'] = '';
        }
        $this->load->model('catalog/information');

        $this->data['informations'] = array();

        foreach ($this->model_catalog_information->getInformations() as $result) {
//			if ($result['bottom']) {

            if (($result['sort_order'] < 0) || ($result['sort_order'] >= 1000)) {
                continue;
            }

            $this->data['informations'][] = array(
                'title' => $result['title'],
                'href' => $this->url->link('information/information', 'information_id=' . $result['information_id'])
            );
//			}
        }


        if ($this->config->get('simple_blog_status')) {

            if ($this->config->get('blog_footer_heading')) {
                $this->data['text_simple_blog'] = $this->config->get('blog_footer_heading');
            } else {
                $this->data['text_simple_blog'] = $this->language->get('text_simple_blog');
            }

            $this->data['simple_blog'] = $this->url->link('simple_blog/article');
        }

        $this->data['contact'] = $this->url->link('information/contact');
        $this->data['return'] = $this->url->link('account/return/insert', '', 'SSL');
        $this->data['sitemap'] = $this->url->link('information/sitemap');
        $this->data['manufacturer'] = $this->url->link('product/manufacturer');
        $this->data['voucher'] = $this->url->link('account/voucher', '', 'SSL');
        $this->data['affiliate'] = $this->url->link('affiliate/account', '', 'SSL');
        $this->data['special'] = $this->url->link('product/special');
        $this->data['account'] = $this->url->link('account/account', '', 'SSL');
        $this->data['order'] = $this->url->link('account/order', '', 'SSL');
        $this->data['wishlist'] = $this->url->link('account/wishlist', '', 'SSL');
        $this->data['newsletter'] = $this->url->link('account/newsletter', '', 'SSL');

        $this->data['powered'] = $this->config->get('config_name') . ' &copy; ' . date('Y', time()); // = sprintf($this->language->get('text_powered'), $this->config->get('config_name'), date('Y', time()));


        $module_data = array();

        $this->load->model('setting/extension');

        $extensions = $this->model_setting_extension->getExtensions('module');

        foreach ($extensions as $extension) {
            $modules = $this->config->get($extension['code'] . '_module');
            if ($modules) {
                foreach ($modules as $module) {
                    if ($module['layout_id'] == 1 && $module['position'] == 'footer' && $module['status']) {
                        $module_data[] = array(
                            'code' => $extension['code'],
                            'setting' => $module,
                            'sort_order' => $module['sort_order']
                        );
                    }
                }
            }
        }

        $this->load->model('catalog/product');
        $this->load->model('catalog/category');
        $this->load->model('tool/image');

        $this->data['categories'] = array();

        $categories = $this->model_catalog_category->getCategories(0);

     
//print_r($options);die;

        foreach ($categories as $category) {
            if ($category['image']) {
                $image = $this->model_tool_image->resize($category['image'], 210, 210);
            } else {
                $image = $this->model_tool_image->resize('/data/default_product.png', 210, 210);
            }
            if (($category['sort_order'] < 0) || ($category['sort_order'] >= 1000)) {
                continue;
            }

            $data = array(
                'filter_category_id' => $category['category_id']
            );

            $products = array();

            $products_details = $this->model_catalog_product->getProducts($data);

            foreach ($products_details as $value) {
                $products[] = array(
                    'product_id' => $value['product_id'],
                    'name' => $value['name'],
                    'image' => $value['image'],
                    'href' => $this->url->link('product/product', 'product_id=' . $value['product_id'])
                );
            }


            $total = $this->model_catalog_product->getTotalProducts(array('filter_category_id' => $category['category_id']));

            $children_data = array();


            $children = $this->model_catalog_category->getCategories($category['category_id']);

            foreach ($children as $child) {
                $data = array(
                    'filter_category_id' => $child['category_id'],
                    'filter_sub_category' => true
                );

                if ($child['image']) {
                    $c_image = $this->model_tool_image->resize($child['image'], 210, 210);
                } else {
                    $c_image = $this->model_tool_image->resize('/data/default_product.png', 210, 210);
                }

                $product_total = $this->model_catalog_product->getTotalProducts($data);

                $total += $product_total;


                if (!isset($child['linkto']))
                    $child['linkto'] = '';
                if ($child['linkto']) {
                    $link = $child['linkto'];
                } else {
                    if (isset($category['database'])) {
                        $link = $this->url->link('product/category', 'path=' . $category['category_id'] . '_' . $child['category_id'] . '&db=' . $category['database']);
                    } else {
                        $link = $this->url->link('product/category', 'path=' . $category['category_id'] . '_' . $child['category_id']);
                    }
                }

                $children_data[] = array(
                    'category_id' => $child['category_id'],
                    'thumb' => $c_image,
                    'name' => $child['name'] . ($this->config->get('config_product_count') ? ' (' . $product_total . ')' : ''),
                    'href' => $link
                );
            }


            if (!isset($category['linkto']))
                $category['linkto'] = '';
            if ($category['linkto']) {
                $link = $category['linkto'];
            } else {
                if (isset($category['database'])) {
                    $link = $this->url->link('product/category', 'path=' . $category['category_id'] . '&db=' . $category['database']);
                } else {
                    $link = $this->url->link('product/category', 'path=' . $category['category_id']);
                }
            }

            $this->data['categories'][] = array(
                'category_id' => $category['category_id'],
                'name' => $category['name'] . ($this->config->get('config_product_count') ? ' (' . $total . ')' : ''),
                'children' => $children_data,
                'products' => $products,
                'thumb' => $image,
                'href' => $link
            );
        }


        $sort_order = array();

        foreach ($module_data as $key => $value) {
            $sort_order[$key] = $value['sort_order'];
        }

        array_multisort($sort_order, SORT_ASC, $module_data);

        $this->data['modules'] = array();

        foreach ($module_data as $module) {
            $module = $this->getChild('module/' . $module['code'], $module['setting']);

            if ($module) {
                $this->data['modules'][] = $module;
            }
        }


        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/common/footer.tpl')) {
            $this->template = $this->config->get('config_template') . '/template/common/footer.tpl';
        } else {
            $this->template = 'default/template/common/footer.tpl';
        }

        $this->data['text_stay'] = $this->language->get('text_stay');
        $this->data['sm_fb'] = $this->config->get('ffsm_fb') ? 'http://www.facebook.com/' . $this->config->get('ffsm_fb') : false;
        $this->data['sm_tw'] = $this->config->get('ffsm_tw') ? 'http://www.twitter.com/' . $this->config->get('ffsm_tw') : false;
        $this->data['sm_gp'] = $this->config->get('ffsm_gp') ? 'http://plus.google.com/' . $this->config->get('ffsm_gp') : false;
        $this->data['sm_yt'] = $this->config->get('ffsm_yt') ? 'http://youtube.com/' . $this->config->get('ffsm_yt') : false;
        $this->data['sm_pin'] = $this->config->get('ffsm_pin') ? 'http://pinterest.com/' . $this->config->get('ffsm_pin') : false;
        $this->render();
    }

}

?>