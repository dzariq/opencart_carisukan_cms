<?php
//-----------------------------------------
// Author: Qphoria@gmail.com
// Web: http://www.opencartguru.com/
//-----------------------------------------
class ControllerPaymentWebcash extends Controller {

	protected function index() {

		# Generic Init
		$classname = str_replace('vq2-catalog_controller_payment_', '', basename(__FILE__, '.php'));
		$store_url = ($this->config->get('config_ssl') && !is_numeric($this->config->get('config_ssl'))) ? $this->config->get('config_ssl') : ((HTTPS_SERVER)?HTTPS_SERVER:HTTP_SERVER);
		$this->data['classname'] = $classname;
		$this->data = array_merge($this->data, $this->load->language('payment/' . $classname));
		$this->data['testmode'] = $this->config->get($classname . '_test');


		# Form Fields
		$this->data['fields'] = array();
		$this->data['fields']['hidden'] = array();

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/payment/' . $classname . '.tpl')) {
			$this->template = $this->config->get('config_template') . '/template/payment/' . $classname . '.tpl';
		} else {
			$this->template = 'default/template/payment/' . $classname . '.tpl';
		}

		$this->id       = 'payment';

		$this->render();
	}

	public function send() {

		# Generic Init
		$classname = str_replace('vq2-catalog_controller_payment_', '', basename(__FILE__, '.php'));
		$store_url = ($this->config->get('config_ssl') && !is_numeric($this->config->get('config_ssl'))) ? $this->config->get('config_ssl') : ((HTTPS_SERVER)?HTTPS_SERVER:HTTP_SERVER);
		$this->data['classname'] = $classname;
		$this->data = array_merge($this->data, $this->load->language('payment/' . $classname));


		# Order Info
		$this->load->model('checkout/order');
		$order_info = $this->model_checkout_order->getOrder($this->session->data['order_id']);


		# v14x Backwards Compatibility
		if (isset($order_info['currency_code'])) { $order_info['currency'] = $order_info['currency_code']; }
		if (isset($order_info['currency_value'])) { $order_info['value'] = $order_info['currency_value']; }


		# Data manipulation
		/*
		$currencies = array(
			'USD' => 'USD',
			'GBP' => 'GBP',
		);
		if (in_array($order_info['currency'], $currencies)) {
			$currency = $currencies[$order_info['currency']];
		} else {
			$currency = 'GBP';
		}
		*/
		$currency = $order_info['currency'];
		
		$amount = str_replace(array(','), '', $this->currency->format($order_info['total'], $currency, FALSE, FALSE));
		$callback = ($store_url . 'index.php?route=payment/' . $classname . '/callback&order_id=' . $order_info['order_id']);
		$storename = ($this->config->get('config_name')) ? $this->config->get('config_name') : $this->config->get('config_store');
				
		$params = array(
			'ord_mercID' 			=> trim($this->config->get($classname . '_mid')),
			'ord_totalamt'			=> $amount,
			'ord_date' 				=> date("d F Y"),
			'ord_mercref'			=> $order_info['order_id'],
			'ord_shipname'			=> trim($order_info['payment_firstname'] . ' ' . $order_info['payment_lastname']),
			'ord_email' 			=> $order_info['email'],
			'ord_shipcountry'		=> $order_info['payment_country'],
			'ord_telephone'			=> $order_info['telephone'],
			'ord_returnURL' 		=> $callback,
			'ord_delcharges' 		=> '0.00',
			'ord_svccharges' 		=> '0.00',
		);
				
		if ($this->config->get($classname . '_test')) {
			$params['test'] = 'true';
		}
	
		
 		require('catalog/controller/payment/'.$classname.'.class.php');
		if ($this->config->get($classname . '_debug')) {
			$payclass = New $classname(DIR_LOGS);
		} else {
 			$payclass = New $classname();
		}
 		$output = $payclass->buildOutput($params);
 		echo $output;
	}
	

	public function callback() {

		$classname = str_replace('vq2-catalog_controller_payment_', '', basename(__FILE__, '.php'));
		$store_url = ($this->config->get('config_ssl') && !is_numeric($this->config->get('config_ssl'))) ? $this->config->get('config_ssl') : ((HTTPS_SERVER)?HTTPS_SERVER:HTTP_SERVER);
		$this->data = array_merge($this->data, $this->load->language('payment/' . $classname));

		// Debug
		if ($this->config->get($classname . '_debug')) { file_put_contents(DIR_LOGS . $classname . '_debug.txt', __FUNCTION__ . "\r\n$classname GET: " . print_r($_GET,1) . "\r\n" . "$classname POST: " . print_r($_POST,1) . "\r\n"); }

		$this->load->model('checkout/order');

		if (!empty($_REQUEST['ord_mercref'])) {
			$order_id = $_REQUEST['ord_mercref'];
		} else {
			$order_id = 0;
		}

		$order_info = $this->model_checkout_order->getOrder($order_id);

		// If there is no order info then fail.
		if (!$order_info) {
			$this->session->data['error'] = $this->language->get('error_no_order');
			$this->fail();
		}

		// If we get a successful response back...
		if (isset($_REQUEST['returncode'])) {
			if (strpos($_REQUEST['returncode'], 'E') === 0) {
				if ($_REQUEST['returncode'] == 'E2') {
					$this->session->data['error'] = $this->language->get('error_canceled');
				} else {
					$this->session->data['error'] = $this->language->get('error_declined');
				}
			} else {
				$this->model_checkout_order->confirm($order_id, $this->config->get($classname . '_order_status_id'));
				$this->model_checkout_order->update($order_id, $this->config->get($classname . '_order_status_id'), '', false);
//				$this->model_checkout_order->update($order_id, $this->config->get($classname . '_order_status_id'), print_r($_REQUEST,1), false);
				$this->redirect($store_url . 'index.php?route=checkout/success');
			}
        } else {
			$this->session->data['error'] = $this->language->get('error_invalid');
		}
		$this->log->write("$classname: ERROR for order id: " . $this->request->get['order_id'] . " :: " . $this->session->data['error']);
        $this->fail();
	}

	private function fail($msg = false) {
		$store_url = ($this->config->get('config_ssl') && !is_numeric($this->config->get('config_ssl'))) ? $this->config->get('config_ssl') : ((HTTPS_SERVER)?HTTPS_SERVER:HTTP_SERVER);
		if (!$msg) { $msg = (!empty($this->session->data['error']) ? $this->session->data['error'] : 'Unknown Error'); }
		if (method_exists($this->document, 'addBreadcrumb')) { //1.4.x
			$this->redirect((isset($this->session->data['guest'])) ? ($store_url . 'index.php?route=checkout/guest_step_3') : ($store_url . 'index.php?route=checkout/confirm'));
		} else {
			echo '<html><head><script type="text/javascript">';
			echo 'alert("'.$msg.'");';
			echo 'window.location="' . ($store_url  . 'index.php?route=checkout/checkout') . '";';
			echo '</script></head></html>';
		}
		exit;
	}
}
?>