<?php

class ModelCatalogCategory extends Model {

    public function getCategory($category_id, $db = null) {
        if ($db != null) {
            $con = mysqli_connect("localhost", DB_USERNAME, DB_PASSWORD, $db);

// Check connection
            if (mysqli_connect_errno($con)) {
                echo "Failed to connect to MySQL: " . mysqli_connect_error();
            }

            $category_info = array();

            $SQL = "SELECT DISTINCT * FROM " . DB_PREFIX . "category c LEFT JOIN " . DB_PREFIX . "category_description cd ON (c.category_id = cd.category_id) LEFT JOIN " . DB_PREFIX . "category_to_store c2s ON (c.category_id = c2s.category_id) WHERE c.category_id = '" . (int) $category_id . "' AND cd.language_id = '" . (int) $this->config->get('config_language_id') . "' AND c2s.store_id = '" . (int) $this->config->get('config_store_id') . "' AND c.status = '1'";
            $result = mysqli_query($con, $SQL);

            while ($row = mysqli_fetch_array($result)) {
                $category_info['category_id'] = $row['category_id'];
                $category_info['image'] = $row['image'];
                $category_info['thumb'] = $row['image'];
                $category_info['top'] = $row['top'];
                $category_info['linkto'] = $row['linkto'];
                $category_info['column'] = $row['column'];
                $category_info['sort_order'] = $row['sort_order'];
                $category_info['status'] = $row['status'];
                $category_info['date_added'] = $row['date_added'];
                $category_info['date_modified'] = $row['date_modified'];
                $category_info['language_id'] = $row['language_id'];
                $category_info['name'] = $row['name'];
                $category_info['meta_description'] = $row['meta_description'];
                $category_info['meta_keyword'] = $row['meta_keyword'];
                $category_info['store_id'] = $row['store_id'];
            }

            mysqli_close($con);

            return $category_info;
        } else {
            $query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "category c LEFT JOIN " . DB_PREFIX . "category_description cd ON (c.category_id = cd.category_id) LEFT JOIN " . DB_PREFIX . "category_to_store c2s ON (c.category_id = c2s.category_id) WHERE c.category_id = '" . (int) $category_id . "' AND cd.language_id = '" . (int) $this->config->get('config_language_id') . "' AND c2s.store_id = '" . (int) $this->config->get('config_store_id') . "' AND c.status = '1'");
            return $query->row;
        }
    }

  

    public function getCategories($parent_id = 0) {

        if (DB_DATABASE == 'no') {
            $link = mysql_connect('localhost', DB_USERNAME, DB_PASSWORD);
            $res = mysql_query("SHOW DATABASES");

            $dbase = '';
            $categories = array();
            $i = 0;

            while ($row = mysql_fetch_assoc($res)) {
                $gg = $this->startsWith($row['Database'], 'oneweb_');
                if ($gg == 1 && $row['Database'] != 'oneweb_web') {
                    $dbase = $row['Database'];
                    $con = mysqli_connect("localhost", "oneweb_admin", "dzariqmirza85", $row['Database']);

// Check connection
                    if (mysqli_connect_errno($con)) {
                        echo "Failed to connect to MySQL: " . mysqli_connect_error();
                    }

                    $SQL = "SELECT * FROM " . DB_PREFIX . "category c LEFT JOIN " . DB_PREFIX . "category_description cd ON (c.category_id = cd.category_id) LEFT JOIN " . DB_PREFIX . "category_to_store c2s ON (c.category_id = c2s.category_id) WHERE c.parent_id = '" . (int) $parent_id . "' AND cd.language_id = '" . (int) $this->config->get('config_language_id') . "' AND c2s.store_id = '" . (int) $this->config->get('config_store_id') . "'  AND c.status = '1' ORDER BY c.sort_order, LCASE(cd.name)";
                    $result = mysqli_query($con, $SQL);

                    $SQLsetting = "SELECT * FROM  " . DB_PREFIX . "setting  WHERE `key` = 'config_name' ";
                    $resultsetting = mysqli_query($con, $SQLsetting);

                    while ($row = mysqli_fetch_array($result)) {
                        while ($row2 = mysqli_fetch_array($resultsetting)) {
                            $categories[$i]['store'] = $row2['value'];
                        }
                        $categories[$i]['category_id'] = $row['category_id'];
                        $categories[$i]['database'] = $dbase;
                        $categories[$i]['parent_id'] = $row['parent_id'];
                        $categories[$i]['name'] = $row['name'];
                        $categories[$i]['top'] = $row['top'];
                        $categories[$i]['linkto'] = $row['linkto'];
                        $categories[$i]['column'] = $row['column'];
                        $categories[$i]['sort_order'] = $row['sort_order'];
                        $categories[$i]['status'] = $row['status'];
                        $categories[$i]['date_added'] = $row['date_added'];
                        $categories[$i]['date_modified'] = $row['date_modified'];
                        $categories[$i]['date_modified'] = $row['date_modified'];
                        $categories[$i]['language_id'] = $row['language_id'];
                        $categories[$i]['meta_description'] = $row['meta_description'];
                        $categories[$i]['meta_keyword'] = $row['meta_keyword'];
                        $categories[$i]['store_id'] = $row['store_id'];
                        $categories[$i]['thumb'] = '../image/' . $row['image'];
                        $categories[$i]['image'] = $row['image'];
                        $categories[$i]['description'] = $row['description'];

                        $i++;
                    }



                    mysqli_close($con);
                }
            }

            return $categories;
        } else {
            $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "category c LEFT JOIN " . DB_PREFIX . "category_description cd ON (c.category_id = cd.category_id) LEFT JOIN " . DB_PREFIX . "category_to_store c2s ON (c.category_id = c2s.category_id) WHERE c.parent_id = '" . (int) $parent_id . "' AND cd.language_id = '" . (int) $this->config->get('config_language_id') . "' AND c2s.store_id = '" . (int) $this->config->get('config_store_id') . "'  AND c.status = '1' ORDER BY c.sort_order, LCASE(cd.name)");
        }
        return $query->rows;
    }

    public function startsWith($haystack, $needle) {
        return $needle === "" || strpos($haystack, $needle) === 0;
    }

    public function getCategoriesByParentId($category_id) {
        $category_data = array();

        $category_query = $this->db->query("SELECT category_id FROM " . DB_PREFIX . "category WHERE parent_id = '" . (int) $category_id . "'");

        foreach ($category_query->rows as $category) {
            $category_data[] = $category['category_id'];

            $children = $this->getCategoriesByParentId($category['category_id']);

            if ($children) {
                $category_data = array_merge($children, $category_data);
            }
        }

        return $category_data;
    }

    public function getCategoryLayoutId($category_id) {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "category_to_layout WHERE category_id = '" . (int) $category_id . "' AND store_id = '" . (int) $this->config->get('config_store_id') . "'");

        if ($query->num_rows) {
            return $query->row['layout_id'];
        } else {
            return $this->config->get('config_layout_category');
        }
    }

    public function getTotalCategoriesByCategoryId($parent_id = 0) {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "category c LEFT JOIN " . DB_PREFIX . "category_to_store c2s ON (c.category_id = c2s.category_id) WHERE c.parent_id = '" . (int) $parent_id . "' AND c2s.store_id = '" . (int) $this->config->get('config_store_id') . "' AND c.status = '1'");

        return $query->row['total'];
    }

}

?>