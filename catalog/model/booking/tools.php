<?php

class ModelBookingTools extends Model {

    public function add($data) {
        $roomDetail = $this->model_catalog_product->getRoom($data['product_id'], $data['room_id']);
        $this->db->query("INSERT INTO bookings SET room_image = '" . $roomDetail['image'] . "',room_name = '" . $roomDetail['caption'] . "',price = '" . $data['price'] . "',date = '" . $data['date'] . "',room_id = '" . $data['room_id'] . "',product_id = '" . $data['product_id'] . "', status = 0 ");
        return $this->db->getLastId();
    }

    public function concatBookings($products, $curr) {
        $newCart = array();
        $dateConcat = "";
        $total = 0;

        foreach ($products as $key => $item) {
            $dateConcat .= date("j F Y h:i A", strtotime($products[$key]['bookingdetails']['date'])) . '<br/>';
            $products[$key]['bookingdetails']['date'] = $dateConcat;
            $total += $products[$key]['rawprice'];
            $products[$key]['total'] = $curr->format($total);
            $newCart[$item['bookingdetails']['room_id']] = $products[$key];
        }

        return $newCart;
    }

    public function getBooked($product_id, $room_id, $quantityRequested = 1) {
        //get product quantity

        $product = $this->db->query('SELECT quantity FROM product WHERE product_id = "' . $product_id . '"');

        $results = $this->db->query('SELECT COUNT(*) as qty,date FROM bookings LEFT JOIN `order` ON order.order_id = bookings.order_id WHERE room_id = "' . $room_id . '" AND product_id = "' . $product_id . '"  AND (order.order_status_id = 1 OR order.order_status_id = 19) GROUP BY date ');

        $datesList = array();
        if (count($results->rows) != 0) {
            foreach ($results->rows as $date) {
                $datesList[] = $date['date'];
            }

            return $datesList;
        } else {
            return array();
        }
    }

    public function getRealPrice($data, $date_booked, $product_id, $roomid, $qty) {

        global $log;
        $log->write("in tools.php, catalog, getRealPrice");
        
        $realprice = 0;
        $this->load->model('catalog/product');

        $holidays = $this->getHolidays($data['mpn']);
        $peaks = $this->getPeaks($product_id);
        $offs = $this->getOffDates($product_id);
        $booked = $this->getBooked($product_id, $roomid, $qty);

        $school = $this->getSchoolHolidays($product_id);

        $setDayTypeBasedOnStartBookingTimeOnly = true;

        $returnJson = array();
        $i = 0;
        foreach ($date_booked as $date) {

            $temp = explode(' ', $date);
            $time = $temp[1];
            if ($i == 0) {
                $dayType = $this->checkDayType($date, $data['location'], $holidays, $peaks, $offs, $booked, $school);
            }
//get room price by room id and product id
//            3-peak, 4-schoolbreak, 2-publichol, 1-weekend, 9-occupied, 10-off day
            if ($dayType == 3) {
                $curPrice = $this->model_catalog_product->getPrice4($roomid, $product_id);
                $curPrice = $this->getPriceRate($curPrice, $time);
                $realprice += $curPrice;
            } else if ($dayType == 4) {
                $curPrice = $this->model_catalog_product->getPrice5($roomid, $product_id);
                $curPrice = $this->getPriceRate($curPrice, $time);
                $realprice += $curPrice;
            } else if ($dayType == 2) {
                $curPrice = $this->model_catalog_product->getPrice3($roomid, $product_id);
                $curPrice = $this->getPriceRate($curPrice, $time);
                $realprice += $curPrice;
            } else if ($dayType == 1) {
                $curPrice = $this->model_catalog_product->getPrice2($roomid, $product_id);
                $curPrice = $this->getPriceRate($curPrice, $time);
                $realprice += $curPrice;
            } else if ($dayType == 9) {
                $returnJson['status'] = 0;
                $returnJson['message'] = 'N/A';
                //get booking details
                $details = $this->getBookingByDateAndRoomId($roomid, $date);
                $returnJson['bookingid'] = $details['order_id'];
                $returnJson['court'] = $details['room_name'];
                $returnJson['date'] = $details['date'];
                $returnJson['name'] = $details['firstname'];
                $returnJson['email'] = $details['email'];
                $returnJson['telephone'] = $details['telephone'];
                return $returnJson;
            } else if ($dayType == 10) {
                $returnJson['status'] = 3;
                $returnJson['message'] = 'N/A';
                return $returnJson;
            } else {
                $dayType = 0;
                $curPrice = $this->model_catalog_product->getPrice1($roomid, $product_id);
                $curPrice = $this->getPriceRate($curPrice, $time);
                $realprice += $curPrice;
            }
            if ($curPrice == 0) {
                $realprice += $this->model_catalog_product->getPrice1($roomid, $product_id);
            }

            if ($setDayTypeBasedOnStartBookingTimeOnly) {
                $i++;
            }
        }
        $returnJson['status'] = 1;
        $returnJson['message'] = $realprice;
        $returnJson['dayType'] = $dayType;

        return $returnJson;
    }

    public function checkDayType($date, $type, $holidays, $peaks, $offs, $booked, $school) {
        $dateRaw = date('Y-m-d H:i:s', strtotime($date));
        $temp = explode(' ', $date);
        $date = $temp[0];
        if (in_array($date, $offs)) {
            return 10;
        }
        if (in_array($dateRaw, $booked)) {
            return 9;
        }
        if (in_array($date, $peaks)) {
            return 3;
        }
        if (in_array($date, $school)) {
            return 4;
        }
        if (in_array($date, $holidays)) {
            return 2;
        }
        if ($type == 1) {
            if (date('N', strtotime($date)) >= 6 && date('N', strtotime($date)) < 8) {
                return 1;
            } else {
                return 0;
            }
        } else {
            if (date('N', strtotime($date)) >= 5 && date('N', strtotime($date)) < 7) {
                return 1;
            } else {
                return 0;
            }
        }
    }

    public function getHolidays($mpn) {
        $key = 'weight_' . $mpn . '_rate';
        $holidays = $this->db->query("SELECT value FROM setting WHERE `group` = 'weight' AND `key` = '$key' ");
        //got holidays
        if (count($holidays->rows) != 0) {
            $holiday_list = $holidays->row['value'];
            explode(',', $holiday_list);
            $lists = explode(',', $holiday_list);
            $hol = array();
            foreach ($lists as $list) {
                $hol[] = date('Y-m-d', strtotime($list));
            }

            return $hol;
        } else {
            return null;
        }
    }

    public function getPeaks($product_id) {

        $peaks = $this->db->query("SELECT isbn FROM product WHERE product_id = '$product_id' ");

        //got holidays
        if (count($peaks->rows) != 0) {
            $peaks_list = $peaks->row['isbn'];
            explode(',', $peaks_list);
            $lists = explode(',', $peaks_list);
            $peakArray = array();
            foreach ($lists as $list) {
                $peakArray[] = date('Y-m-d', strtotime($list));
            }

            return $peakArray;
        } else {
            return null;
        }
    }

    public function getOffDates($product_id) {

        $peaks = $this->db->query("SELECT weight FROM product WHERE product_id = '$product_id' ");

        //got holidays
        if (count($peaks->rows) != 0) {
            $peaks_list = $peaks->row['weight'];
            explode(',', $peaks_list);
            $lists = explode(',', $peaks_list);
            $peakArray = array();
            foreach ($lists as $list) {
                $peakArray[] = date('Y-m-d', strtotime($list));
            }

            return $peakArray;
        } else {
            return null;
        }
    }

    public function getPriceRate($item, $timeChose) {

        $price = 0;
        $data = explode(';', trim($item));
        $priceRates = array();
        $timeNow = '';
        $i = 0;

        foreach ($data as $val) {
            $data2 = explode(':', $val);
            $time = number_format($data2[0], 2);
            if ($i == 0) {
                $priceRates[$i] = array(
                    "00:00",
                    str_replace('.', ':', $time),
                    $data2[1],
                );
            } else {
                $priceRates[$i] = array(
                    $timeNow,
                    str_replace('.', ':', $time),
                    $data2[1],
                );
            }
            $timeNow = $time;
            $i++;
        }


        foreach ($priceRates as $item) {
            if (($item[0] <= $timeChose) && ($timeChose <= $item[1])) {
                $price = $item[2];
            }
        }


        return $price;
    }

    public function getSchoolHolidays($product_id) {

        $peaks = $this->db->query("SELECT length FROM product WHERE product_id = '$product_id' ");

        //got holidays
        if (count($peaks->rows) != 0) {
            $peaks_list = $peaks->row['length'];
            explode(',', $peaks_list);
            $lists = explode(',', $peaks_list);
            $peakArray = array();
            foreach ($lists as $list) {
                $peakArray[] = date('Y-m-d', strtotime($list));
            }

            return $peakArray;
        } else {
            return null;
        }
    }

    public function getBookingByDateAndRoomId($roomid, $date) {
        $result = $this->db->query("SELECT * FROM bookings LEFT JOIN `order` ON `order`.order_id = bookings.order_id WHERE room_id = '$roomid' AND date = '$date' AND bookings.status =1 ");
        
        return $result->row;
    }

    public function createDateRangeArray($strDateFrom, $strDateTo) {

        $temp1 = explode(' ', $strDateFrom);
        $temp2 = explode(' ', $strDateTo);
        $strTimeFrom = $temp1[1];
        $strTimeTo = $temp2[1];

        $strDateFrom = date("Y-m-d H:i", strtotime(str_replace('/', '-', $temp1[0]) . ' ' . $strTimeFrom));
        $strDateTo = date("Y-m-d H:i", strtotime(str_replace('/', '-', $temp2[0] . ' ' . $strTimeTo)));
        // takes two dates formatted as YYYY-MM-DD and creates an
        // inclusive array of the dates between the from and to dates.
        // could test validity of dates here but I'm already doing
        // that in the main script

        $aryRange = array();

        $iDateFrom = mktime(substr($strDateFrom, 11, 2), substr($strDateFrom, 14, 2), 0, substr($strDateFrom, 5, 2), substr($strDateFrom, 8, 2), substr($strDateFrom, 0, 4));
        $iDateTo = mktime(substr($strDateTo, 11, 2), substr($strDateTo, 14, 2), 0, substr($strDateTo, 5, 2), substr($strDateTo, 8, 2), substr($strDateTo, 0, 4));

        if ($iDateTo >= $iDateFrom) {
            array_push($aryRange, date('Y-m-d H:i', $iDateFrom)); // first entry
            while ($iDateFrom < $iDateTo) {
                $iDateFrom+=3600; // add 1 hour
                array_push($aryRange, date('Y-m-d H:i', $iDateFrom));
            }
        }
//        print_r($aryRange);
//       
//        die;

        array_pop($aryRange);

        return ($aryRange);
    }

}

